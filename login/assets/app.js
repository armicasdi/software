$(document).ready(function () {

    $('#login').click(function () {
        var username = $("#username").val();
        var password = $("#password").val();
            
            if ($.trim(username).length > 0 && $.trim(password).length > 0) {
                const dataString = {
                    'username': username, 
                    'password': password
                }
                $.ajax({
                    type: "POST",
                    url: "login/assets/ajaxLogin.php",
                    data: dataString,
                    dataType: 'JSON', 
                    beforeSend: function () {
                        $("#login").val('Connecting...');
                    },
                    success: function (resp) {
                        if (resp[0]['rol'] == "admin") {
                            $("body").load("admin/calendar.php").hide().fadeIn(1500).delay(6000);
                            //or
                            window.location.href = "admin/index.php";
                        }else if (resp[0]['rol'] == "user") {
                            $("body").load("user/calendar.php").hide().fadeIn(1500).delay(6000);
                            //or
                            window.location.href = "user/index.php";
                        }else if (resp[0]['rol'] == "master") {
                            $("body").load("master/index.php").hide().fadeIn(1500).delay(6000);
                            //or
                            window.location.href = "master/index.php";
                        } else {
                            $("#login").val('Login');
                            $("#error").html("<span style='color:#cc0000'>Error: </span><span style='color:#fff'>"+resp[0]['error']+"</span>");
                        }
                    }
                });

            }
            return false;
    });

});