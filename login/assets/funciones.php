<?php 
    include "conexion.php";
    $tabla_usuarios='usuario_login';
    $tabla_usuarios_perfil='usuario_perfil';
    $tabla_usuarios_cargo='usuario_cargo';

    function cantidad($campo,$valor=null,$tabla=null){
        global $conexion;
        switch($valor){
            case null:
                $query=$conexion->query('select * from '.$tabla);
                break;
            default:
                $query=$conexion->query('select * from '.$tabla.' where '.$campo.'="'.$valor.'"');
        }
        return @$query->num_rows;
    }

    function escapar($elemento){
        global $conexion;
        return mysqli_real_escape_string($conexion,$elemento);
    }

    function obtener_datos($campo,$valor=null,$tabla=null){
        global $conexion;
        (empty($valor))?$consulta='select * from '.$tabla:$consulta='select * from '.$tabla.' where '.$campo.'="'.$valor.'"';
        $query=$conexion->query($consulta);
        return $query->fetch_assoc();
    }

    function queryOne($query)
    {
    global $conexion;
        $result = $conexion->query($query);
        if (!$result) {
            return false;
        }
        if (mysqli_num_rows($result) == 0) {
            return false;
        }
        $row = mysqli_fetch_assoc($result);
        mysqli_free_result($result);
        return $row;
    }

    function primer_registro(){
        global $conexion;
        global $tabla_admin;
        if(insertar($tabla_admin,array('name'=>escapar($_POST['usuario']),'email'=>escapar($_POST['email']),'access'=>password_hash($_POST['acceso'],PASSWORD_DEFAULT)))){
            $_SESSION['alert']=array('success','Registro exitoso');
        }else{
            $_SESSION['alert']=array('danger','El usuario o el correo ya han sido utilizados');
        }
    }

    function insertar($tabla,$valores){
        global $conexion;
        $result = $conexion->query('insert into '.$tabla.' ('.array_to_string($valores,',',true).') values ('.array_to_string($valores,',',false,true).')');
        if($result){
            return lastId($conexion);
        }else{
            return false;
        }
    }

    function actualizar($tabla,$valores,$condicion){
        global $conexion;
        foreach($valores as $indice=>$valor){
            $values[]=$indice.'="'.$valor.'"';
        }
        return $conexion->query('update '.$tabla.' set '.array_to_string($values,',').' where '.array_to_string($condicion,'',true).'='.array_to_string($condicion,'',false,true));
    }

    function lastId($conexion)
    {
        return mysqli_insert_id($conexion);
    }

    function borrar($tabla,$condicion){
        global $conexion;
        return $conexion->query('delete from '.$tabla.' where '.array_to_string($condicion,'',true).'='.array_to_string($condicion,'',false,true));
    }

    function array_to_string($array,$separator,$index=false,$mysql=false){
        $string='';
        foreach($array as $indice=>$element){
            if($mysql){
                if(!is_numeric($element)){
                    $element='"'.$element.'"';
                }
            }
            if($index){
                $string.=$indice.$separator;
            }else{
                $string.=$element.$separator;
            }
        }
        return trim($string,$separator);
    }

    function random($longitudPass=11){
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890+-*/!@#$%^&*(_=-)";
        $longitudCadena=strlen($cadena);
        $pass = "";
        for($i=1 ; $i<=$longitudPass ; $i++){
            $pos=rand(0,$longitudCadena-1);
            $pass .= substr($cadena,$pos,1);
        }
        return $pass;
    }

    function valor_alert($separator='<br>'){
        if(isset($_SESSION['alert'][1])){
            return $_SESSION['alert'][1].$separator;
        }
    }

    function slug($str){
        # special accents
        $a = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','Ð','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','?','?','J','j','K','k','L','l','L','l','L','l','?','?','L','l','N','n','N','n','N','n','?','O','o','O','o','O','o','Œ','œ','R','r','R','r','R','r','S','s','S','s','S','s','Š','š','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Ÿ','Z','z','Z','z','Ž','ž','?','ƒ','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','?','?','?','?','?','?');
        $b = array('A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','IJ','ij','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N','n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s','S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','A','a','AE','ae','O','o');
        return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/','/[ -]+/','/^-|-$/'),array('','-',''),str_replace($a,$b,$str)));
    }
    if(!function_exists('password_hash')){
        function password_hash($password,$null=null){
            $salt = '$2a$07$usesomadasdsadsadsadasdasdasdsadesillystringfors';
            return crypt($password,$salt);
        }
    }
    if(!function_exists('password_verify')){
        function password_verify($password,$in_db){
            return crypt($password,$in_db)==$in_db;
        }
    }

    function timeElapsedSinceNow( $datetime, $full = false )
    {
        $now = new DateTime;
        $then = new DateTime( $datetime );
        $diff = (array) $now->diff( $then );
        $diff['w']  = floor( $diff['d'] / 7 );
        $diff['d'] -= $diff['w'] * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach( $string as $k => & $v )
        {
            if ( $diff[$k] )
            {
                $v = $diff[$k] . ' ' . $v .( $diff[$k] > 1 ? 's' : '' );
            }
            else
            {
                unset( $string[$k] );
            }
        }
        if ( ! $full ) $string = array_slice( $string, 0, 1 );
        return $string ? implode( ', ', $string ) . ' ago' : 'just now';
    }
