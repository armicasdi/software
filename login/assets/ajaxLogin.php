<?php 
    include "funciones.php";
    include "conexion.php";

    session_start();
    if(isset($_POST['username']) && isset($_POST['password']))
    {
        // username and password sent from Form
        $username=escapar($_POST['username']); 
        $password=escapar($_POST['password']); 

        $result=$conexion->query('SELECT * FROM '.$tabla_usuarios.' WHERE usuario_name="'.$username.'"');
        $num_rows_user = cantidad('usuario_name',$username,$tabla_usuarios);
        
        $json = array();
        
        if($num_rows_user > 0){
            while($row=$result->fetch_assoc())
            {

                if(password_verify($password,$row['usuario_pass'])){
                    $_SESSION['user'] = $row['usuario_name'];
                    $_SESSION['rol']  = $row['tipo_usuario'];
                    $_SESSION['id']  = $row['id_usuario'];
                    $_SESSION["ultimoAcceso"]= date("Y-m-d H:i:s");
                    $json[]=array(
                        'user' => $_SESSION['user'],
                        'rol' => $_SESSION['rol'],
                        'id' => $_SESSION['id'],
                    );
                    
                }else{
                    $json[]=array(
                       'error' => "Password is incorrect"
                    ); 
                }
            }
        }else if($num_rows_user == 0){
            $json[]=array(
                'error' => "This user is not registered"
             ); 
        }

        $jsonstring = json_encode($json);
        echo $jsonstring;
    }

?>