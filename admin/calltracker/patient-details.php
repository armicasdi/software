<?php
    require("../include/database.php");
    include("../include/funciones.php");

    function calculaedad($fechanacimiento){
        list($ano,$mes,$dia) = explode("-",$fechanacimiento);
        $ano_diferencia  = date("Y") - $ano;
        $mes_diferencia = date("m") - $mes;
        $dia_diferencia   = date("d") - $dia;
        if ($dia_diferencia < 0 || $mes_diferencia < 0)
          $ano_diferencia--;
        return $ano_diferencia;
      }

    if(isset($_POST["patient_chart"])){
        $id_paciente = escapar($_POST["patient_chart"]);

        $result = $conexion->query('SELECT * from '.$tabla_pacientes.' WHERE id_paciente ="'.$id_paciente.'"');
        while($row=$result->fetch_assoc()){
                $chart = $row["id_paciente"];
                $name = ucwords($row["paciente_nombres"]);
                $lastname = ucwords($row["paciente_apellidos"]);
                $dateb = $row["paciente_fechanac"];
                $age = calculaedad($row['paciente_fechanac']);
                $contact = $row["paciente_contacto"];

            echo '
            <tr id="patientRow">
                <td class="text-center text-muted" scope="row">'.$chart.'</td>
                <td class="text-center">'.$name.'</td>
                <td class="text-center">'.$lastname.'</td>
                <td class="text-center">'.$dateb.'</td>
                <td class="text-center">'.$age.'</td>
                <td class="text-center">'.$contact.'</td>
            </tr>';
        }
    }
?>