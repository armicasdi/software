<?php
    require("../include/database.php");
    include("../include/funciones.php");

    if(isset($_POST["patient_chart"])){
        $id_paciente = escapar($_POST["patient_chart"]);

        $result = $conexion->query('SELECT * from '.$tabla_citas.' WHERE id_paciente ="'.$id_paciente.'"');
        while($row=$result->fetch_assoc()){
                $app_id = $row["id_cita"];
                $chart = $row["id_paciente"];
                $clinic = $row["id_clinica"];
                $reason = $row["id_reason"];
                $dateapp = $row["cita_fecha"];
                $timeapp = $row["cita_hora"];
                $reg_state_app = queryOne('SELECT * FROM '. $tabla_citas_estado . ' WHERE id_cita = '. $app_id . '');
                $reg_clinic = queryOne('SELECT * FROM '. $tabla_clinicas . ' WHERE id_clinica = '. $clinic . '');

                if($reg_state_app['estado_cita'] == "Finished" || $reg_state_app['estado_cita'] == "Show Up"){
                    $state = `<i class="fa fa-check-circle" style="color:green"></i>`;
                }else if($reg_state_app['estado_cita'] == "No Show Up"){
                    $state = `<i class="fa fa-check-circle" style="color:red"></i>`;
                }else{
                    $state = "";
                }

            echo '
            <tr id="appRow">
                <td class="text-center text-muted" scope="row">'.$chart.'</td>
                <td class="text-center">'.$dateapp ." / ".$timeapp.'</td>
                <td class="text-center">'.$reason.'</td>
                <td class="text-center">'.$reg_clinic['clinica_nombre'].'</td>
                <td class="text-center">'.$state.'</td>
            </tr>';
        }
    }
?>