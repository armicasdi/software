<?php 
    include 'include/funciones.php';
?>
<!doctype html>
<html lang="en">

<head>
    <title>PPS | Patient [<?php echo $_GET["id"];?>] - Account</title>
    <?php include 'head.php'; ?>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php include 'navbar.php'; ?>
        <div class="ui-theme-settings">
            <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
                <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
            </button>
        </div>
        <div class="app-main">
            <?php include 'sidebar.php'; ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-user icon-gradient bg-happy-itmeo">
                                    </i>
                                </div>
                                <div>Patient [<b><?php echo $_GET["id"];?></b>] - Account
                                    <div class="page-title-subheading">Summary</div>
                                </div>

                            </div>
                            <div class="page-title-actions">
                                <!-- <a type="button" class="mb-2 mr-2 btn btn-danger text-white" href="jobnumbers.php">
                                    <i class="fa fa-arrow-left"></i> Back to Registered Job Numbers
                                </a> -->
                                <a type="button" class="mb-2 mr-2 btn btn-dark text-white" onclick="window.close();">
                                    <i class="fa fa-times-circle"></i> Close Window
                                </a>
                            </div>
                        </div>
                    </div>
                    <ul class="tabs body-tabs body-tabs-layout tabs-animated body-tabs-animated nav" id="myTabs">
                        <li class="nav-item">
                            <a role="tab" class="nav-link active" data-toggle="tab" href="#profile">
                                <span>Profile</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" data-toggle="tab" href="#apps">
                                <span>Appointments</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" data-toggle="tab" href="#jobs">
                                <span>Job Numbers</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" data-toggle="tab" href="#trans">
                                <span>Transactions</span>
                            </a>
                        </li>
                    </ul>
                    <?php
                        $id=$_GET["id"];
                        $result = $conexion->query('SELECT * FROM '. $tabla_pacientes .' WHERE id_paciente = "'.$id.'";');
                            while($reg=$result->fetch_assoc()){
                              
                                $reg_clinica = queryOne('SELECT * FROM '. $tabla_clinicas . ' WHERE id_clinica = '. $reg['id_clinica'] . ';');
                                
                                /* patient info  */
                                $pat_clinic = $reg_clinica['clinica_nombre'];
                                if(strlen(stristr($id,'FX')) > 0){
                                    $id_patient = str_replace('FX','F',$id);
                                }
                                else if(strlen(stristr($id,'WE')) > 0){
                                    $id_patient = str_replace('WE','W',$id);
                                }
                                if(strlen(stristr($id,'MS')) > 0){
                                    $id_patient = str_replace('MS','',$id);
                                }
                                $id_eaglesoft = $id_patient;
                                $pat_first_name = ucfirst(quitar_tildes($reg['paciente_nombres']));
                                $pat_last_name = ucfirst(quitar_tildes($reg['paciente_apellidos']));
                                $pat_dob = $reg['paciente_fechanac'];
                                $pat_primary_contact_phone = $reg['paciente_contacto'];
                                $pat_secondary_contact_phone = $reg['paciente_contacto2'];
                                $pat_genre = $reg['paciente_genero'];
                                $pat_email = $reg['paciente_email'];
                                $pat_lang = $reg['paciente_idioma'];
                                $pat_insurance = $reg['paciente_tiene_seguro'];
                                $pat_status = $reg['paciente_status'];
                                $pat_notas = $reg['paciente_notas'];

                                $countScheduled = queryOne('SELECT COUNT(citas.id_cita) as sch FROM `citas` INNER JOIN citas_estado ON citas.id_cita = citas_estado.id_cita WHERE id_paciente = "'.$id.'" AND citas_estado.estado_cita = "Scheduled";');
                                $countFinished = queryOne('SELECT COUNT(citas.id_cita) as fin FROM `citas` INNER JOIN citas_estado ON citas.id_cita = citas_estado.id_cita WHERE id_paciente = "'.$id.'" AND citas_estado.estado_cita = "Finished";');
                                $countMissed = queryOne('SELECT COUNT(citas.id_cita) as nsu FROM `citas` INNER JOIN citas_estado ON citas.id_cita = citas_estado.id_cita WHERE id_paciente = "'.$id.'" AND citas_estado.estado_cita = "No Show Up";');

                    ?>
                    <input type="hidden" id="patid" value="<?php echo $_GET["id"];?>" />
                    <div class="tab-content">
                        <!-- PROFILE -->
                        <div class="tab-pane tabs-animation fade active show" id="profile" role="tabpanel">
                            <div class="row">
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Patient ID</div>
                                                <div class="widget-subheading">Software</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-primary"><span><?php echo $id;?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Patient ID</div>
                                                <div class="widget-subheading">EagleSoft</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-primary">
                                                    <span><?php echo $id_eaglesoft;?></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Clinic</div>
                                                <div class="widget-subheading">Patient Clinic</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-primary">
                                                    <span><?php echo $pat_clinic;?></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="mb-3 card">
                                    <div class="card-header-tab card-header-tab-animation card-header">
                                        <div class="card-header-title">
                                            <i class="header-icon pe-7s-user icon-gradient bg-malibu-beach"> </i>
                                            Patient Info
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="tabs-eg-77">
                                                <h6
                                                    class="text-muted text-uppercase font-size-md opacity-5 font-weight-normal">
                                                    Basic Info</h6>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <ul
                                                            class="rm-list-borders rm-list-borders-scroll list-group list-group-flush">
                                                            <li class="list-group-item">
                                                                <div class="widget-content p-0">
                                                                    <div class="widget-content-wrapper">
                                                                        <div class="widget-content-left">
                                                                            <div class="widget-heading">
                                                                                <?php echo $reg["id_paciente"]?>
                                                                            </div>
                                                                            <div class="widget-subheading">
                                                                                Chart ID
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <ul
                                                            class="rm-list-borders rm-list-borders-scroll list-group list-group-flush">
                                                            <li class="list-group-item">
                                                                <div class="widget-content p-0">
                                                                    <div class="widget-content-wrapper">
                                                                        <div class="widget-content-left">
                                                                            <div class="widget-heading">
                                                                                <?php echo $reg['paciente_nombres'].' '.$reg['paciente_apellidos'];?>
                                                                            </div>
                                                                            <div class="widget-subheading">Patient
                                                                                Name
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <ul class="rm-list-borders rm-list-borders-scroll list-group list-group-flush">
                                                            <li class="list-group-item">
                                                                <div class="widget-content p-0">
                                                                    <div class="widget-content-wrapper">
                                                                        <div class="widget-content-left">
                                                                            <div class="widget-heading">
                                                                                <a class="phone"><?php echo $reg['paciente_contacto'];?></a>
                                                                            </div>
                                                                            <div class="widget-subheading">Contact Phone </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <ul
                                                            class="rm-list-borders rm-list-borders-scroll list-group list-group-flush">
                                                            <li class="list-group-item">
                                                                <div class="widget-content p-0">
                                                                    <div class="widget-content-wrapper">
                                                                        <div class="widget-content-left">
                                                                            <div class="widget-heading">
                                                                                <a><?php echo date('m-d-Y',strtotime($reg["paciente_fechanac"]));?></a>
                                                                            </div>
                                                                            <div class="widget-subheading">
                                                                                DOB
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <ul class="rm-list-borders rm-list-borders-scroll list-group list-group-flush">
                                                            <li class="list-group-item">
                                                                <div class="widget-content p-0">
                                                                    <div class="widget-content-wrapper">
                                                                        <div class="widget-content-left">
                                                                            <div class="widget-heading">
                                                                                <?php echo $reg_clinica["clinica_nombre"]?>
                                                                            </div>
                                                                            <div class="widget-subheading">Clinic
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="main-card mb-3 card">
                                    <div class="card-header-tab card-header-tab-animation card-header">
                                        <div class="card-header-title">
                                            <i class="header-icon pe-7s-edit icon-gradient bg-sunny-morning"> </i>
                                            Update Patient Profile
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <form class="">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_first_name" class="">First Name</label>
                                                        <input name="pat_first_name" id="pat_first_name" value="<?php echo $pat_first_name;?>"  type="text" class="form-control"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_last_name" class="">Last Name</label>
                                                        <input name="pat_last_name" id="pat_last_name" value="<?php echo $pat_last_name;?>"  type="text" class="form-control"></div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-3">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_primary_contact" class="text-primary"><b>Primary Contact Phone</b></label>
                                                        <input name="pat_primary_contact" id="pat_primary_contact" value="<?php echo $pat_primary_contact_phone;?>" type="text" class="phone form-control"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_secondary_contact" class="">Another Contact Phone</label>
                                                        <input name="pat_secondary_contact" id="pat_secondary_contact" value="<?php echo $pat_secondary_contact_phone;?>" type="text" class="phone form-control" placeholder="(000) 000 0000"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_clinic" class="">Clinic</label>
                                                        <select name="pat_clinic" id="pat_clinic" class="form-control">
                                                            <option value="1" <?php if($pat_clinic == '1'){echo("selected");}?>>Fairfax </option>
                                                            <option value="2" <?php if($pat_clinic == '2'){echo("selected");}?>>Manassas </option>
                                                            <option value="3" <?php if($pat_clinic == '3'){echo("selected");}?>>Woodbridge </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_email" class="">Email</label>
                                                        <input name="pat_email" id="pat_email" type="email" value="<?php echo $pat_email;?>" class="form-control"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_dob" class="">DOB (Date of Birth)</label>
                                                        <input name="pat_dob" id="pat_dob" value="<?php echo date($pat_dob)?>" type="date" class="form-control"></div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_genre" class="">Genre</label>
                                                        <select name="pat_genre" id="pat_genre" class="form-control">
                                                            <option value="" <?php if($pat_genre == ''){echo("selected");}?> disabled>-- Select a Genre --</option>
                                                            <option value="male" <?php if($pat_genre == 'male'){echo("selected");}?>>Male </option>
                                                            <option value="female" <?php if($pat_genre == 'female'){echo("selected");}?>>Female </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_lang" class="">Language</label>
                                                        <select name="pat_lang" id="pat_lang" class="form-control">
                                                            <option value="" <?php if($pat_lang == ''){echo("selected");}?> disabled>-- Select a Language --</option>
                                                            <option value="Spanish" <?php if($pat_lang == 'Spanish'){echo("selected");}?>>Spanish </option>
                                                            <option value="English" <?php if($pat_lang == 'English'){echo("selected");}?>>English </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_insurance" class="">Have Insurance ?</label>
                                                        <select name="pat_insurance" id="pat_insurance" class="form-control">
                                                            <option value="" <?php if($pat_insurance == ''){echo("selected");}?> disabled>-- Select an Option --</option>
                                                            <option value="yes" <?php if($pat_insurance == 'yes'){echo("selected");}?>>Yes </option>
                                                            <option value="no" <?php if($pat_insurance == 'no'){echo("selected");}?>>No </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-8">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_notes" class="">Notes</label>
                                                        <textarea name="pat_notes" value="<?php echo $pat_notas?>" placeholder="" id="pat_notes" cols="30" rows="5" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="position-relative form-group">
                                                        <label for="pat_lang" class="">Patient Status</label>
                                                        <select name="pat_lang" id="pat_lang" class="form-control">
                                                            <option value="" <?php if($pat_status == ''){echo("selected");}?> disabled>-- Update Status--</option>
                                                            <option value="active" <?php if($pat_status == 'active'){echo("selected");}?>>Active </option>
                                                            <option value="inactive" <?php if($pat_status == 'inactive'){echo("selected");}?>>Inactive </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="position-relative form-group">
                                                        <label for="">Clic on <b>UPDATE PROFILE</b> to Save Changes on Patient's Profile</label>
                                                        <br>
                                                        <button class="mt-2 btn btn-warning" disabled>UPDATE PROFILE</button>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </form>
                                    </div>
                                </div>
                        </div>
                        <!-- APPOINTMENTS -->
                        <div class="tab-pane tabs-animation fade" id="apps" role="tabpanel">
                            <div class="row">
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Scheduled</div>
                                                <div class="widget-subheading">Scheduled Appointments</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-success"><span id="countScheduled"><?php echo $countScheduled['sch'];?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Arrived Appointments</div>
                                                <div class="widget-subheading">Finished Appointments</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-primary">
                                                    <span id="countArrives"><?php echo $countFinished['fin'];?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">No Show Up</div>
                                                <div class="widget-subheading">Missed Appointments</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-danger">
                                                    <span id="countMissed"><?php echo $countMissed['nsu'];?></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Patient Appointments</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="display align-middle mb-0 table table-borderless table-striped table-hover table-hover dt-responsive nowrap"
                                                    id="example">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">#</th>
                                                            <th class="text-center">User</th>
                                                            <th class="text-center">ID</th>
                                                            <th class="text-center">Patient</th>
                                                            <th class="text-center">Contact</th>
                                                            <th class="text-center">Reason</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Time</th>
                                                            <th class="text-center">State</th>
                                                            <th class="text-center">Confirmation</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $result2 = $conexion->query('SELECT
                                                            citas.id_cita AS idcita,
                                                            citas.id_user AS user,
                                                            citas.id_paciente AS idpaciente,
                                                            citas.id_reason AS reason,
                                                            citas.cita_fecha AS fecha,
                                                            citas.cita_hora AS hora,
                                                            citas.cita_scheduled AS scheduled,
                                                            citas_estado.estado_cita AS estado,
                                                            citas_estado.estado_color AS color,
                                                            clinicas.clinica_nombre AS clinica,
                                                            CONCAT(
                                                                paciente.paciente_nombres,
                                                                " ",
                                                                paciente.paciente_apellidos
                                                            ) AS paciente,
                                                            paciente.id_paciente_eagle AS ideagle,
                                                            paciente.paciente_contacto AS contacto,
                                                            calendario.id_app_type,
                                                            appt_types.type_description AS reason2,
                                                            appt_types.type_color_hex AS colorhex2,
                                                            citas_reason.reason_color AS colorhex,
                                                            citas.id_clinica AS idclinic
                                                        FROM
                                                            citas
                                                        INNER JOIN citas_estado ON citas.id_cita = citas_estado.id_cita
                                                        INNER JOIN clinicas ON citas.id_clinica = clinicas.id_clinica
                                                        INNER JOIN paciente ON citas.id_paciente = paciente.id_paciente
                                                        INNER JOIN calendario ON citas.id_cita = calendario.id_cita
                                                        LEFT JOIN appt_types ON calendario.id_app_type = appt_types.type_id
                                                        LEFT JOIN citas_reason ON citas.id_reason = citas_reason.reason_nombre
                                                        WHERE
                                                            citas.id_paciente = "'.$_GET['id'].'"
                                                        ORDER BY
                                                            citas.id_cita
                                                        DESC');

                                                            if(!empty($result2)){
                                                            while($app=$result2->fetch_assoc()){
                                                                $app_codigo=$app['idcita'];
                                                                $app_pat=$app['idpaciente'];
                                                                $app_user=$app['user'];
                                                                $app_paciente=$app['paciente'];
                                                                $app_clinica=$app['clinica']; 
                                                                $app_reason=$app['reason']; 
                                                                #$app_confirm=$app['cita_confirmacion'];
                                                                $app_date=date("m-d-Y",strtotime($app['fecha'])); 
                                                                $app_time=date("h:s A",strtotime($app['hora'])); 
                                                                $paciente = ucwords($app['paciente']);

                                                                $color_cita = $app['color'];
                                                                if($color_cita == "#3ac47d"){
                                                                    $estado_cita = "success";
                                                                }
                                                                else if($color_cita == "#16aaff"){
                                                                    $estado_cita = "info";
                                                                }
                                                                else if($color_cita == "#f7b924"){
                                                                    $estado_cita = "warning";
                                                                }
                                                                else if($color_cita == "#dcdcdc"){
                                                                    $estado_cita = "light";
                                                                }
                                                                else if($color_cita == "#343a40"){
                                                                    $estado_cita = "dark";
                                                                }
                                                                else if($color_cita == "#f775f0"){
                                                                    $estado_cita = "danger";
                                                                }
                                                                else{
                                                                    $estado_cita = "info";
                                                                }

                                                               /*  $color_icon = "";
                                                                if($app_confirm == 0){$icon = "question"; $color_icon = "danger";}
                                                                else if($app_confirm == 1){$icon = "check"; $color_icon = "success";}
                                                                else if($app_confirm == 2){$icon = "paper-plane"; $color_icon = "primary";}
                                                                else if($app_confirm == 3){$icon = "phone"; $color_icon = "dark";} */

                                                                $reason = "";
                                                                if(empty($app_reason)){
                                                                    if(!empty($app["reason2"])){
                                                                        $reason = $app["reason2"];
                                                                        $app_color = $app["colorhex2"];
                                                                    }else{
                                                                        $reason = "";
                                                                        $app_color = "";
                                                                    }
                                                                }else{
                                                                    $reason = $app_reason;
                                                                    $app_color = $app["colorhex"];
                                                                }

                                                                if(!empty($app['ideagle'])){
                                                                    if(strpos($app['ideagle'], 'M') !== false){
                                                                        $id_paciente_eagle = str_replace("M", "", $app['ideagle']);
                                                                    }else{
                                                                        $id_paciente_eagle = $app['ideagle'];
                                                                    }
                                                                }else{
                                                                    $id_paciente_eagle = "";
                                                                }
                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row">
                                                                <?php echo $app_codigo;?></td>
                                                            <td class="text-center text-muted" scope="row">
                                                                <?php echo $app_user;?></td>
                                                            <td class="text-center">
                                                                <b><?php echo $app_pat;?></b>
                                                            </td>
                                                            <td>
                                                                <div class="widget-content p-0">
                                                                    <div class="widget-content-wrapper">
                                                                        <div class="widget-content-left flex2">
                                                                            <div class="widget-heading">
                                                                                <?php echo $paciente;?>
                                                                                <div class="widget-subheading opacity-7">
                                                                                    Eagle: <?php echo $id_paciente_eagle;?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center"><?php echo formatTelefono($app['contacto']);?></td>
                                                            <td class="text-left">
                                                                <svg width="25" height="15" viewBox="0 0 250 150">
                                                                    <rect x="70" y="25" height="100" width="120" style="stroke:#000; fill: <?php echo $app_color;?>"></rect>
                                                                </svg> <?php echo $reason;?>
                                                            </td>
                                                            <td class="text-center"><?php echo $app_clinica;?></td>
                                                            <td class="text-center"><?php echo $app_date;?></td>
                                                            <td class="text-center"><?php echo $app_time;?></td>
                                                            <td class="text-center">
                                                                <div class="badge badge-<?php echo $estado_cita;?>">
                                                                    <?php echo $app["estado"];?>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">Cooming Soon</td>
                                                            <td class="text-center">
                                                                <button type="button" id="detailsF" 
                                                                    class="btn btn-alternate btn-sm">Details</button>
                                                            </td>
                                                        </tr>
                                                        <?php }} ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="jobs" role="tabpanel">
                            <!-- <div class="row">
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Jobs</div>
                                                <div class="widget-subheading">Created Jobs</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-primary"><span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Active Jobs</div>
                                                <div class="widget-subheading">Active Job Numbers</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-primary">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card mb-3 widget-content">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Inactive Jobs</div>
                                                <div class="widget-subheading"></div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-primary">
                                                    <span></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">List of Registered Job Numbers</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="jobs">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">id</th>
                                                            <th class="text-center">Creation Date</th>
                                                            <th class="text-center">Job Number</th>
                                                            <th>Patient</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Added By</th>
                                                            <th class="text-center">Treatment Plan</th>
                                                            <th class="text-center">Payment Plan</th>
                                                            <th class="text-center">Status</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="jobsList">
                                                        <?php 
                                                        $countDes = 0;
                                                        $result = $conexion->query('SELECT job_number.id_paciente as id_paciente,
                                                        job_number.id_job as id,
                                                        job_number.id_user as usuario,
                                                        job_number.job_number as job,
                                                        job_number.job_fecha as job_fecha,
                                                        job_number.job_treatmentplanid as tp_id,
                                                        job_number.status as status,
                                                        tp_sign.tp_sign_doc as tpsdoc,
                                                        tp_sign.tp_sign_pat as tpspat,
                                                        paciente.paciente_nombres as pat_name,
                                                        paciente.paciente_apellidos as pat_last,
                                                        clinicas.clinica_nombre as clinica,
                                                        jnf.job_sign_doc as jnfdoc,
                                                        jnf.job_sign_pat as jnfpat FROM `job_number` 
                                                        INNER JOIN paciente ON paciente.id_paciente = job_number.id_paciente
                                                        INNER JOIN clinicas ON clinicas.clinica_char = job_number.id_clinica
                                                        INNER JOIN tp_sign ON tp_sign.tp_id = job_number.job_treatmentplanid 
                                                        LEFT JOIN (SELECT `id_file`, `job_number`, `job_sign_doc`, `job_sign_pat` FROM `job_number_file` 
                                                        WHERE `job_sign_pat` is NOT NULL GROUP BY `job_number`) jnf ON jnf.job_number = job_number.job_number 
                                                        WHERE job_number.id_paciente = "'.$_GET['id'].'";');

                                                        while($reg=$result->fetch_assoc()){
        
                                                            $num_pp = cantidad('id_jobnumber',$reg["job"],$tabla_job_pp);
                                                            if($num_pp > 0){
                                                                if(empty($reg['jnfpat'])){
                                                                    $pp = '<form action="paymentplan_sign.php" method="POST" target="_blank" rel="noopener noreferrer">
                                                                                <input type="hidden" name="enviar_hdn" value="'. $reg['job'].'" />
                                                                                <button type="submit" class="btn btn-danger btn-sm">Sign PP</button>
                                                                            </form>';
                                                                    
                                                                    $edit = '<button class="noteedit btn btn-warning" type="button" id="note">
                                                                            <i class="fa fa-pencil-alt"></i>
                                                                        </button>';
                                                                    
                                                                }
                                                                else if(!empty($reg['jnfpat'])){
                                                                    $pp = '<form action="paymentplan/printpdf.php" method="POST" target="_blank" rel="noopener noreferrer">
                                                                                <input type="hidden" name="getpdf" value="'.$reg['job'].'" />
                                                                                <button type="submit" class="btn btn-alternate btn-sm" id="getpdf" >
                                                                                    <span><i class="fa fa-file-pdf"></i></span> Get PP File</button>
                                                                            </form>';
                                                                    $edit = '';
                                                                }
                                                            }else{
                                                                $pp = "";

                                                                if(empty($reg['tpspat'])){
                                                                    $edit = '<button class="noteedit btn btn-warning" type="button" id="note"><i class="fa fa-pencil-alt"></i></button>';
                                                                }else{
                                                                    $edit = '';
                                                                }
                                                            }

                                                            //firmar o pdf de TX

                                                            if(empty($reg['tpspat'])){
                                                                $tx = '<form action="treatmentplan_sign.php" method="POST" target="_blank" rel="noopener noreferrer">
                                                                <input type="hidden" name="enviar_hdn"
                                                                    value="'. $reg["tp_id"].'" />
                                                                    <button type="submit" class="btn btn-dark btn-sm">Sign TX</button>
                                                                </form>';
                                                            }
                                                            else if(!empty($reg['tpspat'])){
                                                                $tx = '<form action="treatmentplan/printpdf.php" method="POST" target="_blank" rel="noopener noreferrer">
                                                                    <input type="hidden" name="getpdf" value="'.$reg['tp_id'].'" />
                                                                    <button type="submit" class="btn btn-primary btn-sm" id="getpdf">
                                                                        <span><i class="fa fa-file-pdf"></i></span> Get TX File</button>
                                                                </form>';
                                                            }

                                                            if(!empty($reg['pat_name']) && !empty($reg['pat_last'])){
                                                                $patient_name = $reg['pat_name'].' '.$reg['pat_last'];
                                                            }

                                                            $patient = '<div class="widget-content p-0">
                                                                            <div class="widget-content-wrapper">
                                                                                <div class="widget-content-left flex2">
                                                                                    <div class="patidsoft widget-heading">
                                                                                        '.$reg['id_paciente'].'</div>
                                                                                    <div class="widget-subheading opacity-7">
                                                                                        '.$patient_name.'
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>';

                                                            /* status */

                                                            $status = "";
                                                            if($reg['status'] == "active"){
                                                                $status = "checked";
                                                            }else if($reg['status'] == "inactive"){
                                                                $status = "";
                                                            }

                                                            if($reg["status"] == "active"){
                                                                $txt_btn = $reg["status"];
                                                                $color_btn = "success";
                                                            }else if($reg["status"] == "inactive"){
                                                                $txt_btn = $reg["status"];
                                                                $color_btn = "secondary";
                                                            }

                                                            if($reg["status"] == "active"){
                                                                $walkout = '<button class="walkout btn btn-primary" type="button">
                                                                                <i class="fa fa-bolt"></i>
                                                                            </button>';
                                                            }else{
                                                                $walkout = '';
                                                            }
                                                            
                                                            $state = '<div class="custom-control custom-switch">
                                                                        <input type="checkbox" '.$status.' class="swtch custom-control-input"
                                                                            id="switch'.$countDes.'" value="">
                                                                        <label class="custom-control-label"
                                                                            for="switch'.$countDes.'"></label>
                                                                    </div>';

                                                            $action = '
                                                            <div class="row">
                                                                <div class="btn-group">
                                                                    <button class="account btn btn-info" type="button">
                                                                        <i class="fa fa-search-plus"></i>
                                                                    </button>
                                                                    '.$walkout.'
                                                                    '.$edit.'
                                                                </div>
                                                            </div>';

                                
                                                        ?>

                                                        <tr>
                                                            <td class="text-center"><?php echo $reg['id'];?></td>
                                                            <td class="text-center"><?php echo $reg['job_fecha'];?></td>
                                                            <td class="text-left"><?php echo $reg['job'];?></td>
                                                            <td class="text-left"><?php echo $patient;?></td>
                                                            <td class="text-center"><?php echo $reg['clinica'];?></td>
                                                            <td class="text-center"><?php echo $reg['usuario'];?></td>
                                                            <td class="text-center"><?php echo $tx;?></td>
                                                            <td class="text-center"><?php echo $pp;?></td>
                                                            <td class="text-center"><?php echo $state;?></td>
                                                            <td class="text-center"><?php echo $action;?></td>
                                                            
                                                        </tr>
                                                        <?php $countDes++;
                                                }?>
                                                    </tbody>
                                                </table>
                                                <!-- <br>
                                                <button type="submit" class="btn btn-warning btn-sm" id="updateStatus">
                                                    <span><i class="fa fa-edit"></i></span> Update Status
                                                </button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="trans" role="tabpanel"></div>
                    </div>
                    <?php } ?>
                </div>
                <?php include 'footer.php'; ?>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <script type="text/javascript" src="./assets/scripts/jquery-ui/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="./assets/scripts/jquery-ui/jquery-ui.css" />

    <!-- input phone mask -->
    <script src="assets/mask/jquery.mask.js"></script>

    <script src="patients/account.js"></script>

    <script src="assets/dataTable/jquery.dataTables.min.js"></script>
    <script src="assets/dataTable/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            /* swal.fire({
                title: "Updating Page",
                text: "We are developing something amazing for you. I'll let you know when it's ready ;)",
                imageUrl: 'https://gsr-web.s3.amazonaws.com/prod/uploads/2020/02/Best-Sneaker-Bots-For-2020.jpg',
                imageWidth: 100,
                imageHeight: 100,
                imageAlt: 'Custom image'
            }); */

            $('#directCostTable').DataTable();
            $('.phone').mask('(000) 000 0000');

             //Ver account
             $(document).on("click", ".account", function () {
                    opcion = 2; //editar
                    fila = $(this).closest("tr");
                    job_id = $.trim(fila.find('td:eq(2)').text()); //capturo el ID
                    window.open('job_account.php?id=' + job_id, '_blank');
                });

                /* $(document).on('click', '.walkout', function (e) {
                    swal.fire({
                        title: "Walkout is Coming",
                        text: "We are developing something amazing for you. I'll let you know when it's ready ;)",
                        imageUrl: 'https://gsr-web.s3.amazonaws.com/prod/uploads/2020/02/Best-Sneaker-Bots-For-2020.jpg',
                        imageWidth: 100,
                        imageHeight: 100,
                        imageAlt: 'Custom image'
                    });
                }); */

                $(document).on('click', '.noteedit', function (e) {
                    swal.fire({
                        title: "Updating Option",
                        text: "We are developing something amazing for you. I'll let you know when it's ready ;)",
                        imageUrl: 'https://gsr-web.s3.amazonaws.com/prod/uploads/2020/02/Best-Sneaker-Bots-For-2020.jpg',
                        imageWidth: 100,
                        imageHeight: 100,
                        imageAlt: 'Custom image'
                    });
                });

                //GO TO WALKOUT
                $(document).on("click", ".walkout", function () {
                    opcion = 2; //editar
                    fila = $(this).closest("tr");
                    job_id = $.trim(fila.find('td:eq(2)').text()); //capturo el ID
                    patient = $.trim(fila.find('td:eq(3)').text()); //capturo el ID de paciente
                    pat = $.trim(patient.substring(0, 6));

                    //console.log(pat);
                    window.open('walkout.php?id=' + job_id +"&patient=" + pat, '_blank');
                });

                /* Activar y desactivar job */
                $(document).on('click', '.swtch', function (e) {
                    var checkStatus = this.checked ? 'ON' : 'OFF';
                    //var checkStatus = $(this).attr('value');
                    //alert(checkStatus);

                    if(checkStatus == 'ON'){
                        state = "active";
                    }if(checkStatus == 'OFF'){
                        state = "inactive";
                    }

                    //console.log($(this).parents("tr").find("td").eq(0).html());

                    const postData = {
                        status: state,
                        job: $(this).parents("tr").find("td").eq(0).html()
                    };

                    $.ajax({
                        type: 'POST',
                        url: 'job/update-status.php',
                        data: postData,
                        success: function (resp) {
                            console.log(resp);
                        }
                    });
                    //console.log('Yaay, I was changed');
                });

        });
    </script>
    <!-- <script src="patients/account.js"></script> -->
</body>

</html>
