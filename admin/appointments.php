<?php 
    include 'include/funciones.php';
?>

<!doctype html>
<html lang="en">

<head>
    <title>PPS | Appointment List</title>
    <?php include 'head.php'; ?>
    <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />

    <style>
    table.dataTable thead {
        background: linear-gradient(to right, #4A00E0, #8E2DE2);
        color: white;
    }
    </style>

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include 'navbar.php';
        ?>

        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-date icon-gradient bg-happy-itmeo">
                                    </i>
                                </div>
                                <div>Appointments
                                    <div class="page-title-subheading">This is a List of Registered Appointments
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav" id="myTab">
                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                <span>List of Appointments</span>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                <span>My Appointments</span>
                            </a>
                        </li> -->
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">List of Registered Appointments</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="appointments">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Appt</th>
                                                            <th class="text-center">User</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Patient</th>
                                                            <th class="text-center">Contact</th>
                                                            <th class="text-center">Reason</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Time</th>
                                                            <th class="text-center">State</th>
                                                            <th class="text-center">Lab Case</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">List of Registered Appointments</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="manassas">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Appt</th>
                                                            <th class="text-center">User</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Patient</th>
                                                            <th class="text-center">Contact</th>
                                                            <th class="text-center">Reason</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Time</th>
                                                            <th class="text-center">State</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <?php include 'footer.php'; ?>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <!-- DATATABLE -->
    <script src="assets/dataTable/jquery.dataTables.min.js"></script>
    <script src="assets/dataTable/dataTables.bootstrap4.min.js"></script>

    <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <!-- Sweet Alert -->
    <script src="./assets/sweetalert/sweetalert2@9.js"></script>
    <script src='./assets/calendar/moment.min.js'></script>
    <!-- <script src="assets/bootstrap.bundle.min.js"></script> -->

    <script>
    $(window).bind("load", function() {
        $('#manassas').DataTable({
            "order": [
                [4, "desc"]
            ],
        });

        $('#appointments').DataTable({
            //"responsive":true,
            "order": [
                [0, "desc"]
            ],
            "serverSide": true,
            "ajax": {
                "method": "POST",
                "url": "appointments/load-appointments.php"
            },
            "bProcessing": true,
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 10,
            "aoColumns": [{
                    "mData": "idcita"
                },
                {
                    "mData": "user"
                },
                {
                    "mData": "clinica"
                },
                {
                    "mData": "paciente"
                },
                {
                    "mData": "contacto"
                },
                {
                    "mData": "reason"
                },
                {
                    "mData": "fecha"
                },
                {
                    "mData": "hora"
                },
                {
                    "mData": "estado"
                },
                {
                    "mData": "lab"
                },
                {
                    "mData": "action"
                }
            ],
            columnDefs: [{
                    className: 'text-left',
                    targets: [3, 5]
                },
                {
                    className: 'text-center',
                    targets: [0, 1, 2, 4, 6, 7, 8, 9, 10]
                },
            ],
            //"iDisplayLength": -1,


        });

        $(document).on('click', '.details', function(e) {
            var event = $(this).data('event');
            var start = $(this).data('eventofecha');
            $.ajax({
                        url: "appointments/loadEventInfo.php",
                        type: "POST",
                        data: {
                            id: event
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            var texto =
                                "<div class=''><b>Date: </b>" + moment(start).format("MMMM Do YYYY, h:mm a") +
                                " (" + data[0]["duration"] + ")" +
                                "<br><b>Reason         : </b>" + data[0]["reason"] +
                                "<br><b>Patient        : </b>" + data[0]["patient"] +
                                "<br><b>Patient Type   : </b>" + data[0]["type"] +
                                "<br><b>Birthdate      : </b>" + data[0]["birthday"] +
                                "<br><b>Have insurance : </b>" + data[0]["insurance"] +
                                "<br><b>Notes          : </b>" + data[0]["notes"] +
                                "<br><b>Referral       : </b>" + data[0]["referral"] +
                                "<br><b>Contact        : </b>" + data[0]["contact"] +
                                "<br><b>Added by       : </b>" + data[0]["user"] + " on " + data[0]["dateAdd"] +
                                "<br><b>Provider       : </b>" + data[0]["provider"] +
                                "<br><b>Campaign       : </b>" + data[0]["campaign"] +
                                '</div>'; //Event Start Date

                            swal.fire({
                                title: data[0]["eagle"], //Event Title calEvent.title
                                html: texto,
                                /* input: 'select',
                                inputOptions: {
                                    scheduled: 'Schedule Appointment',
                                    canceled: 'Cancel Appointment',
                                    deleted: 'Delete Appointment',
                                },
                                inputPlaceholder: data[0]["icono"],
                                showCancelButton: true,
                                confirmButtonText: 'Update', */
                                cancelButtonText: 'Close',
                                cancelButtonColor: '#3f6ad8',
                                /* confirmButtonColor: '#794c8a', */
                                icon: "info"
                            })/* .then((result) => {
                                if (result.value) {
                                    const dataToSend = {
                                        id: calEvent.id,
                                        icon: result.value
                                    };
                                    $.ajax({
                                        url: "calendar/updateApptState.php",
                                        type: "POST",
                                        data: dataToSend,
                                        success: function (
                                            response
                                        ) {
                                            if (response ==
                                                "Success"
                                            ) {
                                                swal.fire({
                                                        title: "Done!",
                                                        text: "The Appointment State was updated!",
                                                        type: "success"
                                                    })
                                                    .then(
                                                        function () {
                                                            location.reload();
                                                        }
                                                    );
                                            } else if (
                                                response ==
                                                "Error"
                                            ) {
                                                swal.fire(
                                                    "Error updating!",
                                                    "Please try again",
                                                    "error"
                                                );
                                            }

                                        },
                                        error: function (
                                            xhr,
                                            ajaxOptions,
                                            thrownError
                                        ) {
                                            swal.fire(
                                                "Error updating!",
                                                "Please try again",
                                                "error"
                                            );
                                        }
                                    });
                                }
                            }, function (dismiss) {
                                if (dismiss === 'cancel') {

                                }
                            }) */;
                        }
                    });
        });

        function ConvertTimeformat(format, str) {
            var hours = Number(str.match(/^(\d+)/)[1]);
            var minutes = Number(str.match(/:(\d+)/)[1]);
            var AMPM = str.match(/\s?([AaPp][Mm]?)$/)[1];
            var pm = ['P', 'p', 'PM', 'pM', 'pm', 'Pm'];
            var am = ['A', 'a', 'AM', 'aM', 'am', 'Am'];
            if (pm.indexOf(AMPM) >= 0 && hours < 12) hours = hours + 12;
            if (am.indexOf(AMPM) >= 0 && hours == 12) hours = hours - 12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10) sHours = "0" + sHours;
            if (minutes < 10) sMinutes = "0" + sMinutes;
            if (format == '0000') {
                return (sHours + sMinutes);
            } else if (format == '00:00') {
                return (sHours + ":" + sMinutes);
            } else {
                return false;
            }
        }

        $(document).on('click', '.edit', function(e) {
            var idapp = $(this).data('id');
            var chart = $(this).data('chart');
            var paciente = $(this).data('paciente');
            var reason = $(this).data('reason');
            var fecha = $(this).data('fecha');
            var clinic = $(this).data('clinic');
            var contacto = $(this).data('contacto');
            var actualdate = $(this).data('actualdate');
            var actualhour = $(this).data('actualhour');

            console.log(actualhour);

            var clinictext = "";
            if (clinic == 1) {
                clinictext = "Fairfax";
            } else if (clinic == 2) {
                clinictext = "Manassas";
            } else if (clinic == 3) {
                clinictext = "Woodbridge";
            }

            $('#app').val(idapp);
            $('#chart').text(chart);
            $('#patientName').text(paciente);
            $('#appReason').text(reason);
            $('#dateapp').val(fecha);
            $('#clinicapp').val(clinic);
            $('#actualdate').text(actualdate);
            $('#actualhour').text(actualhour);
            $('#clinictext').text(clinictext);
            $('#contacto').text(contacto);

            $("#select_clinic option[value='"+clinic+"']").attr("selected",true);
            $("#timeapp option[value='"+actualhour+"']").attr("selected",true);

            //CREAR ID DE PACIENTE
            //$("#select_clinic").change(function () {
            var availableDates = [];
            $('#dateapp').datepicker('destroy');
            $('#timeapp').empty();

            $.ajax({
                type: "POST",
                url: 'calltrackerentry/loadAvailableDate.php',
                data: 'id_clinica=' + clinic,
                dataType: 'JSON',
                success: function(resp) {

                    for (var d in resp) {
                        availableDates.push(resp[d]);
                    }

                    $('#dateapp').datepicker({
                        format: 'yyyy/mm/dd',
                        todayHighlight: true,
                        daysOfWeekDisabled: [0],
                        daysOfWeekHighlighted: [1, 2, 3, 4, 5, 6],
                        beforeShowDay: function(dt) {
                            mdy = (('' + (dt.getMonth() + 1)).length < 2 ?
                                    '0' : '') + (dt.getMonth() + 1) + "-" +
                                (('' + (dt.getDate())).length < 2 ? '0' :
                                    '') + dt.getDate() + "-" + dt
                                .getFullYear();
                            if ($.inArray(mdy, availableDates) != -1) {
                                return true;
                            } else {
                                return false;
                            }
                        },
                        changeMonth: true,
                        changeYear: false,
                    });


                }
            });

            //});

            //Define time to Appointment
            $('#dateapp').change(function(e) {
                $('#timeapp').empty();
                const data = {
                    id_clinica: $('#clinicapp').val(),
                    date: $('#dateapp').val()
                };

                $.ajax({
                    type: "POST",
                    url: 'calltrackerentry/loadAvailableHours.php',
                    data: data,
                    dataType: 'JSON',
                    success: function(resp) {
                        $(resp).each(function(v) { // valor
                            $('#timeapp').append('<option value="' + (
                                resp[v].time) + '">' + (resp[v]
                                .time) + '</option>');
                        });
                    }

                });
            });
        });

        $('#reschedule').click(function() {

            var idapp = $('#app').val();

            const data = {
                time: ConvertTimeformat("00:00", $('#timeapp').val()),
                id: idapp,
                date: $('#dateapp').val(),
                clinic: $('#select_clinic').val()
            }

            $.ajax({
                type: "POST",
                url: "appointments/update.php",
                data: data,
                success: function(resp) {
                    if (resp == "Success") {
                        swal.fire({
                            title: "Appointment",
                            text: "Has been reschedule successfully",
                            icon: "success"
                        }).then(function() {
                            location.reload();
                        });
                    } else if (resp == "Error") {
                        swal.fire({
                            title: "Appointment",
                            text: "Can't be reschedule. Please Try Again",
                            icon: "error"
                        });
                    } else {
                        swal.fire({
                            title: "Appointment",
                            text: resp,
                            icon: "error"
                        });
                    }
                }
            });
        });

        //Ver account
        $(document).on("click", ".account", function() {
            pat_id = $.trim($(this).text());
            window.open('pat_account.php?id=' + pat_id, '_blank');
        });

        /* $(document).on('click', '.details', function (e) {
            var idapp = $(this).data('idapp');

            //$('#app').val(idapp);

            console.log(idapp);

            $.ajax({
                type: "POST",
                url: "appointments/details.php",
                data: idapp,
                success: function (resp) {
                    $('#detalleCita').html(resp);
                }
            });


        }); */

    });
    </script>
</body>

</html>

<div class="modal fade update-app" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Appointment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <p class="card-title">Appointment Info</p>
                            <div class="widget-content p-0">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading" id="actualdate"></div>
                                            <div class="widget-subheading" id="actualhour"></div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="text-danger" id="clinictext"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="widget-content p-0">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading" id="patientName"></div>
                                            <div class="widget-subheading" id="chart"></div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="text-primary" id="appReason"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <p><i class="pe-7s-call text-danger text-bold"></i> <span id="contacto"></span></p>


                            <input type="hidden" id="app" />
                            <input type="hidden" id="clinicapp" />
                        </div>
                    </div>
                    <p class="card-title">Update Appointment Info</p>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="dateapp">Date</label>
                            <input placeholder="Select a Date" id="dateapp" name="dateapp" class="form-control" />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="timeapp">Time</label>
                            <!-- <input type="text" id="timeapp" name="timeapp" class="form-control" /> -->
                            <select class="form-control" id="timeapp"></select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="dateapp">Clinic</label>
                            <div class="position-relative row form-group">
                                    <select class="form-control" id="select_clinic" name="select_clinic">
                                        <?php
                                            $result = $conexion->query('SELECT * FROM '. $tabla_clinicas .' ORDER BY clinica_nombre');
                                            while($clinica=$result->fetch_assoc()):
                                                $clinic_codigo=$clinica['id_clinica'];
                                                $clinic_name=$clinica['clinica_nombre'];
                                        ?>
                                                <option value="<?php echo $clinic_codigo;?>"><?php echo $clinic_name;?></option>
                                        

                                        <?php endwhile; ?>
                                    </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="reschedule">SAVE CHANGES</button>
                <button type="button" class="btn btn-dark" data-dismiss="modal">CANCEL</button>

            </div>
        </div>
    </div>
</div>

<!-- Details Modal -->

<!-- <div class="modal fade modalDetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <input type="hidden" id="app" />
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <p class="card-title">Patient Info</p>
                            <div class="row">
                                <div class="col">
                                    <ul class="nav flex-column">
                                        <li class="nav-item-header nav-item">Basic Info</li>
                                        <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Name</a>
                                        </li>
                                        <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Lastname</a>
                                        </li>
                                        <li class="nav-item"><a href="javascript:void(0);" class="nav-link">DOB</a></li>
                                        <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Patient
                                                type</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col">
                                    <ul class="nav flex-column">
                                        <li class="nav-item-header nav-item text-white">Basic Info</li>
                                        <li class="nav-item"><b class="nav-link">City</b></li>
                                        <li class="nav-item"><b class="nav-link">City</b></li>
                                        <li class="nav-item"><b class="nav-link">City</b></li>
                                        <li class="nav-item"><b class="nav-link">City</b></li>
                                    </ul>
                                </div>

                                <div class="col">
                                    <ul class="nav flex-column">
                                        <li class="nav-item-header nav-item">Contact</li>
                                        <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                                    class="nav-link-icon pe-7s-call"> </i><span>Phone</span>
                                            </a></li>

                                        <li class="nav-item-header nav-item">Insurance</li>
                                        <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                                    class="nav-link-icon pe-7s-keypad"> </i><span>Insurance</span>
                                            </a></li>
                                        <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                                    class="nav-link-icon pe-7s-box2"> </i><span>Ins. Company</span>
                                            </a></li>
                                    </ul>
                                </div>
                                <div class="col">
                                    <ul class="nav flex-column">
                                        <li class="nav-item-header nav-item text-white">Contact</li>
                                        <li class="nav-item"><b class="nav-link">City</b></li>
                                        <li class="nav-item-header nav-item text-white">Insurance</li>
                                        <li class="nav-item"><a href="javascript:void(0);"
                                                class="nav-link"><span>Insurance</span>
                                            </a></li>
                                        <li class="nav-item"><b class="nav-link">City</b></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <p class="card-title">Appointment Info</p>
                    <div class="row">
                        <div class="col">
                            <ul class="nav flex-column">
                                <li class="nav-item-header nav-item">Basic Info</li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Clinic</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Date</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Time</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Reason</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Notes</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Chat
                                                <div class="ml-auto badge badge-pill badge-info">8</div>
                                            </a></li> 
                            </ul>
                        </div>
                        <div class="col">
                            <ul class="nav flex-column">
                                <li class="nav-item-header nav-item text-light">App info</li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Clinic</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Date</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Time</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Reason</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Notes</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link">Chat
                                                <div class="ml-auto badge badge-pill badge-info">8</div>
                                            </a></li>
                            </ul>
                        </div>
                        <div class="col">
                            <ul class="nav flex-column">
                                <li class="nav-item-header nav-item ">Provider Info</li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                            class="nav-link-icon pe-7s-user text-alternate"> </i><span>Scheduled
                                            By</span>
                                    </a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                            class="nav-link-icon pe-7s-clock text-alternate"> </i><span>Date
                                            Scheduled</span></a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                            class="nav-link-icon pe-7s-like2 text-alternate"> </i><span>Referral</span>
                                    </a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                            class="nav-link-icon pe-7s-box2 text-alternate"> </i><span>Provider</span>
                                    </a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                            class="nav-link-icon pe-7s-chat text-alternate"> </i><span>Chat Name</span>
                                    </a></li>
                            </ul>
                        </div>
                        <div class="col">
                            <ul class="nav flex-column">
                                <li class="nav-item-header nav-item text-light">Provider Info</li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><span>Scheduled
                                            By</span>
                                    </a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><span>Date
                                            Scheduled</span></a></li>
                                <li class="nav-item"><a href="javascript:void(0);"
                                        class="nav-link"><span>Referral</span>
                                    </a></li>
                                <li class="nav-item"><a href="javascript:void(0);"
                                        class="nav-link"><span>Provider</span>
                                    </a></li>
                                <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><span>Chat
                                            Name</span>
                                     <div class="ml-auto badge badge-alternate">Name</div>
                                    </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
</div> -->