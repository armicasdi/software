<?php 
    include 'include/funciones.php';
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset='utf-8' />
    <title>PPS | Scheduler</title>
    <?php include 'head.php'; ?>
    <link href='./assets/calendar/main.css' rel='stylesheet' />
    <link href='./assets/calendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />

    <style>
        .modal-backdrop.fade.in {
            z-index: -1;
        }
    </style>
</head>

<body>

    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include_once "navbar.php";
        ?>
        <div class="ui-theme-settings">
            <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
                <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
            </button>
        </div>
        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-date icon-gradient bg-warm-flame">
                                    </i>
                                </div>
                                <div>Sheduler Appointments <b>(Under Construction)</b>
                                    <div class="page-title-subheading">You can see below, all Sheduled Appointments
                                    </div>
                                </div>
                            </div>
                            <div class="page-title-actions">
                                <!-- <button type="button" id="availableHours" data-toggle="modal" data-target=".available"
                                        class="btn-shadow mr-3 btn btn-primary"> Available Hours
                                    </button> -->
                                <button type="button" id="infoStates" data-toggle="modal" data-target=".states"
                                    class="btn-shadow mr-3 btn btn-dark"> States Info
                                </button>
                            </div>
                        </div>
                    </div>
                    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#fairfax">
                                <span>Fairfax</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#manassas">
                                <span>Manassas</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#woodbridge">
                                <span>Woodbridge</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="fairfax" role="tabpanel">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <div id="calendar-fairfax"></div>
                                    <!-- <div id="calendar-prueba"></div> -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation" id="manassas" role="tabpanel">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <div id="calendar-manassas"></div>
                                    <!-- <div id="calendar-bg-events"></div> -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation" id="woodbridge" role="tabpanel">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <!-- <div id="calendar-bg-events"></div> -->
                                    <div id="calendar-woodbridge"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php include 'footer.php'; ?>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <script type="text/javascript" src="./assets/scripts/jquery-ui/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="./assets/scripts/jquery-ui/jquery-ui.css" />
    <!-- Sweet Alert -->
    <script src="./assets/sweetalert/sweetalert2@9.js"></script>

    <script src='./assets/calendar/moment.min.js'></script>
    <!-- <script src='./assets/calendar/jquery.min.js'></script> -->
    <script src='./assets/calendar/fullcalendar.min.js'></script>
    <script src='./assets/calendar/fullcalendar-rightclick.js'></script>
    <script src='./assets/calendar/bootstrap.min.js'></script>
    <!-- <script src='./assets/calendar/fullcalendar-rightclick.js'></script> -->
    <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src='./assets/calendar/main.js'></script>

</body>

</html>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        $(document).ready(function () {
            genCalendar("calendar-fairfax");
            /* genCalendar("calendar-fairfax");
            genCalendar("calendar-manassas");
            genCalendar("calendar-woodbridge"); */

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href") // activated tab
                if(target == "#fairfax"){
                    genCalendar("calendar-fairfax");
                }else if(target == "#manassas"){
                    genCalendar("calendar-manassas");
                }else if(target == "#woodbridge"){
                    genCalendar("calendar-woodbridge");
                }
            });

        });

            $.ajax({
                url: "calendar/seebhfx4c.php",
                type: "POST",
                data: {},
                dataType: 'JSON',
                success: function (data) {
                    $('#calendar-prueba').fullCalendar({
                        themeSystem: 'bootstrap4',
                        customButtons: {
                            refreshButton: {
                                text: 'Refresh Calendar',
                                click: function () {
                                    $("#calendar-fairfax").fullCalendar('render');
                                }
                            }
                        },
                        header: {
                            left: "prev,next today refreshButton",
                            center: "title",
                            right: "agendaDay,month,agendaWeek,listMonth"
                        },
                        /* eventRender: function (event, element) {
                            if (event.icon == "asterisk") {
                                element.find(".fc-title").prepend(
                                    "<i class='text-danger fa fa-" + event.icon +
                                    "'></i> ");
                            } else if (event.icon == "check") {
                                element.find(".fc-title").prepend(
                                    "<i class='text-alternate fa fa-" + event.icon +
                                    "'></i> ");
                            } else if (event.icon == "paper-plane") {
                                element.find(".fc-title").prepend(
                                    "<i class='text-white fa fa-" + event.icon +
                                    "'></i> ");
                            } else if (event.icon == "ban") {
                                element.find(".fc-title").prepend(
                                    "<i class='text-danger fa fa-" + event.icon +
                                    "'></i> ");
                            } else if (event.icon == "trash") {
                                element.find(".fc-title").prepend(
                                    "<i class='text-white fa fa-" + event.icon +
                                    "'></i> ");
                            }

                            if (event.needform == "yes") {
                                element.find(".fc-title").append("<span class='spinner-grow spinner-grow-sm mr-2 text-danger'></span>");
                            } else if (event.needform == "no" || event.needform == null) {
                                element.find(".fc-title").append("<span></span>");
                            }
                        }, */
                        // businessHours: data, 
                        selectConstraint: data,
                        plugins: ['interaction', 'dayGrid', 'timeGrid'],
                        selectable: true,
                        editable: true,
                        selectOverlap: function (event) {
                            return event.rendering === 'background';
                        },
                        events: 'calendar/scheduler.php',

                        /* eventClick: function (calEvent, jsEvent, view, resourceObj) {
                            $.ajax({
                                url: "calendar/loadEventInfo.php",
                                type: "POST",
                                data: {
                                    id: calEvent.id
                                },
                                dataType: 'JSON',
                                success: function (data) {
                                    var texto =
                                        "<div class=''><b>Date: </b>" +
                                            moment(calEvent.start).format("MMMM Do YYYY, h:mm a") + " (" + data[0]["duration"] + ")" +
                                            "<br><b>Reason         : </b>" + data[0]["reason"] +
                                            "<br><b>Patient        : </b>" + data[0]["patient"] +
                                            "<br><b>Patient Type   : </b>" + data[0]["type"] +
                                            "<br><b>Birthdate      : </b>" + data[0]["birthday"] +
                                            "<br><b>Have insurance : </b>" + data[0]["insurance"] +
                                            "<br><b>Notes          : </b>" + data[0]["notes"] +
                                            "<br><b>Referral       : </b>" + data[0]["referral"] +
                                            "<br><b>Contact        : </b>" + data[0]["contact"] +
                                            "<br><b>Added by       : </b>" + data[0]["user"] + " on " + data[0]["dateAdd"] +
                                            "<br><b>Provider       : </b>" + data[0]["provider"] +
                                            "<br><b>Campaign       : </b>" + data[0]["campaign"] +
                                        '</div>'; //Event Start Date

                                    swal.fire({
                                        title: data[0]["eagle"], //Event Title
                                        html: texto,
                                        input: 'select',
                                        inputOptions: {
                                            scheduled: 'Schedule Appointment',
                                            canceled: 'Cancel Appointment',
                                            deleted: 'Delete Appointment',
                                        },
                                        inputPlaceholder: data[0]["icono"],
                                        showCancelButton: true,
                                        confirmButtonText: 'Update',
                                        cancelButtonText: 'Close',
                                        cancelButtonColor: '#3f6ad8',
                                        confirmButtonColor: '#794c8a',
                                        icon: "info"
                                    }).then((result) => {
                                        if (result.value) {
                                            const dataToSend = {
                                                id: calEvent.id,
                                                icon: result.value
                                            };
                                            $.ajax({
                                                url: "calendar/updateApptState.php",
                                                type: "POST",
                                                data: dataToSend,
                                                success: function (response) {
                                                    if (response == "Success") {
                                                        swal.fire({
                                                                title: "Done!",
                                                                text: "The Appointment State was updated!",
                                                                type: "success"
                                                            })
                                                            .then(
                                                                function () {
                                                                    location.reload();
                                                                    //$('#calendar-fairfax').fullCalendar("refetchEvents");
                                                                }
                                                            );
                                                    } else if (response == "Error") {
                                                        swal.fire("Error updating!","Please try again","error");
                                                    }
                                                },
                                                error: function (xhr,ajaxOptions,thrownError) {
                                                    swal.fire("Error updating!","Please try again","error");
                                                }
                                            });
                                        }
                                    }, function (dismiss) {
                                        if (dismiss === 'cancel') {

                                        }
                                    });
                                }
                            });
                        },
                        eventRightclick: function (event, jsEvent, view) {
                            eventsdate = moment(event.start).format('MMMM Do YYYY');
                            date = moment(event.start).format();
                            hour = moment(event.start).format('hh:mm a');

                            swal.fire({
                                title: event.title,
                                html: "Change the clinic this Appointment?",
                                showCancelButton: true,
                                input: 'select',
                                inputOptions: {
                                    1: 'Fairfax',
                                    2: 'Manassas',
                                    3: 'Woodbridge'
                                },
                                inputPlaceholder: 'Select a clinic',
                                confirmButtonColor: '#794c8a',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Change clinic',
                                closeOnConfirm: false,
                                icon: "warning"
                            }).then(result => {
                                if (result.value) {
                                    const dataToSend = {
                                        id: event.id,
                                        clinic: result.value
                                    };
                                    $.ajax({
                                        url: "calendar/updateClinic.php",
                                        type: "POST",
                                        data: dataToSend,
                                        success: function (response) {
                                            if (response == "Success") {
                                                swal.fire({
                                                    title: "Done!",
                                                    text: "Clinic has been Updated",
                                                    type: "success"
                                                }).then(function () {
                                                    location.reload();
                                                });
                                            } else if (response == "Error") {
                                                swal.fire("Error updating!","Please try again", "error");
                                            }

                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            swal.fire("Error updating!", "Please try again","error");
                                        }
                                    });
                                } else {
                                    console.log('cancel action');
                                    event.revertFunc();
                                }
                            });
                            return false;
                        },
                        eventDrop: function (event, dayDelta, minuteDelta, allDay,
                            revertFunc) {

                            eventsdate = moment(event.start).format('MMMM Do YYYY');
                            date = moment(event.start).format();
                            hour = moment(event.start).format('hh:mm a');

                            swal.fire({
                                title: event.title, //Event Title event.start.toISOString()
                                html: "Do you  want to reschedule this Appointment to <br><b>" + eventsdate + "</b>?" +
                                    '<input id="swal-input2" class="swal2-input" value="' + hour + '">',
                                showCancelButton: true,
                                confirmButtonColor: '#794c8a',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Reschedule',
                                closeOnConfirm: false,
                                icon: "warning"
                            }).then(result => {
                                if (result.value) {
                                    const dataToSend = {
                                        id: event.id,
                                        date: date,
                                        start: ConvertTimeformat("00:00", $('#swal-input2').val())
                                    };
                                    $.ajax({
                                        url: "calendar/update-app.php",
                                        type: "POST",
                                        data: dataToSend,
                                        success: function (response) {
                                            if (response == "Success") {
                                                swal.fire({
                                                    title: "Done!",
                                                    text: "Appointment has been Rescheduled",
                                                    type: "success"
                                                }).then(function () {
                                                    location.reload();
                                                });
                                            } else if (response == "Error") {
                                                swal.fire("Error updating!",
                                                    "Please try again", "error");
                                            }

                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            swal.fire("Error updating!", "Please try again",
                                                "error");
                                        }
                                    });
                                } else {
                                    console.log('cancel action');
                                    event.revertFunc();
                                    location.reload();
                                }
                            });

                        } */

                    });
                }
            });

        function bandera(dato) {
            return dato;
        }

        function genEvent(fecha, idClinica) {
            var arreglo;

            $.ajax({
                    async: false,
                    type: "POST",
                    dataType: 'JSON',
                    url: "scheduler/scheduler.php",
                    data: "fecha=" + getFecha(fecha, 1) + "&idClinica=" + idClinica,
                })
                .done(function (data) {
                    arreglo = data;
                })
                .fail(function (data) {
                    return {};
                });
            return arreglo

        }

        function getFecha(fecha, tipo = null) {
            const d = new Date(fecha);
            const y = new Intl.DateTimeFormat('en', {
                year: 'numeric'
            }).format(d);
            const m = new Intl.DateTimeFormat('en', {
                month: '2-digit'
            }).format(d);
            const da = new Intl.DateTimeFormat('en', {
                day: '2-digit'
            }).format(d);
            if (tipo) {
                return (da + "-" + m + "-" + y);
            } else {
                return (da + "/" + m + "/" + y);
            }

        }

        function getHora(fecha) {
            const d = new Date(fecha);
            const h = new Intl.DateTimeFormat('en-US', {
                hour: 'numeric',
                minute: 'numeric'
            }).format(d);
            //const m = new Intl.DateTimeFormat('en-US', { minute: 'numeric' }).format(d);
            return (h);
        }

        function htmlEscape(s) {
        return (s + '').replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/'/g, '&#039;')
            .replace(/"/g, '&quot;')
            .replace(/\n/g, '<br />')
        }

        function genCalendar(clinica) {
            
            var columnas;
            var idClinica;
            var arrayEvent;
            if (clinica == "calendar-fairfax") {
                columnas = [{
                    id: 'FFX1',
                    title: 'FFX1'
                }, {
                    id: 'FFX2',
                    title: 'FFX2'
                }, {
                    id: 'FFX3',
                    title: 'FFX3'
                }, {
                    id: 'FFX4',
                    title: 'FFX4'
                }];
                idClinica = 1;
                arrayEvent = genEvent(Date(), idClinica);
            }
            if (clinica == "calendar-manassas") {
                columnas = [{
                    id: 'OP 1',
                    title: 'OP 1'
                }, {
                    id: 'OP 2',
                    title: 'OP 2'
                }, {
                    id: 'OP 3',
                    title: 'OP 3'
                }, {
                    id: 'OP 4',
                    title: 'OP 4'
                }/* , {
                    id: 'OP 5',
                    title: 'OP 5'
                } */];
                idClinica = 2;
                arrayEvent = genEvent(Date(), idClinica);
            }
            if (clinica == "calendar-woodbridge") {
                columnas = [{
                    id: 'WBG1',
                    title: 'WBG1'
                }, {
                    id: 'WBG2',
                    title: 'WBG2'
                }, {
                    id: 'WBG3',
                    title: 'WBG3'
                }, {
                    id: 'WBG4',
                    title: 'WBG4'
                }];
                idClinica = 3;
                arrayEvent = genEvent(Date(), idClinica);
            }
            var calendarEl = document.getElementById(clinica);
            var citas;

            var calendar = new FullCalendar.Calendar(calendarEl, {

                themeSystem: 'bootstrap',
                customButtons: {
                    next: {
                        text: 'next',
                        click: function () {
                            calendar.next();
                            arrayEvent = genEvent(calendar.getDate(), idClinica);
                            var eventSources = calendar.getEvents();
                            $.each(eventSources, function (index, value) {
                                eventSources[index].remove();
                            });

                            $.each(arrayEvent, function (index, value) {
                                calendar.addEvent(value);
                            });
                            calendar.refetchEvents();
                        }
                    },
                    prev: {
                        text: 'prev',
                        click: function () {
                            calendar.prev();
                            arrayEvent = genEvent(calendar.getDate(), idClinica);
                            var eventSources = calendar.getEvents();
                            $.each(eventSources, function (index, value) {
                                eventSources[index].remove();
                            });

                            $.each(arrayEvent, function (index, value) {
                                calendar.addEvent(value);
                            });
                            calendar.refetchEvents();
                            //console.log(calendar['currentData']['eventSources'][]);
                            //calendar.render();
                        }
                    },
                    today: {
                        text: 'today',
                        click: function () {
                            calendar.today();
                            arrayEvent = genEvent(calendar.getDate(), idClinica);
                            var eventSources = calendar.getEvents();
                            $.each(eventSources, function (index, value) {
                                eventSources[index].remove();
                            });

                            $.each(arrayEvent, function (index, value) {
                                calendar.addEvent(value);
                            });
                            calendar.refetchEvents();
                        }
                    }
                },
                eventRightclick: function () {
                    alert("hola")
                },
                schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,resourceTimeGridDay'
                },

                navLinks: true, // can click day/week names to navigate views
                selectable: true,
                selectMirror: true,
                slotEventOverlap: false,
                allDaySlot: false,
                minTime: '07:00:00',
                maxTime: '22:00:00',
                slotDuration: '00:15:00',
                slotLabelInterval : '00:30:00',
                /* eventMouseover : function(data, event, view) { var content = '<p>'+data.description +'<p>'+ '<h3>'+data.title+'</h3>' + '<p><b>Start:</b> '+data.start+'<br />' + (data.end && '<p><b>End:</b> '+data.end+'</p>' || ''); tooltip.set({ 'content.text': content }) .reposition(event).show(event); }, */

                

                select: function (arg) {

                    $("#finicio").val(getFecha(arg.start));
                    $("#hinicio").val(getHora(arg.start));
                    $("#hfin").val(getHora(arg.end));
                   
                    //var title = prompt('Event Title:');

                    if (title) {
                        calendar.addEvent({
                            resourceId: arg.resource.id,
                            title: htmlEscape(title),
                            description: description,
                            start: arg.start,
                            end: arg.end,
                            allDay: arg.allDay
                        })
                    }
                    calendar.unselect()
                },
                timeZone: 'local',
                editable: true,
                initialView: 'resourceTimeGridDay',
                resources: (columnas),
                /* eventRender: function(event, element) { element.find('.fc-title').append("<br/>" + event.description); },
                eventDidMount: function(arg) {
                    $(arg.el).popup({
                        observeChanges: false,
                        html: '<div class="ui top label grey attached"><strong>Pop Up</strong></div><br>'+arg.event.extendedProps.description
                    });
                }, */

                events: arrayEvent
            });
            console.log(arrayEvent);
            calendar.render();

            setInterval(function() {
                
                $.ajax({
                    async: false,
                    type: "GET",
                    //dataType: 'JSON',
                    url: "calendar/sync_appt.php",
                   data: "action=scheduler",
                   success: function (data) {
                    console.log(data);

                    arrayEvent = genEvent(calendar.getDate(), idClinica);
                    var eventSources = calendar.getEvents();
                    $.each(eventSources, function (index, value) {
                        eventSources[index].remove();
                    });

                    $.each(arrayEvent, function (index, value) {
                        calendar.addEvent(value);
                    });
                    calendar.refetchEvents();
                    }
                })
            }, 5000)
        }

    });

    function htmlEscape(s) {
        return (s + '').replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/'/g, '&#039;')
            .replace(/"/g, '&quot;')
            .replace(/\n/g, '<br />')
        }
    /*
    events: [{"resourceId":"a","title":"event 1","start":"2020-08-20","end":"2020-08-22"},{"resourceId":"b","title":"event 3","start":"2020-08-21T12:00:00+00:00","end":"2020-08-22T06:00:00+00:00"},{"resourceId":"c","title":"event 4","start":"2020-08-21T07:30:00+00:00","end":"2020-08-21T09:30:00+00:00"},{"resourceId":"d","title":"event 5","start":"2020-08-21T10:00:00+00:00","end":"2020-08-21T15:00:00+00:00"},{"resourceId":"a","title":"event 2","start":"2020-08-21T09:00:00+00:00","end":"2020-08-21T14:00:00+00:00"},{
  "resourceId":"a",
  start: '2020-08-21',
  end: '2020-08-21',
  overlap: false,
  display: 'background',
  color: '#ff9f89'
}]
    */
</script>
