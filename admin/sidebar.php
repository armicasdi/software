<?php 
    $user = $_SESSION['user'];
    $id_user = $_SESSION['id'];
    $user_role = queryOne('SELECT usuario_rol FROM usuario_perfil WHERE id_usuario = '.$id_user.';');
?>
<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                    data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Home</li>
                <li>
                    <a href="index.php" class="mm-active">
                        <i class="metismenu-icon pe-7s-home"></i>
                        Home
                    </a>
                </li>
                <li class="app-sidebar__heading">Appointments and Call Tracker</li>
                <li>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-clock"></i>
                        Appointments
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="appointments.php">
                                <i class="metismenu-icon">
                                </i>Appointments
                            </a>
                        </li>
                        <li>
                            <a href="smsconfirmations.php">
                                <i class="metismenu-icon">
                                </i>SMS Confirmations
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-date"></i>
                        Calendar
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="calendar.php">
                                <i class="metismenu-icon">
                                </i>Calendar
                            </a>
                        </li>
                        <li>
                            <a href="businesshours.php">
                                <i class="metismenu-icon">
                                </i>Business Hours
                            </a>
                        </li>
                        <li>
                            <a href="scheduler.php">
                                <i class="metismenu-icon">
                                </i>Scheduler
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-call"></i>
                        Call Tracker
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <!-- <ul>
                        <li>
                            <a href="calls.php">
                                <i class="metismenu-icon">
                                </i>Calls
                            </a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="calltracker.php">
                                <i class="metismenu-icon">
                                </i>Call Tracker
                            </a>
                        </li>
                    </ul> -->
                    <ul>
                       <!--  <li>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#newCallEntry">
                                <i class="metismenu-icon">
                                </i>New Call Tracker Entry
                            </a>
                        </li> -->
                        <li>
                            <a href="addCallTrackerEntry.php">
                                <i class="metismenu-icon">
                                </i>New Call Tracker Entry
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- <li class="app-sidebar__heading">Chat</li>
                <li>
                    <a href="chat.php">
                        <i class="metismenu-icon pe-7s-chat"></i>
                        RingByName Chat
                    </a>
                </li> -->

                <li class="app-sidebar__heading">Job Number</li>
                <li>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-pin"></i>
                        Job Number
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="NewJobNumber.php">
                                <i class="metismenu-icon"></i>
                                New Job Number
                            </a>
                        </li>
                        <li>
                            <a href="jobnumbers.php">
                                <i class="metismenu-icon">
                                </i>Registered Job Numbers
                            </a>
                        </li>
                    </ul>
                </li>
                <?php 
                    $lock_test_jobs = queryOne('SELECT ul_state FROM usuario_locks WHERE id_rol = '.$user_role['usuario_rol'].' AND ul_account = "admin" AND ul_rol_menu = "Test Jobs";');
                    if($lock_test_jobs['ul_state'] == 0){ $style = 'style="display: none;"';}
                    else if($lock_test_jobs['ul_state'] == 1){ $style = 'style="display: block;"';}
                ?>
                <li <?php echo $style;?>>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-edit"></i>
                        Test Jobs
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="testJobNumber.php">
                                <i class="metismenu-icon"></i>
                                New Test Job
                            </a>
                        </li>
                        <li>
                            <a href="testjobs.php">
                                <i class="metismenu-icon"></i>
                                Registered Test Jobs
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="app-sidebar__heading">Patients</li>
                <li>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-users"></i>
                        Patients
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <!-- <li>
                            <a href="addPatient.php">
                                <i class="metismenu-icon"></i>
                                New Patient
                            </a>
                        </li> -->
                        <li>
                            <a href="patients.php">
                                <i class="metismenu-icon">
                                </i>Registered Patients
                            </a>
                        </li>
                    </ul>
                </li>

                <?php 
                    $lock_test_jobs = queryOne('SELECT ul_state FROM usuario_locks WHERE id_rol = '.$user_role['usuario_rol'].' AND ul_account = "admin" AND ul_rol_menu = "Walkout";');
                    if($lock_test_jobs['ul_state'] == 0){ $style = 'style="display: none;"';}
                    else if($lock_test_jobs['ul_state'] == 1){ $style = 'style="display: block;"';}
                ?>
                
                <li class="app-sidebar__heading" <?php echo $style;?>>Walkout</li>
                <li <?php echo $style;?>>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-cash"></i>
                        Walkout
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="walkout2.php">
                                <i class="metismenu-icon"></i>
                                Walkout Patient
                            </a>
                        </li>
                        <li>
                            <a href="transactions.php">
                                <i class="metismenu-icon">
                                </i>Transactions
                            </a>
                        </li>
                    </ul>
                </li>

                <?php 
                    $lock_test_jobs = queryOne('SELECT ul_state FROM usuario_locks WHERE id_rol = '.$user_role['usuario_rol'].' AND ul_account = "admin" AND ul_rol_menu = "Reports";');
                    if($lock_test_jobs['ul_state'] == 0){ $style2 = 'style="display: none;"';}
                    else if($lock_test_jobs['ul_state'] == 1){ $style2 = 'style="display: block;"';}
                ?>
                
                <li class="app-sidebar__heading" <?php echo $style2;?>>Reports</li>
                <li <?php echo $style2;?>>
                    <a href="reportApps.php">
                        <i class="metismenu-icon pe-7s-date"></i>
                        Appointments Report
                        <!-- <span class="spinner-grow spinner-grow-sm mr-2 text-danger"></span> -->
                    </a>
                </li>
                <li <?php echo $style2;?>>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-graph"></i>
                        Financial Report
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="reportAccountReceivable.php">
                                <i class="metismenu-icon"></i>
                                Account Receivable
                                <span class="spinner-grow spinner-grow-sm mr-2 text-primary"></span>
                            </a>
                        </li>
                        <li>
                            <a href="reportInsurance.php">
                                <i class="metismenu-icon"></i>
                                Insurance Report
                                <span class="spinner-grow spinner-grow-sm mr-2 text-warning"></span>
                            </a>
                        </li>
                        <li>
                            <a href="reportPC.php">
                                <i class="metismenu-icon"></i>
                                Productions vs Collections
                            </a>
                        </li>
                    </ul>
                </li>
                <li <?php echo $style2;?>>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-graph3"></i>
                        Doctors Report
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="reportDoctor2.php">
                                <i class="metismenu-icon"></i>
                                Doctors Report
                            </a>
                        </li>
                        <!-- <li>
                            <a href="reportDoctor.php">
                                <i class="metismenu-icon"></i>
                                Doctors Report v2
                                <span class="spinner-grow spinner-grow-sm mr-2 text-primary"></span>
                            </a>
                        </li> -->
                    </ul>
                </li>

                <?php 
                    $lock_test_jobs = queryOne('SELECT ul_state FROM usuario_locks WHERE id_rol = '.$user_role['usuario_rol'].' AND ul_account = "admin" AND ul_rol_menu = "Users";');
                    if($lock_test_jobs['ul_state'] == 0){ $style3 = 'style="display: none;"';}
                    else if($lock_test_jobs['ul_state'] == 1){ $style3 = 'style="display: block;"';}
                ?>
                
                <li class="app-sidebar__heading" <?php echo $style3;?>>Users</li>
                <li <?php echo $style3;?>>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-user"></i>
                        Users
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="addUser.php">
                                <i class="metismenu-icon"></i>
                                New User
                            </a>
                        </li>
                        <li>
                            <a href="users.php">
                                <i class="metismenu-icon"></i>
                                Regitered Users
                            </a>
                        </li>
                    </ul>
                </li>

                <?php 
                    $lock_test_jobs = queryOne('SELECT ul_state FROM usuario_locks WHERE id_rol = '.$user_role['usuario_rol'].' AND ul_account = "admin" AND ul_rol_menu = "Settings";');
                    if($lock_test_jobs['ul_state'] == 0){ $style4 = 'style="display: none;"';}
                    else if($lock_test_jobs['ul_state'] == 1){ $style4 = 'style="display: block;"';}
                ?>
                
                <!-- <li class="app-sidebar__heading" <?php echo $style2;?>>Settings</li>
                <li <?php echo $style4;?>>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-back-2"></i>
                        BackUp
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Create Patients BakcUp
                            </a>
                        </li>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Create Job Numbers BackUp
                            </a>
                        </li>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Create Sessions BackUp
                            </a>
                        </li>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Create Payments BackUp
                            </a>
                        </li>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Create Users BackUp
                            </a>
                        </li>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Database BackUp
                            </a>
                        </li>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Restore Database
                            </a>
                        </li>
                    </ul>
                </li>
                <li <?php echo $style4;?>>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-angle-up-circle"></i>
                        Manage
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="costs.php">
                                <i class="metismenu-icon"></i>
                                Costs
                            </a>
                        </li>
                        <li>
                            <a href="percentages.php">
                                <i class="metismenu-icon"></i>
                                Percentages
                            </a>
                        </li>
                        <li>
                            <a href="procedures.php">
                                <i class="metismenu-icon"></i>
                                Procedures
                            </a>
                        </li>
                        <li>
                            <a href="manageReferal.php">
                                <i class="metismenu-icon">
                                </i>Referral
                            </a>
                        </li>
                        <li>
                            <a href="treatments.php">
                                <i class="metismenu-icon"></i>
                                Treatments
                            </a>
                        </li>
                        <li>
                            <a href="states.php">
                                <i class="metismenu-icon"></i>
                                States
                            </a>
                        </li>
                    </ul>
                </li>
                <li <?php echo $style4;?>>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-config"></i>
                        Settings
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Company Information
                            </a>
                        </li>
                    </ul>
                </li>
                <li <?php echo $style4;?>>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-culture"></i>
                        Clinics
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="addClinic.php">
                                <i class="metismenu-icon"></i>
                                New Clinic
                            </a>
                        </li>
                        <li>
                            <a href="under/NotYet.php">
                                <i class="metismenu-icon"></i>
                                Registered Clinics
                            </a>
                        </li>
                    </ul>
                </li> -->
                
                <li class="app-sidebar__heading">Utilities</li>
                <li>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-help1 icon-gradient bg-plum-plate"></i>
                        FAQ
                    </a>
                    <a href="under/NotYet.php">
                        <i class="metismenu-icon pe-7s-info"></i>
                        About
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>