<?php 
    include 'include/funciones.php';

  /*   
    if(!isset($_SESSION['login_user'])) 
    { 
       echo "No hay sesión";

    }else{
        echo "Sí hay sesión";
        echo $_SESSION['login_user'];
    } */

?>

<!doctype html>
<html lang="en">

<head>
    <?php include 'head.php'; ?>

    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" />

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include 'navbar.php';
        ?>

        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-user icon-gradient bg-happy-itmeo">
                                    </i>
                                </div>
                                <div>Users
                                    <div class="page-title-subheading">This is a List of Registered Users</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                                                    $result = $conexion->query('SELECT * FROM '. $tabla_usuarios_perfil .' WHERE id_usuario = '.$_SESSION['login_user'].' ORDER BY id_usuario');
                                                    while($user=$result->fetch_assoc()){
                                                        $user_codigo=$user['id_usuario'];
                                                        //$user_nombre=$user['user_nombre'] . " " . $user['user_apellido'];
                                                        $user_nombre=$user['usuario_nombre'];
                                                        $user_genero=$user['usuario_genero'];
                                                        $user_email=$user['usuario_email'];
                                                        $user_contacto=$user['usuario_contacto']; 
                                                        $user_acerca_de=$user['usuario_sobre_mi'];
                                                        $user_foto=$user['usuario_foto'];
                                                        $user_pais=$user['usuario_pais'];
                                                        $user_cargo=$user['id_cargo']; 
                                                        $user_clinica=$user['id_clinica']; 
                                                        $reg_user_log = queryOne('SELECT * FROM '. $tabla_usuarios . ' WHERE id_usuario = '. $user_codigo . '');
                                                        $reg_user_clinica = queryOne('SELECT * FROM '. $tabla_clinicas . ' WHERE id_clinica = '. $user_clinica . '');
                                                ?>


                        <div class="row">

                            <div class="col-md-4">                                
                                <div class="card card-user">
                                                
                                    <div class="image">
                                    </div>
                                    <div class="card-body">                                                
                                        <div class="author">
                                            <a href="#">
                                                <div class="text-center">
                                                    <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png"
                                                        class="avatar img-circle img-thumbnail" alt="avatar">
                                                    <h6>Upload a different photo...</h6>
                                                    <input type="file" class="text-center center-block file-upload">
                                                </div>
                                                </hr><br>
                                                <h5 class="title">Chet Faker</h5>
                                            </a>
                                            <p class="description">
                                                @chetfaker
                                            </p>
                                        </div>
                                        <p class="description text-center">
                                            "I like the way you work it <br>
                                            No diggity <br>
                                            I wanna bag it up"
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        <hr>
                                        <div class="button-container">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 col-6 ml-auto text-center">
                                                    <button class="btn btn-sm btn-outline-success btn-round btn-icon">
                                                        <i class="fa fa-whatsapp"></i></button>
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-6 ml-auto mr-auto text-center">
                                                <button class="btn btn-sm btn-outline-dark btn-round btn-icon">
                                                                <i class="fa fa-envelope"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card card-user">
                                    <div class="card-header">
                                        <h5 class="card-title">Edit Profile</h5>
                                    </div>
                                    <div class="card-body">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-5 pr-1">
                                                    <div class="form-group">
                                                        <label>Company (disabled)</label>
                                                        <input type="text" class="form-control" disabled=""
                                                            placeholder="Company" value="Top Dental">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 px-1">
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <input type="text" class="form-control" placeholder="Username"
                                                            value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pl-1">
                                                    <div class="form-group">
                                                        <label for="exampleInputPhone">Contact</label>
                                                        <input type="text" class="form-control" placeholder="Phone Number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 pr-1">
                                                    <div class="form-group">
                                                        <label>First Name</label>
                                                        <input type="text" class="form-control" placeholder="Name"
                                                            value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 pl-1">
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <input type="text" class="form-control" placeholder="Last Name"
                                                            value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="email" class="form-control"
                                                            placeholder="Email" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 pr-1">
                                                    <div class="form-group">
                                                        <label>Genre</label>
                                                        <select class="form-control" id="profile-genre">
                                                            <option value="female">Female</option>
                                                            <option value="male">Male</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 px-1">
                                                    <div class="form-group">
                                                        <label>Country</label>
                                                        <input type="text" class="form-control" placeholder="Country" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pl-1">
                                                    <div class="form-group">
                                                        <label>Manage</label>
                                                        <input type="number" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>About Me</label>
                                                        <textarea class="form-control textarea"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="update ml-auto mr-auto">
                                                    <button type="submit" class="btn btn-primary btn-round">Update
                                                        Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                    <?php } ?>
                </div>

                <?php include 'footer.php'; ?>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
</body>

</html>
