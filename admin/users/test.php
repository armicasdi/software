<?php

    $dateTimeUTC = "";

    $dateTime = '2021-09-02 01:30:00'; 
    $tz_from = 'America/New_York'; 
    $newDateTime = new DateTime($dateTime, new DateTimeZone($tz_from)); 
    $newDateTime->setTimezone(new DateTimeZone("UTC")); 
    $dateTimeUTC = $newDateTime->format("Y-m-d\TH:i:s\Z");

    $post = [
        'id' => 7000003429823,
        'start' => $dateTimeUTC,
        /* 'end' => '2021-09-02T02:30:00-06:00', */
        'action' => 'updateAppointment'
        ];

        /* $body = json_encode($post);

        print_r($body); */

    $token = [];
    $client_id = "kdqoHWRZ1YB5M99QVrtc9p4v2kxSct89";
    $client_secret = "IqHYGkvrsN7eFfRe";
    $OrganizationID = "5e7b7774c9e1470c0d716320";
    $locations = [
        "manassas" => "7000000000114",
        "fairfax" => "7000000000115",
        "woodbridge" => "",
    ];

    function getToken($client_id, $client_secret)
    {
        $curl = curl_init('https://prod.hs1api.com/oauth/client_credential/accesstoken?grant_type=client_credentials');
        curl_setopt($curl, CURLOPT_POSTFIELDS, "client_id=" . $client_id . "&client_secret=" . $client_secret );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));

        $output = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($output);

        return $result;

    }

    $resultAuthorization = getToken($client_id, $client_secret);
    $token = $resultAuthorization->access_token;

    function getDataById($ep, $id) { 
        global $OrganizationID; 
        global $token; 
 
        $curl = curl_init('https://prod.hs1api.com/ascend-gateway/api/v1/'.$ep."/".$id); 
 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( 
            'Authorization: Bearer '. $token, 
            'Organization-ID: '. $OrganizationID, 
        )); 
 
        $output = curl_exec($curl); 
 
        curl_close($curl); 
 
        return $output; 
    }

    function updateAppointment($data, $id){
        global $OrganizationID;
        global $token;

        $curl = curl_init('https://prod.hs1api.com/ascend-gateway/api/v1/appointments/'. $id);
        $body = json_encode($data);

        print_r($body);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Authorization: Bearer '. $token,
        'Organization-ID: '. $OrganizationID,
        ));

        $output = curl_exec($curl);

        curl_close($curl);


        return $output;
    }
    if (isset($post['action']) && $post['action'] == 'updateAppointment') {
        global $token;
        global $clinica;
        echo "update";

        $appointmentId = $post['id'];
        $data = [];

        $currentAppoinment = getDataById("appointments", $appointmentId);
        $currentAppoinment = json_decode($currentAppoinment);

        $data['start'] = $post['start'];
        $data['provider'] = $currentAppoinment->data->provider;
        $data['operatory'] = $currentAppoinment->data->operatory;
        $data['patient'] = $currentAppoinment->data->patient;
        $data['status'] = $currentAppoinment->data->status;

        $result = updateAppointment($post, $appointmentId);
        print_r($result);
    } 

?>