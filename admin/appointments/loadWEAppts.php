<?php
    require("../include/database.php");
    include("../include/funciones.php");

    $countDes = 0;
    $json = array();
    $result = $conexion->query('SELECT * FROM '. $tabla_citas .' WHERE id_clinica = 3 AND YEAR(cita_fecha) = YEAR(CURRENT_DATE()) 
    AND MONTH(cita_fecha) >= MONTH(CURRENT_DATE()) ORDER BY id_cita DESC');
    while($app=$result->fetch_assoc()){
        $app_codigo=$app['id_cita'];
        $app_paciente=$app['id_paciente'];
        $app_clinica=$app['id_clinica']; 
        $app_reason=$app['id_reason']; 
        $app_date=$app['cita_fecha']; 
        $app_time=$app['cita_hora']; 
        $reg_calendario = queryOne('SELECT * FROM '. $tabla_calendario . ' WHERE id_cita = '. $app_codigo . ';');
        $reg_estado_cita = queryOne('SELECT * FROM '. $tabla_citas_estado . ' WHERE id_cita = "'. $app_codigo . '"');
        $reg_paciente_cita = queryOne('SELECT * FROM '. $tabla_pacientes . ' WHERE id_paciente = "'. $app_paciente . '"');
        $reg_clinica_cita = queryOne('SELECT * FROM '. $tabla_clinicas . ' WHERE id_clinica = "'. $app_clinica . '"');
        $paciente = ucwords($reg_paciente_cita['paciente_nombres'] ." ". $reg_paciente_cita['paciente_apellidos']);
        $color_cita = $reg_estado_cita['estado_color'];
        if($color_cita == "#3ac47d"){
            $estado_cita = "success";
        }
        else if($color_cita == "#16aaff"){
            $estado_cita = "info";
        }
        else if($color_cita == "#f7b924"){
            $estado_cita = "warning";
        }
        else if($color_cita == "#dcdcdc"){
            $estado_cita = "light";
        }
        else if($color_cita == "#343a40"){
            $estado_cita = "dark";
        }
        else if($color_cita == "#f775f0"){
            $estado_cita = "danger";
        }
        else{
            $estado_cita = "info";
        }

        if(!empty($reg_paciente_cita['paciente_nombres']) && !empty($reg_paciente_cita['paciente_apellidos'])){
            $paciente = ucwords($reg_paciente_cita['paciente_nombres'] ." ". $reg_paciente_cita['paciente_apellidos']);
        }

        $reason = "";
        if(empty($app_reason)){
            $rea = queryOne('SELECT type_description FROM '.$tabla_appt_types.' WHERE type_id = '.$reg_calendario['id_app_type'].';');
            if(empty($rea["type_description"])){
                $reason = "";
            }else{
                if($rea["type_description"] == "Restorative"){$app_color ="#007FFF";}
                else if($rea["type_description"] == "Implants"){$app_color ="#FA827F";}
                else if($rea["type_description"] == "6 months follow up"){$app_color ="#7F033D";}
                else if($rea["type_description"] == "New patient"){$app_color ="#FF7F00";}
                else if($rea["type_description"] == "Emergency patient"){$app_color ="#FD0100";}
                else if($rea["type_description"] == "Orthodontics"){$app_color ="#FD81FD";}
                else if($rea["type_description"] == "Perio"){$app_color ="#80FFFC";}
                else if($rea["type_description"] == "Root canal"){$app_color ="#5300A6";}
                else if($rea["type_description"] == "Crown"){$app_color ="#FFFE04";}
                else if($rea["type_description"] == "Bridge"){$app_color ="#03FE7F";}
                else if($rea["type_description"] == "Extraction"){$app_color ="#336402";}
                else if($rea["type_description"] == "Bleaching"){$app_color ="#FFFFFF";}
                else if($rea["type_description"] == "Removable Appliance"){$app_color ="#BB5C00";}
                else if($rea["type_description"] == "Follow up"){$app_color ="#81813F";}
                else if($rea["type_description"] == "Limited Exam"){$app_color ="#43817D";}
                else if($rea["type_description"] == "New patient no cleaning"){$app_color ="#C4C8FE";}
                else if($rea["type_description"] == "DO NOT BOOK"){$app_color ="#000000";}
                $reason = $rea["type_description"];
            }
        }else{
            if($app_reason == "Fillings"){$app_color ="#007FFF";}
            else if($app_reason == "Implant"){$app_color ="#FA827F";}
            else if($app_reason == "Continue Treatment"){$app_color ="#7F033D";}
            else if($app_reason == "New Patient"){$app_color ="#FF7F00";}
            else if($app_reason == "Emergency"){$app_color ="#FD0100";}
            else if($app_reason == "Ortho"){$app_color ="#FD81FD";}
            else if($app_reason == "Deep Cleaning"){$app_color ="#80FFFC";}
            else if($app_reason == "Root canal"){$app_color ="#5300A6";}
            else if($app_reason == "Crown"){$app_color ="#FFFE04";}
            else if($app_reason == "Bridge"){$app_color ="#03FE7F";}
            else if($app_reason == "Extraction"){$app_color ="#336402";}
            else if($app_reason == "Bleaching"){$app_color ="#FFFFFF";}
            else if($app_reason == "Removable Appliance"){$app_color ="#BB5C00";}
            else if($app_reason == "Follow up"){$app_color ="#81813F";}
            else if($app_reason == "Consultation $45"){$app_color ="#43817D";}
            else if($app_reason == "Consultation + Radio $59"){$app_color ="#C4C8FE";}
            else if($app_reason == "DO NOT BOOK"){$app_color ="#000000";}
            else if($app_reason == "Pedo"){$app_color ="#FF0084";}
            $reason = $app_reason;
        }

        $actualhour = date("h:i A", strtotime($app_time));
        $actualdate = date("F j, Y", strtotime($app_date));
        $actualdate_show = date("m-d-Y", strtotime($app_date));

        if(strpos($reg_paciente_cita['id_paciente_eagle'], 'M') !== false){
            $id_paciente_eagle = str_replace("M", "", $reg_paciente_cita['id_paciente_eagle']);
        }else{
            $id_paciente_eagle = $reg_paciente_cita['id_paciente_eagle'];
        }

        $patient = '<div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left flex2">
                                <div class="widget-heading">
                                    '.$paciente.'
                                    <div class="widget-subheading opacity-7">
                                        Eagle: '.$id_paciente_eagle.'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
        
        $rea = '<svg width="25" height="15" viewBox="0 0 250 150">
                    <rect x="70" y="25" height="100" width="120"
                        style="stroke:#000; fill: '.$app_color.'">
                    </rect>
                </svg> '.$reason.'';
        
        $state = '<div class="badge badge-'.$estado_cita.'">
                '.$reg_estado_cita["estado_cita"].'</div>';

        $action = '<button type="button" id="detailsF" data-toggle="modal"
                    data-target=".modalDetails"
                    data-idapp="'.$app_codigo.'"
                    class="details btn btn-focus btn-sm">Details</button>
            <button type="button" id="editF" data-toggle="modal"
                data-target=".update-app"
                data-id="'.$app_codigo.'"
                data-chart="'.$app_paciente.'"
                data-paciente="'.$paciente.'"
                data-fecha="'.$app_date.'"
                data-reason="'.$reason.'"
                data-actualhour="'.$actualhour.'"
                data-actualdate="'.$actualdate.'"
                data-contacto="'.formatTelefono($reg_paciente_cita["paciente_contacto"]).'"
                data-clinic=3
                class="edit btn btn-alternate btn-sm">Edit</button>';

        $json[] =array(
            'appt' => $app_codigo,
            'chart' => $app_paciente,
            'patient' => $patient,
            'contact' => formatTelefono($reg_paciente_cita['paciente_contacto']),
            'reason' => $rea,
            'date' => $actualdate_show,
            'time' => $app_time,
            'state' => $state,
            'action' => $action,
        );
    }

    $jsonstring = json_encode($json);
    echo $jsonstring;
