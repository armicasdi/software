<?php 
    include 'include/funciones.php';
?>

<!doctype html>
<html lang="en">

<head>
    <title>PPS | Users</title>
    <?php include 'head.php'; ?>

    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" />

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include 'navbar.php';
        ?>

        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-user icon-gradient bg-happy-itmeo">
                                    </i>
                                </div>
                                <div>Users
                                    <div class="page-title-subheading">This is a List of Registered Users</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="main-card mb-3 card">
                                <div class="card-header">List of Registered Users </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table
                                            class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                            id="example">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">User</th>
                                                    <th class="text-center">Email</th>
                                                    <th class="text-center">Country</th>
                                                    <th class="text-center">Acc. Type</th>
                                                    <th class="text-center">Rol</th>
                                                    <th class="text-center">State</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                    function initials($str) {
                                                        $ret = '';
                                                        foreach (explode(' ', $str) as $Word)
                                                            $ret .= strtoupper($Word[0]);
                                                        return $ret;
                                                    }
                                                    $bg=""; $badge_color=""; $estado_name="";
                                                    $result = $conexion->query('SELECT * FROM '. $tabla_usuarios_perfil .' ORDER BY id_usuario');
                                                    while($user=$result->fetch_assoc()){
                                                        $user_codigo=$user['id_usuario'];
                                                        $user_nombre=$user['usuario_nombre'];
                                                        $user_email=$user['usuario_email'];
                                                        $user_contacto=$user['usuario_contacto'];
                                                        $user_foto=$user['usuario_foto'];
                                                        $user_pais=$user['usuario_pais'];
                                                        $user_rol=$user['usuario_rol'];
                                                        $user_estado=$user['usuario_estado'];
                                                        $reg_user_log = queryOne('SELECT * FROM '. $tabla_usuarios . ' WHERE id_usuario = '. $user_codigo . '');
                                                        $reg_user_rol = queryOne('SELECT role_name FROM usuario_rol WHERE id = '. $user_rol . ';');

                                                        if($user_rol == 1){ $bg = "bg-strong-bliss";
                                                        }else if($user_rol == 2){ $bg = "bg-night-fade";
                                                        }else if($user_rol == 3){ $bg = "bg-tempting-azure";
                                                        }else if($user_rol == 4){ $bg = "bg-malibu-beach";
                                                        }else if($user_rol == 5){ $bg = "bg-happy-green";
                                                        }else if($user_rol == 6){ $bg = "bg-royal";
                                                        }

                                                        if($user_estado == 0){ $badge_color="secondary"; $estado_name = "Inactive";}
                                                        else if($user_estado == 1){ $badge_color="primary"; $estado_name = "Active";}

                                                ?>
                                                <tr>
                                                    <td class="text-left">
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left mr-3 text-center">
                                                                    <div class="widget-content-left">
                                                                        <div class="swatch-holder swatch-holder-lg <?php echo $bg;?>" style="opacity: 1;">
                                                                            <b class="text-white text-center"><?php echo initials($user_nombre);?></b>
                                                                        </div>
                                                                       <!--  <img width="40" class="rounded-circle" src="assets/images/avatars/4.jpg" alt=""> -->
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-left flex2">
                                                                    <div class="widget-heading"><?php echo $user_nombre;?></div>
                                                                    <div class="widget-subheading opacity-7"><?php echo $reg_user_rol['role_name'];?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center text-muted" scope="row">
                                                        <?php echo $reg_user_log['usuario_name'];?></td>
                                                    <td class="text-center"><?php echo $user_email;?></td>
                                                    <td class="text-center"><?php echo $user_pais;?></td>
                                                    <td class="text-center text-muted"><?php echo strtoupper($reg_user_log['tipo_usuario']);?></td>
                                                    <td class="text-center"><?php echo $reg_user_rol['role_name'];?></td>
                                                    <td class="text-center"><div class="mb-2 mr-2 badge badge-<?php echo $badge_color;?>"><?php echo $estado_name;?></div></td>
                                                    <td class="text-center">
                                                        <button type="button" id="" data-toggle="modal"
                                                            data-target=".user-details"
                                                            class="btn btn-warning btn-sm"><i class="fa fa-pen text-white"></i></button>
                                                        <button type="button" id="" data-toggle="modal"
                                                            data-target=".user-details"
                                                            class="btn btn-alternate btn-sm"><i class="fa fa-key"></i></button>
                                                        <button type="button" id="" data-toggle="modal"
                                                            data-target="#followup"
                                                            class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <?php include 'footer.php'; ?>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
</body>

</html>
