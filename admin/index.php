<?php
include 'include/funciones.php';
?>
<!doctype html>
<html lang="en">

<head>
    <title>PPS | Home</title>
    <?php include 'head.php'; ?>

    <style>
        table.dataTable thead {
            background: linear-gradient(to right, #4A00E0, #8E2DE2);
            color: white;
        }
    </style>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php
        include 'navbar.php';
        ?>
        <div class="ui-theme-settings">
            <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
                <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
            </button>
        </div>
        <div class="app-main">
            <?php
            include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-home icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div>Home
                                    <div class="page-title-subheading">Activities Summary</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-midnight-bloom">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Job Numbers</div>
                                        <div class="widget-subheading">Last Month</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-white"><span>246</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-arielle-smile">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Sales</div>
                                        <div class="widget-subheading">Last Month</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-white"><span>$ 25,678.78</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-grow-early">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Patients</div>
                                        <div class="widget-subheading">New Patients</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-white"><span>+46%</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Job Numbers</div>
                                            <div class="widget-subheading">All</div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-success">1456</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Registered Patients</div>
                                            <div class="widget-subheading">All time</div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-warning">534</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Users</div>
                                            <div class="widget-subheading">All</div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-danger">14</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <div class="mb-3 card">
                                <div class="card-header-tab card-header-tab-animation card-header">
                                    <div class="card-header-title">
                                        <i class="header-icon fa fa-chart-line icon-gradient bg-love-kiss"> </i>
                                        Reporte Mensual
                                    </div>
                                    <ul class="nav">
                                        <li class="nav-item"><a href="javascript:void(0);"
                                                class="active nav-link">Pasados</a></li>
                                        <li class="nav-item"><a href="javascript:void(0);"
                                                class="nav-link second-tab-toggle">Actual</a></li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="tabs-eg-77">
                                            <div class="card mb-3 widget-chart widget-chart2 text-left w-100">
                                                <div class="widget-chat-wrapper-outer">
                                                    <div
                                                        class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                                        <canvas id="canvas"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                            <h6 class="text-muted text-uppercase font-size-md opacity-5 font-weight-normal">
                                                Top Usuarios</h6>
                                            <div class="scroll-area-lg">
                                                <div class="scrollbar-container">
                                                    <ul
                                                        class="rm-list-borders rm-list-borders-scroll list-group list-group-flush">
                                                        <li class="list-group-item">
                                                            <div class="widget-content p-0">
                                                                <div class="widget-content-wrapper">
                                                                    <div class="widget-content-left mr-3">
                                                                        <img width="42" class="rounded-circle"
                                                                            src="assets/images/avatars/user.png" alt="">
                                                                    </div>
                                                                    <div class="widget-content-left">
                                                                        <div class="widget-heading">Dr. Firstname
                                                                        </div>
                                                                        <div class="widget-subheading">Manassas
                                                                        </div>
                                                                    </div>
                                                                    <div class="widget-content-right">
                                                                        <div class="font-size-xlg text-muted">
                                                                            <small class="opacity-5 pr-1">$</small>
                                                                            <span>1.2K</span>
                                                                            <small class="text-danger pl-2">
                                                                                <i class="fa fa-angle-down"></i>
                                                                            </small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="widget-content p-0">
                                                                <div class="widget-content-wrapper">
                                                                    <div class="widget-content-left mr-3">
                                                                        <img width="42" class="rounded-circle"
                                                                            src="assets/images/avatars/user.png" alt="">
                                                                    </div>
                                                                    <div class="widget-content-left">
                                                                        <div class="widget-heading">Dra. Firstname</div>
                                                                        <div class="widget-subheading">Fairfax</div>
                                                                    </div>
                                                                    <div class="widget-content-right">
                                                                        <div class="font-size-xlg text-muted">
                                                                            <small class="opacity-5 pr-1">$</small>
                                                                            <span>269</span>
                                                                            <small class="text-success pl-2">
                                                                                <i class="fa fa-angle-up"></i>
                                                                            </small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="widget-content p-0">
                                                                <div class="widget-content-wrapper">
                                                                    <div class="widget-content-left mr-3">
                                                                        <img width="42" class="rounded-circle"
                                                                            src="assets/images/avatars/user.png" alt="">
                                                                    </div>
                                                                    <div class="widget-content-left">
                                                                        <div class="widget-heading">Dr. Firstname
                                                                        </div>
                                                                        <div class="widget-subheading">Fairfax
                                                                        </div>
                                                                    </div>
                                                                    <div class="widget-content-right">
                                                                        <div class="font-size-xlg text-muted">
                                                                            <small class="opacity-5 pr-1">$</small>
                                                                            <span>674</span>
                                                                            <small class="text-warning pl-2">
                                                                                <i class="fa fa-dot-circle"></i>
                                                                            </small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="widget-content p-0">
                                                                <div class="widget-content-wrapper">
                                                                    <div class="widget-content-left mr-3">
                                                                        <img width="42" class="rounded-circle"
                                                                            src="assets/images/avatars/user.png" alt="">
                                                                    </div>
                                                                    <div class="widget-content-left">
                                                                        <div class="widget-heading">Dr. Firstname
                                                                        </div>
                                                                        <div class="widget-subheading">Manassas
                                                                        </div>
                                                                    </div>
                                                                    <div class="widget-content-right">
                                                                        <div class="font-size-xlg text-muted">
                                                                            <small class="opacity-5 pr-1">$</small>
                                                                            <span>129</span>
                                                                            <small class="text-danger pl-2">
                                                                                <i class="fa fa-angle-down"></i>
                                                                            </small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <div class="widget-content p-0">
                                                                <div class="widget-content-wrapper">
                                                                    <div class="widget-content-left mr-3">
                                                                        <img width="42" class="rounded-circle"
                                                                            src="assets/images/avatars/user.png" alt="">
                                                                    </div>
                                                                    <div class="widget-content-left">
                                                                        <div class="widget-heading">Dr. Firstname</div>
                                                                        <div class="widget-subheading">Manassas</div>
                                                                    </div>
                                                                    <div class="widget-content-right">
                                                                        <div class="font-size-xlg text-muted">
                                                                            <small class="opacity-5 pr-1">$</small>
                                                                            <span>543</span>
                                                                            <small class="text-success pl-2">
                                                                                <i class="fa fa-angle-up"></i>
                                                                            </small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="mb-3 card">
                                <div class="card-header-tab card-header">
                                    <div class="card-header-title">
                                        <i class="header-icon fa fa-caret-up icon-gradient bg-tempting-azure"> </i>
                                        Planes Generados
                                    </div>
                                    <div class="btn-actions-pane-right">
                                        <div class="nav">
                                            <a href="javascript:void(0);"
                                                class="ml-2 btn-pill btn-wide border-0 btn-transition  btn btn-outline-alternate second-tab-toggle-alt">Hoy</a>
                                            <a href="javascript:void(0);"
                                                class="border-0 btn-pill btn-wide btn-transition active btn btn-outline-alternate">Mes</a>
                                            <a href="javascript:void(0);"
                                                class="ml-1 btn-pill btn-wide border-0 btn-transition  btn btn-outline-alternate second-tab-toggle-alt">Año</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="tab-eg-55">
                                        <div class="widget-chart p-3">
                                            <div style="height: 350px">
                                                <canvas id="line-chart"></canvas>
                                            </div>
                                            <div class="widget-chart-content text-center mt-5">
                                                <div class="widget-description mt-0 text-success">
                                                    <i class="fa fa-arrow-right"></i>
                                                    <span class="pl-1">86.5%</span>
                                                    <span class="text-muted opacity-8 pl-1">Planes Generados Respecto al
                                                        Mes de <b>Octubre</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pt-2 card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="widget-content">
                                                        <div class="widget-content-outer">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left">
                                                                    <div class="widget-numbers fsize-3 text-muted">63%
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-right">
                                                                    <div class="text-muted opacity-6">Ganancias
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="widget-progress-wrapper mt-1">
                                                                <div
                                                                    class="progress-bar-sm progress-bar-animated-alt progress">
                                                                    <div class="progress-bar bg-danger"
                                                                        role="progressbar" aria-valuenow="63"
                                                                        aria-valuemin="0" aria-valuemax="100"
                                                                        style="width: 63%;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="widget-content">
                                                        <div class="widget-content-outer">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left">
                                                                    <div class="widget-numbers fsize-3 text-muted">32%
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-right">
                                                                    <div class="text-muted opacity-6">Pacientes
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="widget-progress-wrapper mt-1">
                                                                <div
                                                                    class="progress-bar-sm progress-bar-animated-alt progress">
                                                                    <div class="progress-bar bg-success"
                                                                        role="progressbar" aria-valuenow="32"
                                                                        aria-valuemin="0" aria-valuemax="100"
                                                                        style="width: 32%;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="widget-content">
                                                        <div class="widget-content-outer">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left">
                                                                    <div class="widget-numbers fsize-3 text-muted">71%
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-right">
                                                                    <div class="text-muted opacity-6">Planes Generados
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="widget-progress-wrapper mt-1">
                                                                <div
                                                                    class="progress-bar-sm progress-bar-animated-alt progress">
                                                                    <div class="progress-bar bg-primary"
                                                                        role="progressbar" aria-valuenow="71"
                                                                        aria-valuemin="0" aria-valuemax="100"
                                                                        style="width: 71%;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="widget-content">
                                                        <div class="widget-content-outer">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left">
                                                                    <div class="widget-numbers fsize-3 text-muted">41%
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-right">
                                                                    <div class="text-muted opacity-6">Procedimientos
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="widget-progress-wrapper mt-1">
                                                                <div
                                                                    class="progress-bar-sm progress-bar-animated-alt progress">
                                                                    <div class="progress-bar bg-warning"
                                                                        role="progressbar" aria-valuenow="41"
                                                                        aria-valuemin="0" aria-valuemax="100"
                                                                        style="width: 41%;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="row">
                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <div class="card-header">Procedimientos Actuales
                                    <div class="btn-actions-pane-right">
                                        <div role="group" class="btn-group-sm btn-group">
                                            <button class="btn btn-focus">Hoy</button>
                                            <button class="active btn btn-focus">&Uacute;ltima Semana</button>
                                            <button class="btn btn-focus">Todo el Mes</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th>Paciente</th>
                                                <th class="text-center">Cl&iacute;nica</th>
                                                <th class="text-center">Estado</th>
                                                <th class="text-center">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center text-muted">#345</td>
                                                <td>
                                                    <div class="widget-content p-0">
                                                        <div class="widget-content-wrapper">
                                                            <div class="widget-content-left mr-3">
                                                                <div class="widget-content-left">
                                                                    <img width="40" class="rounded-circle"
                                                                        src="assets/images/avatars/user.png" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="widget-content-left flex2">
                                                                <div class="widget-heading">Firstname Lastname</div>
                                                                <div class="widget-subheading opacity-7">
                                                                    Extracci&oacute;n
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">Manassas</td>
                                                <td class="text-center">
                                                    <div class="badge badge-alternate">Pendiente</div>
                                                </td>
                                                <td class="text-center">
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-primary btn-sm">Detalles</button>
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-warning btn-sm">Editar</button>
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-danger btn-sm">Eliminar</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center text-muted">#347</td>
                                                <td>
                                                    <div class="widget-content p-0">
                                                        <div class="widget-content-wrapper">
                                                            <div class="widget-content-left mr-3">
                                                                <div class="widget-content-left">
                                                                    <img width="40" class="rounded-circle"
                                                                        src="assets/images/avatars/user.png" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="widget-content-left flex2">
                                                                <div class="widget-heading">Firstname Lastname</div>
                                                                <div class="widget-subheading opacity-7">Limpieza</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">Fairfax</td>
                                                <td class="text-center">
                                                    <div class="badge badge-success">Completado</div>
                                                </td>
                                                <td class="text-center">
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-primary btn-sm">Detalles</button>
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-warning btn-sm">Editar</button>
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-danger btn-sm">Eliminar</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center text-muted">#321</td>
                                                <td>
                                                    <div class="widget-content p-0">
                                                        <div class="widget-content-wrapper">
                                                            <div class="widget-content-left mr-3">
                                                                <div class="widget-content-left">
                                                                    <img width="40" class="rounded-circle"
                                                                        src="assets/images/avatars/user.png" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="widget-content-left flex2">
                                                                <div class="widget-heading">Firstname Lastname</div>
                                                                <div class="widget-subheading opacity-7">Implante</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">Manassas</td>
                                                <td class="text-center">
                                                    <div class="badge badge-danger">En Progreso</div>
                                                </td>
                                                <td class="text-center">
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-primary btn-sm">Detalles</button>
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-warning btn-sm">Editar</button>
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-danger btn-sm">Eliminar</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center text-muted">#55</td>
                                                <td>
                                                    <div class="widget-content p-0">
                                                        <div class="widget-content-wrapper">
                                                            <div class="widget-content-left mr-3">
                                                                <div class="widget-content-left">
                                                                    <img width="40" class="rounded-circle"
                                                                        src="assets/images/avatars/user.png" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="widget-content-left flex2">
                                                                <div class="widget-heading">Firstname Lastname</div>
                                                                <div class="widget-subheading opacity-7">Implante
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">Manassas</td>
                                                <td class="text-center">
                                                    <div class="badge badge-focus">Registrado</div>
                                                </td>
                                                <td class="text-center">
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-primary btn-sm">Detalles</button>
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-warning btn-sm">Editar</button>
                                                    <button type="button" id="PopoverCustomT-4"
                                                        class="btn btn-danger btn-sm">Eliminar</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-block text-center card-footer">
                                    <button class="btn-wide btn btn-success">Ver m&aacute;s</button>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <!-- div class="row">
                        <?php
                        $citas_fx = queryOne('SELECT COUNT(`id_cita`) as total_citas FROM ' . $tabla_citas . ' WHERE yearweek(`cita_fecha`) = yearweek(curdate()) and `id_clinica` = 1');
                        $citas_ms = queryOne('SELECT COUNT(`id_cita`) as total_citas FROM ' . $tabla_citas . ' WHERE yearweek(`cita_fecha`) = yearweek(curdate()) and `id_clinica` = 2');
                        $citas_we = queryOne('SELECT COUNT(`id_cita`) as total_citas FROM ' . $tabla_citas . ' WHERE yearweek(`cita_fecha`) = yearweek(curdate()) and `id_clinica` = 3');
                        ?>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-midnight-bloom">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Appointments (Fairfax)</div>
                                        <div class="widget-subheading">Scheduled For This Week</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-white">
                                            <span><?php echo $citas_fx['total_citas']; ?></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-premium-dark">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Appointments (Manassas)</div>
                                        <div class="widget-subheading">Scheduled For This Week</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-white">
                                            <span><?php echo $citas_ms['total_citas']; ?></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-grow-early">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Appointments (Woodbridge)</div>
                                        <div class="widget-subheading">Scheduled For This Week</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-white">
                                            <span><?php echo $citas_we['total_citas']; ?></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="divider mt-0" style="margin-bottom: 30px;"></div> -->
                    <!-- <div class="row">
                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <h5 class="card-title">Synchronization with Eaglesoft Process</h5>
                                    <div class="">
                                        <p class=""><i class="fa fa-exclamation-triangle text-warning"></i> Please <b>DON'T</b> leave this page until the bar reaches 100% </p>
                                    </div>
                                    <div class="mb-3 progress">
                                        <div class="progress-bar progress-bar-animated progress-bar-striped" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                    </div>
                                    <div class="execute-time-content text-center">
                                        Time Elapsed: <span class="execute-time">1 h 41 min</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <?php
                                $finalizadas = queryOne('SELECT COUNT(' . $tabla_citas . '.id_cita) as total_fin FROM ' . $tabla_citas . ' INNER JOIN ' . $tabla_citas_estado . ' ON ' . $tabla_citas . '.id_cita = ' . $tabla_citas_estado . '.id_cita WHERE estado_cita = "Finished" and cita_fecha = CURDATE() AND citas.id_paciente !="MS2";');
                                $canceladas = queryOne('SELECT COUNT(' . $tabla_citas . '.id_cita) as total_canc FROM ' . $tabla_citas . ' INNER JOIN ' . $tabla_citas_estado . ' ON ' . $tabla_citas . '.id_cita = ' . $tabla_citas_estado . '.id_cita WHERE estado_cita IN ("No Show Up","Canceled","Deleted") AND cita_fecha = CURDATE() AND citas.id_paciente !="MS2";');
                                $programadas = queryOne('SELECT COUNT(' . $tabla_citas . '.id_cita) as total_prog FROM ' . $tabla_citas . ' INNER JOIN ' . $tabla_citas_estado . ' ON ' . $tabla_citas . '.id_cita = ' . $tabla_citas_estado . '.id_cita WHERE estado_cita = "Scheduled" and cita_fecha = CURDATE() AND citas.id_paciente !="MS2";');
                                ?>
                                <div class="no-gutters row">
                                    <div class="col-md-4">
                                        <div class="widget-content">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-right ml-0 mr-3">
                                                    <div class="widget-numbers text-success"><?php echo $programadas['total_prog']; ?></div>
                                                </div>
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">Appointments</div>
                                                    <div class="widget-subheading">Scheduled For Today Remaining</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="widget-content">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-right ml-0 mr-3">
                                                    <div class="widget-numbers text-warning"><?php echo $finalizadas['total_fin']; ?></div>
                                                </div>
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">Appointments</div>
                                                    <div class="widget-subheading">Finished Today</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="widget-content">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-right ml-0 mr-3">
                                                    <div class="widget-numbers text-danger"><?php echo $canceladas['total_canc']; ?></div>
                                                </div>
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">Appointments</div>
                                                    <div class="widget-subheading">Canceled or No Show Up Today</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="divider mt-0" style="margin-bottom: 30px;"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <div class="card-header">Today Appointments <?php echo date('m-d-Y'); ?></div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display align-middle mb-0 table table-borderless table-striped table-hover" id="appointments">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Appt</th>
                                                    <th class="text-center">User</th>
                                                    <th class="text-center">Clinic</th>
                                                    <th class="text-center">Patient</th>
                                                    <th class="text-center">Contact</th>
                                                    <th class="text-center">Reason</th>
                                                    <th class="text-center">Date</th>
                                                    <th class="text-center">Time</th>
                                                    <th class="text-center">State</th>
                                                    <th class="text-center">Lab Case</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php include 'footer.php'; ?>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./assets/scripts/main.js"></script>

    <!-- DATATABLE -->
    <script src="assets/dataTable/jquery.dataTables.min.js"></script>
    <script src="assets/dataTable/dataTables.bootstrap4.min.js"></script>

    <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $.ajax({
                type: 'POST',
                url: "index/sync_pat.php",
                data: 1,
                success: function(response) {
                    console.log(response);
                }
            });

            /* $.ajax({
                type: 'POST',
                url: "index/sync_appt.php",
                data:  1,
                success: function(response) {
                    console.log(response);
                }
            }); */
            
            $('#appointments').DataTable({
                //"responsive":true,
                "order": [
                    [0, "desc"]
                ],
                "serverSide": true,
                "ajax": {
                    "method": "POST",
                    "url": "index/load-appointments.php"
                },
                "bProcessing": true,
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "aoColumns": [{
                        "mData": "idcita"
                    },
                    {
                        "mData": "user"
                    },
                    {
                        "mData": "clinica"
                    },
                    {
                        "mData": "paciente"
                    },
                    {
                        "mData": "contacto"
                    },
                    {
                        "mData": "reason"
                    },
                    {
                        "mData": "fecha"
                    },
                    {
                        "mData": "hora"
                    },
                    {
                        "mData": "estado"
                    },
                    {
                        "mData": "lab"
                    }
                ],
                columnDefs: [{
                        className: 'text-left',
                        targets: [3, 5]
                    },
                    {
                        className: 'text-center',
                        targets: [0, 1, 2, 4, 6, 7, 8, 9]
                    },
                ],
            });

            /* function executeProcess(offset, batch = false) {
                if (batch == false) {
                    batch = parseInt($('#batch').val());
                } else {
                    batch = parseInt(batch);
                }

                if (offset == 0) {
                    $('#start_form').hide();
                    $('#sending').show();
                    $('#sended').text(0);
                    $('#total').text($('#total_comments').val());

                    //reset progress bar
                    $('.progress-bar').css('width', '0%');
                    $('.progress-bar').text('0%');
                    $('.progress-bar').attr('data-progress', '0');
                }

                $.ajax({ 
                        type: 'POST',
                        dataType: "json",
                        url : "index/sync.php", 
                        data: {
                            id_process: 1,
                            offset: offset,
                            batch: batch
                        },
                        success: function(response) {
                            $('.progress-bar').css('width', response.percentage+'%');
                            $('.progress-bar').text(response.percentage+'%');
                            $('.progress-bar').attr('data-progress', response.percentage);
                
                            $('#done').text(response.executed);
                            $('.execute-time').text(response.execute_time);
                
                            if (response.percentage == 100) {
                                $('.end-process').show();
                            } else {
                                var newOffset = offset + batch;
                
                                executeProcess(newOffset, batch);
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            if (textStatus == 'parsererror') {
                                textStatus = 'Technical error: Unexpected response returned by server. Sending stopped.';
                            }
                            alert(textStatus);
                    }
                    });
            } */
        });
    </script>
</body>

</html>