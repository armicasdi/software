<?php
    session_start();
    if(!empty($_SESSION['user']))
    {
        unset($_SESSION['user']);
        unset($_SESSION['id']);
        unset($_SESSION['rol']);
        session_destroy();
    }
    header("Location: ../../index.php");
?>