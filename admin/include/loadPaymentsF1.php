<?php 
    require("database.php");
    include("funciones.php");

    $cantidad_pagos = $_POST['cantidad_pagos'];
    $to_financing = $_POST['to_financing'];
    $to_pay = $_POST['to_financing'];
    $days = $_POST['dias'];
    $payamount = round(($to_financing / $cantidad_pagos),2);    
    $saldos = $to_financing-($payamount*$cantidad_pagos);    
    
    for($i=0; $i<$cantidad_pagos;$i++){
        
        if($to_pay<$payamount){
            $payamount=$to_pay;
        }
        if($saldos>0 && $i==($cantidad_pagos-1)){
            $payamount=$payamount+$saldos;
        }
        $result = $conexion->query('SELECT DATE_ADD(CURDATE(), INTERVAL '.($days*$i).' DAY) as date; ');
        while ($row=$result->fetch_assoc()) 
        {
            echo '<tr>
                    <th scope="row" class="pays"><span>Payment '.($i+1).'</span></th>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend"><span
                                    class="input-group-text"><i
                                        class="fa fa-dollar-sign"></i></span>
                            </div>
                            <input type="number" class="form-control"
                                min="0" name="paysPP1[]" value="'.$payamount.'">
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend"><span
                                    class="input-group-text"><i
                                        class="fa fa-calendar"></i></span>
                            </div>
                            <input type="date" class="form-control" name="paysDatePP1[]" value="'.$row['date'].'">
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend"><span
                                    class="input-group-text"><i
                                        class="fa fa-calendar"></i></span>
                            </div>
                            <select class="form-control" name="wtpPP1[]">
                                <option>Cash</option>
                                <option>Check</option>
                                <option>Credit Card</option>
                                <option>Debit Card</option>
                            </select>
                        </div>
                    </td>
                </tr>';
        }
        $to_pay = $to_pay-$payamount;
    }


?>