$(document).ready(function () {

    $('input[type="number"]').val('0');
    $('#phases').val('1');

    $('.nexttab').click(function () {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

    /* $('#savePatient').click(function () {
        swal.fire({
            title: "Patient Information",
            text: "Saved Successfully",
            type: "success"
        }).then(function () {
            $('.nexttab').click(function(){
                $('.nav-tabs > .active').next('li').find('a').trigger('click');
            });
        });
    }); */

    function bootstrapTabControl() {
        var i, items = $('.nav-link'),
            pane = $('.tab-pane');
        // next
        $('.nexttab').on('click', function () {
            for (i = 0; i < items.length; i++) {
                if ($(items[i]).hasClass('active') == true) {
                    break;
                }
            }
            if (i < items.length - 1) {
                // for tab
                $(items[i]).removeClass('active');
                $(items[i + 1]).addClass('active');
                // for pane
                $(pane[i]).removeClass('show active');
                $(pane[i + 1]).addClass('show active');
            }

        });
        // Prev
        $('.prevtab').on('click', function () {

            for (i = 0; i < items.length; i++) {
                if ($(items[i]).hasClass('active') == true) {
                    break;
                }
            }
            if (i != 0) {
                // for tab
                $(items[i]).removeClass('active');
                $(items[i - 1]).addClass('active');
                // for pane
                $(pane[i]).removeClass('show active');
                $(pane[i - 1]).addClass('show active');
            }
        });
    }

    // ========== CHANGE TAB ==========
    $('#infoNextBtn').click(function () {
        $('.tabs > .active').next('li').find('a').trigger('click');
        //trigger the click on the tab same like we click on the tab
    });

    $('#infoPrevBtn').click(function () {
        $('.tabs > .active').prev('li').find('a').trigger('click');
        //trigger the click on the tab same like we click on the tab
    })

    // ================= SYNC PATIENT ===============
    $("#search_patient").autocomplete({
        source: 'include/loadPatient.php',

        classes: {
            "ui-autocomplete": "highlight"
        },

        select: function (event, ui) {
            $('#search_patient').val(ui.item.patient_id + ': ' + ui.item.last_name + ', ' + ui.item.first_name);
            $('#PatientIDhidden').val(ui.item.patient_id);
            //$("#patient_id_tittle").text(ui.item.patient_id);
            $('#search_treatment').val(ui.item.patient_id);
            $('#PatientFirstName').val(ui.item.first_name);
            $('#PatientLastName').val(ui.item.last_name);
            if (ui.item.sex == 'F') {
                $('#female').prop('checked', true);
                $('#male').prop('checked', false);
            } else if (ui.item.sex == 'M') {
                $('#female').prop('checked', false);
                $('#male').prop('checked', true);
            }
            var address_1 = '';
            var address_2 = '';

            //Define patient chart
            var pat_id = ui.item.patient_id;
            var id_paciente = "";
            if(pat_id.indexOf('F') > -1){
                id_paciente = pat_id.replace('F','FX');
                $("#patient_id_tittle").text(id_paciente);
            }
            else if(pat_id.indexOf('W') > -1){
                id_paciente = pat_id.replace('W','WE');
                $("#patient_id_tittle").text(id_paciente);
            }
            else {
                id_paciente = 'MS'+pat_id;
                $("#patient_id_tittle").text(id_paciente);
            }

            if (ui.item.address_1 == null) {
                address_1 = '';

            }
            if (ui.item.address_1 != null) {
                address_1 = ui.item.address_1 + ', ';
            }
            if (ui.item.address_2 == null) {
                address_2 = '';

            }
            if (ui.item.address_2 != null) {
                address_2 = ui.item.address_2 + ', ';
            }
            $('#PatientAddress').val(address_1 + address_2 + ui.item.city + ', ' + ui.item.state + ', ' + ui.item.zipcode);
            $('#PatientBirthDate').val(ui.item.birth_date);

            $('#contact_phone_number').val(ui.item.cell_phone);
            //$('#contact_home_phone_number').val(ui.item.home_phone);
            var contact_phone = ui.item.cell_phone;
            var cp = contact_phone.mask('(000) 000 0000', { reverse: true });
            $('#contact_home_phone_number').val(cp);

            $('#contact_email').val(ui.item.email_address);
            if (ui.item.marital_status == 'X') {
                $('#separated').prop('checked', true);
                $('#married').prop('checked', false);
                $('#single').prop('checked', false);
                $('#unknown').prop('checked', false);
                $('#widowed').prop('checked', false);
                $('#divorced').prop('checked', false);
            } else if (ui.item.marital_status == 'M') {
                $('#married').prop('checked', true);
                $('#unknown').prop('checked', false);
                $('#single').prop('checked', false);
                $('#separated').prop('checked', false);
                $('#widowed').prop('checked', false);
                $('#divorced').prop('checked', false);
            } else if (ui.item.marital_status == 'S') {
                $('#single').prop('checked', true);
                $('#married').prop('checked', false);
                $('#unknown').prop('checked', false);
                $('#separated').prop('checked', false);
                $('#widowed').prop('checked', false);
                $('#divorced').prop('checked', false);
            } else if (ui.item.marital_status == 'U') {
                $('#unknown').prop('checked', true);
                $('#married').prop('checked', false);
                $('#single').prop('checked', false);
                $('#separated').prop('checked', false);
                $('#widowed').prop('checked', false);
                $('#divorced').prop('checked', false);
            } else if (ui.item.marital_status == 'D') {
                $('#divorced').prop('checked', true);
                $('#married').prop('checked', false);
                $('#single').prop('checked', false);
                $('#separated').prop('checked', false);
                $('#widowed').prop('checked', false);
                $('#unknown').prop('checked', false);
            } else if (ui.item.marital_status == 'W') {
                $('#widowed').prop('checked', true);
                $('#married').prop('checked', false);
                $('#single').prop('checked', false);
                $('#separated').prop('checked', false);
                $('#unknown').prop('checked', false);
                $('#divorced').prop('checked', false);
            }
            return false;
        }
    });

    //========================= CONSULT TAB ==========================
    $('.selectpicker').multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        filterPlaceholder: 'Search'
    });

    $('input[name="type[]"]').change(function (){

        $('.selectpicker').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            filterPlaceholder: 'Search'
        });

        var consulta_nombre = "";

        $.ajax({
            type: "POST",
            url: 'include/loadADACodes.php',
            success: function (response) {
                var procedimiento = $('[name="type[]"]:checked').val();
                if (procedimiento == 1) {

                    var consulta_nombre = "New Patient";

                    var newpatient = `
                                <optgroup label="NEW PATIENT">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>
                                <optgroup label="PROFI">
                                    <option value="D1110">D1110: PROPHYLAXIS-ADULT</option>
                                    <option value="D1120">D1120: PROPHYLAXIS-CHILD</option>
                                    <option value="D1208">D1208: TOPICAL AP. OF FLUORIDE</option>
                                </optgroup>`;
                    $("#preceduresConsult").append(newpatient);

                } else if (procedimiento == 2) {

                    var consulta_nombre = "Consult";

                    var consult = `
                                <optgroup label="CONSULT">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>`;
                                $("#preceduresConsult").append(consult);
                } else if (procedimiento == 3) {

                    var consulta_nombre = "Emergency";

                    var emergency = `
                                <optgroup label="EMERGENCY">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>`;
                                $("#preceduresConsult").append(emergency);
                }
                $("#preceduresConsult").append('<optgroup label="Registered Treatments"></optgroup>');
                $("#preceduresConsult").append(response);
                $("#preceduresConsult").multiselect('rebuild');

                $('#consult-name').text(consulta_nombre);

            }
        });

        

        $("#preceduresConsult").multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            includeFilterClearBtn: true,
            enableClickableOptGroups: true,
            enableCollapsibleOptGroups: true,
            enableCaseInsensitiveFiltering: true,
            numberDisplayed: 1,
            filterPlaceholder: 'Search',
            nonSelectedText: 'Select ADA Codes',
            maxHeight: 400,
            onDeselectAll: function () {
                $("#preceduresConsult").val('');
            },
            onChange: function (element, checked) {

                var adacode = $("#preceduresConsult option:selected").map(function () {
                    return this.value;
                }).get();

                console.log(adacode);
                var str = adacode.join(',');
                $("#adaCodesConsult").val(str);
                console.log(str);

            }

        });
    });

    //==================== IMPORT TREAT =====================
    //Cargar Treatment PLans de Eagle por el Id del Paciente

    $("#searchTreat").click(function (e) {
        var val = $('#search_treatment').val();

        $.ajax({
            type: "POST",
            url: 'include/loadTreatmentPlanFromEagle.php',
            data: 'search_treatment=' + val,
            success: function (resp) {
                $('#treat_rows').html(resp);

                //Select TreatmentPLans
                $('input[name="treatId[]"]').on('click', function () {

                    //Read all check checkboxes
                    var treatCodesSelected = $('input[name="treatId[]"]:checked').map(function () {
                        return this.value;
                    }).get();

                    $('#treatIdsCount').text('[' + treatCodesSelected.length + ']');

                    if (treatCodesSelected.length > 0) {

                        $.ajax({
                            type: "POST",
                            url: 'include/loadTreatmentPlanItems.php',
                            data: {
                                search_treatment: val,
                                codigo_tratamiento: treatCodesSelected
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                console.log(data);
                                //const test = data[0]["service_code"];
                                //alert(test);

                                var fee = 0;
                                var count = 0;
                                var est_primary = 0;
                                var discount = 0;
                                var patientPortion = 0;
                                var parts = [];
                                $.each(data, function () {
                                    fee += parseFloat(data[count]['fee']);
                                    est_primary += parseFloat(data[count]['est_primary']);
                                    discount += parseFloat(data[count]['discount']);
                                    service_code = data[count]['service_code'];
                                    description = data[count]['description'];
                                    tooth = data[count]['tooth'];
                                    parts.push(tooth); //parts to work
                                    count++;


                                    //CHECK SERVICE CODE
                                    $.ajax({
                                        type: "POST",
                                        url: 'include/check-tp.php',
                                        data: {
                                            ada_code: service_code
                                        },
                                        dataType: 'JSON',
                                        success: function (response) {
                                            tipo = response[0]['type'];
                                            if (tipo == 5) {
                                                $('#code3').prop('checked', true);
                                                $('#diag3').prop('checked', true);
                                            } else if (tipo == 3) {
                                                $('#code4').prop('checked', true);
                                                $('#diag4').prop('checked', true);
                                            } else if (tipo == 4) {
                                                $('#code5').prop('checked', true);
                                                $('#diag5').prop('checked', true);
                                            } else if (tipo == 3) {
                                                $('#code6').prop('checked', true);
                                                $('#diag6').prop('checked', true);
                                            } else if (tipo == 9) {
                                                $('#code7').prop('checked', true);
                                                $('#diag7').prop('checked', true);
                                            } else if (tipo == 6) {
                                                $('#code8').prop('checked', true);
                                                $('#diag8').prop('checked', true);
                                            } else if (tipo == 6) {
                                                $('#code9').prop('checked', true);
                                                $('#diag9').prop('checked', true);
                                            } else if (tipo == 8) {
                                                $('#code10').prop('checked', true);
                                                $('#diag10').prop('checked', true);
                                            } else if (tipo == 10) {
                                                $('#code11').prop('checked', true);
                                                $('#diag11').prop('checked', true);
                                            } else if (tipo == 11) {
                                                $('#code12').prop('checked', true);
                                                $('#diag12').prop('checked', true);
                                            } else if (tipo == 8) {
                                                $('#code13').prop('checked', true);
                                                $('#diag13').prop('checked', true);
                                            } else if (tipo == 6) {
                                                $('#code14').prop('checked', true);
                                                $('#diag14').prop('checked', true);
                                            }

                                        }
                                    });

                                    //$('[name="procedureSesion[]"]').append('<option value='+service_code+'>'+description+'</option>');
                                    $('[name="procedureSesion_hidden[]"]').append('<optgroup label="FROM EAGLESOFT"></optgroup>');
                                    $('[name="procedureSesion_hidden[]"]').append('<option value=' + service_code + '-' + count + '>' + description + '</option>');
                                });

                                //tooths to work

                                const nosame = (valor, indice, self) => {
                                    return self.indexOf(valor) === indice;
                                }

                                var nstostr = $.map(parts.filter(nosame), function (val, index) {
                                    var str = val;
                                    return str;
                                }).join(", ");
                                //var towork = parts.filter(nosame).join(",");
                                $('[name="parts_hidden[]"]').val(nstostr);
                                console.log(nstostr);

                                //console.log(parts.filter(nosame));

                                $('#totalProposed').val(fee.toFixed(2));
                                $('#totalProposed2').val(fee.toFixed(2));
                                $('#totalProposed3').val(fee.toFixed(2));
                                $('#totalProposedNon').val(fee.toFixed(2));
                                $('#totalInsurance').val(est_primary.toFixed(2));
                                $('#totalInsurance2').val(est_primary.toFixed(2));
                                $('#totalInsurance3').val(est_primary.toFixed(2));
                                $('#totalInsuranceNon').val(est_primary.toFixed(2));
                                $('#totalDiscount').val(discount.toFixed(2));
                                $('#totalDiscount2').val(discount.toFixed(2));
                                $('#totalDiscount3').val(discount.toFixed(2));
                                $('#totalPatientPortion').val((fee - est_primary).toFixed(2));

                                //print discount 
                                var discountPercent = parseFloat((discount / fee) * 100);
                                $('#discountPricing').val(Math.round(discountPercent, 2));
                                $('#discountPricing2').val(Math.round(discountPercent, 2));

                                console.log(fee);
                            }
                        });
                    }
                });
            }
        });
    });


    //Define el codigo del jobnumber
    $("#job_clinica").change(function () {

        var proce = $('[name="procedimiento[]"]:checked').map(function () {
            return this.value;
        }).get();

        var str = proce.join(',');
        var proces = [];

        proce.forEach(function (obj) {
            var procedure_id = (('0' + obj).slice(-2)).toString();

            proces.push(procedure_id);
            var cadena = proces.join('');
            console.log(cadena);

            var patient_chart = $('#patient_id_tittle').text();
            var job_clinica = $('#job_clinica').val();
            var year = (new Date).getFullYear();
            var job_code = "";
            job_code = 'TD' + job_clinica + patient_chart + cadena + year;
            console.log(job_code);
            $("#job_number_id").val(job_code);
            $("#job_code_tittle").text(job_code);
            $("#job_code_tittle_hidden").val(job_code);

        });

        $('[name="procedimiento[]"]').click(function () {
            var proce = $('[name="procedimiento[]"]:checked').map(function () {
                return this.value;
            }).get();

            var str = proce.join(',');
            var proces = [];

            console.log(proces);

            proce.forEach(function (obj) {
                var procedure_id = (('0' + obj).slice(-2)).toString();

                proces.push(procedure_id);
                var cadena = proces.join('');
                console.log(cadena);

                var patient_chart = $('#patient_id_tittle').text();
                var job_clinica = $('#job_clinica').val();
                var year = (new Date).getFullYear();
                var job_id = "<?php echo ultimo_job_creado(); ?>";
                var job_code = "";
                job_code = 'TD' + job_clinica + patient_chart + cadena +
                    year;
                console.log(job_code);
                console.log(job_id);
                $("#job_number_id").val(job_code);
                $("#job_code_tittle").text(job_code);
                $("#job_code_tittle_hidden").val(job_code);

            });
        });
    });

    //============= SURFACE AND TOOTH MODAL ==============
    $('[name="mesial"]').click(function () {

        var surfaceDefined = array();

        if ($('.mesial').attr('aria-pressed') == true) {
            surfaceDefined.push('M');
        } else {
            removeItemFromArr(surfaceDefined, 'M');
        }

        console.log(surfaceDefined);
    });


    // ======== JOB CONFIG ========
    // TREATMENT AND SESSIONS
    // ADD PROCEDURE DESCRIPTION
    $('[name="procedimiento[]"]').click(function () {
        if ($('[name="procedimiento[]"]').is(':checked')) {
            //Read all check procedures checkboxes
            var procedures_description = '';
            $('#descList').empty();

            $.each($('input[name="procedimiento[]"]:checked'), function () {
                procedures_description = $(this).siblings('input[type=hidden]').val();
                procedures_description_key = $('label[for=' + this.id + ']').text();

                $('#descList').append(`
                    <li class="justify-content-between list-group-item">
                        <span class="badge badge-secondary badge-pill">` + procedures_description_key + `</span>
                        <p>` + procedures_description + `</p>
                    </li>`);
            });
        }

        var promarcado = $('[name="procedimiento[]"]:checked').val();
    });

    //ADD OR DELETE SESSION ROWS
    $("#sheduleSessions").click(function () {
        funcAddSessions();

        var rowCount = $('#sessionsTable tr').length;
        $('#cantidadSessions').text(rowCount);
        $('.selectpicker').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            filterPlaceholder: 'Search'
        });
    });

    function funcAddSessions() {
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '/' +
            ((''+month).length<2 ? '0' : '') + month + '/' +
            ((''+day).length<2 ? '0' : '') + day;

        var rowCount = $('#sessionsTable tr').length;

        $("#sessionsTable").append(
            `<tr>
                    <th scope="row" class="session"><span name="sessionName[]">Session ` + (rowCount + 1) + `</span></th>
                    <td>
                        <select class="form-control" multiple name="procedureSesion[]" id="proceduresSe` + (rowCount + 1) + `">
                        </select>                        
                    </td>
                    <td>
                        <textarea type="text" class="form-control" name="adacodes[]" id="adaSe` + (rowCount + 1) + `"></textarea>
                        <input type="hidden" class="form-control" name="preciosesion[]" id="precioSe` + (rowCount + 1) + `"></textarea>
                    </td>
                    <td>
                        <input type="date" class="form-control" value="` + output + `" name="fechaSession[]" id="fechaSe` + (rowCount + 1) + `">
                    </td>
                    <td>
                        <select name="duration[]" class="form-control">
                            <option value="0">00:00 h.</option>
                            <option value="0.25">00:15 h.</option>
                            <option value="0.50">00:30 h.</option>
                            <option value="0.75">00:45 h.</option>
                            <option value="1">01:00 h.</option>
                            <option value="1.25">01:15 h.</option>
                            <option value="1.50">01:30 h.</option>
                            <option value="1.75">01:45 h.</option>
                            <option value="2">02:00 h.</option>
                            <option value="2.25">02:15 h.</option>
                            <option value="2.50">02:30 h.</option>
                            <option value="2.75">02:45 h.</option>
                            <option value="3">03:00 h.</option>
                        </select>                        
                    </td>
                    <td>
                        <input type="text" class="form-control" name="parts[]" id="parts` + (rowCount + 1) + `"/>
                    </td>
                    <td>                        
                        <button type="button" class="DeleteButton mb-2 mr-2 btn btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>`);
        //<button type="button" class="mb-2 mr-2 btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">Details</button>
        var parts_towork = $("#partsHidden").val();
        $("#parts" + (rowCount + 1) + "").val(parts_towork);

        var $options = $("#proceduresSeHidden > option").clone();
        $("#proceduresSe" + (rowCount + 1) + "").append($options);

        $("#proceduresSe" + (rowCount + 1) + "").multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            includeFilterClearBtn: true,
            enableClickableOptGroups: true,
            enableCollapsibleOptGroups: true,
            enableCaseInsensitiveFiltering: true,
            numberDisplayed: 1,
            filterPlaceholder: 'Search',
            nonSelectedText: 'Select ADA Codes',
            maxHeight: 400,
            onDeselectAll: function () {
                $("#adaSe" + (rowCount + 1) + "").val('');
            },
            onChange: function (element, checked) {

                var adacode = $("#proceduresSe" + (rowCount + 1) + " option:selected").map(function () {
                    return this.value;
                }).get();

                console.log(adacode);
                var str = adacode.join(',');
                $("#adaSe" + (rowCount + 1) + "").val(str);
                console.log(str);

                $.ajax({
                    type: "POST",
                    url: 'include/loadADACodesPrice.php',
                    data: {
                        adacode: adacode
                    },
                    success: function (response) {
                        console.log(response);
                        $("#precioSe" + (rowCount + 1) + "").val(response);

                        var selec = $('input[name="preciosesion[]"]').map(function () {
                            return this.value;
                        }).get();

                        console.log(selec);
                        var totalProposedNon = 0;
                        selec.forEach(function (obj) {
                            totalProposedNon += parseFloat(obj);
                        });
                        console.log(totalProposedNon);
                        $("#totalProposedNon").val(totalProposedNon);
                    }
                });
            }

        });


        //if ($('[name="procedimiento[]"]').is(':checked')) {
        $.ajax({
            type: "POST",
            url: 'include/loadADACodes.php',
            success: function (response) {
                var procedimiento = $('[name="procedimiento[]"]:checked').val();
                if (procedimiento == 1) {

                    var newpatient = `
                                <optgroup label="NEW PATIENT">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>
                                <optgroup label="PROFI">
                                    <option value="D1110">D1110: PROPHYLAXIS-ADULT</option>
                                    <option value="D1120">D1120: PROPHYLAXIS-CHILD</option>
                                    <option value="D1208">D1208: TOPICAL AP. OF FLUORIDE</option>
                                </optgroup>`;
                    $("#proceduresSe" + (rowCount + 1) + "").append(newpatient);

                } else if (procedimiento == 2) {

                    var consult = `
                                <optgroup label="CONSULT">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>`;
                    $("#proceduresSe" + (rowCount + 1) + "").append(consult);
                    //$('[name="procedureSesion[]"]').multiselect('rebuild') 
                } else if (procedimiento == 3) {

                    var emergency = `
                                <optgroup label="EMERGENCY">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>`;
                    $("#proceduresSe" + (rowCount + 1) + "").append(emergency);
                    //$('[name="procedureSesion[]"]').multiselect('rebuild') 
                }
                $("#proceduresSe" + (rowCount + 1) + "").append('<optgroup label="Registered Treatments"></optgroup>');
                $("#proceduresSe" + (rowCount + 1) + "").append(response);
                $("#proceduresSe" + (rowCount + 1) + "").multiselect('rebuild');
            }
        });

        $(".DeleteButton").on('click', function () {

            deleteSessions();
            $('#cantidadSessions').text(rowCount);

        });

        //CANTIDAD DE HORAS QUE DURARA EL JOBNUMBER
        $('select[name="duration[]"]').click(function () {

            var durArr = $('select[name="duration[]"] option:selected').map(function () {
                return this.value;
            }).get();

            var cantidadHoras = 0;
            durArr.forEach(function (obj) {
                cantidadHoras += parseFloat(obj);
            });
            $("#cantidadHours").val(cantidadHoras);
            var decimalTimeString = cantidadHoras;
            var n = new Date(0, 0);
            n.setSeconds(+decimalTimeString * 60 * 60);
            var a = n.toTimeString().slice(0, 8);
            $("#cantidadHours_hidden").text(a);

            $('[name="directos[]"]:checked').each(function () {
                var multi = parseFloat($(this).closest('td').find('span').text());
                var cd = parseFloat(this.value * cantidadHoras * multi);
                $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                sumarDirectos();
                calcularCostos();
            });

            $('[name="otrosDirectos[]"]:checked').each(function () {
                var multi = parseFloat($(this).closest('td').find('span').text());
                var cd = parseFloat(this.value * cantidadHoras * multi);
                $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                sumarOtrosDirectos();
                calcularCostos();
            });

            $('[name="indirectos[]"]:checked').each(function () {
                var multi = parseFloat($(this).closest('td').find('span').text());
                var cd = parseFloat(this.value * cantidadHoras * multi);
                $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                sumarIndirectos();
                calcularCostos();
            });

            $('[name="lab[]"]:checked').each(function () {
                var multi = parseFloat($(this).closest('td').find('span').text());
                var cd = parseFloat(this.value * cantidadHoras * multi);
                $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                sumarLaboratory();
                calcularCostos();
            });
        });

    }

    function deleteSessions() {
        $("#sessionsTable").on("click", ".DeleteButton", function (e) {
            e.preventDefault();
            var rowCount = $('#sessionsTable tr').length;
            $('#cantidadSessions').text(rowCount);
            if ($('#sessionsTable tr').length > 0) {
                $(this).closest("tr").remove();
                $('th.session').text(function (i) {
                    return 'Session ' + (i + 1);
                });

                var durArr = $('select[name="duration[]"] option:selected').map(function () {
                    return this.value;
                }).get();

                var cantidadHoras = 0;
                durArr.forEach(function (obj) {
                    cantidadHoras += parseFloat(obj);
                });
                $("#cantidadHours").val(cantidadHoras);
                var decimalTimeString = cantidadHoras;
                var n = new Date(0, 0);
                n.setSeconds(+decimalTimeString * 60 * 60);
                var a = n.toTimeString().slice(0, 8);
                $("#cantidadHours_hidden").text(a);

                $('#cantidadSessions').text(rowCount);

                $('[name="directos[]"]:checked').each(function () {
                    var multi = parseFloat($(this).closest('td').find('span').text());
                    var cd = parseFloat(this.value * cantidadHoras * multi);
                    $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                    sumarDirectos();
                    calcularCostos();
                });
    
                $('[name="otrosDirectos[]"]:checked').each(function () {
                    var multi = parseFloat($(this).closest('td').find('span').text());
                    var cd = parseFloat(this.value * cantidadHoras * multi);
                    $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                    sumarOtrosDirectos();
                    calcularCostos();
                });
    
                $('[name="indirectos[]"]:checked').each(function () {
                    var multi = parseFloat($(this).closest('td').find('span').text());
                    var cd = parseFloat(this.value * cantidadHoras * multi);
                    $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                    sumarIndirectos();
                    calcularCostos();
                });
    
                $('[name="lab[]"]:checked').each(function () {
                    var multi = parseFloat($(this).closest('td').find('span').text());
                    var cd = parseFloat(this.value * cantidadHoras * multi);
                    $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                    sumarLaboratory();
                    calcularCostos();
                });
            }
            return false;
        });
    }

    //Phase 2 
    $("#sheduleSessionsPhase2").click(function () {
        funcAddSessionsPhase2();

        var rowCount = $('#sessionsTablePhase2 tr').length;
        $('#cantidadSessionsPhase2').text(rowCount);
        $('.selectpicker').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            filterPlaceholder: 'Search'
        });
    });

    function funcAddSessionsPhase2() {
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '/' +
            ((''+month).length<2 ? '0' : '') + month + '/' +
            ((''+day).length<2 ? '0' : '') + day;

        var rowCount = $('#sessionsTablePhase2 tr').length;

        $("#sessionsTablePhase2").append(
            `<tr>
                    <th scope="row" class="sessionPhase2"><span name="sessionNamePhase2[]">Session ` + (rowCount + 1) + `</span></th>
                    <td>
                        <select class="form-control" multiple name="procedureSesionPhase2[]" id="proceduresSePhase2` + (rowCount + 1) + `">
                        </select>                        
                    </td>
                    <td>
                        <textarea type="text" class="form-control" name="adacodesPhase2[]" id="adaSePhase2` + (rowCount + 1) + `"></textarea>
                        <input type="hidden" class="form-control" name="preciosesionPhase2[]" id="precioSePhase2` + (rowCount + 1) + `"></textarea>
                    </td>
                    <td>
                        <input type="date" class="form-control" value="` + output + `" name="fechaSessionPhase2[]" id="fechaSePhase2` + (rowCount + 1) + `">
                    </td>
                    <td>
                        <select name="durationPhase2[]" class="form-control">
                            <option value="0">00:00 h.</option>
                            <option value="0.25">00:15 h.</option>
                            <option value="0.50">00:30 h.</option>
                            <option value="0.75">00:45 h.</option>
                            <option value="1">01:00 h.</option>
                            <option value="1.25">01:15 h.</option>
                            <option value="1.50">01:30 h.</option>
                            <option value="1.75">01:45 h.</option>
                            <option value="2">02:00 h.</option>
                            <option value="2.25">02:15 h.</option>
                            <option value="2.50">02:30 h.</option>
                            <option value="2.75">02:45 h.</option>
                            <option value="3">03:00 h.</option>
                        </select>                        
                    </td>
                    <td>
                        <input type="text" class="form-control" name="partsPhase2[]" id="parts` + (rowCount + 1) + `"/>
                    </td>
                    <td>                        
                        <button type="button" class="DeleteButtonPhase2 mb-2 mr-2 btn btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>`);
        //<button type="button" class="mb-2 mr-2 btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">Details</button>
        var parts_towork = $("#partsHiddenPhase2").val();
        $("#parts" + (rowCount + 1) + "").val(parts_towork);

        var $options = $("#proceduresSeHidden > option").clone();
        $("#proceduresSePhase2" + (rowCount + 1) + "").append($options);

        $("#proceduresSePhase2" + (rowCount + 1) + "").multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            includeFilterClearBtn: true,
            enableClickableOptGroups: true,
            enableCollapsibleOptGroups: true,
            enableCaseInsensitiveFiltering: true,
            numberDisplayed: 1,
            filterPlaceholder: 'Search',
            nonSelectedText: 'Select ADA Codes',
            maxHeight: 400,
            onDeselectAll: function () {
                $("#adaSe" + (rowCount + 1) + "").val('');
            },
            onChange: function (element, checked) {

                var adacode = $("#proceduresSePhase2" + (rowCount + 1) + " option:selected").map(function () {
                    return this.value;
                }).get();

                console.log(adacode);
                var str = adacode.join(',');
                $("#adaSe" + (rowCount + 1) + "").val(str);
                console.log(str);

            }

        });


        //if ($('[name="procedimiento[]"]').is(':checked')) {
        $.ajax({
            type: "POST",
            url: 'include/loadADACodes.php',
            success: function (response) {
                var procedimiento = $('[name="procedimiento[]"]:checked').val();
                if (procedimiento == 1) {

                    var newpatient = `
                                <optgroup label="NEW PATIENT">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>
                                <optgroup label="PROFI">
                                    <option value="D1110">D1110: PROPHYLAXIS-ADULT</option>
                                    <option value="D1120">D1120: PROPHYLAXIS-CHILD</option>
                                    <option value="D1208">D1208: TOPICAL AP. OF FLUORIDE</option>
                                </optgroup>`;
                    $("#proceduresSe" + (rowCount + 1) + "").append(newpatient);

                } else if (procedimiento == 2) {

                    var consult = `
                                <optgroup label="CONSULT">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>`;
                    $("#proceduresSe" + (rowCount + 1) + "").append(consult);
                    //$('[name="procedureSesion[]"]').multiselect('rebuild') 
                } else if (procedimiento == 3) {

                    var emergency = `
                                <optgroup label="EMERGENCY">
                                    <option value="D0150">D0150: COMPREHENSIVE ORAL EVALUATION</option>
                                    <option value="D0140">D0140: LIMITED ORAL EVALUATION-PROBLEM FOCUSED</option>
                                    <option value="D0120">D0120: PERIODIC ORAL EVALUATION</option>
                                </optgroup>
                                <optgroup label="RADIO">
                                    <option value="D0272">D0272: BITEWINGS-TWO FILMS</option>
                                    <option value="D0274">D0274: BITEWINGS-FOUR FILMS</option>
                                    <option value="D0220">D0220: INTRAORAL-PERIAPICAL FIRST FILM</option>
                                    <option value="D0230">D0230: INTRAORAL-PERIAPICAL-EACH ADDITIONAL FIL</option>
                                    <option value="D0330">D0330: PANORAMIC FILM</option>
                                </optgroup>`;
                    $("#proceduresSe" + (rowCount + 1) + "").append(emergency);
                    //$('[name="procedureSesion[]"]').multiselect('rebuild') 
                }
                $("#proceduresSe" + (rowCount + 1) + "").append('<optgroup label="Registered Treatments"></optgroup>');
                $("#proceduresSe" + (rowCount + 1) + "").append(response);
                $("#proceduresSe" + (rowCount + 1) + "").multiselect('rebuild');
            }
        });

        $(".DeleteButtonPhase2").on('click', function () {

            deleteSessionsPhase2();
            $('#cantidadSessionsPhase2').text(rowCount);

        });

        //CANTIDAD DE HORAS QUE DURARA EL JOBNUMBER
        $('select[name="durationPhase2[]"]').click(function () {

            var durArr = $('select[name="durationPhase2[]"] option:selected').map(function () {
                return this.value;
            }).get();

            var cantidadHoras = 0;
            durArr.forEach(function (obj) {
                cantidadHoras += parseFloat(obj);
            });
            $("#cantidadHoursPhase2").val(cantidadHoras);
            var decimalTimeString = cantidadHoras;
            var n = new Date(0, 0);
            n.setSeconds(+decimalTimeString * 60 * 60);
            var a = n.toTimeString().slice(0, 8);
            $("#cantidadHoursPhase2_hidden").text(a);

            $('[name="directosPhase2[]"]:checked').each(function () {
                var multi = parseFloat($(this).closest('td').find('span').text());
                var cd = parseFloat(this.value * cantidadHoras * multi);
                $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                sumarDirectos();
                calcularCostos();
            });

            $('[name="otrosDirectosPhase2[]"]:checked').each(function () {
                var multi = parseFloat($(this).closest('td').find('span').text());
                var cd = parseFloat(this.value * cantidadHoras * multi);
                $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                sumarOtrosDirectos();
                calcularCostos();
            });

            $('[name="indirectosPhase2[]"]:checked').each(function () {
                var multi = parseFloat($(this).closest('td').find('span').text());
                var cd = parseFloat(this.value * cantidadHoras * multi);
                $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                sumarIndirectos();
                calcularCostos();
            });

            $('[name="labPhase2[]"]:checked').each(function () {
                var multi = parseFloat($(this).closest('td').find('span').text());
                var cd = parseFloat(this.value * cantidadHoras * multi);
                $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                sumarLaboratory();
                calcularCostos();
            });
        });

    }

    function deleteSessionsPhase2() {
        $("#sessionsTablePhase2").on("click", ".DeleteButton", function (e) {
            e.preventDefault();
            var rowCount = $('#sessionsTablePhase2 tr').length;
            $('#cantidadSessionsPhase2').text(rowCount);
            if ($('#sessionsTablePhase2 tr').length > 0) {
                $(this).closest("tr").remove();
                $('th.sessionPhase2').text(function (i) {
                    return 'Session ' + (i + 1);
                });

                var durArr = $('select[name="durationPhase2[]"] option:selected').map(function () {
                    return this.value;
                }).get();

                var cantidadHoras = 0;
                durArr.forEach(function (obj) {
                    cantidadHoras += parseFloat(obj);
                });
                $("#cantidadHoursPhase2").val(cantidadHoras);
                var decimalTimeString = cantidadHoras;
                var n = new Date(0, 0);
                n.setSeconds(+decimalTimeString * 60 * 60);
                var a = n.toTimeString().slice(0, 8);
                $("#cantidadHoursPhase2_hidden").text(a);

                $('#cantidadSessionsPhase2').text(rowCount);

                $('[name="directosPhase2[]"]:checked').each(function () {
                    var multi = parseFloat($(this).closest('td').find('span').text());
                    var cd = parseFloat(this.value * cantidadHoras * multi);
                    $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                    sumarDirectos();
                    calcularCostos();
                });
    
                $('[name="otrosDirectosPhase2[]"]:checked').each(function () {
                    var multi = parseFloat($(this).closest('td').find('span').text());
                    var cd = parseFloat(this.value * cantidadHoras * multi);
                    $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                    sumarOtrosDirectos();
                    calcularCostos();
                });
    
                $('[name="indirectosPhase2[]"]:checked').each(function () {
                    var multi = parseFloat($(this).closest('td').find('span').text());
                    var cd = parseFloat(this.value * cantidadHoras * multi);
                    $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                    sumarIndirectos();
                    calcularCostos();
                });
    
                $('[name="labPhase2[]"]:checked').each(function () {
                    var multi = parseFloat($(this).closest('td').find('span').text());
                    var cd = parseFloat(this.value * cantidadHoras * multi);
                    $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
                    sumarLaboratory();
                    calcularCostos();
                });
            }
            return false;
        });
    }

    //============================ COSTS TAB ===============================

    //editar precio unitario
    $(document).on('click', '.edit',function (e) {
        var nombre = $(this).data('nombre');
        var tipo = $(this).data('tipo');
        var costo = $(this).data('costo');
        var multi = $(this).data('multi');
        var inputid = $(this).data('idinp');

        if(tipo == 1){
            tipo = "Direct"
        }
        else if(tipo == 2){
            tipo = "Indirect"
        }
        else if(tipo == 4){
            tipo = "Laboratory"
        }
        else if(tipo == 3){
            tipo = "Other Direct"
        }

        $('#nombreCostoChange').text(nombre);
        $('#tipoCostoChange').text(tipo);
        $('#valorCostoChange').val(costo);
        $('#idhidden').val(inputid);

        
        //console.log($('#'+inputid).val());

        $('#change').click(function (){
            console.log($('#'+inputid).val());

            var totalHours = 0;
            totalHours = parseFloat($("#cantidadHours").val());
            var idhidden = $("#idhidden").val()

            var cd = parseFloat($('#valorCostoChange').val() * totalHours * multi);
            $('#'+idhidden).val(parseFloat(cd.toFixed(2)));
            console.log(cd);
            sumarDirectos();
            sumarIndirectos();
            sumarOtrosDirectos();
            calcularCostos();
        });

    });

    //DIRECT COST
    //Habilita la Edicion de los Costos
   /*  $('[name="directosEdit[]"]').click(function () {
        //$(this).closest('td').find('input').removeAttr('disabled');
    }); */

    

    function sumarDirectos() {
        var arr = [];
        $('[name="directos[]"]:checked').each(function () {
            var costoDirecto = parseFloat($(this).closest('td').next().find('input').val());

            arr.push(costoDirecto);
            var sumaDirectos = 0;
            arr.forEach(function (obj) {
                sumaDirectos += parseFloat(obj);
            });
            $("#totalDirectos").val(sumaDirectos.toFixed(2));
        });
    }

    $('[name="directosInput[]"]').change(function () {
        sumarDirectos();
        calcularCostos();
    });

    $('[name="directos[]"]').click(function () {
        var totalHours = parseFloat($("#cantidadHours").val());
        var multi = parseFloat($(this).closest('td').find('span').text());
        var cd = parseFloat(this.value * totalHours * multi);
        if ($(this).is(':checked') && totalHours == 0) {
            $(this).closest('td').next().find('input').val(0);
            sumarDirectos();
        calcularCostos();
        } 
        else if ($(this).is(':checked') && totalHours > 0) {
            $(this).closest('td').next().find('input').val(parseFloat(cd.toFixed(2)));
            sumarDirectos();
        calcularCostos();
        }  
        else if ($(this).not(':checked').length){
            $(this).closest('td').next().find('input').val(0);
            sumarDirectos();
        calcularCostos();
        }
        
    });

    

    //OTHER DIRECT COST
    //Habilita la Edicion de los Costos
    $('[name="otrosDirectosEdit[]"]').click(function () {
        $(this).closest('td').find('input').removeAttr('disabled');
    });

    function sumarOtrosDirectos() {
        var arr = [];
        $('[name="otrosDirectos[]"]:checked').each(function () {
            var costoOtroDirecto = $(this).closest('td').next().find('input').val();
            arr.push(costoOtroDirecto);
            var sumaOtrosDirectos = 0;
            arr.forEach(function (obj) {
                sumaOtrosDirectos += parseFloat(obj);
            });
            $("#totalOtros").val(sumaOtrosDirectos.toFixed(2));
        });
    }

    $('[name="otrosDirectosInput[]"]').change(function () {
        sumarOtrosDirectos();
        calcularCostos();
    });

    $('[name="otrosDirectos[]"]').click(function () {
        var totalHours = parseFloat($("#cantidadHours").val());
        var multi = parseFloat($(this).closest('td').find('span').text());
        var cod = parseFloat(this.value * totalHours * multi);
        if ($(this).is(':checked') && totalHours == 0) {
            $(this).closest('td').next().find('input').val(0);
            sumarOtrosDirectos();
            calcularCostos();
        } 
        else if ($(this).is(':checked') && totalHours > 0) {
            $(this).closest('td').next().find('input').val(parseFloat(cod.toFixed(2)));
            sumarOtrosDirectos();
            calcularCostos();
        }  
        else if ($(this).not(':checked').length){
            $(this).closest('td').next().find('input').val(0);
            sumarOtrosDirectos();
            calcularCostos();
        }
        
    });

    //INDIRECT COST
    //Habilita la Edicion de los Costos
    $('[name="indirectosEdit[]"]').click(function () {
        $(this).closest('td').find('input').removeAttr('disabled');
    });

    function sumarIndirectos() {
        var arr = [];
        $('[name="indirectos[]"]:checked').each(function () {
            var costoIndirecto = $(this).closest('td').next().find('input').val();
            arr.push(costoIndirecto);
            var sumaIndirectos = 0;
            arr.forEach(function (obj) {
                sumaIndirectos += parseFloat(obj);
            });
            $("#totalIndirectos").val(sumaIndirectos.toFixed(2));
        });
    }

    $('[name="indirectosInput[]"]').change(function () {
        sumarIndirectos();
        calcularCostos();
    });

    $('[name="indirectos[]"]').click(function () {
        var totalHours = parseFloat($("#cantidadHours").val());
        var multi = parseFloat($(this).closest('td').find('span').text());
        var ci = parseFloat(this.value * totalHours * multi);
        if ($(this).is(':checked') && totalHours == 0) {
            $(this).closest('td').next().find('input').val(0);
            sumarIndirectos();
        calcularCostos();
        } 
        else if ($(this).is(':checked') && totalHours > 0) {
            $(this).closest('td').next().find('input').val(parseFloat(ci.toFixed(2)));
            sumarIndirectos();
        calcularCostos();
        }  
        else if ($(this).not(':checked').length){
            $(this).closest('td').next().find('input').val(0);
            sumarIndirectos();
        calcularCostos();
        }
    });

    //LAB COST
    //Habilita la Edicion de los Costos (pencil button)
    $('[name="labEdit[]"]').click(function () {
        $(this).closest('td').find('input').removeAttr('disabled');
    });

    $('[name="labInput[]"]').change(function () {
        sumarLaboratory();
        calcularCostos();
    });

    $('[name="lab[]"]').click(function () {

        var multi = parseFloat($(this).closest('td').find('span').text());
        var cl = parseFloat(this.value * multi);
        if ($(this).is(':checked')) {
            $(this).closest('td').next().find('input').val(cl);
            sumarLaboratory();
        }
        else if ($(this).not(':checked').length){
            $(this).closest('td').next().find('input').val(0);
            sumarLaboratory();
        }
        
        calcularCostos();
    });

    function sumarLaboratory() {
        var arr = [];
        $('[name="lab[]"]:checked').each(function () {
            var costLab = $(this).closest('td').next().find('input').val();
            arr.push(costLab);
            var sumalab = 0;
            arr.forEach(function (obj) {
                sumalab += parseFloat(obj);
            });
            $("#totalLaboratory").val(sumalab);
        });
    }

    //Suma todos los costos y los imprime en algunos input
    function calcularCostos() {
        var suma = 0;
        suma = Number($("input[id='totalIndirectos']").val());
        suma += Number($("input[id='totalDirectos']").val());
        suma += Number($("input[id='totalOtros']").val());
        suma += Number($("input[id='totalLaboratory']").val());
        console.log(suma);
        $("#totalCostos").val(suma.toFixed(2));
        $("#testCost1").val(suma.toFixed(2));
        $("#testCost2").val(suma.toFixed(2));
        $("#totalCostPricing").val(suma.toFixed(2));
        $("#totalCostPricing2").val(suma.toFixed(2));
        $("#totalCostPricing3").val(suma.toFixed(2));
        $("#totalCostNon").val(suma.toFixed(2));
        $("#totalCost").val(suma.toFixed(2));
        $("#costPricingInput").val(suma.toFixed(2));
    };

    //============ PRICING =============

    

    // ========= keyup EVENTS ==========
    $('#totalProposed').keyup(function () {
        var proposed = parseFloat($('#totalProposed2').val());
        $('#totalProposed2').val(proposed.toFixed(2));
        $('#totalProposed3').val(proposed.toFixed(2));
    });

    $('#totalInsurance').keyup(function () {
        var insurance = parseFloat($('#totalInsurance').val());
        $('#totalInsurance2').val(insurance.toFixed(2));
        $('#totalInsurance3').val(insurance.toFixed(2));
    });
  
    //========== DISCOUNT ==========+

    $('#discountPricing').keyup(function () {
        var proposed = parseFloat($('#totalProposed2').val());
        var discount = parseFloat($('#discountPricing').val());
        var discountPercentage = parseFloat(discount / 100);
        var totalDiscountAmount = parseFloat(((proposed - discount) * discountPercentage));
        $('#totalDiscount').val(totalDiscountAmount.toFixed(2));
        $('#totalDiscount2').val(totalDiscountAmount.toFixed(2));

        $('#discountPricing2').val(discount);

        var totalPatientPortion = parseFloat((proposed - totalDiscountAmount));
        $('#totalPatientPortion2').val(totalPatientPortion.toFixed(2));
    });

    //CALCULATE
    $("#calculate").click(function () {
        if ($('#totalCostPricing').val() == 0 || $('#totalCostPricing').val() == "") {
            swal.fire({
                title: "Error",
                text: "Please calculate the costs in the cost tab",
                icon: "error"
            });
        }
        else{
            var cost = parseFloat($("#totalCostPricing").val());
            var proposed = parseFloat($('#totalProposed').val());
            var insurance = parseFloat($('#totalInsurance').val());
            var marginResult = 0;

            marginResult = parseFloat((((proposed - cost) / cost) * 100));

            $("#marginPricing").text(marginResult.toFixed(2));

            //print total without discount
            var totalPatientPortion = parseFloat((proposed - insurance));
            $('#totalPatientPortion').val(totalPatientPortion.toFixed(2));
            $('#totalPricingInput').val(totalPatientPortion.toFixed(2));
            $('#totalPricing').text(totalPatientPortion.toFixed(2));
            $('#toFinancingInput').val(totalPatientPortion.toFixed(2));
            $('#toFinancingInput1').val(totalPatientPortion.toFixed(2));
        }
    });

    //CALCULATE MARGIN WITH DISCOUNT
    $("#calculateWD").click(function () {
        if ($('#totalCostPricing2').val() == 0 || $('#totalCostPricing2').val() == "") {
            swal.fire({
                title: "Error",
                text: "Please calculate the costs in the cost tab",
                icon: "error"
            });
        }
        else {
            var cost = parseFloat($("#totalCostPricing2").val());
            var proposed = parseFloat($('#totalProposed2').val());
            var insurance = parseFloat($('#totalInsurance2').val());
            var discount = parseFloat($('#totalDiscount').val());
            var marginResult = 0;

            //print total without discount
            var totalPatientPortion = parseFloat((proposed - insurance - discount));
            var totalMargin = parseFloat((proposed - discount));
            $('#totalPatientPortion2').val(totalPatientPortion.toFixed(2));

            marginResult = parseFloat((((totalMargin - cost) / cost) * 100));

            $("#marginWD").text(marginResult.toFixed(2));
            $("#marginWDInput").text(marginResult.toFixed(2));
            $('#totalPricingInput').val(totalPatientPortion.toFixed(2));
            $('#totalPricing').text(totalPatientPortion.toFixed(2));
            $('#toFinancingInput').val(totalPatientPortion.toFixed(2));
            $('#toFinancingInput1').val(totalPatientPortion.toFixed(2));
        }
    });

    //CALCULATE MARGIN WITH EXTRA DISCOUNT
    $("#calculateWED").click(function () {
        if ($('#totalCostPricing2').val() == 0 || $('#totalCostPricing2').val() == "") {
            swal.fire({
                title: "Error",
                text: "Please calculate the costs in the cost tab",
                icon: "error"
            });
        }
        else {
            var cost = parseFloat($("#totalCostPricing2").val());
            var proposed = parseFloat($('#totalProposed2').val());
            var insurance = parseFloat($('#totalInsurance2').val());
            var discount = parseFloat($('#totalDiscount').val());
            var marginResult = 0;
            var extraDiscount = parseFloat($('#extraDiscount').val() / 100);
            var totalExtraDiscountAmount = parseFloat(parseFloat((proposed - discount) * extraDiscount));
            $('#totalExtraDiscount').val(totalExtraDiscountAmount.toFixed(2));

            //print total without discount
            var totalPatientPortion = parseFloat((proposed - insurance - discount - totalExtraDiscountAmount));
            var totalMargin = parseFloat((proposed - discount - totalExtraDiscountAmount));
            $('#totalPatientPortion3').val(totalPatientPortion.toFixed(2));

            marginResult = parseFloat((((totalMargin - cost) / cost) * 100));

            $("#marginWED").text(marginResult.toFixed(2));
            $("#marginWEDInput").text(marginResult.toFixed(2));

            $('#totalPricingInput').val(totalPatientPortion.toFixed(2));
            $('#totalPricing').text(totalPatientPortion.toFixed(2));
            $('#toFinancingInput').val(totalPatientPortion.toFixed(2));
            $('#toFinancingInput1').val(totalPatientPortion.toFixed(2));
        }

        //========== EXTRA DISCOUNT ==========

        $('#extraDiscount').change(function () {
            var extraDiscount = parseFloat($('#extraDiscount').val() / 100);
            var totalExtraDiscountAmount = parseFloat(parseFloat((proposed - discount) * extraDiscount));
            $('#totalExtraDiscount').val(totalExtraDiscountAmount.toFixed(2));

            var totalPricing = parseFloat((proposed - discount - extraDiscount));
            $('#totalPricingInput').val(totalPricing.toFixed(2));
            $('#totalPricing').text(totalPricing.toFixed(2));
            $('#toFinancingInput').val(totalPricing.toFixed(2));
            $('#toFinancingInput1').val(totalPricing.toFixed(2));
        });

    });

    //================ CHANGE TOTAL PROPOSED EVENT ================
    $('#totalProposed').keyup(function () {
        var proposed = parseFloat($('#totalProposed').val());
        $('#totalProposed2').val(proposed.toFixed(2));
        $('#totalProposed3').val(proposed.toFixed(2));
    });

    //========== EXTRA DISCOUNT ==========

    $('#extraDiscount').change(function () {
        var proposed = parseFloat($('#totalProposed3').val());
        var discount = parseFloat($('#totalDiscount2').val());
        var extraDiscount = parseFloat($('#extraDiscount').val() / 100);
        var totalExtraDiscountAmount = parseFloat(((proposed - discount) * extraDiscount));
        $('#totalExtraDiscount').val(totalExtraDiscountAmount.toFixed(2));

        var totalPricing = parseFloat((proposed - discount - extraDiscount));
        $('#totalPricingInput').val(totalPricing.toFixed(2));
        $('#totalPricing').text(totalPricing.toFixed(2));
    });

    $('#totalPricingInput').change(function () {

        //total to payment plan
        var proposed = parseFloat($('#totalProposed').val());
        var insurance = parseFloat($('#totalInsurance').val());
        var totalPriceWithoutLoan = parseFloat((proposed - insurance));
        $('#totalPriceWithoutLoanPP').val(totalPriceWithoutLoan.toFixed(2));

    });

    //=============== MARGIN WHEN UPDATE EXTRA DISCOUNT VALUE ============
    $('#extraDiscount').change(function () {
        var price = parseFloat($('#totalPricingInput').val());
        var cost = parseFloat($("#totalCostPricing").val());
        var marginResult = 0;

        marginResult = parseFloat((((price - cost) / cost) * 100));
        console.log(marginResult);
        $("#marginWithDiscount").text(marginResult.toFixed(2));
        $("#marginWithDiscountInput").val(marginResult.toFixed(2));
    });

    //TEST PRICING
    //Si ingresa el precio, obtiene el margin
    $("#testPrice1").keyup(function () {
        var price = parseFloat(parseFloat($("#testPrice1").val()));
        var cost = parseFloat(parseFloat($("#testCost1").val()));
        var marginResult = 0;

        marginResult = parseFloat((((price - cost) / cost) * 100));
        $("#testMargin1").val(marginResult.toFixed(2));
    });

    //Si ingresa el margin, obtiene el precio
    $("#testMargin2").keyup(function () {
        var margin2 = parseFloat(parseFloat($("#testMargin2").val()));
        var cost2 = parseFloat(parseFloat($("#testCost2").val()));
        var profit = parseFloat(parseFloat(((margin2 / 100) * cost2)));
        var priceResult = 0;

        priceResult = parseFloat((profit + cost2));
        $("#testPrice2").val(priceResult.toFixed(2));
    });

    $('#selectPrice').click(function () {
        $('#totalProposed').val($('#testPrice1').val());
        $('#totalProposed2').val($('#testPrice1').val());
        $('#totalProposed3').val($('#testPrice1').val());
        $('#marginPricing').text($('#testMargin1').val());
    });

    $('#selectMargin').click(function () {
        $('#marginPricing').text($('#testMargin2').val());
        $('#totalProposed').val($('#testPrice2').val());
        $('#totalProposed2').val($('#testPrice2').val());
        $('#totalProposed3').val($('#testPrice2').val());
    });

    // ################ PAYMENT PLAN TAB ##################
    //================== SELECT PAY AND PHASES =======================
    $("#phases").change(function () {
        //var valor = $(this).val();
        var valor = $('input[name="payMet[]"]:checked').val();
        var phases = $('#phases').val();
        if (valor == 'payplan') {
            $("#fin-div1").css("display", "none");
            $("#fin-div2").css("display", "none");
            if (phases == 1) {
                $("#pay-div1").css("display", "block");
                $("#pay-div2").css("display", "none");
            } else if (phases == 2) {
                $("#pay-div1").css("display", "block");
                $("#pay-div2").css("display", "block");
            }

        } else if (valor == 'financing') {
            $("#pay-div1").css("display", "none");
            $("#pay-div2").css("display", "none");
            if (phases == 1) {
                $("#fin-div1").css("display", "block");
                $("#fin-div2").css("display", "none");
            } else if (phases == 2) {
                $("#fin-div1").css("display", "block");
                $("#fin-div2").css("display", "block");
            }
        }
    });

    //================== CALCULATE PAYMENTS BY =======================
    $('#calculate_payments_by_PP1').change(function () {
        var opcion = $('#calculate_payments_by_PP1').val();
        if (opcion == 'nofp') {
            $("#paymentsTablePP1").empty();
            $('#payment_amount_PP1').css("display", "none");
            $('#number_of_payments_PP1').css("display", "block");
            $('#shedulePaymentsPP1_1').css("display", "block");
            $('#shedulePaymentsPP1_2').css("display", "none");
        } else if (opcion == 'pa') {
            $("#paymentsTablePP1").empty();
            $('#payment_amount_PP1').css("display", "block");
            $('#number_of_payments_PP1').css("display", "none");
            $('#shedulePaymentsPP1_1').css("display", "none");
            $('#shedulePaymentsPP1_2').css("display", "block");
        }
    });

    $('#calculate_payments_by_PP2').change(function () {
        var opcion = $('#calculate_payments_by_PP2').val();
        if (opcion == 'nofp') {
            $("#paymentsTablePP2").empty();
            $('#payment_amount_PP2').css("display", "none");
            $('#number_of_payments_PP2').css("display", "block");
            $('#shedulePaymentsPP2_1').css("display", "block");
            $('#shedulePaymentsPP2_2').css("display", "none");
        } else if (opcion == 'pa') {
            $("#paymentsTablePP2").empty();
            $('#payment_amount_PP2').css("display", "block");
            $('#number_of_payments_PP2').css("display", "none");
            $('#shedulePaymentsPP2_1').css("display", "none");
            $('#shedulePaymentsPP2_2').css("display", "block");
        }
    });

    //================== DEFINE AMOUNT TO FINANCING ======================
    $('#phases').change(function () {
        var phases = $(this).val();
        if (phases == 1) {
            $('#toFinDiv1').css("display", "block");
            $('#toFinDiv2').css("display", "none");
        } else if (phases == 2) {
            $('#toFinDiv1').css("display", "block");
            $('#toFinDiv2').css("display", "block");
        }
    });

    $('#toFinancingInput1').change(function () {
        var finAmount = parseFloat($('#toFinancingInput').val());
        var finAmount1 = parseFloat($('#toFinancingInput1').val());

        $('#toFinancingInput2').val(parseFloat(finAmount - finAmount1).toFixed(2));

    });

    // =========== TOTAL A FINANCIAR EN PAGOS  =============
    $('#dp1PP1').keyup(function () {
        var dp1 = parseFloat($('#dp1PP1').val());
        var dp2 = parseFloat($('#dp2PP1').val());
        var dp3 = parseFloat($('#dp3PP1').val());
        var toFinancing = parseFloat($('#toFinancingInput1').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp1').val(tfp1.toFixed(2));
    });

    $('#dp2PP1').keyup(function () {
        var dp1 = parseFloat($('#dp1PP1').val());
        var dp2 = parseFloat($('#dp2PP1').val());
        var dp3 = parseFloat($('#dp3PP1').val());
        var toFinancing = parseFloat($('#toFinancingInput1').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp1').val(tfp1.toFixed(2));
    });

    $('#dp3PP1').keyup(function () {
        var dp1 = parseFloat($('#dp1PP1').val());
        var dp2 = parseFloat($('#dp2PP1').val());
        var dp3 = parseFloat($('#dp3PP1').val());
        var toFinancing = parseFloat($('#toFinancingInput1').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp1').val(tfp1.toFixed(2));
    });

    $('#dp1PP2').keyup(function () {
        var dp1 = parseFloat($('#dp1PP2').val());
        var dp2 = parseFloat($('#dp2PP2').val());
        var dp3 = parseFloat($('#dp3PP2').val());
        var toFinancing = parseFloat($('#toFinancingInput2').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp2').val(tfp1.toFixed(2));
    });

    $('#dp2PP2').keyup(function () {
        var dp1 = parseFloat($('#dp1PP2').val());
        var dp2 = parseFloat($('#dp2PP2').val());
        var dp3 = parseFloat($('#dp3PP2').val());
        var toFinancing = parseFloat($('#toFinancingInput2').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp2').val(tfp1.toFixed(2));
    });

    $('#dp3PP2').keyup(function () {
        var dp1 = parseFloat($('#dp1PP2').val());
        var dp2 = parseFloat($('#dp2PP2').val());
        var dp3 = parseFloat($('#dp3PP2').val());
        var toFinancing = parseFloat($('#toFinancingInput2').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp2').val(tfp1.toFixed(2));
    });

    //=========== ADD PAYMENTS =============
    //PHASE ONE
    //CALCULATE BY NUMBER OF PAYMENTS
    $("#shedulePaymentsPP1_1").click(function () {
        var cantPays = $('#cantidadPaymentsPP1').val();
        var dp1 = parseFloat($('#dp1PP1').val());
        var dp2 = parseFloat($('#dp2PP1').val());
        var dp3 = parseFloat($('#dp3PP1').val());
        var toFinancing = parseFloat($('#toFinancingInput1').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp1').val(tfp1.toFixed(2));

        if ($("#paysEveryPP1").val() == 'Day') {
            var dias = 1;
        } else if ($("#paysEveryPP1").val() == 'Week') {
            var dias = 7;
        } else if ($("#paysEveryPP1").val() == 'Semi-Monthly') {
            var dias = 15;
        } else if ($("#paysEveryPP1").val() == 'Month') {
            var dias = 30;
        }

        $("#paymentsTablePP1").empty();

        $.ajax({
            type: "POST",
            url: 'include/loadPaymentsF1.php',
            data: {
                cantidad_pagos: cantPays,
                to_financing: tfp1,
                dias: dias
            },
            success: function (resp) {
                $('#paymentsTablePP1').html(resp);
                var rowCount = $('#paymentsTablePP1 tr').length;
                $('#nofpPP1').text(rowCount);
                console.log(rowCount);

                var arr = $('input[name="paysPP1[]"]').map(function () {
                    return this.value;
                }).get();

                var sumaPays = 0;
                arr.forEach(function (obj) {
                    sumaPays += parseFloat(obj);
                });

                $('#totalWLPP1').val(parseFloat(sumaPays + dp1 + dp2 + dp3).toFixed(2));
            }
        });

    });

    //CALCULATE BY PAY AMOUNT
    $("#shedulePaymentsPP1_2").click(function () {
        var payamount = $('#paysAmountPP1').val();
        var dp1 = parseFloat($('#dp1PP1').val());
        var dp2 = parseFloat($('#dp2PP1').val());
        var dp3 = parseFloat($('#dp3PP1').val());
        var toFinancing = parseFloat($('#toFinancingInput1').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp1').val(tfp1.toFixed(2));

        if ($("#paysEveryPP1").val() == 'Day') {
            var dias = 1;
        } else if ($("#paysEveryPP1").val() == 'Week') {
            var dias = 7;
        } else if ($("#paysEveryPP1").val() == 'Semi-Monthly') {
            var dias = 15;
        } else if ($("#paysEveryPP1").val() == 'Month') {
            var dias = 30;
        }

        $("#paymentsTablePP1").empty();

        $.ajax({
            type: "POST",
            url: 'include/loadPayments2F1.php',
            data: {
                paymentamount: payamount,
                to_financing: tfp1,
                dias: dias
            },
            success: function (resp) {
                $('#paymentsTablePP1').html(resp);

                var arr = $('input[name="paysPP1[]"]').map(function () {
                    return this.value;
                }).get();

                var sumaPays = 0;
                arr.forEach(function (obj) {
                    sumaPays += parseFloat(obj);
                });

                $('#totalWLPP1').val(parseFloat(sumaPays + dp1 + dp2 + dp3).toFixed(2));
            }
        });

    });

    // ================== PHASE TWO EVENT ==========================
    $("#shedulePaymentsPP2").click(function () {
        $("#paymentsTablePP2").empty();

        var cantPays = $('#cantidadPaymentsPP2').val();
        var dp1 = parseFloat($('#dp1PP2').val());
        var dp2 = parseFloat($('#dp2PP2').val());
        var dp3 = parseFloat($('#dp3PP2').val());
        var toFinancing = parseFloat($('#toFinancingInput2').val());
        var tfp2 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp2').val(tfp2.toFixed(2));
        funcAddPaymentsPP2(cantPays, dp1, dp2, dp3);
        console.log(cantPays, dp1, dp2, dp3);
        /* var rowCount = $('#paymentsTablePP1 tr').length;
        $('#cantidadPaymentsPP1').text(rowCount); */

        var arr = $('input[name="paysPP2[]"]').map(function () {
            return this.value;
        }).get();

        var sumaPays = 0;
        arr.forEach(function (obj) {
            sumaPays += parseFloat(obj);
        });

        $('#totalWLPP2').val(parseFloat(sumaPays + dp1 + dp2 + dp3).toFixed(2));

        //contar meses
        /* var dates = $('input[name="paysDatePP1[]"]').map(function () {
            return this.value;
        }).get(); */

    });

    //CALCULATE BY NUMBER OF PAYMENTS
    $("#shedulePaymentsPP2_1").click(function () {
        var cantPays = $('#cantidadPaymentsPP2').val();
        var dp1 = parseFloat($('#dp1PP2').val());
        var dp2 = parseFloat($('#dp2PP2').val());
        var dp3 = parseFloat($('#dp3PP2').val());
        var toFinancing = parseFloat($('#toFinancingInput2').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp2').val(tfp1.toFixed(2));

        if ($("#paysEveryPP2").val() == 'Day') {
            var dias = 1;
        } else if ($("#paysEveryPP2").val() == 'Week') {
            var dias = 7;
        } else if ($("#paysEveryPP2").val() == 'Semi-Monthly') {
            var dias = 15;
        } else if ($("#paysEveryPP2").val() == 'Month') {
            var dias = 30;
        }

        $("#paymentsTablePP2").empty();

        $.ajax({
            type: "POST",
            url: 'include/loadPaymentsF2.php',
            data: {
                cantidad_pagos: cantPays,
                to_financing: tfp1,
                dias: dias
            },
            success: function (resp) {
                $('#paymentsTablePP2').html(resp);
                var rowCount = $('#paymentsTablePP2 tr').length;
                $('#nofpPP2').text(rowCount);
                console.log(rowCount);

                var arr = $('input[name="paysPP2[]"]').map(function () {
                    return this.value;
                }).get();

                var sumaPays = 0;
                arr.forEach(function (obj) {
                    sumaPays += parseFloat(obj);
                });

                $('#totalWLPP2').val(parseFloat(sumaPays + dp1 + dp2 + dp3).toFixed(2));
            }
        });

    });

    //CALCULATE BY PAY AMOUNT
    $("#shedulePaymentsPP2_2").click(function () {
        var payamount = $('#paysAmountPP2').val();
        var dp1 = parseFloat($('#dp1PP2').val());
        var dp2 = parseFloat($('#dp2PP2').val());
        var dp3 = parseFloat($('#dp3PP2').val());
        var toFinancing = parseFloat($('#toFinancingInput2').val());
        var tfp1 = parseFloat(toFinancing - dp1 - dp2 - dp3);
        $('#tfp2').val(tfp1.toFixed(2));

        if ($("#paysEveryPP2").val() == 'Day') {
            var dias = 1;
        } else if ($("#paysEveryPP2").val() == 'Week') {
            var dias = 7;
        } else if ($("#paysEveryPP2").val() == 'Semi-Monthly') {
            var dias = 15;
        } else if ($("#paysEveryPP2").val() == 'Month') {
            var dias = 30;
        }

        $("#paymentsTablePP2").empty();

        $.ajax({
            type: "POST",
            url: 'include/loadPayments2F2.php',
            data: {
                paymentamount: payamount,
                to_financing: tfp1,
                dias: dias
            },
            success: function (resp) {
                $('#paymentsTablePP2').html(resp);

                var arr = $('input[name="paysPP2[]"]').map(function () {
                    return this.value;
                }).get();

                var sumaPays = 0;
                arr.forEach(function (obj) {
                    sumaPays += parseFloat(obj);
                });

                $('#totalWLPP2').val(parseFloat(sumaPays + dp1 + dp2 + dp3).toFixed(2));
            }
        });

    });

    function funcAddPaymentsPP2(cantidad, dp1, dp2, dp3) {
        var toFinancing = parseFloat($('#toFinancingInput2').val());
        var totalAmount = parseFloat(toFinancing - dp1 - dp2 - dp3);
        var cantidadPaymentsPP2 = cantidad;
        var payAmount = parseFloat(totalAmount / cantidadPaymentsPP2).toFixed(2);

        var f = new Date();
        var mes = (f.getMonth() + 1);
        if (mes < 10) {
            mes = '0' + mes;
        }

        var rowCount = $('#paymentsTablePP2 tr').length;
        var fecha = [];

        for (var i = 0; i < cantidad; i++) {
            if ($("#paysEveryPP1").val() == 'Day') {
                var fecha = f.getFullYear() + "-" + mes + "-" + (f.getDate() + i);
            } else if ($("#paysEveryPP1").val() == 'Week') {
                var fecha = f.getFullYear() + "-" + mes + "-" + (f.getDate() + (i * 7));
            } else if ($("#paysEveryPP1").val() == 'Semi-Monthly') {
                var fecha = f.getFullYear() + "-" + mes + "-" + (f.getDate() + (i * 15));
            } else if ($("#paysEveryPP1").val() == 'Month') {
                var fecha = f.getFullYear() + "-" + (mes + i) + "-" + f.getDate();
            }

            $("#paymentsTablePP2").append(
                `<tr>
                    <th scope="row" class="pays">
                        <span>Payment ` + (i + 1) + `</span>
                    </th>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend"><span
                                    class="input-group-text"><i
                                        class="fa fa-dollar-sign"></i></span>
                            </div>
                            <input type="number" class="form-control"
                                min="0" id="pay` + (i + 1) + `PP2" name="paysPP2[]" value="` + payAmount + `">
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend"><span
                                    class="input-group-text"><i
                                        class="fa fa-calendar"></i></span>
                            </div>
                            <input type="date" class="form-control" id="payDate` + (i + 1) + `PP2"
                             name="paysDatePP1[]" value="` + fecha + `">
                        </div>
                    </td>                    
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend"><span
                                    class="input-group-text"><i
                                        class="fa fa-calendar"></i></span>
                            </div>
                            <select class="form-control" id="wtp` + (i + 1) + `PP1" name="wtpPP1[]">
                                <option>Cash</option>
                                <option>Check</option>
                                <option>Credit Card</option>
                                <option>Debit Card</option>
                            </select>
                        </div>
                    </td>
                </tr>`
            );

            /* <td>
                        <button type="button" class="deletePayment mb-2 mr-2 btn btn-danger"><i class="fa fa-times"></i></button>
                    </td> */

            $(".deletePayment").on('click', function () {

                deletePaymentsPP2();
                //$('#cantidadPayments').text(rowCount);

            });
        }
    }

    function deletePaymentsPP2() {
        $("#paymentsTablePP2").on("click", ".deletePayment", function (e) {
            e.preventDefault();
            if ($('#paymentsTablePP1 tr').length > 1) {
                $(this).closest("tr").remove();
                $('th.pays').text(function (i) {
                    return 'Payment ' + (i + 1);
                });
            }

            return false;

        });
    }

    // ============ NO PAYMENT PLAN ==============
    $('#calulateNon').click(function () {
        var cost = parseFloat($("#totalCostNon").val());
        var proposed = parseFloat($('#totalProposedNon').val());
        var insurance = parseFloat($('#totalInsuranceNon').val());
        var marginResult = 0;
        var marginwd = 0;
        var discountAmount = parseFloat($('#discountAmountNon').val());

        //margin 
        marginResult = parseFloat((((proposed - cost) / cost) * 100));
        $("#marginNon").val(marginResult.toFixed(2));

        var totwd = parseFloat(proposed - insurance);
        $('#totalWithoutDiscountNon').val(totwd.toFixed(2));

        //print total with discount
        var total = parseFloat((proposed - insurance - discountAmount));
        $('#totalNon').val(total.toFixed(2));

        //margin 
        marginwd = parseFloat((((total - cost) / cost) * 100));
        $("#marginwdNon").val(marginwd.toFixed(2));
    });

    $('#totalProposedNon').click(function () {
        var amount = parseFloat($('#totalProposedNon').val());
        var insurance = parseFloat($('#totalInsuranceNon').val());
        var cost = parseFloat($('#totalCostNon').val());
        var twd = parseFloat(amount - insurance).toFixed(2);

        $('#totalWithoutDiscountNon').val(twd.toFixed(2));
    });

    //with discount
    $('#discountNon').change(function () {
        var discount = $('#discountNon').val();
        var amount = parseFloat($('#totalWithoutDiscountNon').val());

        var disAmount = parseFloat((discount / 100) * amount);
        $('#discountAmountNon').val(disAmount.toFixed(2));

    });

    //Asignando valor de descuento
    $('#discountAmountNon').keyup(function () {
        var discountAmount = $('#discountAmountNon').val();
        var amount = parseFloat($('#totalWithoutDiscountNon').val());

        var discountPercent = parseFloat((discountAmount / amount) * 100);
        $('#discountNon').val(discountPercent.toFixed(1));

    });

    //Asignando el precio al que se va a dar la consulta
    $('#totalNon').keyup(function () {
        var cost = parseFloat($("#totalCostNon").val());
        var proposed = parseFloat($('#totalProposedNon').val());
        var insurance = parseFloat($('#totalInsuranceNon').val());
        var marginResult = 0;
        var marginwd = 0;

        var totwd = parseFloat(proposed - insurance);
        $('#totalWithoutDiscountNon').val(totwd.toFixed(2));

        var totalNon = $('#totalNon').val();
        var subtotal = parseFloat(proposed - insurance - totalNon);
        $('#discountAmountNon').val(subtotal.toFixed(2));

        var discountPercent = parseFloat((subtotal / totwd) * 100);
        $('#discountNon').val(discountPercent.toFixed(1));

        //margin 
        marginResult = parseFloat((((proposed - cost) / cost) * 100));
        $("#marginNon").val(marginResult.toFixed(2));

        //margin with discount
        marginwd = parseFloat((((subtotal - cost) / cost) * 100));
        $("#marginwdNon").val(marginwd.toFixed(2));

    });

    // ============ SAVE TABS =============
    $('#savePatient').click(function () {
        const postData = {
            id_paciente: $('#patient_id_tittle').text(),
            id_paciente_eagle: $('#PatientIDhidden').val(),
            PatientFirstName: $('#PatientFirstName').val(),
            PatientLastName: $('#PatientLastName').val(),
            genre: $('input[name="genre[]"]:checked').val(),
            PatientBirthDate: $('#PatientBirthDate').val(),
            homeA: $('#PatientAddress').val(),
            occupation: $('#occupation').val(),
            workA: $('#workA').val(),
            language: $('#language').val(),
            contact_phone_number: $('#contact_phone_number').val(),
            contact_home_phone_number: $('#contact_home_phone_number').val(),
            contact_email: $('#contact_email').val(),
            status: $('input[name="status[]"]:checked').val(),
            emergency_contact_person_name: $('#emergency_contact_person_name').val(),
            emergency_contact_relationship: $('#emergency_contact_relationship').val(),
            emergency_contact_phone_number: $('#emergency_contact_phone_number').val(),
            emergency_contact_email: $('#emergency_contact_email').val(),
            wifeName: $('#wifeName').val(),
            motherName: $('#motherName').val(),
            fatherName: $('#fatherName').val(),
            childrens: $('#childrens').val(),
            siblings: $('#siblings').val()
        }

        //confirma si ha seleccionado un paciente
        if ($('#PatientIDhidden').val() == 0) {
            swal.fire({
                title: "Alert",
                text: "You must choose a patient to continue",
                type: "warning",
                icon: "warning"
            });
        } else {
            $.ajax({
                type: 'POST',
                url: 'jobnumber/add-patient.php',
                data: postData,
                success: function (resp) {
                    if (resp == "Success") {
                        swal.fire({
                            title: "Success",
                            text: "Patient Information Has been entered successfully",
                            type: "success",
                            icon: "success"
                        }).then(function () {
                            //location.reload();
                            //$('.body-tabs a[href="#sessions"]').tab('show');
                            //$('.body-tabs > .active').next('li').find('a').trigger('click');
                            var next = $('.nav-link > .active').next('li');
                            if (next.length) {
                                next.find('a').trigger('click');
                            } else {
                                $('#myTabs a:first').tab('show');
                            }
                        });
                    } else if (resp == "Error") {
                        swal.fire({
                            title: "Error",
                            text: "Patient Could Not Be Saved. Please try again",

                            icon: "error"
                        });
                    } else {
                        swal.fire({
                            title: "Error",
                            text: resp,

                            icon: "error"
                        });
                    }
                }
            });
        }

    });

    //Treatment and Sesions tab
    $('#saveSessions').click(function () {
        if ($('#job_code_tittle_hidden').val() == 0 || $('#job_code_tittle_hidden').val() == "") {
            swal.fire({
                title: "Error",
                text: "Please select a clinic and define procedures to continue",
                icon: "error"
            });
        } else if ($('[name="diagnostico[]"]:checked').length == 0) {
            swal.fire({
                title: "Error",
                text: "Please check 1 or more diagnistic to continue",
                icon: "error"
            });
        } else if ($('#sessionsTable tr').length == 0) {
            swal.fire({
                title: "Error",
                text: "Please define 1 or more session(s) to continue",
                icon: "error"
            });
        } else {

            var sesion = [];
            var adacode = [];
            var fecha = [];
            var duracion = [];
            var towork = [];

            $('.session').each(function () {
                sesion.push($(this).text());
            });
            $('[name="adacodes[]"]').each(function () {
                adacode.push($(this).val());
            });
            $('[name="fechaSession[]"]').each(function () {
                fecha.push($(this).val());
            });
            $('[name="duration[]"] option:selected').each(function () {

                var decimalTimeString = $(this).val();
                var n = new Date(0, 0);
                n.setSeconds(+decimalTimeString * 60 * 60);
                var a = n.toTimeString().slice(0, 8);

                duracion.push(a);
            });
            $('[name="parts[]"]').each(function () {
                towork.push($(this).val());
            });

            var proce = $('[name="procedimiento[]"]:checked').map(function () {
                return this.value;
            }).get();

            var diag = $('[name="diagnostico[]"]:checked').map(function () {
                return this.value;
            }).get();

            var procedures = proce.join(',');
            var diagnostic = diag.join(',');

            const postTSData = {

                //job_number info
                job_sesiones: $('#cantidadSessions').text(),
                id_paciente: $('#patient_id_tittle').text(),
                id_clinica: $('#job_clinica').val(),
                job_number: $('#job_code_tittle_hidden').val(),
                paymethod: $('input[name="paymethod[]"]:checked').val(),

                //diagnostic and procedures
                id_procedimiento: procedures,
                id_diagnostico: diagnostic, //cambiar nombre en php
                diagnostico_notas: $('#diag_notas').val(),

                //Sessions table
                sesion: sesion,
                procedures: adacode,
                fecha: fecha,
                duracion: duracion,
                tooth: towork
            }

            $.ajax({
                type: 'POST',
                url: 'jobnumber/add-treatment-session.php',
                data: postTSData,
                success: function (resp) {
                    if (resp == "Success") {
                        swal.fire({
                            title: "Success",
                            text: "Treatment and Sessions Information Has been entered successfully",
                            icon: "success"
                        }).then(function () {
                            //Load Fininancing info 

                            var pac = $('#patient_id_tittle').text();
                            const dataFin = {
                                id_paciente: pac
                            }
                            $.ajax({
                                type: 'POST',
                                url: 'jobnumber/load-financing-info.php',
                                data: dataFin,
                                success: function (resp) {
                                    $('#ssn').val(resp.ssn);
                                    $('#tax_id').val(resp.tax_id);
                                    $('#citizen_ship_status').val(resp.citizen_status);
                                    $('#employee_status').val(resp.employee_status);
                                    $('#total_gross_income').val(resp.total_gross_income);
                                    $('#total_gross_income_type').val(resp.total_gross_income_type);
                                    $('#rent_payment').val(resp.rent_payment);
                                    $('#bank_account_own').val(resp.bank);
                                }
                            });

                        });
                    } else if (resp == "Error") {
                        swal.fire({
                            title: "Error",
                            text: "Treatment and Sessions Information Could Not Be Saved. Please try again",
                            icon: "error"
                        });
                    } else {
                        swal.fire({
                            title: "Error",
                            text: resp,
                            icon: "error"
                        });
                    }
                }
            });
        }
    });

    // =============== COSTS TAB ==================

    // ************** SINGLE COST *************
    $('.costos').click(function () {
        var name = $(this).text();

        $.ajax({
            url:"jobnumber/get-cost-info.php",
            type:"POST",
            data:{name:name},
            dataType: 'JSON',
            success:function(data){
                var texto = "<b>Cost Type: </b>" + " (" + data[0]["type"] + ")"
                + "<br><b>Cost Name: </b>" + data[0]["name"] 
                + "<br><b>Amount (Per Hour): </b>$" + data[0]["amount"] ; //Costo Info

                swal.fire({
                    title: "Cost Info", //Title
                    html: texto,
                    icon: "info",
                });
            }
        });
    });

    // ****** SAVES SELECED COST ******
    $('#saveCosts').click(function () {

        var arr = [];
        var costo_amount = [];

        $('[name="directos[]"]:checked').each(function () {
            var nombre = $(this).closest('td').prev().find('input').val();
            var costo = parseFloat($(this).closest('td').next().find('input').val());
            arr.push(nombre);
            costo_amount.push(costo);
        });
        $('[name="otrosDirectos[]"]:checked').each(function () {
            var nombre = $(this).closest('td').prev().find('input').val();
            var costo = parseFloat($(this).closest('td').next().find('input').val());
            arr.push(nombre);
            costo_amount.push(costo);
        });
        $('[name="indirectos[]"]:checked').each(function () {
            var nombre = $(this).closest('td').prev().find('input').val();
            var costo = parseFloat($(this).closest('td').next().find('input').val());
            arr.push(nombre);
            costo_amount.push(costo);
        });
        $('[name="lab[]"]:checked').each(function () {
            var nombre = $(this).closest('td').prev().find('input').val();
            var costo = parseFloat($(this).closest('td').next().find('input').val());
            arr.push(nombre);
            costo_amount.push(costo);
        });
        console.log(arr);
        console.log(costo_amount);

        const postCData = {

            //job_number info
            job_number: $('#job_code_tittle_hidden').val(),

            //diagnostic and procedures
            costo: arr,
            valor: costo_amount
        }

        $.ajax({
            type: 'POST',
            url: 'jobnumber/add-costs.php',
            data: postCData,
            success: function (resp) {
                if (resp == "Success") {
                    swal.fire({
                        title: "Success",
                        text: "Costs Has been entered successfully",
                        icon: "success"
                    }).then(function () {

                    });
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Error",
                        text: "Costs Could Not Be Saved. Please try again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Error",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });

    //Pricing tab
    $('#savePricing').click(function () {

        if ($('#totalPricingInput').val() == 0 || $('#totalPricingInput').val() == "") {
            swal.fire({
                title: "Error",
                text: "Please click the calculate button to determine the total",
                icon: "error"
            });
        }
        else{
            const postPrData = {

                //job_number info
                job_number: $('#job_code_tittle_hidden').val(),
    
                //data
                total_proposed: $('#totalProposed').val(),
                total_insurance: $('#totalInsurance').val(),
                total_cost: $('#totalCostPricing').val(),
                margin_percent: $('#marginPricing').text(),
                discount: $('#discountPricing').val(),
                discount_amount: $('#totalDiscount').val(),
                extra_discount: $('#extraDiscount').val(),
                extra_discount_amount: $('#totalExtraDiscount').val(),
                margin_wd_percent: $('#marginWD').text(),
                margin_wed_percent: $('#marginWED').text(),
                total: $('#totalPricingInput').val()
            }
    
            $.ajax({
                type: 'POST',
                url: 'jobnumber/add-pricing.php',
                data: postPrData,
                success: function (resp) {
                    if (resp == "Success") {
                        swal.fire({
                            title: "Success",
                            text: "Pricing Has been entered successfully",
                            icon: "success"
                        }).then(function () {});
                    } else if (resp == "Error") {
                        swal.fire({
                            title: "Error",
                            text: "Pricing Could Not Be Saved. Please try again",
                            icon: "error"
                        });
                    } else {
                        swal.fire({
                            title: "Error",
                            text: resp,
                            icon: "error"
                        });
                    }
                }
            });
        }

        
    });

    //Update Pay method
    $('[name="paymethod[]"]').click(function () {
        const paymethodData = {

            //job_number info
            job_number: $('#job_code_tittle_hidden').val(),

            //paymethod
            paymethod: $('input[name="paymethod[]"]:checked').val()
        }

        $.ajax({
            type: 'POST',
            url: 'jobnumber/update-paymethod.php',
            data: paymethodData,
            success: function (resp) {
                if (resp == "Success") {
                    swal.fire({
                        title: "Success",
                        text: "Payment Method Has been defined successfully",
                        icon: "success"
                    }).then(function () {});
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Error",
                        text: "Payment Method Not Be defined. Please try again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Error",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });

    //Payment Plan tab
    $('#savePP').click(function () {

        var f1_pay_name = [];
        var f1_pay_amount = [];
        var f1_pay_date = [];
        var f1_pay_wtp = [];

        $('.pays').each(function () {
            f1_pay_name.push($(this).text());
        });
        $('[name="paysPP1[]"]').each(function () {
            f1_pay_amount.push($(this).val());
        });
        $('[name="paysDatePP1[]"]').each(function () {
            f1_pay_date.push($(this).val());
        });
        $('[name="wtpPP1[]"] option:selected').each(function () {
            f1_pay_wtp.push($(this).val());
        });

        var f2_pay_name = [];
        var f2_pay_amount = [];
        var f2_pay_date = [];
        var f2_pay_wtp = [];

        $('.pays2').each(function () {
            f2_pay_name.push($(this).text());
        });
        $('[name="paysPP2[]"]').each(function () {
            f2_pay_amount.push($(this).val());
        });
        $('[name="paysDatePP2[]"]').each(function () {
            f2_pay_date.push($(this).val());
        });
        $('[name="wtpPP2[]"] option:selected').each(function () {
            f2_pay_wtp.push($(this).val());
        });

        if ($('#phases').val() == 1) {
            const postPPData = {

                //job_number info
                job_number: $('#job_code_tittle_hidden').val(),

                //financing info
                ssn: $('#ssn').val(),
                tax_id: $('#tax_id').val(),
                citizen_status: $('#citizen_ship_status').val(),
                employee_status: $('#employee_status').val(),
                total_gross_income: $('#total_gross_income').val(),
                total_gross_income_type: $('#total_gross_income_type').val(),
                rent_payment: $('#rent_payment').val(),
                bank: $('#bank_account_own').val(),

                phases: $('#phases').val(),

                //phase 1
                dp1f1: $('#dp1PP1').val(),
                datedp1f1: $('#dp1DPP1').val(),
                wtpdp1f1: $('#wtp-dp1PP1').val(),

                dp2f1: $('#dp2PP1').val(),
                datedp2f1: $('#dp2DPP1').val(),
                wtpdp2f1: $('#wtp-dp2PP1').val(),

                dp3f1: $('#dp3PP1').val(),
                datedp3f1: $('#dp3DPP1').val(),
                wtpdp3f1: $('#wtp-dp3PP1').val(),

                totalwl: $('#totalWLPP1').val(),
                imp: $('#interesMPP1').val(),
                ima: $('#interestAmountPP1').val(),
                nofm: $('#numberMPP1').val(),
                total_loan: $('#totalPriceWithLoanPP1').val(),

                //pagos phase 1
                pagos_name_1: f1_pay_name,
                pagos_amount_1: f1_pay_amount,
                pagos_date_1: f1_pay_date,
                pagos_wtp_1: f1_pay_wtp,
            }

            $.ajax({
                type: 'POST',
                url: 'jobnumber/add-payment-plan-1.php',
                data: postPPData,
                success: function (resp) {
                    if (resp == "Success") {
                        swal.fire({
                            title: "Success",
                            text: "Payment Plan Has been entered successfully",
                            icon: "success"
                        }).then(function () {
                            window.location = "jobs.php";
                        });
                    } else if (resp == "Error") {
                        swal.fire({
                            title: "Error",
                            text: "Payment Plan Could Not Be Saved. Please try again",
                            icon: "error"
                        });
                    } else {
                        swal.fire({
                            title: "Error",
                            text: resp,
                            icon: "error"
                        });
                    }
                }
            });
        } else if ($('#phases').val() == 2) {
            const postPPData = {

                //job_number info
                job_number: $('#job_code_tittle_hidden').val(),

                //financing info
                ssn: $('#ssn').val(),
                tax_id: $('#tax_id').val(),
                citizen_status: $('#citizen_ship_status').val(),
                employee_status: $('#employee_status').val(),
                total_gross_income: $('#total_gross_income').val(),
                total_gross_income_type: $('#total_gross_income_type').val(),
                rent_payment: $('#rent_payment').val(),
                bank: $('#bank_account_own').val(),

                phases: $('#phases').val(),

                //phase 1
                dp1f1: $('#dp1PP1').val(),
                datedp1f1: $('#dp1DPP1').val(),
                wtpdp1f1: $('#wtp-dp1PP1').val(),

                dp2f1: $('#dp2PP1').val(),
                datedp2f1: $('#dp2DPP1').val(),
                wtpdp2f1: $('#wtp-dp2PP1').val(),

                dp3f1: $('#dp3PP1').val(),
                datedp3f1: $('#dp3DPP1').val(),
                wtpdp3f1: $('#wtp-dp3PP1').val(),

                totalwl: $('#totalWLPP1').val(),
                imp: $('#interesMPP1').val(),
                ima: $('#interestAmountPP1').val(),
                nofm: $('#numberMPP1').val(),
                total_loan: $('#totalPriceWithLoanPP1').val(),

                //pagos phase 1
                pagos_name_1: f1_pay_name,
                pagos_amount_1: f1_pay_amount,
                pagos_date_1: f1_pay_date,
                pagos_wtp_1: f1_pay_wtp,

                //phase 2
                dp1f2: $('#dp1PP2').val(),
                datedp1f2: $('#dp1DPP2').val(),
                wtpd1f2: $('#wtp-dp1PP2').val(),
                dp2f2: $('#dp2PP2').val(),
                datedp2f2: $('#dp2DPP2').val(),
                wtpd2f2: $('#wtp-dp2PP2').val(),
                dp3f2: $('#dp3PP2').val(),
                datedp3f2: $('#dp3DPP2').val(),
                wtpdp3f2: $('#wtp-dp3PP2').val(),

                totalwl2: $('#totalWLPP2').val(),
                imp2: $('#interesMPPF2').val(),
                ima2: $('#interestAmountPPF2').val(),
                nofm2: $('#numberMPPF2').val(),
                total_loan2: $('#totalPriceWithLoanPPF2').val(),

                //pagos phase 2
                pagos_name_2: f2_pay_name,
                pagos_amount_2: f2_pay_amount,
                pagos_date_2: f2_pay_date,
                pagos_wtp_2: f2_pay_wtp
            }

            $.ajax({
                type: 'POST',
                url: 'jobnumber/add-payment-plan-2.php',
                data: postPPData,
                success: function (resp) {
                    if (resp == "Success") {
                        swal.fire({
                            title: "Success",
                            text: "Payment Plan Has been entered successfully",
                            icon: "success"
                        }).then(function () {
                            window.location = "jobs.php";
                        });
                    } else if (resp == "Error") {
                        swal.fire({
                            title: "Error",
                            text: "Payment Plan Could Not Be Saved. Please try again",
                            icon: "error"
                        });
                    } else {
                        swal.fire({
                            title: "Error",
                            text: resp,
                            icon: "error"
                        });
                    }
                }
            });
        }
    });

    //No Payment Plan tab
    $('#saveNoPP').click(function () {
        if ($('[name="paymethod[]"]:checked').val() == "PAYMENT PLAN") {
            swal.fire({
                title: "Error",
                text: "Please select 'NO PLAYMENT PLAN (ONE PAY)' as pay method in Pricing Tab to continue",
                icon: "error"
            });
        } else if ($('[name="paymethod[]"]:checked').val() == "NO PLAYMENT PLAN") {


            const postNoPPData = {

                //job_number info
                job_number: $('#job_code_tittle_hidden').val(),

                //data
                date: $('#dateNon').val(),
                way_to_pay: $('#wtpNon').val(),
                total_proposed: $('#totalProposedNon').val(),
                total_insurance: $('#totalInsuranceNon').val(),
                total_cost: $('#totalCostNon').val(),
                margin_percent: $('#marginNon').val(),
                discount: $('#discountNon').val(),
                discount_amount: $('#discountAmountNon').val(),
                margin_wd: $('#marginwdNon').val(),
                totalwd: $('#totalWithoutDiscountNon').val(),
                total: $('#totalNon').val()
            }

            $.ajax({
                type: 'POST',
                url: 'jobnumber/add-no-payment-plan.php',
                data: postNoPPData,
                success: function (resp) {
                    if (resp == "Success") {
                        swal.fire({
                            title: "Success",
                            text: "Pricing Has been entered successfully",
                            icon: "success"
                        }).then(function () {
                            window.location = "jobs.php";
                        });
                    } else if (resp == "Error") {
                        swal.fire({
                            title: "Error",
                            text: "Pricing Could Not Be Saved. Please try again",
                            icon: "error"
                        });
                    } else {
                        swal.fire({
                            title: "Error",
                            text: resp,
                            icon: "error"
                        });
                    }
                }
            });
        }
    });


});