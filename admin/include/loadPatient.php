<?php 
    include("eagle_con.php");
    require("database.php");
    include("funciones.php");

    $codigo_paciente = $_GET['term'];

    #Sql anywhere
    $query = "SELECT TOP 30 * FROM patient where patient_id LIKE '".$codigo_paciente."%' or first_name LIKE '".$codigo_paciente."%' or last_name LIKE '".$codigo_paciente."%';";
    $result = odbc_exec($conexion_anywhere,$query);
    
    $json = array();

    while (odbc_fetch_row($result)) 
    {
        $json[]=array(
            'value' => odbc_result($result,'patient_id'),
            'label' => odbc_result($result,'patient_id') .': '.odbc_result($result,'last_name').', '.odbc_result($result,'first_name'),
            'patient_id' => odbc_result($result,'patient_id'),
            'first_name' => odbc_result($result,'first_name'), 
            'last_name' => odbc_result($result,'last_name'),
            'address_1' => odbc_result($result,'address_1'),
            'address_2' => odbc_result($result,'address_2'),
            'city' => odbc_result($result,'city'),
            'state' => odbc_result($result,'state'),
            'zipcode' => odbc_result($result,'zipcode'),
            'home_phone' => odbc_result($result,'home_phone'),
            'cell_phone' => odbc_result($result,'cell_phone'),
            'sex' => odbc_result($result,'sex'),
            'marital_status' => odbc_result($result,'marital_status'),
            'birth_date' => odbc_result($result,'birth_date'),
            'email_address' => odbc_result($result,'email_address')
        );
    }

    function jsonEncodeArray( $array ){
        array_walk_recursive( $array, function(&$item) { 
           $item = utf8_encode( $item ); 
        });
        return json_encode( $array );
    }
    echo jsonEncodeArray($json);
?>