<?php
        if(isset($_SESSION['user']) && empty($_SESSION['user']) || empty($_SESSION['id']) || empty($_SESSION['rol']))
        {
            unset($_SESSION['user']);
            unset($_SESSION['id']);
            unset($_SESSION['rol']);
            session_destroy();
            header("Location: ../index.php");
        }
        
    ?>