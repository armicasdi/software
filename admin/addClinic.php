<?php 
    include 'include/funciones.php';
?>
<!doctype html>
<html lang="en">

<head>
    <?php include 'head.php'; ?>

    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include 'navbar.php';
        ?>
        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-pen icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div>New Clinic
                                    <div class="page-title-subheading">Register a new clinic in the following form
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <h5 class="card-title">Basic Information</h5>
                                    <form class="needs-validation" novalidate>
                                        <div class="form-row">
                                            <div class="col-md-12 mb-3">
                                                <label for="validationCustom01"></label>
                                                <button type="button" class="btn btn-secondary">
                                                    Clinic ID <span class="badge badge-light" id="clinicID" name="clinicID"></span>
                                                </button>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom01">Clinic Number</label>
                                                <input type="number" class="form-control" id="clinicNumber" name="clinicNumber" required>
                                                <div class="valid-feedback">
                                                    Looks good!
                                                </div>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <label for="validationCustom02">Clinic Name</label>
                                                <input type="text" class="form-control" id="clinicName" name="clinicName"
                                                    placeholder="Input the name of the clinic" required>
                                                <div class="valid-feedback">
                                                    Looks good!
                                                </div>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <label for="validationCustom02">Clinic Address</label>
                                                <textarea type="text" rows="3" class="form-control" id="clinicAddress" name="clinicAddress"
                                                    placeholder="Input the address"></textarea>
                                                <div class="valid-feedback">
                                                    Looks good!
                                                </div>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <label for="validationCustom02">Clinic Manager</label>
                                                <!-- <input type="text" class="form-control" id="validationCustom02"
                                                    placeholder="" value="" required> -->

                                                    <button type="button" class="btn btn-secondary" data-target=".tooth" data-toggle="modal">
                                                    tooths
                                                </button>
                                                <div class="valid-feedback">
                                                    Looks good!
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include 'footer.php'; ?>
            </div>
            <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
         $(document).ready(function () {
            $("#clinicNumber").change(function () {
                    var numero = $("#clinicNumber").val();
                    var clinic_id = ('00' + numero).slice(-3);
                    console.log(clinic_id);
                    $("#clinicID").text(clinic_id);
                });

            $("#clinicName").keypress(function () {
                var clinicName = $('#clinicName').val();
                var letra = clinicName.charAt(0);
                letra = letra.toUpperCase();
                console.log(letra);

                
            });
         });
    </script>
</body>

</html>

<!-- Define Surface Modal -->

<div class="modal fade tooth" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tooth and Surface</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                <h3>Session :</h3><br>
                        <p>Define Tooth(s) to work in this Session</p>
                    <div class="d-flex justify-content-center row">
                        
                        <?php
                        $th=0;
                            for($th=1; $th<17; $th++){
                                $tooth = $th;
                        ?>
                        <button class="btn-circle btn-circle-sm m-1 btn-transition btn btn-outline-alternate" name>
                            <b><?php echo $tooth;?></b>
                        </button>
                        <!-- <div role="group" class="btn-group-lg btn-group btn-group-toggle btn-circle btn-circle-sm m-1" data-toggle="buttons">
                            <button type="button" class="btn-transition btn btn-outline-alternate btn-circle btn-circle-sm m-1" name="tooth[]"
                                        id="<?php echo $tooth;?>">
                                <b><?php echo $tooth;?></b>
                            </button>
                        </div> -->
                        <?php
                                $tooth++;
                            }
                        ?>
                    </div>
                    <div class="d-flex justify-content-center row">
                        <?php
                            for($th=32; $th>16; $th--){
                                $tooth = $th;
                        ?>
                        <button
                            class="btn btn-outline-alternate btn-circle btn-circle-sm m-1"><b><?php echo $tooth;?></b></button>
                        <?php
                                $tooth--;
                            }
                        ?>
                    </div>

                    <div class="d-flex justify-content-center row">
                        <button class="mb-2 mr-2 btn-transition btn btn-outline-secondary">UA</button>
                        <button class="mb-2 mr-2 btn-transition btn btn-outline-secondary">LA</button>
                        <?php
                            for( $char = 65; $char<75; $char++) {
                        ?>
                        <button
                            class="btn btn-outline-danger btn-circle btn-circle-sm m-1"><b><?php echo chr($char);?></b></button>
                        <?php
                            }
                        ?>
                        <button class="mb-2 mr-2 btn-transition btn btn-outline-secondary">UR</button>
                        <button class="mb-2 mr-2 btn-transition btn btn-outline-secondary">UL</button>
                    </div>

                    <div class="d-flex justify-content-center row">
                        <button class="mb-2 mr-2 btn-transition btn btn-outline-secondary">Pm</button>
                        <button class="mb-2 mr-2 btn-transition btn btn-outline-secondary">Or</button>
                        <?php
                            for( $char = 84; $char>74; $char--) {
                        ?>

                        <button
                            class="btn btn-outline-danger btn-circle btn-circle-sm m-1"><b><?php echo chr($char);?></b></button>

                        <?php
                            }
                        ?>
                        <button class="mb-2 mr-2 btn-transition btn btn-outline-secondary">LR</button>
                        <button class="mb-2 mr-2 btn-transition btn btn-outline-secondary">LL</button>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <p>Selected Tooth(s)</p>
                            <div class="text-center">
                                <!-- <div class="font-icon-wrapper font-icon-lg">
                                    <img src="" id="toothImg" />
                                    <p><span class="badge badge-pill badge-primary" id="toothNumber"></span></p>
                                </div> -->

                                <div class="font-icon-wrapper font-icon-lg"><img src="images/ic-dental/DN1.png" />
                                    <p><span class="badge badge-pill badge-primary">1</span></p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <p>Surface</p>

                            <div class="text-center" id="surfaceButtons">
                                <div role="group" class="btn-group-lg btn-group btn-group-toggle" data-toggle="buttons">
                                    <input type="button" class="btn-transition btn btn-outline-danger" name="surface[]"
                                        id="buccal" value="BucMFacl">
                                </div>
                                <br />
                                <div role="group" class="btn-group-lg btn-group btn-group-toggle" data-toggle="buttons">
                                    <input type="button" class="btn-transition btn btn-outline-alternate"
                                        name="surface[]" id="mesial" value="Mesial">
                                    <input type="button" class="btn-transition btn btn-outline-alternate"
                                        name="surface[]" id="occlusal" value="OccMnesl">
                                    <input type="button" class="btn-transition btn btn-outline-alternate"
                                        name="surface[]" id="distal" value="Distal">
                                </div>
                                <br />
                                <div role="group" class="btn-group-lg btn-group btn-group-toggle" data-toggle="buttons">
                                    <input type="button" class="btn-transition btn btn-outline-danger" name="surface[]"
                                        id="lingual" value="Lingual">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <p>To work</p>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button class="btn btn-alternate"> TOOTHS </button>
                                </div>
                                <input type="text" name="parts" class="form-control" id="parts" />
                            </div>
                            <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button class="btn btn-alternate"> SURFACE </button>
                                </div>
                                <input class="form-control" type="text" id="surface" name="surface">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Ok</button>
            </div>
        </div>
    </div>
</div>