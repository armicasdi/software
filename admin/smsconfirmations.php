<?php 
    include 'include/funciones.php';
?>

<!doctype html>
<html lang="en">

<head>
    <title>PPS | SMS Confirmations</title>
    <?php include 'head.php'; ?>
    <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php include 'navbar.php'; ?>

        <div class="app-main">
            <?php include 'sidebar.php'; ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-paper-plane icon-gradient bg-happy-itmeo">
                                    </i>
                                </div>
                                <div>Sms Confirmations
                                    <div class="page-title-subheading">This is the list of Appointments to Confirm
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">

                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                <span>To Confirm</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                <span>Sent Messages</span>
                            </a>
                        </li>
                    </ul> -->

                    <!-- <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">List of SMS Confirmations</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="example">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">ID</th>
                                                            <th class="text-center">Patient</th>
                                                            <th class="text-center">Contact</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Reason</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Time</th>
                                                            <th class="text-center">State</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $arrayClient = array();
                                                            $query = $conexion->query('SELECT NOW()');
                                                            $fecha = $query->fetch_assoc()["NOW()"];
                                                            $fecha=date("Y-m-d",strtotime("+2 day",strtotime($fecha)));
                                                            //$fecha=date("Y-m-d",strtotime("+1 day",strtotime($fecha)));
                                                            $consulta='SELECT ci.id_cita AS id,ci.id_paciente AS idPaciente, CONCAT(UCASE(LEFT(paciente_nombres, 1)), SUBSTRING(paciente_nombres, 2)," ",UCASE(LEFT(paciente_apellidos, 1)), SUBSTRING(paciente_apellidos, 2)) AS Nombre, ci.id_reason AS RazonCita, ca.evento_fecha AS Fecha, ca.evento_inicio AS Hora, sms.status AS Estado,p.paciente_contacto AS Telefono, cl.clinica_nombre AS Clinica, p.paciente_idioma AS Idioma FROM citas AS ci LEFT JOIN calendario as ca ON ca.id_cita=ci.id_cita LEFT JOIN citas_estado AS cie ON cie.id_cita=ci.id_cita LEFT JOIN sms_status AS sms ON sms.id_cita=ci.id_cita LEFT JOIN paciente AS p ON p.id_paciente=ci.id_paciente LEFT JOIN clinicas AS cl ON cl.id_clinica=ci.id_clinica WHERE DATE_FORMAT(ca.evento_fecha, "%Y-%m-%d") = "'.$fecha.'" AND cie.estado_cita != "Canceled" AND cie.estado_cita != "No Show Up" AND cie.estado_cita != "Deleted" AND p.id_paciente NOT IN ("MS2","MSNP777","MS6");';
                                                            $query = $conexion->query($consulta);
                                                            if($query->num_rows<= 0){
                                                                $fecha2=date("Y-m-d",strtotime("+1 day",strtotime($fecha)));
                                                                $consulta='SELECT ci.id_cita AS id,ci.id_paciente AS idPaciente, CONCAT(UCASE(LEFT(paciente_nombres, 1)), SUBSTRING(paciente_nombres, 2)," ",UCASE(LEFT(paciente_apellidos, 1)), SUBSTRING(paciente_apellidos, 2)) AS Nombre, ci.id_reason AS RazonCita, ca.evento_fecha AS Fecha, ca.evento_inicio AS Hora, sms.status AS Estado,p.paciente_contacto AS Telefono, cl.clinica_nombre AS Clinica, p.paciente_idioma AS Idioma FROM citas AS ci LEFT JOIN calendario as ca ON ca.id_cita=ci.id_cita LEFT JOIN citas_estado AS cie ON cie.id_cita=ci.id_cita LEFT JOIN sms_status AS sms ON sms.id_cita=ci.id_cita LEFT JOIN paciente AS p ON p.id_paciente=ci.id_paciente LEFT JOIN clinicas AS cl ON cl.id_clinica=ci.id_clinica WHERE DATE_FORMAT(ca.evento_fecha, "%Y-%m-%d") = "'.$fecha2.'" AND cie.estado_cita != "Canceled" AND cie.estado_cita != "No Show Up" AND cie.estado_cita != "Deleted" AND p.id_paciente NOT IN ("MS2","MSNP777","MS6");';
                                                                $query = $conexion->query($consulta);
                                                            }
                                                            
                                                            while($value = $query->fetch_assoc()) {
                                                                $arreglo["id"] = $value["id"];
                                                                $arreglo["idPaciente"] = $value["idPaciente"];
                                                                $arreglo["Nombre"] = ucfirst(quitar_tildes($value["Nombre"]));
                                                                $arreglo["Razon"] = $value["RazonCita"];
                                                                $arreglo["Fecha"] = $value["Fecha"];
                                                                $arreglo["Dia"] = getDaySpanish($value["Fecha"],$value["Idioma"]);
                                                                $arreglo["Idioma"] = $value["Idioma"];
                                                                $arreglo["Clinica"] = $value["Clinica"]; 
                                                                $arreglo["Hora"] = date_format(date_create($value["Hora"]),'g:i A'); 
                                                                $arreglo["Telefono"] = formatTelefono($value["Telefono"]); 
                                                                array_push($arrayClient, $arreglo);

                                                                if(empty($value["Estado"])){
                                                                    $opt = '<button type="button" id="'.$value["id"].'" onclick="cargarCliente(this.id);"
                                                                        class="btn btn-focus btn-sm">Send SMS</button>';
                                                                }else{
                                                                    $opt = "";
                                                                }

                                                                if(empty( $arreglo["Razon"])){
                                                                    $cal = queryOne('SELECT id_app_type FROM calendario WHERE id_cita = '.$arreglo["id"].';');
                                                                    if(!empty($cal['id_app_type'])){
                                                                        $reason_name = queryOne('SELECT type_description FROM appt_types WHERE type_id = '.$cal['id_app_type'].';');
                                                                        $razon = $reason_name['type_description'];
                                                                    }
                                                                }else{
                                                                    $razon = $arreglo["Razon"];
                                                                }
                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row">
                                                                <?php echo $value["id"];?></td>
                                                            <td>
                                                                <div class="widget-content p-0">
                                                                    <div class="widget-content-wrapper">
                                                                        <div class="widget-content-left flex2">
                                                                            <div class="widget-heading">
                                                                                <?php echo ucfirst(quitar_tildes($value["Nombre"]));?>
                                                                            <div class="widget-subheading opacity-7">
                                                                                <?php echo $value["idPaciente"];?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center"><?php echo formatTelefono($value["Telefono"]);?></td>
                                                            <td class="text-center"><?php echo $value["Clinica"];?></td>
                                                            <td class="text-center"><?php echo $razon;?></td>
                                                            <td class="text-center"><?php echo $value["Fecha"];?></td>
                                                            <td class="text-center"><?php echo date_format(date_create($value["Hora"]),'g:i A');?></td>
                                                            <td class="text-center">
                                                            <?php
                                                                if(!empty($value["Estado"])){
                                                                    $estado = '<div class="badge badge-primary">'.$value["Estado"].'</div>';
                                                                }else{
                                                                    $estado = "";
                                                                }
                                                            ?>
                                                            <?php echo $estado; ?>
                                                            </td>
                                                            <td class="text-center"><?php echo $opt; ?></td>

                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel"> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">List of Patient to Confirm by SMS</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="sent">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Appt</th>
                                                            <th class="text-center">Patient</th>
                                                            <th class="text-center">Contact</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Reason</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Time</th>
                                                            <th class="text-center">Appt State</th>
                                                            <th class="text-center">Lab Case</th>
                                                            <th class="text-center">Confirm SMS</th>
                                                            <th class="text-center">Second SMS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $arraySent = array();
                                                           foreach (getSentSms() as $value) {
                                                            $arreglo["id"] = $value["id"];
                                                            $arreglo["idPaciente"] = $value["idPaciente"];
                                                            $arreglo["Nombre"] = ucfirst(quitar_tildes($value["Nombre"]));
                                                            $arreglo["Razon"] = $value["RazonCita"];
                                                            $arreglo["Fecha"] = $value["Fecha"];
                                                            $arreglo["Dia"] = getDaySpanish($value["Fecha"],$value["Idioma"]);
                                                            $arreglo["Idioma"] = $value["Idioma"];
                                                            $arreglo["Clinica"] = $value["Clinica"]; 
                                                            $arreglo["Hora"] = date_format(date_create($value["Hora"]),'g:i A'); 
                                                            $arreglo["Telefono"] = formatTelefono($value["Telefono"]); 
                                                            array_push($arraySent, $arreglo);

                                                            if(empty($arreglo["Razon"])){
                                                                if(!empty($arreglo["Apptype"])){
                                                                    $razon = $value['RazonCita2'];
                                                                    $razon_color = $value['RazonCitaColor2'];
                                                                }
                                                            }else{
                                                                $razon = $value['RazonCita'];
                                                                $razon_color = $value['RazonCitaColor'];
                                                                /* echo 'SELECT reason_color FROM citas_reason WHERE reason_nombre = "'.$razon.'";'; */
                                                            }

                                                            $color_cita = $value["EstadoCitaColor"];
                                                            if($color_cita == "#3ac47d"){
                                                                $estado_cita = "success";
                                                            }
                                                            else if($color_cita == "#16aaff"){
                                                                $estado_cita = "info";
                                                            }
                                                            else if($color_cita == "#f7b924"){
                                                                $estado_cita = "warning";
                                                            }
                                                            else if($color_cita == "#dcdcdc"){
                                                                $estado_cita = "light";
                                                            }
                                                            else if($color_cita == "#343a40"){
                                                                $estado_cita = "dark";
                                                            }
                                                            else if($color_cita == "#f775f0"){
                                                                $estado_cita = "danger";
                                                            }
                                                            else{
                                                                $estado_cita = "info";
                                                            }

                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row">
                                                                <?php echo $value["id"];?></td>
                                                            <td>
                                                                <div class="widget-content p-0">
                                                                    <div class="widget-content-wrapper">
                                                                        <div class="widget-content-left flex2">
                                                                            <div class="widget-heading">
                                                                                <?php echo ucfirst(quitar_tildes($value["Nombre"]));?>
                                                                            <div class="widget-subheading opacity-9">
                                                                                <a id="<?php echo $value["idPaciente"]?>" href="javascript:void(0)" class="account text-alternate"><?php echo $value["idPaciente"];?></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center"><?php echo formatTelefono($value["Telefono"]);?></td>
                                                            <td class="text-center"><?php echo $value["Clinica"];?></td>
                                                            <td class="text-left">
                                                                <svg width="25" height="15" viewBox="0 0 250 150">
                                                                    <rect x="70" y="25" height="100" width="120"
                                                                        style="stroke:#000; fill: <?php echo $razon_color;?>;">
                                                                    </rect>
                                                                </svg> <?php echo $razon;?>
                                                            </td>
                                                            <td class="text-center"><?php echo date('m-d-Y',strtotime($value["Fecha"]));?></td>
                                                            <td class="text-center"><?php echo date_format(date_create($value["Hora"]),'g:i A');?></td>
                                                            <td class="text-center">
                                                                <div class="badge badge-<?php echo $estado_cita;?>"><?php echo $value["EstadoCita"];?></div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" disabled id="exampleCustomCheckbox" class="custom-control-input">
                                                                    <label class="custom-control-label" for="exampleCustomCheckbox"></label>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php 
                                                                    if(!empty($value["Estado"])){
                                                                        $estado = '<div class="badge badge-primary">'.$value["Estado"].'</div>';
                                                                    }else{
                                                                        $estado = '<button type="button" id="'.$value["id"].'" onclick="week_cargarCliente(this.id);"
                                                                        class="btn btn-focus btn-sm">Send SMS</button>';
                                                                    }
                                                                ?>
                                                                <?php echo $estado; ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <button type="button"  id="<?php echo $value["id"];?>" onclick="segundosms(this.id);"
                                                                        class="btn btn-danger btn-sm">Send SMS</button>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        <!-- </div>               
                    </div> -->
                </div>
                <?php include 'footer.php'; ?>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <!-- DATATABLE -->
    <script src="assets/dataTable/jquery.dataTables.min.js"></script>
    <script src="assets/dataTable/dataTables.bootstrap4.min.js"></script>

    <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="include/sms.js"></script>
</body>

</html>
<script>
    var arreglo;
    var csms;
    $(document).ready(function() {
    
        //Ver account
        $(document).on("click", ".account", function () {
            pat_id = $.trim($(this).text());
            window.open('pat_account.php?id=' + pat_id, '_blank');
        });

        /* $('#example').DataTable({
            "iDisplayLength": -1,
            "aaSorting": [[ 3, "asc" ]] // Sort by first column descending
        }); */
        $('#sent').DataTable({
            //"iDisplayLength": -1,
            "aaSorting": [[ 5, "asc" ]] // Sort by first column descending
        });
        
        arreglo=<?php echo json_encode($arrayClient);?>;
        arregloSent=<?php echo json_encode($arraySent);?>;
        arregloWeek=<?php echo json_encode($arraySent);?>;
    });
    /* function cargarCliente(id){
        let dat=arreglo.find(dato => dato.id === id);
        var mensaje;
        if(dat['Idioma']=="English"){
            mensaje = "Hello "+dat["Nombre"]+" this message is from Top Dental to confirm your appointment for "+dat["Dia"]+" at our "+dat["Clinica"]+" office at "+dat["Hora"]+". Please reply YES to confirm or NO to cancel. Thanks";
        }else{
            mensaje = "Hola "+dat["Nombre"]+" este mensaje es de Top Dental para confirmar su cita programada para este "+dat["Dia"]+" en nuestra oficina de "+dat["Clinica"]+" a las "+dat["Hora"]+". Por favor responda con un SI para confirmar o un NO para cancelar. Gracias"; 
        }
        

        swal.fire({
                title: "Send Confirm Message", //Event Title
                html: "<b>Do you want to send the message: </b><i>"+ mensaje +"</i>",
                showCancelButton: true,
                confirmButtonText: 'SEND',
                cancelButtonText: 'Cancel',
                cancelButtonColor: '#3f6ad8',
                confirmButtonColor: '#d33',
                icon: "question"
            }).then((result) => {
                if (result.value) {
                    
                    var id = dat["id"];
                    $.ajax({
                        type: "POST",
                        url: 'appointments/sms.php',
                        data: 'id_cita=' + id,
                        success: function (resp) {
                            
                            if(resp == "Success"){
                                //sendMesage(mensaje, "7033939393");
                                sendMesage(mensaje,dat["Telefono"]);
                            }else{
                            }

                        }
                    });
                }
            }, function (dismiss) {
                if (dismiss === 'cancel') {

                }
            }); 

    }   */

    function week_cargarCliente(id){
        let data=arregloWeek.find(dato => dato.id === id);
        var mensaje;
        if(data['Idioma']=="English"){
            mensaje = "Hello "+data["Nombre"]+" this message is from Top Dental to confirm your appointment for "+data["Dia"]+" at our "+data["Clinica"]+" office at "+data["Hora"]+". Please reply YES to confirm or NO to cancel. Thanks";
        }else{
            mensaje = "Hola "+data["Nombre"]+" este mensaje es de Top Dental para confirmar su cita programada para este "+data["Dia"]+" en nuestra oficina de "+data["Clinica"]+" a las "+data["Hora"]+". Por favor responda con un SI para confirmar o un NO para cancelar. Gracias";
        }

        swal.fire({
                title: "Send Confirm Message", //Event Title
                html: "<b>Do you want to send the message: </b><i>"+ mensaje +"</i>",
                showCancelButton: true,
                confirmButtonText: 'SEND',
                cancelButtonText: 'Cancel',
                cancelButtonColor: '#3f6ad8',
                confirmButtonColor: '#d33',
                icon: "question"
            }).then((result) => {
                if (result.value) {
                    
                    var id = data["id"];
                    $.ajax({
                        type: "POST",
                        url: 'appointments/sms.php',
                        data: 'id_cita=' + id,
                        success: function (resp) {
                            
                            if(resp == "Success"){
                                //sendMesage(mensaje, "7033939393");
                                sendMesage(mensaje,data["Telefono"]);
                            }else{
                            }

                        }
                    });
                }
            }, function (dismiss) {
                if (dismiss === 'cancel') {

                }
            }); 

    }  

    //Segundo mensaje
    function segundosms(id){
        let info=arregloSent.find(dato => dato.id === id);
        var sms;
        if(info['Idioma']=="English"){
            sms = "Hello "+info["Nombre"]+" this message is from Top Dental to confirm your appointment for "+info["Dia"]+" at our "+info["Clinica"]+" office at "+info["Hora"]+". Please reply YES to confirm or NO to cancel. If you don't confirm this message before 8am, your appointment will be canceled. Thanks";
        }else{
            sms = "Hola "+info["Nombre"]+" este mensaje es de Top Dental para confirmar su cita programada para este "+info["Dia"]+" en nuestra oficina de "+info["Clinica"]+" a las "+info["Hora"]+". Por favor responda con un SI para confirmar o un NO para cancelar. Favor confirmar su cita antes de las 8 am de mañana para que su cita no sea cancelada. Gracias.";
        }
        

        swal.fire({
                title: "Second SMS", //Event Title
                html: "<b>Do you want to send the message: </b><i>"+ sms +"</i>",
                showCancelButton: true,
                confirmButtonText: 'SEND',
                cancelButtonText: 'Cancel',
                cancelButtonColor: '#3f6ad8',
                confirmButtonColor: '#d33',
                icon: "question"
            }).then((result) => {
                if (result.value) {
                    
                    var id = info["id"];
                    $.ajax({
                        type: "POST",
                        url: 'appointments/sms2.php',
                        data: 'id_cita=' + id,
                        success: function (resp) {
                            
                            if(resp == "Success"){
                                //sendMesage(sms, "7033939393");
                                sendMesage(sms,info["Telefono"]);
                            }else{
                            }

                        }
                    });
                }
            }, function (dismiss) {
                if (dismiss === 'cancel') {

                }
            }); 

    }
</script>
