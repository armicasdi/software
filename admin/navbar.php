<?php 
    $fechaGuardada = $_SESSION["ultimoAcceso"];
    $ahora = date("Y-n-j H:i:s");
    $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

    //comparamos el tiempo transcurrido
    if($tiempo_transcurrido >= 3600) {
    //si pasaron 10 minutos o más
    unset($_SESSION['user']);
    unset($_SESSION['id']);
    unset($_SESSION['rol']);
    session_destroy();
    header("Location: ../index.php");
    //sino, actualizo la fecha de la sesión
    }else {
    $_SESSION["ultimoAcceso"] = $ahora;
    }
?>

<div class="app-header header-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                    data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="app-header__content">
        <div class="app-header-left">
            <div class="search-wrapper">
                <div class="input-holder">
                    <input type="text" class="search-input" placeholder="Type to search">
                    <button class="search-icon"><span></span></button>
                </div>
                <button class="close"></button>
            </div>
            <div class="widget-content-left">
                <div class="btn-group">
                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                        <img width="42" class="rounded-circle" src="assets/images/languages/eng.png" alt="">
                        <!-- <i class="fa fa-angle-down ml-2 opacity-8"></i> -->
                    </a>
                    <!-- <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                        <h6 tabindex="-1" class="dropdown-header">Select Language</h6>
                        <button type="button" tabindex="0" class="dropdown-item"> <img width="38" class="rounded-circle"
                                src="assets/images/languages/esp.png" alt=""> Spanish</button>
                        <button type="button" tabindex="0" class="dropdown-item"> <img width="38" class="rounded-circle"
                                src="assets/images/languages/eng.png" alt=""> English</button>
                    </div> -->
                </div>
            </div>

            <ul class="header-menu nav">
                <!--  <li class="nav-item">
                    <a href="javascript:void(0);" class="nav-link">
                        <i class="nav-link-icon fa fa-bar-chart"> </i>
                        Estad&iacute;sticas
                    </a>
                </li> -->
                <li class="btn-group nav-item">
                    <a href="NewJobNumber.php" class="nav-link">
                        <i class="nav-link-icon fa fa-certificate icon-gradient bg-amy-crisp"> </i>
                        <b>New Job Number</b>
                    </a>
                </li>
               <!--  <li class="btn-group nav-item">
                    <a href="addPaymentPlan.php" class="nav-link">
                        <i class="nav-link-icon fa fa-certificate icon-gradient bg-amy-crisp"> </i>
                        <b>New Job Number</b>
                    </a>
                </li>
                <li class="btn-group nav-item">
                    <a href="addTreatmentPlan.php" class="nav-link">
                        <i class="nav-link-icon fa fa-asterisk icon-gradient bg-grow-early"> </i>
                        <b>New Treatment Plan</b>
                    </a>
                </li> -->
                <li class="dropdown nav-item">
                    <!-- <a class="nav-link" data-toggle="modal" data-target="#newCallEntry">
                        <i class="nav-link-icon fa fa-phone icon-gradient bg-happy-itmeo"> </i>
                        <b>Call Tracker Entry</b>
                    </a> -->
                    <a href="addCallTrackerEntry.php" class="nav-link">
                        <i class="nav-link-icon fa fa-phone icon-gradient bg-happy-itmeo"> </i>
                        <b>Call Tracker Entry</b>
                    </a>
                </li>
                <!-- <li class="dropdown nav-item">
                    <a href="javascript:void(0);" class="nav-link">
                        <i class="nav-link-icon fa fa-cog"></i>
                        Ajustes
                    </a>
                </li> -->
            </ul>
        </div>
        <div class="app-header-right">
            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-right">
                            <div class="btn-group">
                                <?php
                                    /* function initials($str) {
                                        $ret = '';
                                        foreach (explode(' ', $str) as $Word)
                                            $ret .= strtoupper($Word[0]);
                                        return $ret;
                                    } */
                                    $reg_usuario = queryOne('SELECT * FROM '. $tabla_usuarios_perfil . ' WHERE id_usuario = '. $_SESSION['id'] . ';');
                                    $rol = $_SESSION['rol'];
                                    $nombre = $reg_usuario['usuario_nombre'];
                                    $foto = $reg_usuario['usuario_foto'];
                                    $user_rol=$reg_usuario['usuario_rol'];
                                    $user_estado=$reg_usuario['usuario_estado'];
                                    $reg_user_rol = queryOne('SELECT role_name FROM usuario_rol WHERE id = '. $user_rol . ';');

                                    
                                    $bg=""; $badge_color=""; $estado_name="";
                                    if($user_rol == 1){ $bg = "bg-strong-bliss";
                                    }else if($user_rol == 2){ $bg = "bg-night-fade";
                                    }else if($user_rol == 3){ $bg = "bg-tempting-azure";
                                    }else if($user_rol == 4){ $bg = "bg-malibu-beach";
                                    }else if($user_rol == 5){ $bg = "bg-happy-green";
                                    }else if($user_rol == 6){ $bg = "bg-royal";
                                    }
                                ?>
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">

                                <!-- <?php if($foto != ""){?> -->
                                    <img width="42" class="rounded-circle" src="<?php echo $galeria_usuarios.$foto; ?>" alt="">
                                <!-- <?php } else if($foto == ""){?>
                                <div class="swatch-holder swatch-holder-lg <?php echo $bg;?>" style="opacity: 1;">
                                    <b class="text-white text-center"><?php echo initials($nombre);?></b>
                                </div> 
                                <?php }?>-->
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true"
                                    class="dropdown-menu dropdown-menu-right">
                                    <!-- <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                                class="nav-link-icon pe-7s-chat"> </i><span>Chat</span>
                                            <div class="ml-auto badge badge-pill badge-info">Comming Soon</div>
                                        </a></li> -->
                                    <!-- <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                                class="nav-link-icon pe-7s-wallet"> </i><span>Recover
                                                Password</span></a></li> -->
                                    <li class="nav-item-header nav-item">My Account</li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            <i class="nav-link-icon pe-7s-user"></i>
                                            <span>My Profile</span>
                                            <div class="ml-auto badge badge-secondary">Disabled</div>
                                            <!-- <span class="spinner-grow spinner-grow-sm mr-2 text-danger"></span> -->
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            <i class="nav-link-icon pe-7s-lock"> </i>
                                            <span>Change Password</span>
                                            <span class="spinner-grow spinner-grow-sm mr-2 text-danger"></span>
                                            <!-- <div class="ml-auto badge badge-danger">Comming Soon</div> -->
                                        </a>
                                    </li>
                                    <!-- <li class="nav-item"><a href="javascript:void(0);" class="nav-link"><i
                                                class="nav-link-icon pe-7s-coffee"> </i><span>Messages</span>
                                            <div class="ml-auto badge badge-warning">Comming Soon</div>
                                        </a></li>
 -->
                                    <li class="nav-item-divider nav-item"></li>
                                    <li class="nav-item-btn nav-item">
                                        <a class="btn-pill btn btn-info btn-sm" href="include/logout.php">Log out</a>
                                    </li>
                                </div>
                            </div>
                        </div>
                        
                        <div class="widget-content-left ml-4 header-user-info">
                            <div class="widget-heading">
                                <?php echo $nombre;?>
                            </div>
                            <div class="widget-subheading">
                                <?php echo strtoupper($rol)." | ".$reg_user_rol["role_name"];?> 
                            </div>
                        </div>

                        <div class="widget-content-right header-user-info ml-3">
                            <button type="button" class="p-1 btn btn-primary btn-sm"
                                onclick="location.href='calendar.php'">
                                <i class="fa text-white fa-calendar pr-1 pl-1"></i>
                            </button>
                            <button type="button" class="p-1 btn btn-alternate btn-sm">
                                <i class="fa text-white fa-bell pr-1 pl-1"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ui-theme-settings">
    <!-- <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
        <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
    </button> -->
    <div class="theme-settings__inner">
        <div class="scrollbar-container">
            <div class="theme-settings__options-wrapper">
                <!-- <h3 class="themeoptions-heading">Layout Options
                </h3>
                <div class="p-3">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-3">
                                        <div class="switch has-switch switch-container-class" data-class="fixed-header">
                                            <div class="switch-animate switch-on">
                                                <input type="checkbox" checked data-toggle="toggle"
                                                    data-onstyle="success">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Fixed Header
                                        </div>
                                        <div class="widget-subheading">Makes the header top fixed, always
                                            visible!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-3">
                                        <div class="switch has-switch switch-container-class"
                                            data-class="fixed-sidebar">
                                            <div class="switch-animate switch-on">
                                                <input type="checkbox" checked data-toggle="toggle"
                                                    data-onstyle="success">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Fixed Sidebar
                                        </div>
                                        <div class="widget-subheading">Makes the sidebar left fixed, always
                                            visible!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-3">
                                        <div class="switch has-switch switch-container-class" data-class="fixed-footer">
                                            <div class="switch-animate switch-off">
                                                <input type="checkbox" data-toggle="toggle" data-onstyle="success">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Fixed Footer
                                        </div>
                                        <div class="widget-subheading">Makes the app footer bottom fixed, always
                                            visible!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <h3 class="themeoptions-heading">
                    <div>
                        Header Options
                    </div>
                    <button type="button"
                        class="btn-pill btn-shadow btn-wide ml-auto btn btn-focus btn-sm switch-header-cs-class"
                        data-class="">
                        Restore Default
                    </button>
                </h3>
                <div class="p-3">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <h5 class="pb-2">Choose Color Scheme
                            </h5>
                            <div class="theme-settings-swatches">
                                <div class="swatch-holder bg-primary switch-header-cs-class"
                                    data-class="bg-primary header-text-light">
                                </div>
                                <div class="swatch-holder bg-secondary switch-header-cs-class"
                                    data-class="bg-secondary header-text-light">
                                </div>
                                <div class="swatch-holder bg-success switch-header-cs-class"
                                    data-class="bg-success header-text-dark">
                                </div>
                                <div class="swatch-holder bg-info switch-header-cs-class"
                                    data-class="bg-info header-text-dark">
                                </div>
                                <div class="swatch-holder bg-warning switch-header-cs-class"
                                    data-class="bg-warning header-text-dark">
                                </div>
                                <div class="swatch-holder bg-danger switch-header-cs-class"
                                    data-class="bg-danger header-text-light">
                                </div>
                                <div class="swatch-holder bg-light switch-header-cs-class"
                                    data-class="bg-light header-text-dark">
                                </div>
                                <div class="swatch-holder bg-dark switch-header-cs-class"
                                    data-class="bg-dark header-text-light">
                                </div>
                                <div class="swatch-holder bg-focus switch-header-cs-class"
                                    data-class="bg-focus header-text-light">
                                </div>
                                <div class="swatch-holder bg-alternate switch-header-cs-class"
                                    data-class="bg-alternate header-text-light">
                                </div>
                                <div class="divider">
                                </div>
                                <div class="swatch-holder bg-vicious-stance switch-header-cs-class"
                                    data-class="bg-vicious-stance header-text-light">
                                </div>
                                <div class="swatch-holder bg-midnight-bloom switch-header-cs-class"
                                    data-class="bg-midnight-bloom header-text-light">
                                </div>
                                <div class="swatch-holder bg-night-sky switch-header-cs-class"
                                    data-class="bg-night-sky header-text-light">
                                </div>
                                <div class="swatch-holder bg-slick-carbon switch-header-cs-class"
                                    data-class="bg-slick-carbon header-text-light">
                                </div>
                                <div class="swatch-holder bg-asteroid switch-header-cs-class"
                                    data-class="bg-asteroid header-text-light">
                                </div>
                                <div class="swatch-holder bg-royal switch-header-cs-class"
                                    data-class="bg-royal header-text-light">
                                </div>
                                <div class="swatch-holder bg-warm-flame switch-header-cs-class"
                                    data-class="bg-warm-flame header-text-dark">
                                </div>
                                <div class="swatch-holder bg-night-fade switch-header-cs-class"
                                    data-class="bg-night-fade header-text-dark">
                                </div>
                                <div class="swatch-holder bg-sunny-morning switch-header-cs-class"
                                    data-class="bg-sunny-morning header-text-dark">
                                </div>
                                <div class="swatch-holder bg-tempting-azure switch-header-cs-class"
                                    data-class="bg-tempting-azure header-text-dark">
                                </div>
                                <div class="swatch-holder bg-amy-crisp switch-header-cs-class"
                                    data-class="bg-amy-crisp header-text-dark">
                                </div>
                                <div class="swatch-holder bg-heavy-rain switch-header-cs-class"
                                    data-class="bg-heavy-rain header-text-dark">
                                </div>
                                <div class="swatch-holder bg-mean-fruit switch-header-cs-class"
                                    data-class="bg-mean-fruit header-text-dark">
                                </div>
                                <div class="swatch-holder bg-malibu-beach switch-header-cs-class"
                                    data-class="bg-malibu-beach header-text-light">
                                </div>
                                <div class="swatch-holder bg-deep-blue switch-header-cs-class"
                                    data-class="bg-deep-blue header-text-dark">
                                </div>
                                <div class="swatch-holder bg-ripe-malin switch-header-cs-class"
                                    data-class="bg-ripe-malin header-text-light">
                                </div>
                                <div class="swatch-holder bg-arielle-smile switch-header-cs-class"
                                    data-class="bg-arielle-smile header-text-light">
                                </div>
                                <div class="swatch-holder bg-plum-plate switch-header-cs-class"
                                    data-class="bg-plum-plate header-text-light">
                                </div>
                                <div class="swatch-holder bg-happy-fisher switch-header-cs-class"
                                    data-class="bg-happy-fisher header-text-dark">
                                </div>
                                <div class="swatch-holder bg-happy-itmeo switch-header-cs-class"
                                    data-class="bg-happy-itmeo header-text-light">
                                </div>
                                <div class="swatch-holder bg-mixed-hopes switch-header-cs-class"
                                    data-class="bg-mixed-hopes header-text-light">
                                </div>
                                <div class="swatch-holder bg-strong-bliss switch-header-cs-class"
                                    data-class="bg-strong-bliss header-text-light">
                                </div>
                                <div class="swatch-holder bg-grow-early switch-header-cs-class"
                                    data-class="bg-grow-early header-text-light">
                                </div>
                                <div class="swatch-holder bg-love-kiss switch-header-cs-class"
                                    data-class="bg-love-kiss header-text-light">
                                </div>
                                <div class="swatch-holder bg-premium-dark switch-header-cs-class"
                                    data-class="bg-premium-dark header-text-light">
                                </div>
                                <div class="swatch-holder bg-happy-green switch-header-cs-class"
                                    data-class="bg-happy-green header-text-light">
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <h3 class="themeoptions-heading">
                    <div>Sidebar Options</div>
                    <button type="button"
                        class="btn-pill btn-shadow btn-wide ml-auto btn btn-focus btn-sm switch-sidebar-cs-class"
                        data-class="">
                        Restore Default
                    </button>
                </h3>
                <div class="p-3">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <h5 class="pb-2">Choose Color Scheme
                            </h5>
                            <div class="theme-settings-swatches">
                                <div class="swatch-holder bg-primary switch-sidebar-cs-class"
                                    data-class="bg-primary sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-secondary switch-sidebar-cs-class"
                                    data-class="bg-secondary sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-success switch-sidebar-cs-class"
                                    data-class="bg-success sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-info switch-sidebar-cs-class"
                                    data-class="bg-info sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-warning switch-sidebar-cs-class"
                                    data-class="bg-warning sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-danger switch-sidebar-cs-class"
                                    data-class="bg-danger sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-light switch-sidebar-cs-class"
                                    data-class="bg-light sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-dark switch-sidebar-cs-class"
                                    data-class="bg-dark sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-focus switch-sidebar-cs-class"
                                    data-class="bg-focus sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-alternate switch-sidebar-cs-class"
                                    data-class="bg-alternate sidebar-text-light">
                                </div>
                                <div class="divider">
                                </div>
                                <div class="swatch-holder bg-vicious-stance switch-sidebar-cs-class"
                                    data-class="bg-vicious-stance sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-midnight-bloom switch-sidebar-cs-class"
                                    data-class="bg-midnight-bloom sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-night-sky switch-sidebar-cs-class"
                                    data-class="bg-night-sky sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-slick-carbon switch-sidebar-cs-class"
                                    data-class="bg-slick-carbon sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-asteroid switch-sidebar-cs-class"
                                    data-class="bg-asteroid sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-royal switch-sidebar-cs-class"
                                    data-class="bg-royal sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-warm-flame switch-sidebar-cs-class"
                                    data-class="bg-warm-flame sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-night-fade switch-sidebar-cs-class"
                                    data-class="bg-night-fade sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-sunny-morning switch-sidebar-cs-class"
                                    data-class="bg-sunny-morning sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-tempting-azure switch-sidebar-cs-class"
                                    data-class="bg-tempting-azure sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-amy-crisp switch-sidebar-cs-class"
                                    data-class="bg-amy-crisp sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-heavy-rain switch-sidebar-cs-class"
                                    data-class="bg-heavy-rain sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-mean-fruit switch-sidebar-cs-class"
                                    data-class="bg-mean-fruit sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-malibu-beach switch-sidebar-cs-class"
                                    data-class="bg-malibu-beach sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-deep-blue switch-sidebar-cs-class"
                                    data-class="bg-deep-blue sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-ripe-malin switch-sidebar-cs-class"
                                    data-class="bg-ripe-malin sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-arielle-smile switch-sidebar-cs-class"
                                    data-class="bg-arielle-smile sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-plum-plate switch-sidebar-cs-class"
                                    data-class="bg-plum-plate sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-happy-fisher switch-sidebar-cs-class"
                                    data-class="bg-happy-fisher sidebar-text-dark">
                                </div>
                                <div class="swatch-holder bg-happy-itmeo switch-sidebar-cs-class"
                                    data-class="bg-happy-itmeo sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-mixed-hopes switch-sidebar-cs-class"
                                    data-class="bg-mixed-hopes sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-strong-bliss switch-sidebar-cs-class"
                                    data-class="bg-strong-bliss sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-grow-early switch-sidebar-cs-class"
                                    data-class="bg-grow-early sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-love-kiss switch-sidebar-cs-class"
                                    data-class="bg-love-kiss sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-premium-dark switch-sidebar-cs-class"
                                    data-class="bg-premium-dark sidebar-text-light">
                                </div>
                                <div class="swatch-holder bg-happy-green switch-sidebar-cs-class"
                                    data-class="bg-happy-green sidebar-text-light">
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <h3 class="themeoptions-heading">
                    <div>Main Content Options</div>
                    <button type="button"
                        class="btn-pill btn-shadow btn-wide ml-auto active btn btn-focus btn-sm">Restore Default
                    </button>
                </h3>
                <div class="p-3">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <h5 class="pb-2">Page Section Tabs
                            </h5>
                            <div class="theme-settings-swatches">
                                <div role="group" class="mt-2 btn-group">
                                    <button type="button"
                                        class="btn-wide btn-shadow btn-primary btn btn-secondary switch-theme-class"
                                        data-class="body-tabs-line">
                                        Line
                                    </button>
                                    <button type="button"
                                        class="btn-wide btn-shadow btn-primary active btn btn-secondary switch-theme-class"
                                        data-class="body-tabs-shadow">
                                        Shadow
                                    </button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div> -->
            </div>
        </div>
    </div>
</div>