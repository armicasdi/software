<?php
    require("../include/database.php");
    include("../include/funciones.php");

    if(isset($_POST['reason'])){

        $campos_reason = array(
            'reason_nombre' => escapar($_POST['reason']),
            'reason_descripcion' => escapar($_POST['description']),
            'reason_duracion' => escapar($_POST['duration'])
        );

        if(insertar($tabla_citas_reason,$campos_reason))
        {
            echo "Success";
        }else{
            echo "Error";
        }
    }
?>