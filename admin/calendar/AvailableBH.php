<?php

    include('../include/database.php');
    include("../include/funciones.php");

    function intervaloHora($hora_inicio, $hora_fin, $intervalo = 30) {

        $hora_inicio = new DateTime( $hora_inicio );
        $hora_fin    = new DateTime( $hora_fin );
        $hora_fin->modify('+1 second'); // Añadimos 1 segundo para que nos muestre $hora_fin
    
        // Si la hora de inicio es superior a la hora fin
        // añadimos un día más a la hora fin
        if ($hora_inicio > $hora_fin) {
    
            $hora_fin->modify('+1 day');
        }
    
        // Establecemos el intervalo en minutos        
        $intervalo = new DateInterval('PT'.$intervalo.'M');
    
        // Sacamos los periodos entre las horas
        $periodo   = new DatePeriod($hora_inicio, $intervalo, $hora_fin);        
    
        foreach( $periodo as $hora ) {
    
            // Guardamos las horas intervalos 
            $horas[] =  $hora->format('H:i:s');
        }
    
        return $horas;
    }

    $id_clinica = $_POST["clinica"];
    $date = $_POST['select_date'];
    $result = $conexion->query('SELECT * FROM '.$tabla_calendario_horario.' WHERE id_clinica = '.$id_clinica.' AND bh_date = "'.$date.'";');
    $json = array();

    while($row=$result->fetch_assoc())
    {
        $array_horas = intervaloHora($row['bh_start'],$row['bh_end']);

        for($i = 0; $i < count($array_horas); $i++){
            $condicion = array(
                'id_clinica' => $id_clinica,
                'cita_fecha' => $date,
                'cita_hora' => $array_horas[$i]
            );
            $num_app = cantidad_2($tabla_citas,$condicion);
            $disponible = 4 - $num_app;

            echo '<tr id="ava-app">
                <td class="text-center">
                    <div class="custom-checkbox custom-control custom-control-inline">
                        <input type="checkbox" name="treatId[]" value="'.$treatment_plan_id.'"
                            class="treatId custom-control-input" id="'.$treatment_plan_id.'">
                        <label class="custom-control-label" for="'.$treatment_plan_id.'">
                        </label>
                    </div>
                </td>
                <td class="text-center text-muted" scope="row">'.$treatment_plan_id.'</td>
                <td class="text-center">'.$description.'</td>
                <td class="text-center">'.$patient_id.'</td>
                <td class="text-center">'.$date_entered.'</td>
                
            </tr>';
        }

        /* $json[]=array(
            'dow'   => $row["day"],
            'startTime'   => date('G:i',strtotime($row["start"])),
            'endTime'   => date('G:i',strtotime($row["end"]))
        ); */
    }

    /* $jsonstring = json_encode($json);
    echo $jsonstring; */

?>
