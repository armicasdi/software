$(document).ready(function () {
    $('#selectFXdates').datepicker('destroy');
    $('#selectMSdates').datepicker('destroy');
    $('#selectWEdates').datepicker('destroy');

    $.ajax({
        type: "POST",
        url: 'calendar/loadDisabledDates.php',
        data: 'id_clinica=' + 1,
        dataType: 'JSON',
        success: function (resp) {
            $('#selectFXdates').datepicker({
                format: 'mm/dd/yyyy',
                multidate: true,
                daysOfWeekDisabled: [0],
                datesDisabled: resp,
                clearBtn: true,
                todayHighlight: true,
                daysOfWeekHighlighted: [1, 2, 3, 4, 5, 6]
            });
        }
    });

    $.ajax({
        type: "POST",
        url: 'calendar/loadDisabledDates.php',
        data: 'id_clinica=' + 2,
        dataType: 'JSON',
        success: function (resp) {
            $('#selectMSdates').datepicker({
                format: 'mm/dd/yyyy',
                multidate: true,
                daysOfWeekDisabled: [0],
                datesDisabled: resp,
                clearBtn: true,
                todayHighlight: true,
                daysOfWeekHighlighted: [1, 2, 3, 4, 5, 6]
            });
        }
    });

    $.ajax({
        type: "POST",
        url: 'calendar/loadDisabledDates.php',
        data: 'id_clinica=' + 3,
        dataType: 'JSON',
        success: function (resp) {
            $('#selectWEdates').datepicker({
                format: 'mm/dd/yyyy',
                multidate: true,
                daysOfWeekDisabled: [0],
                datesDisabled: resp,
                clearBtn: true,
                todayHighlight: true,
                daysOfWeekHighlighted: [1, 2, 3, 4, 5, 6]
            });
        }
    });

    function ConvertTimeformat(format, str) {
        var hours = Number(str.match(/^(\d+)/)[1]);
        var minutes = Number(str.match(/:(\d+)/)[1]);
        var AMPM = str.match(/\s?([AaPp][Mm]?)$/)[1];
        var pm = ['P', 'p', 'PM', 'pM', 'pm', 'Pm'];
        var am = ['A', 'a', 'AM', 'aM', 'am', 'Am'];
        if (pm.indexOf(AMPM) >= 0 && hours < 12) hours = hours + 12;
        if (am.indexOf(AMPM) >= 0 && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        if (format == '0000') {
            return (sHours + sMinutes);
        } else if (format == '00:00') {
            return (sHours + ":" + sMinutes);
        } else {
            return false;
        }
    }

    $('#startfx').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        minTime: new Date(0, 0, 0, 9, 0, 0),
        maxTime: '7:00pm',
        defaultTime: '9:00am',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#endfx').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        minTime: new Date(0, 0, 0, 12, 0, 0),
        maxTime: '7:00pm',
        defaultTime: '5:00pm',
        startTime: '17:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#startms').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        minTime: new Date(0, 0, 0, 9, 0, 0),
        maxTime: '7:00pm',
        defaultTime: '9:00am',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#endms').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        minTime: new Date(0, 0, 0, 12, 0, 0),
        maxTime: '7:00pm',
        defaultTime: '5:00pm',
        startTime: '17:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#startwe').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        minTime: new Date(0, 0, 0, 9, 0, 0),
        maxTime: '7:00pm',
        defaultTime: '9:00am',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#endwe').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        minTime: new Date(0, 0, 0, 12, 0, 0),
        maxTime: '7:00pm',
        defaultTime: '5:00pm',
        startTime: '17:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    //cargar horarios en tabla fairfax
    /* $.ajax({
        type: "POST",
        url: 'calendar/loadbhfx.php',
        data: 'id_clinica=' + 1,
        success: function (resp) {
            $('#bhfairfax').html(resp);

            $('[name="startime[]"]').timepicker({
                timeFormat: 'hh:mm p',
                interval: 30,
                minTime: new Date(0, 0, 0, 9, 0, 0),
                maxTime: '7:00pm',
                startTime: '09:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            $('[name="endtime[]"]').timepicker({
                timeFormat: 'hh:mm p',
                interval: 30,
                minTime: new Date(0, 0, 0, 12, 0, 0),
                maxTime: '7:00pm',
                startTime: '17:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            //Definiendo estado cerrado
            $('[name="chk_closed[]"]').each(function () {
                $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
                $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
            });

            //Definiendo estado cerrado
            $('[name="chk_closed[]"]').click(function () {
                $('[name="chk_closed[]"]').each(function () {
                    $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
                    $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
                });
            });
        }
    }); */
    

    //AGREGAR NUEVAS HORAS
    $('#savesinglefx').click(function () {
        var valorDates = $('#selectFXdates').val();
        var start = ConvertTimeformat("00:00", $('#startfx').val());
        var end = ConvertTimeformat("00:00", $('#endfx').val());

        var arreglo = valorDates.split(",");
        console.log(arreglo);
        console.log(start);
        console.log(end);

        const dataBH = {
            dates: arreglo,
            clinica: 1,
            start: start,
            end: end
        }

        $.ajax({
            type: "POST",
            url: "calendar/newBH.php",
            data: dataBH,
            success: function (resp) {
                if (resp == "Success") {
                    
                    swal.fire({
                        title: "Business Hours (Fairfax)",
                        text: "Has been updated successfully",
                        icon: "success"
                    }).then(function () {
                        location.reload();
                    });
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Business Hours (Fairfax)",
                        text: "An error has occurred. Please Try Again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Business Hours (Fairfax)",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });

    //Actualizando Horario Manassas
    $('#savesinglems').click(function () {
        var valorDates = $('#selectMSdates').val();
        var start = ConvertTimeformat("00:00", $('#startms').val());
        var end = ConvertTimeformat("00:00", $('#endms').val());

        var arreglo = valorDates.split(",");
        console.log(arreglo);
        console.log(start);
        console.log(end);

        const dataBH = {
            dates: arreglo,
            clinica: 2,
            start: start,
            end: end
        }

        $.ajax({
            type: "POST",
            url: "calendar/newBH.php",
            data: dataBH,
            success: function (resp) {
                if (resp == "Success") {
                    swal.fire({
                        title: "Business Hours (Manassas)",
                        text: "Has been updated successfully",
                        icon: "success"
                    }).then(function () {
                        location.reload();;
                    });
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Business Hours (Manassas)",
                        text: "An error has occurred. Please Try Again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Business Hours (Manassas)",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });

    //Actualizando Horario Woodbridge
    $('#savesinglewe').click(function () {
        var valorDates = $('#selectWEdates').val();
        var start = ConvertTimeformat("00:00", $('#startwe').val());
        var end = ConvertTimeformat("00:00", $('#endwe').val());

        var arreglo = valorDates.split(",");
        console.log(arreglo);
        console.log(start);
        console.log(end);

        const dataBH = {
            dates: arreglo,
            clinica: 3,
            start: start,
            end: end
        }

        $.ajax({
            type: "POST",
            url: "calendar/newBH.php",
            data: dataBH,
            success: function (resp) {
                if (resp == "Success") {
                    swal.fire({
                        title: "Business Hours (Woodbridge)",
                        text: "Has been updated successfully",
                        icon: "success"
                    }).then(function () {
                        location.reload();
                    });
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Business Hours (Woodbridge)",
                        text: "An error has occurred. Please Try Again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Business Hours (Woodbridge)",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });

    //cargar horarios actuales fairfax

    
 
    /* $.ajax({
        type: "POST",
        url: 'calendar/seebh.php',
        data: 'id_clinica=' + 1,
        success: function (resp) {
            //$('#singlefx').html(resp);
            $('#bhf').DataTable({
                destroy: true,
                searching: false
            } );
            $('#bhw').DataTable({
                'data': JSON.parse(resp)
            });
        }
    }); */

    /* //cargar horarios actuales manassas
    $.ajax({
        type: "POST",
        url: 'calendar/seebh.php',
        data: 'id_clinica=' + 2,
        success: function (resp) {
            $('#singlems').html(resp);;
        }
    });

    //cargar horarios actuales woodbridge
    $.ajax({
        type: "POST",
        url: 'calendar/seebh.php',
        data: 'id_clinica=' + 3,
        success: function (resp) {
            $('#singlewe').html(resp);
        }
    }); */

    //Modal Edit Current BH
    $('#curstart').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        minTime: new Date(0, 0, 0, 9, 0, 0),
        maxTime: '7:00pm',
        defaultTime: '9:00am',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#curend').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        minTime: new Date(0, 0, 0, 12, 0, 0),
        maxTime: '7:00pm',
        defaultTime: '5:00pm',
        startTime: '17:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $(document).on('click', '.editButton',function (e) {
        $('#curdate').datepicker('destroy');
        var id = $(this).data('id');
        var date = $(this).data('fecha');
        var start = $(this).data('start');
        var end = $(this).data('end');
        var clinic = $(this).data('clinic');

        $('#curdate').val(date);
        $('#curstart').val(start);
        $('#curend').val(end);
        $('#idcur').val(id);

        $.ajax({
            type: "POST",
            url: 'calendar/loadDisabledDates.php',
            data: 'id_clinica=' + clinic,
            dataType: 'JSON',
            success: function (resp) {
                $('#curdate').datepicker({
                    format: 'mm/dd/yyyy',
                    multidate: false,
                    daysOfWeekDisabled: [0],
                    datesDisabled: resp,
                    clearBtn: true,
                    todayHighlight: true,
                    daysOfWeekHighlighted: [1, 2, 3, 4, 5, 6]
                });
            }
        });
    });

    $('#editBH').click(function () {

        var idcur = $('#idcur').val();

        const data = {
            starttime: ConvertTimeformat("00:00", $('#curstart').val()),
            endtime: ConvertTimeformat("00:00", $('#curend').val()),
            id: idcur,
            date: $('#curdate').val()
        }
        
        $.ajax({
            type: "POST",
            url: "calendar/updateCurrentBH.php",
            data: data,
            success: function (resp) {
                if (resp == "Success") {
                    swal.fire({
                        title: "Current Business Hour",
                        text: "Has been updated successfully",
                        icon: "success"
                    }).then(function () {
                        location.reload();
                    });
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Current Business Hour",
                        text: "Can't be updated. Please Try Again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Current Business Hour",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });

    //Eliminar Current BH
    $(document).on('click', '.deleteButton',function (e) {
        
        $('#curdate').datepicker('destroy');
        var id = $(this).data('id');
        var date = $(this).data('fecha');
        var clinic = $(this).data('clinic');
        var c = "";

        if(clinic == 1){c="Fairfax";}
        else if(clinic == 2){c="Manassas";}
        else if(clinic == 3){c="Woodbridge";}

        swal.fire({
            title: "Delete Current Business Hours From ("+c+")",
            text: "Are you sure you want to delete the business hour of the "+date+"?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }).then(result => { 
            if(result.value){
                $.ajax({
                    type: "POST",
                    url: 'calendar/deleteCurrentBH.php',
                    data: 'id=' + id,
                    success: function (resp) {
                        if (resp == "Success") {
                            swal.fire({
                                title: "Current Business Hours",
                                text: "Has been removed successfully",
                                icon: "success"
                            }).then(function () {
                                location.reload();
                            });
                        } else if (resp == "Error") {
                            swal.fire({
                                title: "Error",
                                text: "An error has occurred. Please Try Again",
                                icon: "error"
                            });
                        } else {
                            swal.fire({
                                title: "Error",
                                text: resp,
                                icon: "error"
                            });
                        }
        
                    }
                });
            }
            else{
                console.log('cancel action');
            }
        });

    });

});