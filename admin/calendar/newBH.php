<?php
include('../include/database.php');
include("../include/funciones.php");

if (isset($_POST['dates'])) {
    
    $user = escapar($_SESSION['user']);

    //Arreglos de BH
    $fechas = $_POST['dates'];
    $clinica = $_POST['clinica'];
    $inicio = $_POST['start'];
    $fin = $_POST['end'];
    $contador = 0;

    for ($count = 0; $count < count($fechas); $count++) {

        $condicion_cantidad = array(
            'bh_date' => date("Y-m-d", strtotime($fechas[$count])),
            'id_clinica' => $clinica
        );

        $horario_existente = cantidad_2($tabla_calendario_horas,$condicion_cantidad);

        if ($horario_existente > 0) {

            $campos_bh = array(
                'id_clinica' => $clinica,
                'bh_date' => date("Y-m-d", strtotime($fechas[$count])),
                'bh_start' => $inicio,
                'bh_end' => $fin,
                'bh_user' => $user
            );

            $condicion_update = array(
                'bh_date' => date("Y-m-d", strtotime($fechas[$count])),
                'id_clinica' => $clinica
            );
            actualizar_2($tabla_calendario_horas, $campos_bh, $condicion_update);
            $contador++;
            
        } else if ($horario_existente == 0) {
            $campos_bh = array(
                'id_clinica' => $clinica,
                'bh_date' => date("Y-m-d", strtotime($fechas[$count])),
                'bh_start' => $inicio,
                'bh_end' => $fin,
                'bh_user' => $user
            );

            if (insertar($tabla_calendario_horas, $campos_bh)) {
                $contador++;
            }
        }
    }

    if ($contador == count($fechas)) {
        echo "Success";
    } else {
        echo "Error";
        print_r($campos_bh);
    }
}
