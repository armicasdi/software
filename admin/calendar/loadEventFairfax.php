<?php 
    include("../include/eagle_con.php");
    require("../include/database.php");
    include("../include/funciones.php");

    $conexion->query('CREATE TEMPORARY TABLE `appointments_eagle` (
        `appointment_id` int NOT NULL,
        `patient_id` varchar(10) NOT NULL,
        `start_time` datetime NOT NULL,
        `end_time` datetime NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=utf8;');

    $query = "SELECT start_time,end_time,appointment_id,patient_id FROM appointment WHERE start_time BETWEEN dateadd(DAY,-7,NOW()) AND dateadd(WEEK,6,NOW());";

    $to_insert = array();
    
    $result = odbc_exec($conexion_anywhere,$query);

    while (odbc_fetch_row($result)) 
    {
        $patient_id = odbc_result($result,"patient_id");
        if (strpos($patient_id, 'M') === FALSE && strpos($patient_id, 'F') === FALSE && strpos($patient_id, 'W') === FALSE) {
            $id_pat_to_insert = trim('M'.$patient_id);
        }else{
            $id_pat_to_insert = trim($patient_id);
        }

        $parentesis = '('.odbc_result($result,"appointment_id").', "'.$id_pat_to_insert.'", "'.odbc_result($result,"start_time").'", "'.odbc_result($result,"end_time").'")';

        array_push($to_insert,$parentesis);
    }

    $array_string = implode(",",$to_insert);

    $conexion->query('INSERT INTO '.$tabla_appointments.' (`appointment_id`, `patient_id`, `start_time`, `end_time`) VALUES '.$array_string.';');

    #Colores
    $azul = "#16aaff";
    $verde = "#3ac47d";
    $naranja = "#f7b924";
    $gris = "#dcdcdc";
    $negro = "#343a40";
    $rosado = "#f775f0";
    $rojo = "#d92550";

    #MySQL
    $resultado = $conexion->query('SELECT appointment_id,patient_id,id_cita,id_paciente_eagle FROM (SELECT appointment_id,patient_id,id_cita,id_paciente_eagle FROM '.$tabla_appointments.' INNER JOIN '.$tabla_calendario.' ON patient_id = id_paciente_eagle AND id_clinica=1 WHERE id_cita_eagle IS NULL AND appointment_id NOT IN (SELECT id_cita_eagle FROM '.$tabla_calendario.' WHERE id_cita_eagle IS NOT NULL) GROUP BY appointment_id ORDER BY start_time ASC) b GROUP BY patient_id;');

    while($row=$resultado->fetch_assoc()){
        $app_id = $row['appointment_id'];
        $pat_id = $row['id_paciente_eagle'];

        //Asignar id_cita_eagle
        $condicion = array(
            'id_cita' => $row['id_cita']
        );
        $campos_calendario = array(
            'id_cita_eagle' => $app_id
        );

        $conexion->query('UPDATE '.$tabla_calendario.' SET id_cita_eagle = '.$app_id.' WHERE evento_fecha BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND DATE_ADD(CURDATE(), INTERVAL 6 WEEK) AND id_cita_eagle IS NULL AND id_paciente_eagle = "'.$pat_id.'" ORDER BY evento_fecha LIMIT 1;');

    }

    #MySQL
    $resultado = $conexion->query('SELECT id_paciente_eagle,evento_fecha,evento_inicio,'.$tabla_calendario.'.id_cita,id_cita_eagle,start_time,end_time,appointment_id,patient_id,estado_color FROM '.$tabla_calendario.' INNER JOIN '.$tabla_appointments.' ON patient_id = id_paciente_eagle AND appointment_id = id_cita_eagle INNER JOIN '.$tabla_citas_estado.' ON '.$tabla_citas_estado.'.id_cita = '.$tabla_calendario.'.id_cita WHERE id_clinica=1 AND id_cita_eagle IS NOT NULL;');

    $json = array();
    $array_eagle = array();

    while($pre = $resultado->fetch_assoc()){
        $id_paciente_eagle = $pre["id_paciente_eagle"];
        $cal_fecha_start = $pre["evento_fecha"].' '.$pre["evento_inicio"];
        $id_cita = $pre["id_cita"];
        $id_cita_eagle = $pre["id_cita_eagle"];
        $app_date_start = $pre['start_time'];
        $app_date_end = $pre['end_time'];
        $estado_color = $pre['estado_color'];

        array_push($array_eagle,$id_cita_eagle);

            //Actualizar el registro segun eaglesoft
            if(strtotime($cal_fecha_start) != strtotime($app_date_start)){ //Ok!
                //Si esta registrado en el soft pero en eaglesoft tiene una hora distinta
                //se actualiza en mysql con los datos de eaglesoft y pasa a green 
                if($estado_color != $naranja){

                    $date = date_create($app_date_start);
                    $date_fin = date_create($app_date_end);
                    $fecha = date_format($date, 'Y-m-d');
                    $hora_inicio = date_format($date, 'H:i:s');
                    $hora_final = date_format($date_fin, 'H:i:s');

                    $condicion = array(
                        'id_cita' => $pre['id_cita']
                    );
                    $campos_cita = array(
                        'cita_fecha' => $fecha,
                        'cita_hora' => $hora_inicio
                    );
                    $campos_calendario = array(
                        'evento_fecha' => $fecha,
                        'evento_inicio' => $hora_inicio,
                        'evento_fin' => $hora_final,
                        'evento_icon' => 'check',
                        'evento_needforms' => 'no'
                    );
                    $valores = array(
                        'estado_cita' => "Scheduled",
                        'estado_color' => $verde
                    );

                    actualizar($tabla_citas_estado,$valores,$condicion);
                    actualizar($tabla_calendario,$campos_calendario,$condicion);
                    actualizar($tabla_citas,$campos_cita,$condicion);

                }
            }
            else if($estado_color == $azul || $estado_color == $naranja){
                $condicion = array(
                    'id_cita' => $pre['id_cita']
                );
                $campos_calendario = array(
                    'evento_icon' => 'check',
                    'evento_needforms' => 'no'
                );
                $valores = array(
                    'estado_cita' => "Scheduled",
                    'estado_color' => $verde
                );

                actualizar($tabla_citas_estado,$valores,$condicion);
                actualizar($tabla_calendario,$campos_calendario,$condicion);
            }
    }

    #SqlAnywhere
    $array_eagle = array_filter($array_eagle);
    $array_eagle_string = implode(",",$array_eagle);

    $consulta_anywhere = "SELECT appointment_id,start_time,end_time,appointment.patient_id,location_id,date_appointed,appointment_type_id,chair_num,view_id,first_name,last_name,home_phone,birth_date FROM appointment 
    INNER JOIN scheduler_view_chair ON location_id = chair_num AND show_chair = 'Y' AND view_id = 17 
    INNER JOIN patient ON appointment.patient_id = patient.patient_id
    WHERE start_time BETWEEN NOW() AND dateadd(WEEK,6,NOW()) AND appointment_id NOT IN (".$array_eagle_string.") ORDER BY appointment_id ASC";

    $respuesta = odbc_exec($conexion_anywhere,$consulta_anywhere);

    while (odbc_fetch_row($respuesta))
    {
        $app_date_start = odbc_result($respuesta,'start_time');
        $app_date_end = odbc_result($respuesta,'end_time');
        $appointment_id = odbc_result($respuesta,'appointment_id');
        $patient_id = odbc_result($respuesta,'patient_id');
        $location_id = odbc_result($respuesta,'location_id');
        $date_appointed = odbc_result($respuesta,'date_appointed');
        $appointment_type_id = odbc_result($respuesta,'appointment_type_id');
        $chair_num = odbc_result($respuesta,'chair_num');
        $view_id = odbc_result($respuesta,'view_id');
        $first_name = odbc_result($respuesta,'first_name');
        $last_name = odbc_result($respuesta,'last_name');
        $home_phone = odbc_result($respuesta,'home_phone');
        $birth_date = odbc_result($respuesta,'birth_date');

        $date = date_create($app_date_start);
        $date_fin = date_create($app_date_end);
        $fecha = date_format($date, 'Y-m-d');
        $hora_inicio = date_format($date, 'H:i:s');
        $hora_final = date_format($date_fin, 'H:i:s');

        $intervalo = $date->diff($date_fin);
        $intervalo = $intervalo->format('%i min');
        $sync_color = $verde;

        if (strpos($patient_id, 'F') !== false) {
            $id_paciente = str_replace('F','FX',$patient_id);
            $id_paciente_eagle = $patient_id;
            $clinica = 1;
        }
        else if (strpos($patient_id, 'W') !== false) {
            $id_paciente = str_replace('W','WE',$patient_id);
            $id_paciente_eagle = $patient_id;
            $clinica = 3;
        }
        else {
            $id_paciente = 'MS'.$patient_id;
            $id_paciente_eagle = 'M'.$patient_id;
            $clinica = 2;
        }

        $num_row_paciente = cantidad('id_paciente',$id_paciente,$tabla_pacientes);
        $num_row_app_id = cantidad('id_cita_eagle',$appointment_id,$tabla_calendario);

        //Fecha de nacimiento vacia
        if($birth_date == ""){
            $fecha_nac = "0000-00-00";
        }else{
            $fecha_nac = $birth_date;
        }

        //Numero de contacto vacio
        if($home_phone == ""){
            $contacto = "(000) 000 0000";
        }else{
            $contacto = $home_phone;
        }

        if($num_row_app_id == 0){

            if($num_row_paciente == 0){

                $campos_paciente = array(
                    'id_paciente' => $id_paciente,
                    'id_paciente_eagle' => $id_paciente_eagle,
                    'paciente_nombres' => $first_name,
                    'paciente_apellidos' => $last_name,
                    'paciente_fechanac' => $fecha_nac,
                    'paciente_contacto' => $contacto,
                    'id_clinica' => $clinica,
                );

                $conexion->autocommit(false);

                $paciente = insertar($tabla_pacientes,$campos_paciente);
                if($paciente){
                    $pat = queryOne('SELECT id_paciente, id_paciente_eagle FROM '.$tabla_pacientes.' WHERE id = '.$paciente.';');

                    $campos_calltracker = array(
                        'id_clinica' => $clinica,
                        'id_patient' => $pat['id_paciente'],
                        'call_schedule_app' => "yes",
                        'call_user' => "eaglesoft"
                    );

                    $id_call = insertar($tabla_call_tracker,$campos_calltracker);
                    if($id_call){
                        $campos_cita = array(
                            'id_paciente' => $pat['id_paciente'],
                            'id_clinica' => 1,
                            'id_user' => "eaglesoft",
                            //'id_reason' => $appointment_type_id,
                            'cita_fecha' => $fecha,
                            'cita_hora' => $hora_inicio,
                            'cita_duracion' => $intervalo
                        );
                        $id_cita = insertar($tabla_citas,$campos_cita);

                        if($id_cita){
                            $valores = array(
                                'id_cita' => $id_cita,
                                'estado_color' => $sync_color,
                                'estado_cita' => "Scheduled"
                            );
                            
                            $citas_estado = insertar($tabla_citas_estado,$valores);

                            if($citas_estado){
                                $campos_calendario = array(
                                    'id_cita' => $id_cita,
                                    'id_clinica' => 1,
                                    'id_paciente' => $pat['id_paciente'],
                                    'id_paciente_eagle' => $pat['id_paciente_eagle'],
                                    'id_call' => $id_call,
                                    'evento_fecha' => $fecha,
                                    'evento_inicio' => $hora_inicio,
                                    'evento_fin' => $hora_final,
                                    'id_cita_eagle' => $appointment_id,
                                    'id_app_type' => $appointment_type_id
                                );
                                $cal = insertar($tabla_calendario,$campos_calendario);
                            }
                        }
                    }
                }

                if($paciente && $id_call && $id_cita && $citas_estado && $cal){
                    $conexion->commit();
                    echo "Success";
                }else{
                    $conexion->rollback();
                    echo "Error";
                }

                $conexion->autocommit(true);
            }
            else if($num_row_paciente > 0){

                //$tracker_id = queryOne('SELECT id_call FROM '.$tabla_call_tracker.' WHERE id_patient = "'.$id_paciente.'";');
                $conexion->autocommit(false);
                $campos_call_tracker = array(
                    'id_patient' => $id_paciente,
                    'id_clinica' => 1,
                    'tipo_paciente' => "Current Patient",
                    'call_schedule_app' => "yes",
                    'call_user' => "eaglesoft"
                );

                $tracker_id = insertar($tabla_call_tracker,$campos_call_tracker);

                if($tracker_id){
                    $campos_cita = array(
                        'id_paciente' => $id_paciente,
                        'id_clinica' => 1,
                        'id_user' => "eaglesoft",
                        'cita_fecha' => $fecha,
                        'cita_hora' => $hora_inicio,
                        'cita_duracion' => $intervalo
                    );
    
                    $id_cita = insertar($tabla_citas,$campos_cita);

                    if($id_cita){
                        $valores = array(
                            'id_cita' => $id_cita,
                            'estado_color' => $sync_color,
                            'estado_cita' => "Scheduled"
                        );
        
                        $campos_calendario = array(
                            'id_cita' => $id_cita,
                            'id_clinica' => 1,
                            'id_paciente' => $id_paciente,
                            'id_paciente_eagle' => $id_paciente_eagle,
                            'id_call' => $tracker_id,
                            'evento_fecha' => $fecha,
                            'evento_inicio' => $hora_inicio,
                            'evento_fin' => $hora_final,
                            'id_cita_eagle' => $appointment_id,
                            'id_app_type' => $appointment_type_id
                        );
        
                        
                        $id_cita_estado = insertar($tabla_citas_estado,$valores);
                        $id_calendario = insertar($tabla_calendario,$campos_calendario); 
                    }
                }

                if($tracker_id && $id_cita && $id_cita_estado && $id_calendario){
                    $conexion->commit();
                }else{
                    $conexion->rollback();
                    echo "Error";
                }
            }
        }
    }

    $conexion->query('UPDATE '.$tabla_citas_estado.' SET estado_cita = "Scheduled", estado_color = "'.$verde.'" WHERE estado_color NOT in ("'.$azul.'","'.$naranja.'","'.$rosado.'","'.$rojo.'") AND id_cita in (SELECT id_cita FROM '.$tabla_calendario.' WHERE id_clinica=1 AND NOW() < STR_TO_DATE(concat_ws(" ",evento_fecha,evento_inicio),"%Y-%m-%d %H:%i:%s") AND evento_inicio >= STR_TO_DATE("09:00:00", "%H:%i:%s") AND evento_inicio <= STR_TO_DATE("19:00:00", "%H:%i:%s"))');

    $conexion->query('UPDATE '.$tabla_citas_estado.' SET estado_cita = "Finished", estado_color = "'.$gris.'" WHERE estado_color NOT in ("'.$azul.'","'.$naranja.'","'.$rosado.'","'.$rojo.'") AND id_cita in (SELECT id_cita FROM '.$tabla_calendario.' WHERE id_clinica=1 AND NOW() > STR_TO_DATE(concat_ws(" ", evento_fecha,evento_inicio),"%Y-%m-%d %H:%i:%s") AND (evento_inicio >= STR_TO_DATE("09:00:00", "%H:%i:%s") AND evento_inicio <= STR_TO_DATE("19:00:00", "%H:%i:%s")))');

    $conexion->query('UPDATE '.$tabla_citas_estado.' SET estado_cita = "No Show Up", estado_color = "'.$negro.'" WHERE estado_color NOT in ("'.$azul.'","'.$naranja.'","'.$rosado.'","'.$rojo.'") AND id_cita in (SELECT id_cita FROM '.$tabla_calendario.' WHERE id_clinica=1 AND NOW() > STR_TO_DATE(concat_ws(" ", evento_fecha,evento_inicio),"%Y-%m-%d %H:%i:%s") AND (evento_inicio < STR_TO_DATE("08:30:00", "%H:%i:%s") OR evento_inicio > STR_TO_DATE("19:00:00", "%H:%i:%s")))');

    //update event icon and confirm app when event color is black
    $conexion->query('UPDATE '.$tabla_calendario.' SET evento_icon = "check", evento_needforms = "no" WHERE id_cita in (SELECT id_cita FROM '.$tabla_citas_estado.' WHERE estado_color in ("'.$gris.'","'.$negro.'"))');

    $events = $conexion->query('SELECT * FROM '.$tabla_calendario.' WHERE id_clinica=1 ORDER BY id_evento');
    while($row_fill=$events->fetch_assoc())
    {
        $color = queryOne('SELECT * FROM '. $tabla_citas_estado . ' WHERE id_cita = '. $row_fill['id_cita'] . '');
        $json[]=array(
            'id'   => $row_fill["id_evento"],
            'title'   => $row_fill["id_paciente"],
            'start'   => $row_fill["evento_fecha"].'T'.$row_fill["evento_inicio"],
            'end'   => $row_fill["evento_fecha"].'T'.$row_fill["evento_fin"],
            'color'   => $color["estado_color"],
            'icon' => $row_fill["evento_icon"],
            'needform' => $row_fill["evento_needforms"]
        );
    }

    #Available hours background
    $horario = $conexion->query('SELECT * FROM '.$tabla_calendario_horas.' WHERE id_clinica=1;');
    while($business=$horario->fetch_assoc())
    {
        $bh[]=array(
            'start'   => $business["bh_date"].'T'.$business["bh_start"],
            'end'   => $business["bh_date"].'T'.$business["bh_end"],
            'rendering' => 'background',
            'overlap' => true,
            'color' => '#ff9f89',
            'groupId' => 'availableForAppointment'
        );
    }
    
    $jsonstring = json_encode(array_merge($json,$bh));
    echo $jsonstring;   
?>