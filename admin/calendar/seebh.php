<?php
    include('../include/database.php');
    include("../include/funciones.php");

    /* $clinic = $_POST['id_clinica']; */
    $result = $conexion->query('SELECT * FROM '.$tabla_calendario_horas.' WHERE id_clinica = 1;');
    $json = array();
    while($row=$result->fetch_assoc()){
        $fecha = $row['bh_date'];
        $fechaComoEntero = strtotime($fecha);
        $mes = date("F", $fechaComoEntero);
        $dia = date("l", $fechaComoEntero);

        $time_start = $row["bh_start"];
        $start = date("g:i A",strtotime("$time_start"));
        $time_end = $row["bh_end"];
        $end = date("g:i A",strtotime("$time_end"));

        /* echo '<tr>
                <td class="text-center" scope="row">'.$fecha.'</td>
                <td class="text-center" scope="row">'.$mes.'</td>
                <td class="text-center" scope="row">'.$dia.'</td>
                <td class="text-center" scope="row">'.$start.'</td>
                <td class="text-center" scope="row">'.$end.'</td>
            </tr>'; */
            
        }

        $json[]=array(
            'Date'   => $fecha,
            'Month'   => $mes,
            'Day'   => $dia,
            'Start'   => $start,
            'End'   => $end
        );
    
        $jsonstring = json_encode($json);
        echo $jsonstring;

