<?php 
    require("../include/database.php");
    include("../include/funciones.php");

    if(isset($_POST['id'])){
        $id = $_POST['id'];
        $condicion_actualizar = array(
            'id_hora' => $id
        );
        $campos_hora = array(
            'bh_date' => date("Y/m/d",strtotime(escapar($_POST['date']))),
            'bh_start' => escapar($_POST['starttime']),
            'bh_end' => escapar($_POST['endtime'])
        );
        if(actualizar($tabla_calendario_horas,$campos_hora,$condicion_actualizar)){
            echo "Success";
        }else{
            echo "Error";
        }
    }

?>