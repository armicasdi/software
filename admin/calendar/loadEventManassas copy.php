<?php 
    /* set_time_limit(120); */
    include("../include/eagle_con.php");
    require("../include/database.php");
    include("../include/funciones.php");

    #Colores
    $azul = "#16aaff";
    $verde = "#3ac47d";
    $naranja = "#f7b924";
    $gris = "#dcdcdc";
    $negro = "#343a40";
    $rosado = "#f775f0";

    #MySQL
    $resultado = $conexion->query('SELECT * FROM '.$tabla_calendario.' WHERE id_clinica=2 ORDER BY id_evento');
    /* $resultado = $conexion->query('SELECT * FROM '.$tabla_calendario.' WHERE id_clinica=2 and id_paciente = "MS6008";'); */
    $json = array();
    $array_eagle = array();

    while($row=$resultado->fetch_assoc())
    {
        $id_paciente = $row["id_paciente"];
        $id_paciente_eagle = trim($row["id_paciente_eagle"]);
        $manassas_chart = substr($id_paciente_eagle, 1);
        $cal_fecha_start = $row["evento_fecha"].' '.$row["evento_inicio"];
        $id_cita = $row["id_cita"];
        $id_cita_eagle = $row["id_cita"];

        array_push($array_eagle,$id_cita_eagle);
        #Sql anywhere
        #compara si existe la cita en el horario en mysql
        $query = "SELECT TOP 1 start_time,end_time,appointment_id FROM appointment WHERE YEAR(start_time) = YEAR(NOW()) AND patient_id = '".$manassas_chart."' ORDER BY appointment_id ASC;";

        /* $query = "SELECT start_time,end_time,appointment_id  FROM appointment WHERE patient_id = '".$manassas_chart."' AND start_time = '".$cal_fecha_start."' ;"; */
        $result = odbc_exec($conexion_anywhere,$query);

        while (odbc_fetch_row($result)) 
        {
            $app_date_start = odbc_result($result,'start_time');
            $app_date_end = odbc_result($result,'end_time');
            $app_id = odbc_result($result,'appointment_id');

            array_push($array_eagle,$app_id);

            $date = date_create($app_date_start);
            $date_fin = date_create($app_date_end);
            $fecha = date_format($date, 'Y-m-d');
            $hora_inicio = date_format($date, 'H:i:s');
            $hora_final = date_format($date_fin, 'H:i:s');
            $sync_color = $verde;

            $condicion = array(
                'id_cita' => $row['id_cita']
            );
            $campos_cita = array(
                'cita_fecha' => $fecha,
                'cita_hora' => $hora_inicio
            );
            $campos_calendario = array(
                'evento_fecha' => $fecha,
                'evento_inicio' => $hora_inicio,
                'evento_fin' => $hora_final,
                'id_cita_eagle' => $app_id
            );
            $valores = array(
                'estado_color' => $sync_color,
                'estado_cita' => "Scheduled",
            );

            actualizar($tabla_citas_estado,$valores,$condicion);
            actualizar($tabla_calendario,$campos_calendario,$condicion);
            actualizar($tabla_citas,$campos_cita,$condicion);
            /* } *///TERMINA SINCRONIZADOR
        }

        $query3 = "SELECT start_time,end_time FROM appointment 
        WHERE appointment_id = ".$id_cita_eagle." AND start_time <> '".$cal_fecha_start."';";
        $result3 = odbc_exec($conexion_anywhere,$query3);

        while (odbc_fetch_row($result3)) 
        {
            $app_date_start = odbc_result($result3,'start_time');
            $app_date_end = odbc_result($result3,'end_time');

            $date = date_create($app_date_start);
            $date_fin = date_create($app_date_end);
            $fecha = date_format($date, 'Y-m-d');
            $hora_inicio = date_format($date, 'H:i:s');
            $hora_final = date_format($date_fin, 'H:i:s');
            $sync_color = $verde;

            $condicion = array(
                'id_cita' => $row['id_cita']
            );
            $campos_cita = array(
                'cita_fecha' => $fecha,
                'cita_hora' => $hora_inicio
            );
            $campos_calendario = array(
                'evento_fecha' => $fecha,
                'evento_inicio' => $hora_inicio,
                'evento_fin' => $hora_final
            );
            $valores = array(
                'estado_color' => $sync_color,
                'estado_cita' => "Scheduled",
            );

            actualizar($tabla_citas_estado,$valores,$condicion);
            actualizar($tabla_calendario,$campos_calendario,$condicion);
            actualizar($tabla_citas,$campos_cita,$condicion);
            /* } *///TERMINA SINCRONIZADOR
        }
    }

    /* $array_eagle_string = implode(',',$array_eagle);

    $consulta_anywhere = "SELECT appointment_id,start_time,end_time,appointment.patient_id,location_id,date_appointed,appointment_type_id,chair_num,view_id,first_name,last_name,home_phone,birth_date FROM appointment 
    INNER JOIN scheduler_view_chair ON location_id = chair_num AND show_chair = 'Y' AND view_id = 3 
    INNER JOIN patient ON appointment.patient_id = patient.patient_id
    WHERE WEEKS(start_time) = WEEKS(NOW()) AND appointment_id NOT IN (".$array_eagle_string.") ORDER BY appointment_id ASC";

    $respuesta = odbc_exec($conexion_anywhere,$consulta_anywhere);

    while (odbc_fetch_row($respuesta))
    {
        $app_date_start = odbc_result($respuesta,'start_time');
        $app_date_end = odbc_result($respuesta,'end_time');
        $appointment_id = odbc_result($respuesta,'appointment_id');
        $patient_id = odbc_result($respuesta,'patient_id');
        $location_id = odbc_result($respuesta,'location_id');
        $date_appointed = odbc_result($respuesta,'date_appointed');
        $appointment_type_id = odbc_result($respuesta,'appointment_type_id');
        $chair_num = odbc_result($respuesta,'chair_num');
        $view_id = odbc_result($respuesta,'view_id');
        $first_name = odbc_result($respuesta,'first_name');
        $last_name = odbc_result($respuesta,'last_name');
        $home_phone = odbc_result($respuesta,'home_phone');
        $birth_date = odbc_result($respuesta,'birth_date');

        if (strpos($patient_id, 'F') !== false) {
            $id_paciente = str_replace('F','FX',$patient_id);
            $id_paciente_eagle = $patient_id;
        }else if (strpos($patient_id, 'W') !== false) {
            $id_paciente = str_replace('W','WE',$patient_id);
            $id_paciente_eagle = $patient_id;
        }
        else {
            $id_paciente = 'MS'.$patient_id;
            $id_paciente_eagle = 'M'.$patient_id;
        }

        $num_row_paciente = cantidad('id_paciente_eagle',$id_paciente_eagle,$tabla_pacientes);
        $num_row_app_id = cantidad('id_cita_eagle',$appointment_id,$tabla_calendario);        

        $date = date_create($app_date_start);
        $date_fin = date_create($app_date_end);
        $fecha = date_format($date, 'Y-m-d');
        $hora_inicio = date_format($date, 'H:i:s');
        $hora_final = date_format($date_fin, 'H:i:s');

        $intervalo = $date_fin->diff($date);
        $intervalo = $intervalo->format('%i min');
        $sync_color = $verde;

        if($num_row_paciente == 0){

            /* if($num_row_app_id == 0){

                $campos_paciente = array(
                    'id_paciente' => $id_paciente,
                    'id_paciente_eagle' => $id_paciente_eagle,
                    'paciente_nombres' => $first_name,
                    'paciente_apellidos' => $last_name,
                    'paciente_fechanac' => $birth_date,
                    'paciente_contacto' => $home_phone
                );

                $paciente = insertar($tabla_pacientes,$campos_paciente);

                $campos_cita = array(
                    'id_paciente' => $id_paciente,
                    'id_clinica' => 2,
                    'id_user' => escapar($_SESSION['user']),
                    'cita_fecha' => $fecha,
                    'cita_hora' => $hora_inicio,
                    'cita_duracion' => $intervalo
                );
                $id_cita = insertar($tabla_citas,$campos_cita);
                
                $valores = array(
                    'id_cita' => $id_cita,
                    'estado_color' => $sync_color,
                    'estado_cita' => "Scheduled"
                );
                insertar($tabla_citas_estado,$valores);

                $campos_calltracker = array(
                    'id_clinica' => 2,
                    'id_patient' => $id_paciente,
                    'call_schedule_app' => "yes",
                );
                $id_call = insertar($tabla_call_tracker,$campos_calltracker);

                $campos_calendario = array(
                    'id_cita' => $id_cita,
                    'id_clinica' => 2,
                    'id_paciente' => $id_paciente,
                    'id_paciente_eagle' => $id_paciente_eagle,
                    'id_call' => $id_call,
                    'evento_fecha' => $fecha,
                    'evento_inicio' => $hora_inicio,
                    'evento_fin' => $hora_final,
                    'id_cita_eagle' => $appointment_id,
                    'id_app_type' => $appointment_type_id
                );
                
                insertar($tabla_calendario,$campos_calendario);  
            }
        }
        else if($num_row_paciente > 0){
            if($num_row_app_id == 0){

                $id_call = queryOne('SELECT id_call as id FROM '.$tabla_call_tracker.' WHERE id_patient = "'.$id_paciente.'";');

                $campos_cita = array(
                    'id_paciente' => $id_paciente,
                    'id_clinica' => 2,
                    'id_user' => escapar($_SESSION['user']),
                    'cita_fecha' => $fecha,
                    'cita_hora' => $hora_inicio,
                    'cita_duracion' => $intervalo
                );

                $id_cita = insertar($tabla_citas,$campos_cita);
                
                $valores = array(
                    'id_cita' => $id_cita,
                    'estado_color' => $sync_color,
                    'estado_cita' => "Scheduled"
                );
            
                $campos_calendario = array(
                    'id_cita' => $id_cita,
                    'id_clinica' => 2,
                    'id_paciente' => $id_paciente,
                    'id_paciente_eagle' => $id_paciente_eagle,
                    'id_call' => $id_call['id'],
                    'evento_fecha' => $fecha,
                    'evento_inicio' => $hora_inicio,
                    'evento_fin' => $hora_final,
                    'id_cita_eagle' => $appointment_id,
                    'id_app_type' => $appointment_type_id
                );

                insertar($tabla_citas_estado,$valores);
                insertar($tabla_calendario,$campos_calendario); 
            }

        }
    } */


    /* $conexion->query('UPDATE '.$tabla_citas_estado.' SET estado_cita = "Scheduled", estado_color = "'.$verde.'" WHERE estado_color NOT in ("'.$azul.'","'.$naranja.'","'.$rosado.'") AND id_cita in (SELECT id_cita FROM '.$tabla_calendario.' WHERE id_clinica =1 AND NOW() < STR_TO_DATE(concat_ws(" ",evento_fecha,evento_inicio),"%Y-%m-%d %H:%i:%s") AND evento_inicio >= STR_TO_DATE("09:00:00", "%H:%i:%s") AND evento_inicio <= STR_TO_DATE("19:00:00", "%H:%i:%s"))'); */

    $conexion->query('UPDATE '.$tabla_citas_estado.' SET estado_cita = "Scheduled", estado_color = "'.$verde.'" WHERE estado_color NOT in ("'.$azul.'","'.$naranja.'","'.$rosado.'") AND id_cita in (SELECT id_cita FROM '.$tabla_calendario.' WHERE id_clinica =2 AND NOW() < STR_TO_DATE(concat_ws(" ",evento_fecha,evento_inicio),"%Y-%m-%d %H:%i:%s") AND evento_inicio >= STR_TO_DATE("09:00:00", "%H:%i:%s") AND evento_inicio <= STR_TO_DATE("19:00:00", "%H:%i:%s"))');

    $conexion->query('UPDATE '.$tabla_citas_estado.' SET estado_cita = "Finished", estado_color = "'.$gris.'" WHERE estado_color NOT in ("'.$azul.'","'.$naranja.'","'.$rosado.'") AND id_cita in (SELECT id_cita FROM '.$tabla_calendario.' WHERE id_clinica =2 AND NOW() > STR_TO_DATE(concat_ws(" ", evento_fecha,evento_inicio),"%Y-%m-%d %H:%i:%s") AND (evento_inicio >= STR_TO_DATE("09:00:00", "%H:%i:%s") AND evento_inicio <= STR_TO_DATE("19:00:00", "%H:%i:%s")))');

    $conexion->query('UPDATE '.$tabla_citas_estado.' SET estado_cita = "No Show Up", estado_color = "'.$negro.'" WHERE estado_color NOT in ("'.$azul.'","'.$naranja.'","'.$rosado.'") AND id_cita in (SELECT id_cita FROM '.$tabla_calendario.' WHERE id_clinica =2 AND NOW() > STR_TO_DATE(concat_ws(" ", evento_fecha,evento_inicio),"%Y-%m-%d %H:%i:%s") AND (evento_inicio < STR_TO_DATE("08:00:00", "%H:%i:%s") OR evento_inicio > STR_TO_DATE("19:00:00", "%H:%i:%s")))');

    //update event icon and confirm app when event color is black
    $conexion->query('UPDATE '.$tabla_calendario.' SET evento_icon = "check", evento_needforms = "no" WHERE id_cita in (SELECT id_cita FROM '.$tabla_citas_estado.' WHERE estado_color in ("'.$gris.'","'.$negro.'"))');

    $events = $conexion->query('SELECT * FROM '.$tabla_calendario.' WHERE id_clinica=2 ORDER BY id_evento');
    while($row_fill=$events->fetch_assoc())
    {
        $color = queryOne('SELECT * FROM '. $tabla_citas_estado . ' WHERE id_cita = '. $row_fill['id_cita'] . '');
        $json[]=array(
            'id'   => $row_fill["id_evento"],
            'title'   => $row_fill["id_paciente"],
            'start'   => $row_fill["evento_fecha"].'T'.$row_fill["evento_inicio"],
            'end'   => $row_fill["evento_fecha"].'T'.$row_fill["evento_fin"],
            'color'   => $color["estado_color"],
            'icon' => $row_fill["evento_icon"],
            'needform' => $row_fill["evento_needforms"]
        );
    }

    #Available hours background
    $horario = $conexion->query('SELECT * FROM '.$tabla_calendario_horas.' WHERE id_clinica=2;');
    while($business=$horario->fetch_assoc())
    {
        $bh[]=array(
            'start'   => $business["bh_date"].'T'.$business["bh_start"],
            'end'   => $business["bh_date"].'T'.$business["bh_end"],
            'rendering' => 'background',
            'overlap' => true,
            'color' => '#ff9f89',
            'groupId' => 'availableForAppointment'
        );
    }
    
    $jsonstring = json_encode(array_merge($json,$bh));
    echo $jsonstring;   
?>