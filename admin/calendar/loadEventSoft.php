<?php

    include('../include/database.php');
    include("../include/funciones.php");

    #MySQL
    $result = $conexion->query('SELECT * FROM '.$tabla_calendario.' WHERE id_clinica = 1 ORDER BY id_evento');
    $json = array();
    $bh = array(); //horario

    while($row=$result->fetch_assoc())
    {
        $id_cita = $row["id_cita"];
        $color = queryOne('SELECT * FROM '. $tabla_citas_estado . ' WHERE id_cita = '. $id_cita . '');

        $json[]=array(
            'id'   => $row["id_evento"],
            'title'   => $row["id_paciente"],
            'start'   => $row["evento_fecha"].'T'.$row["evento_inicio"],
            'end'   => $row["evento_fecha"].'T'.$row["evento_fin"],
            'color'   => $color["estado_color"],
            'icon' => $row["evento_icon"],
            'needform' => $row["evento_needforms"]
        );
    }

    $horario = $conexion->query('SELECT * FROM '.$tabla_calendario_horas.' WHERE id_clinica = 1;');
    while($business=$horario->fetch_assoc())
    {
        $bh[]=array(
            'start'   => $business["bh_date"].'T'.$business["bh_start"],
            'end'   => $business["bh_date"].'T'.$business["bh_end"],
            'rendering' => 'background',
            'overlap' => true,
			'color' => '#ff9f89',
			'groupId' => 'availableForAppointment'
        );
    }


    $jsonstring = json_encode(array_merge($json,$bh));
    /* $jsonstring = json_encode($json); */
    echo $jsonstring;

?>
