<?php
    echo "Exito";
    die();
    include("../include/eagle_con.php");
    require("../include/database.php");
    include("../include/funciones.php");

    $count_pat_update = 0;
    $count_appt_insert = 0;

    //creating Temporal Table 

    $conexion->query('CREATE TEMPORARY TABLE appointment_eagle (
        id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        appointment_id int(15) NULL,
        description varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
        allday_event bit(10) NULL,
        start_time datetime NULL,
        end_time datetime NULL,
        patient_id varchar(15) NULL,
        recall_id int(10) NULL,
        location_id int(10) NULL,
        location_free_form varchar(100) NULL,
        classification int(10) NULL,
        appointment_type_id int(11) NULL,
        prefix varchar(10) NULL,
        dollars_scheduled decimal(10,2) NULL,
        date_appointed datetime NULL,
        date_confirmed datetime NULL,
        appointment_notes TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
        deletion_note text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
        sooner_if_possible bit(10) NULL,
        scheduled_by varchar(25) NULL,
        modified_by varchar(35) NULL,
        appointment_name varchar(200) NULL,
        arrival_status int(10) NULL,
        arrival_time datetime NULL,
        inchair_time datetime NULL,
        walkout_time datetime NULL,
        confirmation_status int(10) NULL,
        confirmation_note text NULL,
        auto_confirm_sent int(10) NULL,
        recurrence_id int(10) NULL,
        private bit(10) NULL,
        priority int(10) NULL,
        appointment_data varchar(40) NULL,
        INDEX index_1 (appointment_id),
        INDEX index_2 (start_time),
        INDEX index_3 (patient_id),
        INDEX index_4 (end_time),
        INDEX index_5 (date_confirmed),
        INDEX index_6 (confirmation_status),
        INDEX index_7 (walkout_time)
      ) ENGINE=MyISAM DEFAULT CHARSET=utf8;');

      // Query to EagleSoft

    $query = "SELECT appointment_id,description,allday_event,start_time,end_time,patient_id,recall_id,location_id,location_free_form,classification,appointment_type_id,prefix,dollars_scheduled,date_appointed,date_confirmed,appointment_notes,deletion_note,sooner_if_possible,scheduled_by,modified_by,appointment_name,arrival_status,arrival_time,inchair_time,walkout_time,confirmation_status,confirmation_note,auto_confirm_sent,recurrence_id,private,priority,appointment_data FROM appointment WHERE date(date_appointed)>'2021-06-25';";

    $to_insert = array();
    
    $result = odbc_exec($conexion_anywhere,$query); // Call to Eaglesoft Database 

    while (odbc_fetch_row($result)) 
    {
        $patient_id = odbc_result($result,"patient_id");
        if (strpos($patient_id, 'F') !== false) {
            $id_paciente = str_replace('F','FX',$patient_id);
            $id_paciente_eagle = $patient_id;
        }
        else if (strpos($patient_id, 'W') !== false) {
            $id_paciente = str_replace('W','WE',$patient_id);
            $id_paciente_eagle = $patient_id;
        }
        else {
            $id_paciente = 'MS'.$patient_id;
            $id_paciente_eagle = 'M'.$patient_id;
        }

        $appointment_id = odbc_result($result,"appointment_id");
        #$description = trim(odbc_result($result,"description"));
        $description = trim(utf8_encode(json_encode(odbc_result($result,"description"))));
        $allday_event = odbc_result($result,"allday_event");
        /* $start_time = date("Y-m-d H:m:s",strtotime(odbc_result($result,"start_time")));
        $end_time = date("Y-m-d H:m:s",strtotime(odbc_result($result,"end_time"))); */

        if(!empty(odbc_result($result,"start_time"))){
            $start_time = date("Y-m-d H:m:s",strtotime(odbc_result($result,"start_time")));
        }else{
            $start_time = null;
        }

        if(!empty(odbc_result($result,"end_time"))){
            $end_time = date("Y-m-d H:m:s",strtotime(odbc_result($result,"end_time")));
        }else{
            $end_time = null;
        }


        $patient_id = odbc_result($result,"patient_id");
        $recall_id = odbc_result($result,"recall_id") ?? 0;
        $location_id = odbc_result($result,"location_id") ?? 0;
        $location_free_form = odbc_result($result,"location_free_form");
        $classification = odbc_result($result,"classification") ?? 0;
        $appointment_type_id = odbc_result($result,"appointment_type_id") ?? 0;
        $prefix = odbc_result($result,"prefix");
        $dollars_scheduled = odbc_result($result,"dollars_scheduled") ?? 0;
        $date_appointed = date("Y-m-d H:m:s",strtotime(odbc_result($result,"date_appointed"))) ?? "0000-00-00 00:00:00";
        $date_confirmed = date("Y-m-d H:m:s",strtotime(odbc_result($result,"date_confirmed"))) ?? "0000-00-00 00:00:00";

        /* if(!empty(odbc_result($result,"date_appointed"))){
            $date_appointed = date("Y-m-d H:m:s",strtotime(odbc_result($result,"date_appointed")));
        }else{
            $date_appointed = null;
        }

        if(!empty(odbc_result($result,"date_confirmed"))){
            $date_confirmed = date("Y-m-d H:m:s",strtotime(odbc_result($result,"date_confirmed")));
        }else{
            $date_confirmed = null;
        } */

        $appointment_notes = utf8_encode(json_encode(odbc_result($result,"appointment_notes"),JSON_INVALID_UTF8_IGNORE));
        $deletion_note = utf8_encode(json_encode(odbc_result($result,"deletion_note"),JSON_INVALID_UTF8_IGNORE));
        #$deletion_note = odbc_result($result,"deletion_note");
        $sooner_if_possible = odbc_result($result,"sooner_if_possible");
        $scheduled_by = odbc_result($result,"scheduled_by");
        $modified_by = odbc_result($result,"modified_by");
        $appointment_name = odbc_result($result,"appointment_name");
        $arrival_status = odbc_result($result,"arrival_status") ?? 0;
        $arrival_time = date("Y-m-d H:m:s",strtotime(odbc_result($result,"arrival_time"))) ?? "0000-00-00 00:00:00";
        $inchair_time = date("Y-m-d H:m:s",strtotime(odbc_result($result,"inchair_time"))) ?? "0000-00-00 00:00:00";
        $walkout_time = date("Y-m-d H:m:s",strtotime(odbc_result($result,"walkout_time"))) ?? "0000-00-00 00:00:00";

        $confirmation_status = odbc_result($result,"confirmation_status") ?? 0;
        $confirmation_note = utf8_encode(json_encode(odbc_result($result,"confirmation_note")));
        #$confirmation_note = odbc_result($result,"confirmation_note");
        $auto_confirm_sent = odbc_result($result,"auto_confirm_sent") ?? 0;
        $recurrence_id = odbc_result($result,"recurrence_id") ?? 0;
        $private = odbc_result($result,"private") ?? 0;
        $priority = odbc_result($result,"priority") ?? 0;
        $appointment_data = odbc_result($result,"appointment_data");

        $parentesis = '(NULL,'.$appointment_id.','.$description.','.$allday_event.',"'.$start_time.'","'.$end_time.'","'.$patient_id.'",'.$recall_id.','.$location_id.',"'.$location_free_form.'",'.$classification.','.$appointment_type_id.',"'.$prefix.'",'.$dollars_scheduled.',"'.$date_appointed.'","'.$date_confirmed.'",'.$appointment_notes.','.$deletion_note.','.$sooner_if_possible.',"'.$scheduled_by.'","'.$modified_by.'","'.$appointment_name.'",'.$arrival_status.',"'.$arrival_time.'","'.$inchair_time.'","'.$walkout_time.'",'.$confirmation_status.','.$confirmation_note.','.$auto_confirm_sent.','.$recurrence_id.','.$private.','.$priority.',"'.$appointment_data.'")';

        array_push($to_insert,$parentesis);
    }

    $array_string = implode(",",$to_insert);

    //Inserting in Temporal Table data From Eaglesoft

    $conexion->query('INSERT INTO appointment_eagle (id,appointment_id,description,allday_event,start_time,end_time,patient_id,recall_id,location_id,location_free_form,classification,appointment_type_id,prefix,dollars_scheduled,date_appointed,date_confirmed,appointment_notes,deletion_note,sooner_if_possible,scheduled_by,modified_by,appointment_name,arrival_status,arrival_time,inchair_time,walkout_time,confirmation_status,confirmation_note,auto_confirm_sent,recurrence_id,private,priority,appointment_data) VALUES '.$array_string.';');

    /* echo 'INSERT INTO appointment_eagle (id,appointment_id,description,allday_event,start_time,end_time,patient_id,recall_id,location_id,location_free_form,classification,appointment_type_id,prefix,dollars_scheduled,date_appointed,date_confirmed,appointment_notes,deletion_note,sooner_if_possible,scheduled_by,modified_by,appointment_name,arrival_status,arrival_time,inchair_time,walkout_time,confirmation_status,confirmation_note,auto_confirm_sent,recurrence_id,private,priority,appointment_data) VALUES '.$array_string.';';*/

    #MOSTRANDO LA TABLA TEMPORAL

    /* $resultado = $conexion->query('SELECT * FROM appointment_eagle');
    while ($reg=$resultado->fetch_assoc()) {
        print_r($reg);
    } */

    # ================ INSERTAR NUEVOS ================

    // Select from Temporal Table 
    $resultado_insert = $conexion->query('SELECT appointment_id,description,allday_event,start_time,end_time,patient_id,recall_id,location_id,location_free_form,classification,appointment_type_id,prefix,dollars_scheduled,date_appointed,date_confirmed,appointment_notes,deletion_note,sooner_if_possible,scheduled_by,modified_by,appointment_name,arrival_status,arrival_time,inchair_time,walkout_time,confirmation_status,confirmation_note,auto_confirm_sent,recurrence_id,private,priority,appointment_data FROM appointment_eagle WHERE appointment_id NOT IN (SELECT appointment_id FROM appointment) and date(date_appointed)>"2021-06-25"');
    
    if(!empty($resultado_insert)){
        while ($row=$resultado_insert->fetch_assoc()) {
            if(insertar('appointment',$row)){
                $count_appt_insert++;
            }
        }
        echo $count_appt_insert." Nuevas citas han sido agregadas satisfactoriamente".PHP_EOL;
    }else{
        echo "no encontre registros nuevos";
    }

    # ================ ACTUALIZAR SEGURO EN PACIENTES EXISTENTES ================

    $count_pat_update = $conexion->query('UPDATE
                            appointment appt
                        INNER JOIN appointment_eagle appte ON
                            appt.appointment_id = appte.appointment_id
                        SET
                            appt.start_time = appte.start_time,
                            appt.end_time = appte.end_time,
                            appt.location_id = appte.location_id,
                            appt.appointment_type_id = appte.appointment_type_id,
                            appt.date_confirmed = appte.date_confirmed,
                            appt.modified_by = appte.modified_by,
                            appt.appointment_notes = appte.appointment_notes,
                            appt.arrival_status = appte.arrival_status,
                            appt.arrival_time = appte.arrival_time,
                            appt.inchair_time = appte.inchair_time,
                            appt.walkout_time = appte.walkout_time,
                            appt.confirmation_status = appte.confirmation_status,
                            appt.confirmation_note = appte.confirmation_note
                        WHERE appt.start_time <> appte.start_time or appt.end_time <> appte.end_time or appt.location_id <> appte.location_id or appt.appointment_type_id <> appte.appointment_type_id or appt.date_confirmed <> appte.date_confirmed or appt.modified_by <> appte.modified_by or appt.appointment_notes <> appte.appointment_notes or appt.arrival_time <> appte.arrival_time or appt.arrival_status <> appte.arrival_status or appt.inchair_time <> appte.inchair_time or appt.walkout_time <> appte.walkout_time or appt.confirmation_status <> appte.confirmation_status or appt.confirmation_note <> appte.confirmation_note;');
    
    echo PHP_EOL." REGISTROS MODIFICADOS: ". $count_pat_update;