<!-- Modal -->
<div class="modal fade" id="addEntry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-file-phone icon-gradient bg-happy-itmeo">
                    </i>New Call Tracker Entry
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="" enctype="multipart/form-data" class="needs-validation" novalidate>
                <div class="modal-body">
                    <div class="card-body">
                        
                        <h5 class="card-title">PATIENT TYPE</h5>
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <div>
                                        <div class="custom-radio custom-control"><input type="radio" checked
                                                id="new_patient" name="patient_type[]" value="New Patient"
                                                class="custom-control-input"><label class="custom-control-label"
                                                for="new_patient">New Patient</label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    <div>
                                        <div class="custom-radio custom-control"><input type="radio"
                                                id="current_patient" name="patient_type[]" value="Current Patient"
                                                class="custom-control-input"><label class="custom-control-label"
                                                for="current_patient">Current Patient</label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5" style="display: block;" id="patientchartdiv">
                                <div class="position-relative row form-group"><label for="patientName"
                                        class="col-sm-5 col-form-label"><b>Patient Chart</b></label>
                                    <div class="col-sm-7">
                                        <input class="form-control" type="text" id="id_paciente" name="id_paciente"
                                            required>
                                        <input class="form-control" type="hidden" id="id_paciente_eagle"
                                            name="id_paciente_eagle">
                                        <div class="invalid-feedback">Please select a clinic to create the id.</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="np_patient" style="display: block;">
                            <h5 class="card-title">PATIENT INFO</h5>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="new_name"
                                            class="col-sm-3 col-form-label"><b>Name</b></label>
                                        <div class="col-sm-9">
                                            <input input class="form-control" type="text" id="new_name" name="new_name"
                                                required>
                                            <div class="invalid-feedback">Please input a valid name.</div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="new_lastname"
                                            class="col-sm-3 col-form-label"><b>Lastname</b></label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" id="new_lastname"
                                                name="new_lastname" required>
                                            <div class="invalid-feedback">Please input a valid last name.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="new_contact"
                                            class="col-sm-4 col-form-label"><b>Contact Phone</b></label>
                                        <div class="col-sm-8">
                                            <input class="phone form-control" type="text" id="new_contact"
                                                name="new_contact" required>
                                            <div class="invalid-feedback">Please input a phone number.</div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="new_birth"
                                            class="col-sm-4 col-form-label"><b>Date of Birth</b></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="date" id="new_birth" name="new_birth"
                                                required>
                                            <div class="invalid-feedback">Please input a valid birthday.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h5 class="card-title">SHEDULE APPOINTMENT</h5>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <div>
                                                <div class="custom-radio custom-control">
                                                    <input type="radio" checked id="sche_yes" name="schedule_app[]" value="yes"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="sche_yes">Schedule Appointment</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="position-relative form-group">
                                            <div>
                                                <div class="custom-radio custom-control">
                                                    <input type="radio" id="sche_no" name="schedule_app[]" value="no"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="sche_no">Only Call</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="form-row" id="schedule_app_div" style="display: block;">
                                
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Date</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" value="<?php echo date("Y-m-d");?>"
                                                    id="dateApp">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Clinic</b></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="clinic" name="clinic">
                                                    <?php
                                                            $result = $conexion->query('SELECT * FROM '. $tabla_clinicas .' ORDER BY clinica_nombre');
                                                            while($costo=$result->fetch_assoc()):
                                                                $clinic_codigo=$costo['id_clinica'];
                                                                $clinic_name=$costo['clinica_nombre'];
                                                        ?>
                                                    <option value="<?php echo $clinic_codigo?>"><?php echo $clinic_name?></option>
                                                    <?php endwhile; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for="notes"
                                                class="col-sm-3 col-form-label"><b>Notes</b></label>
                                            <div class="col-sm-9">
                                                <textarea id="notes" name="notes" rows="2" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="position-relative row form-group"><label for="time"
                                                class="col-sm-3 col-form-label"><b>Time</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="time">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Duration</b></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="durationApp">
                                                    <option value="30 min">00:30 min</option>
                                                    <option value="45 min">00:45 min</option>
                                                    <option value="60 min">01:00 hour</option>
                                                    <option value="75 min">01:15 hour</option>
                                                    <option value="90 min">01:30 hour</option>
                                                    <option value="105 min">01:45 hour</option>
                                                    <option value="120 min">02:00 hour</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for="reasonApp"
                                                class="col-sm-3 col-form-label"><b>Reason</b></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="reasonApp" name="reasonApp">
                                                    <?php
                                                        $result = $conexion->query('SELECT * FROM '. $tabla_citas_reason .' ORDER BY reason_nombre');
                                                        while($costo=$result->fetch_assoc()):
                                                            $reason_codigo=$costo['id_reason'];
                                                            $reason_name=$costo['reason_nombre'];
                                                    ?>
                                                    <option value="<?php echo $reason_codigo?>">
                                                        <?php echo $reason_name?>
                                                    </option>
                                                    <?php endwhile; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h5 class="card-title">INSURANCE</h5>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="position-relative row form-group"><label for="new_have_insurance"
                                                class="col-sm-5 col-form-label"><b>Have Insurance ?</b></label>
                                            <div class="col-sm-7">
                                                <select class="form-control" id="new_have_insurance"
                                                    name="new_have_insurance">
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>
                                                <div class="invalid-feedback">Please select an option.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="insurancetype" style="display: block;">
                                            <div class="position-relative row form-group"><label for="new_insurance_type"
                                                    class="col-sm-5 col-form-label"><b>Type Insurance</b></label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="new_insurance_type"
                                                        name="new_insurance_type">
                                                        <option value="Self">Self</option>
                                                        <option value="Policy Holder">Policy Holder</option>
                                                    </select>
                                                    <div class="invalid-feedback">Please select an option.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row" id="no_schedule_app_div" style="display: none;">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for=""
                                            class="col-sm-3 col-form-label"><b>Clinic</b></label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="clinic_noapp" name="clinic_noapp">
                                                <?php
                                                    $result = $conexion->query('SELECT * FROM '. $tabla_clinicas .' ORDER BY clinica_nombre');
                                                    while($clinica=$result->fetch_assoc()):
                                                        $clinic_codigo=$clinica['id_clinica'];
                                                        $clinic_name=$clinica['clinica_nombre'];
                                                ?>
                                                <option value="<?php echo $clinic_codigo;?>"><?php echo $clinic_name;?></option>
                                                <?php endwhile; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group">
                                        <label for="notes_noschedule" class="col-sm-3 col-form-label"><b>Notes</b></label>
                                        <div class="col-sm-9">
                                            <textarea id="notes_noschedule" name="notes_noschedule" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="policyholder" style="display: none;">
                                <h5 class="card-title">POLICY HOLDER INFORMATION</h5>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="position-relative row form-group"><label for="name_ph"
                                                class="col-sm-3 col-form-label"><b>Name</b></label>
                                            <div class="col-sm-9">
                                                <input input class="form-control" type="text" id="name_ph" name="name_ph" required>
                                                <input class="form-control" type="hidden" id="id_paciente_ph">
                                                <input class="form-control" type="hidden" id="id_paciente_eagle_ph">
                                                <div class="invalid-feedback">Please input a valid name.</div>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for="lastname_ph"
                                                class="col-sm-3 col-form-label"><b>Lastname</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" id="lastname_ph"
                                                    name="lastname_ph" required>
                                                <div class="invalid-feedback">Please input a valid last name.</div>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for="relation_ph"
                                                class="col-sm-3 col-form-label"><b>Relationship</b></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="relation_ph" name="relation_ph">
                                                    <option value="Spouse">Spouse</option>
                                                    <option value="Child">Child</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                                <div class="invalid-feedback">Please select an option.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="position-relative row form-group"><label for="phone_ph"
                                                class="col-sm-4 col-form-label"><b>Contact Phone</b></label>
                                            <div class="col-sm-8">
                                                <input class="phone form-control" type="text" id="phone_ph"
                                                    name="phone_ph" required>
                                                <div class="invalid-feedback">Please input a phone number.</div>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for="birth_ph"
                                                class="col-sm-4 col-form-label"><b>Date of Birth</b></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="date" id="birth_ph" name="birth_ph"
                                                    required>
                                                <div class="invalid-feedback">Please input a valid birthday.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <h5 class="card-title">Referral</h5>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="insurance_type"
                                            class="col-sm-3 col-form-label"><b>Channel</b></label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="select_channel" name="select_channel">
                                                <option selected disabled>Select channel ..</option>

                                                <?php
                                                        $result = $conexion->query('SELECT * FROM '. $tabla_call_channel .' ORDER BY channel');
                                                        while($referal=$result->fetch_assoc()):
                                                            $channel_codigo=$referal['id_channel'];
                                                            $channel_name=$referal['channel'];
                                                    ?>

                                                <option value="<?php echo $channel_codigo?>">
                                                    <?php echo $channel_name?>
                                                </option>
                                                <?php endwhile; ?>
                                            </select>
                                            <div class="invalid-feedback">Select a Channel option</div>
                                            <!-- <input class="form-control" type="text" id="select_channel" placeholder="type a channel"
                                                name="select_channel"> -->
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div id="sub_div_referal" style="display: none;">
                                        <div class="position-relative row form-group">
                                            <label for="insurance_type"
                                                class="col-sm-3 col-form-label"><b>Referral</b></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="select_referal" name="select_referal"
                                                    style="text-transform: capitalize;">

                                                </select>
                                                <div class="invalid-feedback">Select a Referral Option</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="currentpatient" style="display: none;">
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-7 col-form-label"><b>Current Patient Chart
                                                    (ID)</b></label>
                                            <div class="col-sm-5">
                                                <input class="form-control" type="text" id="currentID" name="currentID">
                                                <div class="invalid-feedback">Please input a current patient ID.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="campaign" style="display: none;">
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Campaign</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" id="campaignR" name="campaignR">
                                                <div class="invalid-feedback">Please input a campaign</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="cp_patient" style="display: none;">
                            <h5 class="card-title">PATIENT INFO</h5>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="position-relative row form-group"><label for="patientName"
                                            class="col-sm-3 col-form-label"><b>Patient</b></label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-search"></i></span>

                                                </div>
                                                <input type="text" id="search-patient" class="form-control" name="patient"
                                                    placeholder="Type ID, Patient Name or Last Name">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h5 class="card-title">SHEDULE APPOINTMENT</h5>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <div>
                                                <div class="custom-radio custom-control">
                                                    <input type="radio" checked id="sche_yes_cp" name="schedule_app_cp[]" value="yes"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="sche_yes_cp">Schedule Appointment</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="position-relative form-group">
                                            <div>
                                                <div class="custom-radio custom-control">
                                                    <input type="radio" id="sche_no_cp" name="schedule_app_cp[]" value="no"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="sche_no_cp">Only Call</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="form-row" id="schedule_app_div_current_patient" style="display: block;">
                                
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Date</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" value="<?php echo date("Y-m-d");?>"
                                                    name="current_date_app">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Clinic</b></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="current_clinic" name="current_clinic">
                                                    <?php
                                                            $result = $conexion->query('SELECT * FROM '. $tabla_clinicas .' ORDER BY clinica_nombre');
                                                            while($costo=$result->fetch_assoc()):
                                                                $clinic_codigo=$costo['id_clinica'];
                                                                $clinic_name=$costo['clinica_nombre'];
                                                        ?>
                                                    <option value="<?php echo $clinic_codigo?>"><?php echo $clinic_name?></option>
                                                    <?php endwhile; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Notes</b></label>
                                            <div class="col-sm-9">
                                                <textarea id="current_notes" name="current_notes" rows="2"
                                                    class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Time</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="time" value="<?php $mDate=new DateTime();
                                                    $hoy=$mDate->format("H:i"); echo $hoy;?>" name="current_time_app">
                                            </div>
                                        </div>

                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Duration</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="time" min="00:30" max="03:00" step="900"
                                                    value="00:30" name="current_duration_app">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for=""
                                                class="col-sm-3 col-form-label"><b>Reason</b></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="current_reason_app"
                                                    name="current_reason_app">
                                                    <?php
                                                        $result = $conexion->query('SELECT * FROM '. $tabla_citas_reason .' ORDER BY reason_nombre');
                                                        while($costo=$result->fetch_assoc()):
                                                            $reason_codigo=$costo['id_reason'];
                                                            $reason_name=$costo['reason_nombre'];
                                                    ?>
                                                    <option value="<?php echo $reason_codigo?>">
                                                        <?php echo $reason_name?>
                                                    </option>
                                                    <?php endwhile; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h5 class="card-title">INSURANCE</h5>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="position-relative row form-group"><label for="new_have_insurance"
                                                class="col-sm-5 col-form-label"><b>Have Insurance ?</b></label>
                                            <div class="col-sm-7">
                                                <select class="form-control" id="new_have_insurance"
                                                    name="new_have_insurance">
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>
                                                <div class="invalid-feedback">Please select an option.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="insurancetype" style="display: block;">
                                            <div class="position-relative row form-group"><label for="new_insurance_type"
                                                    class="col-sm-5 col-form-label"><b>Type Insurance</b></label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="new_insurance_type"
                                                        name="new_insurance_type">
                                                        <option value="Self">Self</option>
                                                        <option value="Policy Holder">Policy Holder</option>
                                                    </select>
                                                    <div class="invalid-feedback">Please select an option.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row" id="no_schedule_app_div_current_patient" style="display: none;">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for=""
                                            class="col-sm-3 col-form-label"><b>Clinic</b></label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="clinic_noapp_cp" name="clinic_noapp">
                                                <?php
                                                    $result = $conexion->query('SELECT * FROM '. $tabla_clinicas .' ORDER BY clinica_nombre');
                                                    while($clinica=$result->fetch_assoc()):
                                                        $clinic_codigo=$clinica['id_clinica'];
                                                        $clinic_name=$clinica['clinica_nombre'];
                                                ?>
                                                <option value="<?php echo $clinic_codigo;?>"><?php echo $clinic_name;?></option>
                                                <?php endwhile; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group">
                                        <label for="notes_noschedule" class="col-sm-3 col-form-label"><b>Notes</b></label>
                                        <div class="col-sm-9">
                                            <textarea id="notes_noschedule" name="notes_noschedule_cp" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h5 class="card-title">REFERRAL</h5>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for=""
                                            class="col-sm-3 col-form-label"><b>Channel</b></label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="current_channel" name="current_channel">
                                                <option selected disabled>Select channel ..</option>

                                                <?php
                                                        $result = $conexion->query('SELECT * FROM '. $tabla_call_channel .' ORDER BY channel');
                                                        while($referal=$result->fetch_assoc()):
                                                            $channel_codigo=$referal['id_channel'];
                                                            $channel_name=$referal['channel'];
                                                    ?>

                                                <option value="<?php echo $channel_codigo?>">
                                                    <?php echo $channel_name?>
                                                </option>
                                                <?php endwhile; ?>
                                            </select>
                                            <!-- <input class="form-control" type="text" id="select_channel" placeholder="type a channel"
                                                name="select_channel"> -->
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div id="current_referral_div" style="display: none;">
                                        <div class="position-relative row form-group">
                                            <label for="insurance_type"
                                                class="col-sm-3 col-form-label"><b>Referral</b></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="current_referral"
                                                    name="current_referral" style="text-transform: capitalize;">

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="currentpatient" style="display: none;">
                                        <div class="position-relative row form-group"><label for="insurance_type"
                                                class="col-sm-7 col-form-label"><b>Current Patient Chart
                                                    (ID)</b></label>
                                            <div class="col-sm-5">
                                                <input class="form-control" type="text" id="current_pid_ref"
                                                    name="current_pid_ref">
                                                <div class="invalid-feedback">Please input a current patient ID.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="campaign" style="display: none;">
                                        <div class="position-relative row form-group"><label for="insurance_type"
                                                class="col-sm-3 col-form-label"><b>Campaign</b></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" id="current_campaign"
                                                    name="current_campaign">
                                                <div class="invalid-feedback">Please input a campaign</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" name="crearRegistro" id="crearRegistro">Save
                        Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //timepicker
        $('#time').timepicker({
            timeFormat: 'hh:mm p',
            interval: 30,
            minTime: new Date(0, 0, 0, 9, 0, 0),
            maxTime: '7:00pm',
            defaultTime: '09:00',
            startTime: '09:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
        

        function ConvertTimeformat(format, str) {
            var hours = Number(str.match(/^(\d+)/)[1]);
            var minutes = Number(str.match(/:(\d+)/)[1]);
            var AMPM = str.match(/\s?([AaPp][Mm]?)$/)[1];
            var pm = ['P', 'p', 'PM', 'pM', 'pm', 'Pm'];
            var am = ['A', 'a', 'AM', 'aM', 'am', 'Am'];
            if (pm.indexOf(AMPM) >= 0 && hours < 12) hours = hours + 12;
            if (am.indexOf(AMPM) >= 0 && hours == 12) hours = hours - 12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10) sHours = "0" + sHours;
            if (minutes < 10) sMinutes = "0" + sMinutes;
            if (format == '0000') {
                return (sHours + sMinutes);
            } else if (format == '00:00') {
                return (sHours + ":" + sMinutes);
            } else {
                return false;
            }
        }

        
        /* $("#patientID").change(function () {
                var numero = $("#clinicNumber").val();
                var clinic_id = ('00' + numero).slice(-3);
                console.log(clinic_id);
                $("#clinicID").text(clinic_id);
            });

        $("#clinicName").keypress(function () {
            var clinicName = $('#clinicName').val();
            var letra = clinicName.charAt(0);
            letra = letra.toUpperCase();
            console.log(letra);
        }); */


         //Autocomplete Current Patient Search
        $("#search-patient").autocomplete({
            
            source: 'include/loadCode.php',
            appendTo: "#newCallEntry",
            select: function (event, ui) {
                $('#search-patient').val(ui.item.id_paciente+': '+ ui.item.paciente_apellidos+', '+ui.item.paciente_nombres);
                $('#id_paciente').val(ui.item.id_paciente);
                return false;
            }
        }); 

        $('[name="schedule_app[]"]').click(function () {
            var hizo_cita = $('[name="schedule_app[]"]:checked').val();
            if(hizo_cita == "yes"){
                $('#id_paciente').css('display','block');
                $('#schedule_app_div').css('display','block');
                $('#no_schedule_app_div').css('display','none');
                $("#new_contact").prop("required",true);
                $("#new_birth").prop("required",true);
            }
            else if(hizo_cita == "no"){
                $('#schedule_app_div').css('display','none');
                $('#no_schedule_app_div').css('display','block');
                $("#new_contact").removeAttr("required");
                $("#new_birth").removeAttr("required");
                $('#id_paciente').css('display','none');
            }
        });

        $('[name="patient_type[]"]').click(function () {
            var valor = $('[name="patient_type[]"]:checked').val();
            if (valor == 'New Patient') {
                $('#np_patient').css('display', 'block');
                $('#cp_patient').css('display', 'none');

               /*  $("#new_name").prop("required",true);
                $("#new_lastname").prop("required",true);
                $("#new_contact").prop("required",true);
                $("#new_birth").prop("required",true); */

            } else if (valor == 'Current Patient') {
                $('#np_patient').css('display', 'none');
                $('#cp_patient').css('display', 'block');

                $("#new_name").removeAttr("required");
                $("#new_lastname").removeAttr("required");
                $("#new_contact").removeAttr("required");
                $("#new_birth").removeAttr("required");
            }

        });

        $("#new_have_insurance").change(function () {
            var val = $('#new_have_insurance').val();

            if (val == 'yes') {
                $('#insurancetype').css('display', 'block');
            } else if (val == 'no') {
                $('#insurancetype').css('display', 'none');
                $('#policyholder').css('display', 'none')
            }
        });

        $("#new_insurance_type").change(function () {
            var val = $('#new_insurance_type').val();

            if (val == 'Policy Holder') {
                $('#policyholder').css('display', 'block');

                var clinicName = $('#clinic option:selected').html();
                var clinicId = $('#clinic').val();
                $.ajax({
                    type: "POST",
                    url: 'include/loadPatientId2.php',
                    data: 'id_clinica=' + clinicId,
                    success: function (resp) {

                        var letraI = clinicName.charAt(0);
                        var letraF = clinicName.charAt(clinicName.length - 1);
                        var id_paciente = letraI.toUpperCase() + letraF.toUpperCase();
                        var id_paciente_eagle = letraI.toUpperCase();

                        $("#id_paciente_ph").val(id_paciente + resp);
                        $("#id_paciente_eagle_ph").val(id_paciente_eagle + resp);

                    }
                });
            } else if (val == 'self') {
                $('#policyholder').css('display', 'none');
            }

        });

        $("#select_channel").change(function () {
            var val = $('#select_channel :selected').val();
            $('#sub_div_referal').css('display', 'block');

            $.ajax({
                type: "POST",
                url: 'include/loadReferal.php',
                data: 'id_channel=' + val,
                success: function (resp) {
                    $('#select_referal').html(resp);
                }
            });

        });

        $("#current_channel").change(function () {
            var val = $('#current_channel :selected').val();
            $('#current_referral_div').css('display', 'block');

            $.ajax({
                type: "POST",
                url: 'include/loadReferal.php',
                data: 'id_channel=' + val,
                success: function (resp) {
                    $('#current_referral').html(resp);
                }
            });

        });

        //CREAR ID DE PACIENTE
        $("#clinic").change(function () {
            var clinicName = $('#clinic option:selected').html();
            var clinicId = $('#clinic').val();

            var valor = $('[name="patient_type[]"]:checked').val();

            if (valor == 'New Patient') {

                $.ajax({
                    type: "POST",
                    url: 'include/loadPatientId.php',
                    data: 'id_clinica=' + clinicId,
                    success: function (resp) {
                        console.log(resp);
                        var letraI = clinicName.charAt(0);
                        var letraF = clinicName.charAt(clinicName.length - 1);
                        var id_paciente = letraI.toUpperCase() + letraF.toUpperCase();
                        var id_paciente_eagle = letraI.toUpperCase();

                        $("#id_paciente").val(id_paciente + resp);
                        $("#id_paciente_eagle").val(id_paciente_eagle + resp);
                        console.log(id_paciente + resp);
                    }
                });

            } 
        });

        /* $("#clinic_noapp").change(function () {
            var nombre = $('#clinic_noapp option:selected').html();
            var id = $('#clinic_noapp').val();

                $.ajax({
                    type: "POST",
                    url: 'include/loadPatientId.php',
                    data: 'id_clinica='+id,
                    success: function (resp) {
                        console.log(resp);
                        var letraI = nombre.charAt(0);
                        var letraF = nombre.charAt(nombre.length - 1);
                        var id_paciente = letraI.toUpperCase() + letraF.toUpperCase();
                        var id_paciente_eagle = letraI.toUpperCase();

                        $("#id_paciente").val(id_paciente + resp);
                        $("#id_paciente_eagle").val(id_paciente_eagle + resp);
                        console.log(id_paciente + resp);
                    }
                });

        }); */

        //guardar nueva entrada de call tracker
        $("#crearRegistro").click(function (e) {
            e.preventDefault();
            var valor = $('[name="patient_type[]"]:checked').val();
            var seguro = $('#new_insurance_type').val();
            var color = '';

            var time24 = ConvertTimeformat("00:00", $('#time').val());

            if ($('#reasonApp').val() == 7) {
                color = "#ab82ba";
            } else {
                color = "#16aaff";
            }

            var hc = $('[name="schedule_app[]"]:checked').val();
            console.log(hc);

            const postData = {
                id_paciente: $('#id_paciente').val(),
                id_paciente_eagle: $('#id_paciente_eagle').val(),
                id_clinica: $('#clinic').val(),
                paciente_nombres: $('#new_name').val(),
                paciente_apellidos: $('#new_lastname').val(),
                paciente_fechanac: $('#new_birth').val(),
                paciente_contacto: $('#new_contact').val(),
                paciente_tiene_seguro: $('#new_have_insurance').val(),

                id_channel: $('#select_channel').val(),
                id_referal: $('#select_referal').val(),
                id_campaign: $('#campaignR').val(),
                tipo_paciente: $('[name="patient_type[]"]:checked').val(),
                call_notas: $('#notes').val(),

                id_reason: $('#reasonApp').val(),
                cita_fecha: $('#dateApp').val(),
                cita_hora: time24,
                cita_duracion: $('#durationApp').val(),

                tipo_seguro: $('#new_insurance_type').val(),
                current_patient_referal_id: $('#currentID').val(),

                id_paciente_ph: $('#id_paciente_ph').val(),
                id_paciente_eagle_ph: $('#id_paciente_eagle_ph').val(),
                paciente_nombres_ph: $('#name_ph').val(),
                paciente_apellidos_ph: $('#lastname_ph').val(),
                paciente_relacion_ph: $('#relation_ph').val(),
                paciente_contacto_ph: $('#phone_ph').val(),
                paciente_birth_ph: $('#birth_ph').val(),

                color: color,
                call_hizo_cita: $('[name="schedule_app[]"]:checked').val()
            }

            if (valor == 'New Patient' && hc == "yes") {

                if ($('#new_have_insurance').val() == "yes") {
                    if (seguro == "Self") {

                        $.ajax({
                            type: "POST",
                            url: 'include/saveCallTrackerEntry.php',
                            data: postData,
                            success: function (resp) {
                                if (resp == "Success") {
                                    swal({
                                        title: "New Call Tracker Entry",
                                        text: "Has been entered successfully",
                                        icon: "success"
                                    }).then(function () {
                                        location.reload();
                                    });
                                } else if (resp == "Error") {
                                    swal({
                                        title: "New Call Tracker Entry",
                                        text: "Can't be entered. Please Try Again",
                                        icon: "error"
                                    });
                                } else {
                                    swal({
                                        title: "New Call Tracker Entry",
                                        text: resp,
                                        icon: "error"
                                    });
                                }
                            }
                        });
                    } else if (seguro == "Policy Holder") {

                        $.ajax({
                            type: "POST",
                            url: 'include/saveCallTrackerEntry2.php',
                            data: postData,
                            success: function (resp) {
                                if (resp == "Success") {
                                    swal({
                                        title: "New Call Tracker Entry",
                                        text: "Has been entered successfully",
                                        icon: "success"
                                    }).then(function () {
                                        location.reload();
                                    });
                                } else if (resp == "Error") {
                                    swal({
                                        title: "New Call Tracker Entry",
                                        text: "Can't be entered. Please Try Again",
                                        icon: "error"
                                    });
                                } else {
                                    swal({
                                        title: "New Call Tracker Entry",
                                        text: resp,
                                        icon: "error"
                                    });
                                }
                            }
                        });
                    }
                } else if ($('#new_have_insurance').val() == "no") {
                    $.ajax({
                        type: "POST",
                        url: 'include/saveCallTrackerEntry.php',
                        data: postData,
                        success: function (resp) {
                            if (resp == "Success") {
                                swal({
                                    title: "New Call Tracker Entry",
                                    text: "Has been entered successfully",
                                    icon: "success"
                                }).then(function () {
                                    location.reload();
                                });
                            } else if (resp == "Error") {
                                swal({
                                    title: "New Call Tracker Entry",
                                    text: "Can't be entered. Please Try Again",
                                    icon: "error"
                                });
                            } else {
                                swal({
                                    title: "New Call Tracker Entry",
                                    text: resp,
                                    icon: "error"
                                });
                            }
                        }
                    });
                }
            } else if (valor == 'New Patient' && hc == "no") {
                const dataWA = {
                    id_clinica: $('#clinic').val(),
                    paciente_nombres: $('#new_name').val(),
                    paciente_apellidos: $('#new_lastname').val(),
                    paciente_fechanac: $('#new_birth').val(),
                    paciente_contacto: $('#new_contact').val(),

                    id_channel: $('#select_channel').val(),
                    id_referal: $('#select_referal').val(),
                    id_campaign: $('#campaignR').val(),
                    tipo_paciente: $('[name="patient_type[]"]:checked').val(),
                    call_notas: $('#notes_noschedule').val(),

                    current_patient_referal_id: $('#currentID').val()
                }
                $.ajax({
                    type: "POST",
                    url: 'include/saveCallTrackerEntryWApp.php',
                    data: dataWA,
                    success: function (resp) {
                        if (resp == "Success") {
                            swal({
                                title: "New Call Tracker Entry",
                                text: "Has been entered successfully",
                                icon: "success"
                            }).then(function () {
                                location.reload();
                            });
                        } else if (resp == "Error") {
                            swal({
                                title: "New Call Tracker Entry",
                                text: "Can't be entered. Please Try Again",
                                icon: "error"
                            });
                        } else {
                            swal({
                                title: "New Call Tracker Entry",
                                text: resp,
                                icon: "error"
                            });
                        }
                    }
                });
            }

        /* } else if (valor == 'Current Patient') {

        } */

        });
    });
</script>

<!-- input phone mask -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script>
    $('.phone').mask('(000) 000 0000');
</script>

<!-- input time 12h -->
<script>

</script>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    if (form.checkValidity() === true) {
                        //event.preventDefault();

                    //do ajax here

                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>