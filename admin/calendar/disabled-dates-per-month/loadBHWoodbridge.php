<?php

    include('../include/database.php');
    include("../include/funciones.php");

    #MySQL
    $result = $conexion->query('SELECT * FROM '.$tabla_calendario_horario.' WHERE id_clinica = 3 AND closed = "";');
    $json = array();

    while($row=$result->fetch_assoc())
    {
        $json[]=array(
            'dow'   => $row["day"],
            'startTime'   => date('G:i',strtotime($row["start"])),
            'endTime'   => date('G:i',strtotime($row["end"]))
        );
    }

    $jsonstring = json_encode($json);
    echo $jsonstring;

?>
