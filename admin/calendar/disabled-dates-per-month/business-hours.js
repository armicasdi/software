$(document).ready(function () {
    function ConvertTimeformat(format, str) {
        var hours = Number(str.match(/^(\d+)/)[1]);
        var minutes = Number(str.match(/:(\d+)/)[1]);
        var AMPM = str.match(/\s?([AaPp][Mm]?)$/)[1];
        var pm = ['P', 'p', 'PM', 'pM', 'pm', 'Pm'];
        var am = ['A', 'a', 'AM', 'aM', 'am', 'Am'];
        if (pm.indexOf(AMPM) >= 0 && hours < 12) hours = hours + 12;
        if (am.indexOf(AMPM) >= 0 && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        if (format == '0000') {
            return (sHours + sMinutes);
        } else if (format == '00:00') {
            return (sHours + ":" + sMinutes);
        } else {
            return false;
        }
    }

    //cargar horarios en tabla fairfax
    $.ajax({
        type: "POST",
        url: 'calendar/loadbhfx.php',
        data: 'id_clinica=' + 1,
        success: function (resp) {
            $('#bhfairfax').html(resp);

            $('[name="startime[]"]').timepicker({
                timeFormat: 'hh:mm p',
                interval: 30,
                minTime: new Date(0, 0, 0, 9, 0, 0),
                maxTime: '7:00pm',
                startTime: '09:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            $('[name="endtime[]"]').timepicker({
                timeFormat: 'hh:mm p',
                interval: 30,
                minTime: new Date(0, 0, 0, 12, 0, 0),
                maxTime: '7:00pm',
                startTime: '17:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            //Definiendo estado cerrado
            $('[name="chk_closed[]"]').each(function () {
                $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
                $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
            });

            //Definiendo estado cerrado
            $('[name="chk_closed[]"]').click(function () {
                $('[name="chk_closed[]"]').each(function () {
                    $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
                    $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
                });
            });
        }
    });

    //cargar horarios actuales fairfax
    $.ajax({
        type: "POST",
        url: 'calendar/loadactualbhfx.php',
        data: 'id_clinica=' + 1,
        success: function (resp) {
            $('#actualbhFX').html(resp);
        }
    });

    //cargar horarios actuales manassas
    $.ajax({
        type: "POST",
        url: 'calendar/loadactualbhms.php',
        data: 'id_clinica=' + 2,
        success: function (resp) {
            $('#actualbhMS').html(resp);
        }
    });

    //cargar horarios en tabla manassas
    $.ajax({
        type: "POST",
        url: 'calendar/loadbhms.php',
        data: 'id_clinica=' + 2,
        success: function (resp) {
            $('#bhmanassas').html(resp);

            $('[name="startime_ms[]"]').timepicker({
                timeFormat: 'hh:mm p',
                interval: 30,
                minTime: new Date(0, 0, 0, 9, 0, 0),
                maxTime: '7:00pm',
                defaultTime: '09:00',
                startTime: '09:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            $('[name="endtime_ms[]"]').timepicker({
                timeFormat: 'hh:mm p',
                interval: 30,
                minTime: new Date(0, 0, 0, 12, 0, 0),
                maxTime: '7:00pm',
                defaultTime: '17:00',
                startTime: '17:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            //Definiendo estado cerrado
            $('[name="chk_closed_ms[]"]').each(function () {
                $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
                $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
            });

            //Definiendo estado cerrado
            $('[name="chk_closed_ms[]"]').click(function () {
                $('[name="chk_closed_ms[]"]').each(function () {
                    $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
                    $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
                });
            });
        }
    });

    //cargar horarios actuales woodbridge
    $.ajax({
        type: "POST",
        url: 'calendar/loadactualbhwe.php',
        data: 'id_clinica=' + 3,
        success: function (resp) {
            $('#actualbhWE').html(resp);
        }
    });

    //cargar horarios en tabla manassas
    $.ajax({
        type: "POST",
        url: 'calendar/loadbhwe.php',
        data: 'id_clinica=' + 3,
        success: function (resp) {
            $('#bhwoodbridge').html(resp);

            $('[name="startime_we[]"]').timepicker({
                timeFormat: 'hh:mm p',
                interval: 30,
                minTime: new Date(0, 0, 0, 9, 0, 0),
                maxTime: '7:00pm',
                startTime: '09:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            $('[name="endtime_we[]"]').timepicker({
                timeFormat: 'hh:mm p',
                interval: 30,
                minTime: new Date(0, 0, 0, 12, 0, 0),
                maxTime: '7:00pm',
                startTime: '17:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            //Definiendo estado cerrado
            $('[name="chk_closed_we[]"]').each(function () {
                $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
                $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
            });

            //Definiendo estado cerrado
            $('[name="chk_closed_we[]"]').click(function () {
                $('[name="chk_closed_we[]"]').each(function () {
                    $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
                    $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
                });
            });
        }
    });

    //Definiendo estado cerrado
    $('[name="chk_closed[]"]').click(function () {
        $('[name="chk_closed[]"]').each(function () {
            $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
            $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
        });
    });

    //Definiendo estado cerrado
    $('[name="chk_closed_ms[]"]').click(function () {
        $('[name="chk_closed_ms[]"]').each(function () {
            $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
            $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
        });
    });

    //Definiendo estado cerrado
    $('[name="chk_closed_we[]"]').click(function () {
        $('[name="chk_closed_we[]"]').each(function () {
            $(this).closest('td').prev().find('input:text').attr('disabled', this.checked);
            $(this).closest('td').prev().prev().find('input:text').attr('disabled', this.checked);
        });
    });

    //Actualizando Horario
    $('#save_bh_fairfax').click(function () {
        var days = [];
        var start = [];
        var end = [];
        var closed = [];
        var closeopt;

        $('[name="day[]"]').each(function () {
            var dia = $(this).text();
            if (dia == "Monday") {
                dia = 1;
            } else if (dia == "Tuesday") {
                dia = 2;
            } else if (dia == "Wednesday") {
                dia = 3;
            } else if (dia == "Thursday") {
                dia = 4;
            } else if (dia == "Friday") {
                dia = 5;
            } else if (dia == "Saturday") {
                dia = 6;
            } else if (dia == "Sunday") {
                dia = 0;
            }
            days.push(dia);
        });
        $('[name="startime[]"]').each(function () {
            start.push(ConvertTimeformat("00:00", $(this).val()));
        });
        $('[name="endtime[]"]').each(function () {
            end.push(ConvertTimeformat("00:00", $(this).val()));
        });
        $('[name="chk_closed[]"]').each(function () {
            if ($(this).is(':checked')) {
                closeopt = $(this).val();
            } else {
                closeopt = "";
            }
            closed.push(closeopt);
        });

        const dataBH = {
            dias: days,
            inicio: start,
            fin: end,
            cerrado: closed
        }

        $.ajax({
            type: "POST",
            url: "calendar/addbhfx.php",
            data: dataBH,
            success: function (resp) {
                if (resp == "Success") {
                    swal.fire({
                        title: "Business Hours (Fairfax)",
                        text: "Has been updated successfully",
                        icon: "success"
                    }).then(function () {
                        location.reload();
                    });
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Business Hours (Fairfax)",
                        text: "An error has occurred. Please Try Again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Business Hours (Fairfax)",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });


    $('#save_bh_ms').click(function () {
        var days = [];
        var start = [];
        var end = [];
        var closed = [];
        var closeopt;

        $('[name="day_ms[]"]').each(function () {
            var dia = $(this).text();
            if (dia == "Monday") {
                dia = 1;
            } else if (dia == "Tuesday") {
                dia = 2;
            } else if (dia == "Wednesday") {
                dia = 3;
            } else if (dia == "Thursday") {
                dia = 4;
            } else if (dia == "Friday") {
                dia = 5;
            } else if (dia == "Saturday") {
                dia = 6;
            } else if (dia == "Sunday") {
                dia = 0;
            }
            days.push(dia);
        });
        $('[name="startime_ms[]"]').each(function () {
            start.push(ConvertTimeformat("00:00", $(this).val()));
        });
        $('[name="endtime_ms[]"]').each(function () {
            end.push(ConvertTimeformat("00:00", $(this).val()));
        });
        $('[name="chk_closed_ms[]"]').each(function () {
            if ($(this).is(':checked')) {
                closeopt = $(this).val();
            } else {
                closeopt = "";
            }
            closed.push(closeopt);
        });

        const dataBH = {
            dias: days,
            inicio: start,
            fin: end,
            cerrado: closed
        }

        $.ajax({
            type: "POST",
            url: "calendar/addbhms.php",
            data: dataBH,
            success: function (resp) {
                if (resp == "Success") {
                    swal.fire({
                        title: "Business Hours (Manassas)",
                        text: "Has been updated successfully",
                        icon: "success"
                    }).then(function () {
                        location.reload();
                    });
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Business Hours (Manassas)",
                        text: "An error has occurred. Please Try Again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Business Hours (Manassas)",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });


    $('#save_bh_we').click(function () {
        var days = [];
        var start = [];
        var end = [];
        var closed = [];
        var closeopt;

        $('[name="day_we[]"]').each(function () {
            var dia = $(this).text();
            if (dia == "Monday") {
                dia = 1;
            } else if (dia == "Tuesday") {
                dia = 2;
            } else if (dia == "Wednesday") {
                dia = 3;
            } else if (dia == "Thursday") {
                dia = 4;
            } else if (dia == "Friday") {
                dia = 5;
            } else if (dia == "Saturday") {
                dia = 6;
            } else if (dia == "Sunday") {
                dia = 0;
            }
            days.push(dia);
        });
        $('[name="startime_we[]"]').each(function () {
            start.push(ConvertTimeformat("00:00", $(this).val()));
        });
        $('[name="endtime_we[]"]').each(function () {
            end.push(ConvertTimeformat("00:00", $(this).val()));
        });
        $('[name="chk_closed_we[]"]').each(function () {
            if ($(this).is(':checked')) {
                closeopt = $(this).val();
            } else {
                closeopt = "";
            }
            closed.push(closeopt);
        });

        const dataBH = {
            dias: days,
            inicio: start,
            fin: end,
            cerrado: closed
        }

        $.ajax({
            type: "POST",
            url: "calendar/addbhwe.php",
            data: dataBH,
            success: function (resp) {
                if (resp == "Success") {
                    swal.fire({
                        title: "Business Hours (Woodbridge)",
                        text: "Has been updated successfully",
                        icon: "success"
                    }).then(function () {
                        location.reload();
                    });
                } else if (resp == "Error") {
                    swal.fire({
                        title: "Business Hours (Woodbridge)",
                        text: "An error has occurred. Please Try Again",
                        icon: "error"
                    });
                } else {
                    swal.fire({
                        title: "Business Hours (Woodbridge)",
                        text: resp,
                        icon: "error"
                    });
                }
            }
        });
    });
});