<?php
    include('../include/database.php');
    include("../include/funciones.php");

    $clinic = $_POST['id_clinica'];
    $result = $conexion->query('SELECT * FROM '.$tabla_calendario_horario.' WHERE id_clinica = '.$clinic.' AND closed = "";');
    while($row=$result->fetch_assoc()){
        $time_start = $row["start"];
        $start = date("g:i A",strtotime("$time_start"));
        $time_end = $row["end"];
        $end = date("g:i A",strtotime("$time_end"));
        $day = $row["day"];
        if($day == 1){
            $day = "Monday";
        }
        else if($day == 2){
            $day = "Tuesday";
        }
        else if($day == 3){
            $day = "Wednesday";
        }
        else if($day == 4){
            $day = "Thursday";
        }
        else if($day == 5){
            $day = "Friday";
        }
        else if($day == 6){
            $day = "Saturday";
        }
        else if($day == 0){
            $day = "Sunday";
        }

        echo '<p>
                <b>'.$day.': </b>
                <span>'.$start.'</span> to <span>'.$end.'</span>
            </p>';
    }

