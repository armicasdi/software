<?php
    include('../include/database.php');
    include("../include/funciones.php");

    $clinic = $_POST['id_clinica'];
    $result = $conexion->query('SELECT * FROM '.$tabla_calendario_horario.' WHERE id_clinica = '.$clinic.';');
    while($row=$result->fetch_assoc()){
        $time_start = $row["start"];
        $start = date("g:i A",strtotime("$time_start"));
        $time_end = $row["end"];
        $end = date("g:i A",strtotime("$time_end"));
        $day = $row["day"];
        if($day == 1){
            $day = "Monday";
        }
        else if($day == 2){
            $day = "Tuesday";
        }
        else if($day == 3){
            $day = "Wednesday";
        }
        else if($day == 4){
            $day = "Thursday";
        }
        else if($day == 5){
            $day = "Friday";
        }
        else if($day == 6){
            $day = "Saturday";
        }
        else if($day == 0){
            $day = "Sunday";
        }
        
        $closeopt = $row['closed'];
        if($closeopt == ""){
            $closeopt = "";
        }
        else if($closeopt == "closed"){
            $closeopt = 'checked="checked"';
        }

        echo '<tr>
                <td class="text-center" scope="row" class="day"><span name="day_we[]">'.$day.'</span></td>
                <td class="text-center">
                    <input type="text" name="startime_we[]" class="form-control" value="'.$start.'"/>
                </td>
                <td class="text-center">
                    <input type="text" name="endtime_we[]" class="form-control" value="'.$end.'"/>
                </td>
                <td class="text-center">
                    <div class="custom-checkbox custom-control custom-control-inline">
                        <input type="checkbox" value="closed" name="chk_closed_we[]" '.$closeopt.' 
                            id="we'.$row["day"].'" class="custom-control-input">
                        <label class="custom-control-label" for="we'.$row["day"].'"></label>
                    </div>
                </td>
            </tr>';
    }

