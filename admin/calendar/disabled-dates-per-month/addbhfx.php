<?php
include('../include/database.php');
include("../include/funciones.php");

if (isset($_POST['dias'])) {
    $num_bh = cantidad('id_clinica', 1, $tabla_calendario_horario);
    $user = escapar($_SESSION['user']);

    //Arreglos de BH
    $dias = $_POST['dias'];
    $inicio = $_POST['inicio'];
    $fin = $_POST['fin'];
    $cerrado = $_POST['cerrado'];
    $contador = 0;

    for ($count = 0; $count < count($dias); $count++) {

        if ($num_bh > 0) {

            $campos_bh = array(
                'day' => $dias[$count],
                'start' => $inicio[$count],
                'end' => $fin[$count],
                'closed' => $cerrado[$count],
                'id_clinica' => 1,
                'bh_user' => $user
            );

            $condicion_update = array(
                'day' => $dias[$count],
                'id_clinica' => 1
            );

            //Registros por dia
            $num_d = cantidad('day', $dias[$count], $tabla_calendario_horario);
            if ($num_d > 0) {
                actualizar_2($tabla_calendario_horario, $campos_bh, $condicion_update);
                $contador++;
            }
        } else if ($num_bh == 0) {
            $campos_bh = array(
                'day' => $dias[$count],
                'start' => $inicio[$count],
                'end' => $fin[$count],
                'closed' => $cerrado[$count],
                'id_clinica' => 1,
                'bh_user' => $user
            );

            if (insertar($tabla_calendario_horario, $campos_bh)) {
                $contador++;
            }
        }
    }

    if ($contador == count($dias)) {
        echo "Success";
    } else {
        echo "Error";
    }
}
