<?php 
    include("../include/eagle_con.php");
    require("../include/database.php");
    include("../include/funciones.php");

    $resultado = $conexion->query('SELECT * FROM calendario WHERE id_cita_eagle != NULL AND evento_fecha BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()');
    while($data=$resultado->fetch_assoc()){
        $id_cita_eagle = $data['id_cita_eagle'];
        $evento_inicio = $data['evento_inicio'];
        $evento_final = $data['evento_fin'];
        $chair = $data['chair'];
        $confirmation = $data['confirm_eagle'];

        $query = "SELECT start_time,end_time,appointment_id,patient_id,location_id,confirmation_status FROM appointment WHERE appointment_id = $id_cita_eagle";
        #start_time BETWEEN dateadd(DAY,-7,NOW()) AND dateadd(WEEK,6,NOW());
        #DATE_ADD(CURDATE(), INTERVAL 2 WEEK)

        $result = odbc_exec($conexion_anywhere,$query);

        while (odbc_fetch_row($result)) 
        {
            $appointment_id = odbc_result($result,"appointment_id");
            $start_time = date('H:i:s',strtotime(odbc_result($result,"start_time")));
            $end_time = date('H:i:s',strtotime(odbc_result($result,"end_time")));
            $location_id = odbc_result($result,"location_id");
            $confirmation_status = odbc_result($result,"confirmation_status");

            if($evento_inicio != $start_time){
                $actualizar($tabla_calendario,array('evento_inicio'=>$start_time),array('id_cita_eagle'=>$appointment_id));
            }
            if($evento_final != $end_time){
                actualizar($tabla_calendario,array('evento_final'=>$end_time),array('id_cita_eagle'=>$appointment_id));
            }
            if($chair != $location_id){
                actualizar($tabla_calendario,array('chair'=>$location_id),array('id_cita_eagle'=>$appointment_id));
            }
            if($confirmation != $confirmation_status){
                actualizar($tabla_calendario,array('confirm_eagle'=>$confirmation_status),array('id_cita_eagle'=>$appointment_id));
            }
        }
    }

?>