<?php
    require("database.php");
    include("funciones.php");

    $nombreToImage = time();
    if (isset($_FILES["file"]))
    {
        $reporte = null;
        for($x=0; $x<count($_FILES["file"]["name"]); $x++)
        {
            $file = $_FILES["file"];
            //arreglos varios
            $extension=explode('.',$file["name"][$x]);
            $extension=end($extension);
            $nombreImagenGrande=$nombreToImage.'.'.$extension;
            $tipo = $file["type"][$x];
            $ruta_provisional = $file["tmp_name"][$x];
            $size = $file["size"][$x];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = $galeria_menu;
            if ($tipo != 'image/jpeg' && $tipo != 'image/jpg' && $tipo != 'image/png' )
            {
                echo "Error ".$file["name"][$x].", el archivo no es una imagen";
            }
            else
            {
                $src = $carpeta.$nombreImagenGrande;
                $camposPerfil = array(
                    'usuario_foto'=>$nombreImagenGrande,
                    'id_cargo'=>escapar($_POST['nombre_menu']),
                    'usuario_nombre'=>escapar($_POST['precio_menu']),
                    'usuario_genero'=>escapar($_POST['descripcion_menu']),
                    'usuario_email'=>escapar($_POST['descripcion_menu']),
                    'usuario_contacto'=>escapar($_POST['descripcion_menu']),
                    'usuario_sobre_mi'=>escapar($_POST['descripcion_menu']),
                    'usuario_pais'=>escapar($_POST['descripcion_menu'])
                );
                //Caragamos imagenes al servidor
                if(move_uploaded_file($ruta_provisional, $src)){
                    //Guardar thumbnail
                    $image_p = makeThumbnail($src,370,370,null,$tipo);
                    if($image_p){
                        if( $tipo == 'image/jpeg' || $tipo == 'image/jpg') {
                            $nombre_imagen_miniatura = $nombreToImage.'_thumbnail.jpg';
                            imagejpeg($image_p, $carpeta.$nombre_imagen_miniatura,100);
                        }
                        else{ 
                            $nombre_imagen_miniatura = $nombreToImage.'_thumbnail.png';
                            imagejpeg($image_p, $carpeta.$nombre_imagen_miniatura,100);
                        }
                        $camposPerfil['thumbnail'] = $nombre_imagen_miniatura;
                    }else{
                        //Si en caso de error el resize
                        $camposPerfil['thumbnail'] = $nombreImagenGrande;
                    }
                    //Insertar registro
                    if(insertar($tabla_usuarios_perfil,$camposPerfil)){
                        //echo "Profile update!";
                        echo "Success";
                    }
                    else{
                        echo "Error";
                    }
                }else{
                    echo ":(";
                }
            }
        }
    }
?>