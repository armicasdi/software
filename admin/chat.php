<?php 
    include 'include/funciones.php';
?>

<!doctype html>
<html lang="en">

<head>
    <?php include 'head.php'; ?>

    <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />

    <style>
        table.dataTable thead {
            background: linear-gradient(to right, #4A00E0, #8E2DE2);
            color: white;
        }
    </style>

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include 'navbar.php';
        ?>

        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-chat icon-gradient bg-happy-itmeo">
                                    </i>
                                </div>
                                <div>Chat
                                    <div class="page-title-subheading">Provided by RingByName
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">

                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                <span>Fairfax</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                <span>Manassas</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                                <span>Woodbridge</span>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Registered Patients</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="example">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Chart</th>
                                                            <th class="text-center">Patient</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Contact</th>
                                                            <th class="text-center">BirthDate</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $result = $conexion->query('SELECT * FROM '. $tabla_pacientes .' WHERE id_clinica = 1 ORDER BY id_paciente DESC;');
                                                            while($pat=$result->fetch_assoc()){
                                                                $pat_codigo=$pat['id_paciente'];
                                                                $pat_paciente=$pat['paciente_nombres'];
                                                                $pat_apellidos=$pat['paciente_apellidos'];
                                                                $pat_clinica=$pat['id_clinica'];
                                                                $pat_fechanac=$pat['paciente_fechanac']; 
                                                                $pat_contacto=$pat['paciente_contacto'];  
                                                                $reg_clinica = queryOne('SELECT * FROM '. $tabla_clinicas . ' WHERE id_clinica = "'. $pat_clinica . '"');
                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row">
                                                                <?php echo $pat_codigo;?></td>
                                                            <td class="text-center">
                                                                <?php echo $pat_paciente." ". $pat_apellidos;?></td>
                                                            <td class="text-center">
                                                                <?php echo $reg_clinica['clinica_nombre'];?></td>
                                                            <td class="text-center"><?php echo $pat_contacto;?></td>
                                                            <td class="text-center"><?php echo $pat_fechanac;?></td>
                                                            <td class="text-center">
                                                                <button type="button" id="PopoverCustomT-4"
                                                                    data-toggle="modal" data-target="#modalDetails"
                                                                    class="btn btn-focus btn-sm">Details</button>
                                                                <button type="button" id="PopoverCustomT-4"
                                                                    data-toggle="modal" data-target="#modalEdit"
                                                                    class="btn btn-warning btn-sm">Edit</button>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Registered Patients</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="manassas">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Chart</th>
                                                            <th class="text-center">Patient</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Contact</th>
                                                            <th class="text-center">BirthDate</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $result = $conexion->query('SELECT * FROM '. $tabla_pacientes .' WHERE id_clinica = 2 ORDER BY id_paciente DESC');
                                                            while($pat=$result->fetch_assoc()){
                                                                $pat_id=$pat['id'];
                                                                $pat_codigo=$pat['id_paciente'];
                                                                $pat_paciente=$pat['paciente_nombres'];
                                                                $pat_apellidos=$pat['paciente_apellidos'];
                                                                $pat_clinica=$pat['id_clinica'];
                                                                $pat_fechanac=$pat['paciente_fechanac']; 
                                                                $pat_contacto=$pat['paciente_contacto'];  
                                                                $reg_clinica = queryOne('SELECT * FROM '. $tabla_clinicas . ' WHERE id_clinica = "'. $pat_clinica . '"');
                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row">
                                                                <?php echo $pat_codigo;?></td>
                                                            <td class="text-center">
                                                                <?php echo $pat_paciente." ". $pat_apellidos;?></td>
                                                            <td class="text-center">
                                                                <?php echo $reg_clinica['clinica_nombre'];?></td>
                                                            <td class="text-center"><?php echo $pat_contacto;?></td>
                                                            <td class="text-center"><?php echo $pat_fechanac;?></td>
                                                            <td class="text-center">
                                                                <button type="button" id="detailsM"
                                                                    data-id="<?php echo $pat_id;?>" data-toggle="modal"
                                                                    data-target="#modalDetails"
                                                                    class="details btn btn-focus btn-sm">Details</button>
                                                                <button type="button" id="editM"
                                                                    data-id="<?php echo $pat_id;?>" data-toggle="modal"
                                                                    data-target="#modalEdit"
                                                                    class="edit btn btn-warning btn-sm">Edit</button>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Registered Patients</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="woodbridge">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Chart</th>
                                                            <th class="text-center">Patient</th>
                                                            <th class="text-center">Clinic</th>
                                                            <th class="text-center">Contact</th>
                                                            <th class="text-center">BirthDate</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $result = $conexion->query('SELECT * FROM '. $tabla_pacientes .' WHERE id_clinica = 3 ORDER BY id_paciente DESC');
                                                            while($pat=$result->fetch_assoc()){
                                                                $pat_codigo=$pat['id_paciente'];
                                                                $pat_paciente=$pat['paciente_nombres'];
                                                                $pat_apellidos=$pat['paciente_apellidos'];
                                                                $pat_clinica=$pat['id_clinica'];
                                                                $pat_fechanac=$pat['paciente_fechanac']; 
                                                                $pat_contacto=$pat['paciente_contacto'];  
                                                                $reg_clinica = queryOne('SELECT * FROM '. $tabla_clinicas . ' WHERE id_clinica = "'. $pat_clinica . '"');
                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row">
                                                                <?php echo $pat_codigo;?></td>
                                                            <td class="text-center">
                                                                <?php echo $pat_paciente." ". $pat_apellidos;?></td>
                                                            <td class="text-center">
                                                                <?php echo $reg_clinica['clinica_nombre'];?></td>
                                                            <td class="text-center"><?php echo $pat_contacto;?></td>
                                                            <td class="text-center"><?php echo $pat_fechanac;?></td>
                                                            <td class="text-center">
                                                                <button type="button" id="PopoverCustomT-4"
                                                                    data-toggle="modal" data-target="#modalDetails"
                                                                    class="btn btn-focus btn-sm">Details</button>
                                                                <button type="button" id="PopoverCustomT-4"
                                                                    data-toggle="modal" data-target="#modalEdit"
                                                                    class="btn btn-warning btn-sm">Edit</button>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="wrapper">


                        <!-- Content Wrapper. Contains page content -->
                        <div class="content-wrapper" style="margin: 0">

                            <!-- Main content -->
                            <section class="content">
                                <div class="container-fluid">
                                    <div class="row rowContainer">
                                        <div class="col-md-6 dataTableContainer">

                                            <div class="card">
                                                <div class="card-header">Messages</div>
                                                <!-- /.card-header -->
                                                <div class="card-body">
                                                    <table id="Mensajes" class="table table-striped table-borderless">
                                                        <thead>
                                                            <tr>
                                                                <th>Contact</th>
                                                                <th>Phone</th>
                                                                <th>Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th>Contact</th>
                                                                <th>Phone</th>
                                                                <th>Date</th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6" id="MovedWraper"
                                            style="height: 90vh; border: 1px solid lightgrey; background-color: white">
                                            <div id="messagesWrapper" class="col-12"
                                                style=" height: 70vh; overflow-y: scroll">
                                            </div>
                                            <div class="col-12" style="width: 100%; height: 100px; ">
                                                <form id="enviar">
                                                    <input type="text" name="message"
                                                        style="width: 100%; padding: 20px 30px; margin: 10px; ">
                                                    <input id="message_id" type="hidden" name="convo_id" value="">
                                                    <input class="col-12 btn btn-primary" type="submit">
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.row -->

                                </div>
                                <!-- /.container-fluid -->
                            </section>
                            <!-- /.content -->
                        </div>
                        <!-- /.content-wrapper -->
                        <footer class="main-footer">

                        </footer>

                        <!-- Control Sidebar -->

                        <!-- /.control-sidebar -->
                    </div>
                </div>
                <?php include 'footer.php'; ?>
            </div>

        </div>
    </div>

    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <!-- DATATABLE -->
    <script src="assets/dataTable/jquery.dataTables.min.js"></script>
    <script src="assets/dataTable/dataTables.bootstrap4.min.js"></script>

    <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>


    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <!-- Bootstrap 4 -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/responsive.bootstrap4.min.js"></script>
    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/js/demo.min.js"></script>




    <script>
        $(document).ready(function () {

            var sessionId;
            var t = $("#Mensajes").DataTable({
                "responsive": true,
                "autoWidth": false,
                /* "order": [
                        [2, "desc"]
                    ] */
            });
            
            var data = JSON.stringify({
                "data": {
                    "username": "armicasdi@gmail.com",
                    "password": "0123Castro"
                }
            });



            $.ajax({
                type: "POST",
                url: " https://api.ringbyname.com/auth",
                data: data,
                success: function (response) {
                    sessionId = response.data.session_id;
                    conversations(sessionId);
                },
                dataType: "json",
                contentType: "application/json"
            });

            function conversations(id) {
                //sessionId = id;
                var resultadoConversaciones = $.ajax({
                    type: "GET",
                    url: "https://api.ringbyname.com/sms/conversation",
                    // headers: { "X-Session-Id" : sessionId},
                    beforeSend: function (request) {
                        request.setRequestHeader("X-Session-Id", id)
                    },
                    success: function (response) {
                        var rows = response.data.rows;
                        table(rows, sessionId);
                    }
                });
            }

            function table(response, sessionId) {
                for (var i = 0; i < response.length; i++) {
                    let date = new Date(response[i].last_message.date_created);
                    let options = {
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        hour12: false
                    }
                    let dateFormated = date.toLocaleString('default', options);
                    let time = date.toLocaleTimeString('en-US');
                    id = response[i].id
                    sId = sessionId
                    t.row.add(["<span style='cursor: pointer;' onclick='mensaje(sId, " + id + ")'><b>" + response[i].contact_name +
                        "</b></span>", response[i].contact_phone_number, dateFormated + "  " + time
                    ]);
                }
                t.draw();


            }

            $("#enviar").submit(function (e) {
                e.preventDefault();
                var mensajePut = $('input[name=message]').val()
                let convo_id = $('input[name=convo_id]').val()
                $.ajax({
                    type: "PUT",
                    url: "https://api.ringbyname.com/sms/reply/" + convo_id,
                    data: JSON.stringify({
                        "data": {
                            "message": mensajePut
                        }
                    }),
                    beforeSend: function (request) {
                        request.setRequestHeader("X-Session-Id", sessionId)
                    },
                    success: mensaje(sessionId, convo_id),
                    dataType: "json",
                    contentType: "application/json"
                });

                mensajeReload(sessionId, convo_id)
            })

            function mensajeReload(sessionId, id) {
                $("#messagesWrapper").html(`<div id="mensaje"  class="mensaje" style="position: absolute; bottom: 0; height: 100px; width: 100%" >
                </div>`)


                $.ajax({
                    type: "GET",
                    url: "https://api.ringbyname.com/sms/message?search=conversation.id:" + id,
                    // headers: { "X-Session-Id" : sessionId},
                    beforeSend: function (request) {
                        request.setRequestHeader("X-Session-Id", sessionId)
                    },
                    success: function (response) {
                        rows = response.data.rows
                        last_index = rows.length - 1;
                        convo_id = rows[last_index].conversation.id;
                        console.log(sessionId)

                        for (let y = 0; y < rows.length; y++) {
                            let mensaje = rows[y].message
                            let date = rows[y].date_created
                            inbound = rows[y].is_inbound ? "left" : "right"
                            backcolor = rows[y].is_inbound ? "lightgreen" : "lightblue"

                            $("#mensaje").before(
                                '<div class="col-4 mensaje" style="margin: 20px 10px; padding:20px 20px; clear:both; float:' +
                                inbound + '; background-color: ' + backcolor + ' " >' +
                                mensaje +
                                '<p style="margin-top: 25px; clear:both; color: gray; font-size: 14px; float: right">' +
                                date + '</p></div>')
                        }

                        $('#messagesWrapper').animate({
                            scrollTop: $('#messagesWrapper').prop("scrollHeight")
                        }, 2500);

                        fillForm(convo_id);
                    }

                });


            }

            function fillForm(id) {

                $("input:hidden").val(id);
                $("input:text").val("");


            }



        })

        function mensaje(sessionId, id) {

            $("#messagesWrapper").html(`<div id="mensaje"  class="mensaje" style="position: absolute; bottom: 0; height: 100px; width: 100%" >

                </div>`)


            $.ajax({
                type: "GET",
                url: "https://api.ringbyname.com/sms/message?search=conversation.id:" + id,
                // headers: { "X-Session-Id" : sessionId},
                beforeSend: function (request) {
                    request.setRequestHeader("X-Session-Id", sessionId)
                },
                success: function (response) {
                    rows = response.data.rows
                    last_index = rows.length - 1;
                    convo_id = rows[last_index].conversation.id;
                    console.log(sessionId)

                    let date = new Date("2020-09-06 20:15:49");
                    let options = {
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        hour12: false
                    }
                    let dateFormated = date.toLocaleString('default', options);
                    let time = date.toLocaleTimeString('en-US');

                    for (let y = 0; y < rows.length; y++) {
                        let mensaje = rows[y].message

                        let date = new Date(rows[y].date_created);
                        let options = {
                            weekday: 'long',
                            year: 'numeric',
                            month: 'long',
                            day: 'numeric',
                            hour12: false
                        }
                        let dateFormated = date.toLocaleString('default', options);
                        let time = date.toLocaleTimeString('en-US');


                        inbound = rows[y].is_inbound ? "left" : "right"
                        backcolor = rows[y].is_inbound ? "lightgreen" : "lightblue"

                        $("#mensaje").before(
                            '<div class="col-6 mensaje" style="margin: 20px 10px; padding:20px 20px 10px 20px; clear:both; float:' +
                            inbound + '; border-radius: 35px 0px 35px 0px; -moz-border-radius: 35px 0px 35px 0px; -webkit-border-radius: 35px 0px 35px 0px; background-color: ' + backcolor + ' " >' + mensaje +
                            '<p style="margin: 15px 0 0 0; clear:both; color: gray; font-size: 14px; float: right">' +
                            dateFormated +
                            '</p><p style="margin: 0px 0 0 0; clear:both; color: gray; font-size: 14px; float: right">' +
                            time + '</p></div>')
                    }

                    $('#messagesWrapper').animate({
                        scrollTop: $('#messagesWrapper').prop("scrollHeight")
                    }, 2500);
                    $('#MovedWraper').animate({
                        left: 0
                    }, 1000);


                    fillForm(convo_id);
                }

            });


        }

        function fillForm(id) {

            $("input:hidden").val(id);
        }
    </script>
</body>

</html>