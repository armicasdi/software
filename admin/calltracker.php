<?php 
    include 'include/funciones.php';
?>

<!doctype html>
<html lang="en">

<head>
    <?php include 'head.php'; ?>
    <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include 'navbar.php';
        ?>

        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-call icon-gradient bg-happy-itmeo">
                                    </i>
                                </div>
                                <div>Call Tracker
                                    <div class="page-title-subheading">This is a List of Registered Calls
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">

                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                <span>Fairfax</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                <span>Manassas</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                                <span>Woodbridge</span>
                            </a>
                        </li>
                    </ul>
                    <?php
                        $result = $conexion->query('SELECT * FROM '. $tabla_clinicas .' ORDER BY clinica_nombre');
                        while($costo=$result->fetch_assoc()):
                            $clinic_codigo=$costo['id_clinica'];
                            $clinic_name=$costo['clinica_nombre'];
                    ?>

                    <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">List of Registered Calls</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="example">
                                                    <thead>
                                                        <tr>
                                                            <!-- <th class="text-center">ID</th>
                                                            <th class="text-center">Patient Chart</th>
                                                            <th class="text-center">Referral</th>
                                                            <th class="text-center">Book Appointment</th>
                                                            <th class="text-center">Show Up</th>
                                                            <th class="text-center">Start Treatment</th>
                                                            <th class="text-center">Need more Treatments</th>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Action</th> -->
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Patient Chart</th>
                                                            <th class="text-center">Referral</th>
                                                            <th class="text-center">Book Appointment</th>
                                                            <th class="text-center">Show Up</th>
                                                            <th class="text-center">Start Treatment</th>
                                                            <th class="text-center">Need more Treatments</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $result = $conexion->query('SELECT * FROM '. $tabla_call_tracker .' WHERE id_clinica = 1 ORDER BY id_call');
                                                            while($call=$result->fetch_assoc()){
                                                                $call_codigo=$call['id_call'];
                                                                $call_paciente=$call['id_patient'];
                                                                $call_referal=$call['id_referal']; 
                                                                $call_campaign=$call['id_campaign']; 
                                                                $call_date=$call['call_date']; 
                                                                $call_provider=$call['call_provider']; 
                                                                $date = date_create($call_date);
                                                                $fecha = date_format($date,'m-d-Y');
                                                                $reg_patient = queryOne('SELECT * FROM '. $tabla_pacientes . ' WHERE id_paciente_eagle = "'. $call_paciente . '"');
                                                                $reg_referal = queryOne('SELECT * FROM '. $tabla_call_referal . ' WHERE id_referal = '. $call_referal . '');
                                                                $reg_campaign = queryOne('SELECT * FROM '. $tabla_call_campaign . ' WHERE id_campaign = '. $call_campaign . '');
                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row"><?php echo $fecha;?></td>
                                                            <td class="text-center"><?php echo $call_paciente;?></td>
                                                            <td class="text-center"><?php echo $reg_referal['referal_name'];?></td>
                                                            <td class="text-center"><i class="fa fa-check-circle" style="color:green"></i></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center">Diagnostic</td>
                                                            <td class="text-center">
                                                                <button type="button" 
                                                                    data-toggle="modal" 
                                                                    data-id="<?php echo $call_codigo;?>" 
                                                                    data-paciente="<?php echo $call_paciente;?>"
                                                                    data-target="#call-details" 
                                                                    class="buttonD btn btn-focus btn-sm" 
                                                                    id="buttonD">Details
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">List of Registered Calls</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="manassas">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Patient Chart</th>
                                                            <th class="text-center">Referral</th>
                                                            <th class="text-center">Book Appointment</th>
                                                            <th class="text-center">Show Up</th>
                                                            <th class="text-center">Start Treatment</th>
                                                            <th class="text-center">Need more Treatments</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $result = $conexion->query('SELECT * FROM '. $tabla_call_tracker .' WHERE id_clinica = 2 ORDER BY id_call');
                                                            while($call=$result->fetch_assoc()){
                                                                $call_codigo=$call['id_call'];
                                                                $call_paciente=$call['id_patient'];
                                                                $call_referal=$call['id_referal']; 
                                                                $call_campaign=$call['id_campaign']; 
                                                                $call_date=$call['call_date']; 
                                                                $call_provider=$call['call_provider']; 
                                                                $date = date_create($call_date);
                                                                $fecha = date_format($date,'m-d-Y');
                                                                $reg_patient = queryOne('SELECT * FROM '. $tabla_pacientes . ' WHERE id_paciente_eagle = "'. $call_paciente . '"');
                                                                $reg_referal = queryOne('SELECT * FROM '. $tabla_call_referal . ' WHERE id_referal = '. $call_referal . '');
                                                                $reg_campaign = queryOne('SELECT * FROM '. $tabla_call_campaign . ' WHERE id_campaign = '. $call_campaign . '');
                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row"><?php echo $fecha;?></td>
                                                            <td class="text-center"><?php echo $call_paciente;?></td>
                                                            <td class="text-center"><?php echo $reg_referal['referal_name'];?></td>
                                                            <td class="text-center"><i class="fa fa-check-circle" style="color:green"></i></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center">Diagnostic</td>
                                                            <td class="text-center">
                                                                <button type="button" 
                                                                    data-toggle="modal" 
                                                                    data-id="<?php echo $call_codigo;?>" 
                                                                    data-paciente="<?php echo $call_paciente;?>"
                                                                    data-target="#call-details" 
                                                                    class="buttonD btn btn-focus btn-sm" 
                                                                    id="buttonD">Details
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">List of Registered Calls</div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table
                                                    class="display align-middle mb-0 table table-borderless table-striped table-hover"
                                                    id="woodbridge">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Patient Chart</th>
                                                            <th class="text-center">Referral</th>
                                                            <th class="text-center">Book Appointment</th>
                                                            <th class="text-center">Show Up</th>
                                                            <th class="text-center">Start Treatment</th>
                                                            <th class="text-center">Need more Treatments</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $result = $conexion->query('SELECT * FROM '. $tabla_call_tracker .' WHERE id_clinica = 3 ORDER BY id_call');
                                                            while($call=$result->fetch_assoc()){
                                                                $call_codigo=$call['id_call'];
                                                                $call_paciente=$call['id_patient'];
                                                                $call_referal=$call['id_referal']; 
                                                                $call_campaign=$call['id_campaign']; 
                                                                $call_date=$call['call_date']; 
                                                                $call_provider=$call['call_provider']; 
                                                                $date = date_create($call_date);
                                                                $fecha = date_format($date,'m-d-Y');
                                                                $reg_patient = queryOne('SELECT * FROM '. $tabla_pacientes . ' WHERE id_paciente_eagle = "'. $call_paciente . '"');
                                                                $reg_referal = queryOne('SELECT * FROM '. $tabla_call_referal . ' WHERE id_referal = '. $call_referal . '');
                                                                $reg_campaign = queryOne('SELECT * FROM '. $tabla_call_campaign . ' WHERE id_campaign = '. $call_campaign . '');
                                                        ?>
                                                        <tr>
                                                            <td class="text-center text-muted" scope="row"><?php echo $fecha;?></td>
                                                            <td class="text-center"><?php echo $call_paciente;?></td>
                                                            <td class="text-center"><?php echo $reg_referal['referal_name'];?></td>
                                                            <td class="text-center"><i class="fa fa-check-circle" style="color:green"></i></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center">Diagnostic</td>
                                                            <td class="text-center">
                                                                <button type="submit" 
                                                                    data-toggle="modal" 
                                                                    data-id="<?php echo $call_codigo;?>" 
                                                                    data-paciente="<?php echo $call_paciente;?>"
                                                                    data-target="#call-details" 
                                                                    class="buttonD btn btn-focus btn-sm" 
                                                                    id="buttonD">Details
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php include 'footer.php'; ?>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="assets/scripts/main.js"></script>

    <!-- DATATABLE -->
    <script src="assets/dataTable/jquery.dataTables.min.js"></script>
    <script src="assets/dataTable/dataTables.bootstrap4.min.js"></script>

    <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
            $('#manassas').DataTable();
            $('#woodbridge').DataTable();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.buttonD',function (e) {
                var call_id = $(this).data('id');
                var paciente = $(this).data('paciente');
                $.ajax({
                    type: "POST",
                    url: 'calltracker/patient-details.php',
                    data: {
                        patient_chart: paciente
                    },
                    success: function (response) {
                        $('#patient_information').html(response);
                    }
                });

                $.ajax({
                    type: "POST",
                    url: 'calltracker/app-details.php',
                    data: {
                        patient_chart: paciente
                    },
                    success: function (response) {
                        $('#app_information').html(response);
                    }
                });
            });
        });
    </script>
</body>

</html>

<!-- DETAILS MODAL -->
<div class="modal fade" id="call-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-file-phone icon-gradient bg-happy-itmeo">
                    </i>DETAILS
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="" enctype="multipart/form-data" class="needs-validation" novalidate>
                <div class="modal-body">
                    <div class="card-header">Patient Information</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Patient Chart</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Lastname</th>
                                        <th class="text-center">Date of Birth</th>
                                        <th class="text-center">Age</th>
                                        <th class="text-center">Contact</th>
                                    </tr>
                                </thead>
                                <tbody id="patient_information">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-header">Appointments Information</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Patient Chart</th>
                                        <th class="text-center">Date / Time</th>
                                        <th class="text-center">Appointment Reason</th>
                                        <th class="text-center">Clinic</th>
                                        <th class="text-center">Show Up</th>
                                    </tr>
                                </thead>
                                <tbody id="app_information">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info">Go to Patient File</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>