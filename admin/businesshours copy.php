<?php 
    include 'include/funciones.php';
?>
<!doctype html>
<html lang="en">

<head>
    <?php include 'head.php'; ?>
    <link  href="assets/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet"/>

    <style>
        table.dataTable thead{
            background: linear-gradient(to right, #4A00E0, #8E2DE2);
            color: white;
        }
    </style>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include 'navbar.php';
        ?>
        <div class="ui-theme-settings">
            <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
                <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
            </button>
        </div>
        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-clock icon-gradient bg-premium-dark">
                                    </i>
                                </div>
                                <div>Business Hours
                                    <div class="page-title-subheading">Manage all the business hours for the calendar
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav" id="myTab">

                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                <span>Fairfax</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                <span>Manassas</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                                <span>Woodbridge</span>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3 card">
                                        <div class="card-header">
                                            <ul class="nav nav-justified">
                                                <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-0"
                                                        class="nav-link active show">Single</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-1"
                                                        class="nav-link show">Month</a></li>
                                            </ul>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active show" id="tab-eg7-0" role="tabpanel">
                                                    <div class="col-md-12">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Business Hours (Fairfax)</h5>
                                                            <div class="row">
                                                                <div class="col-md-7">

                                                                    <input type="text" class="form-control"
                                                                        id="selectFXdates"
                                                                        placeholder="Pick the multiple dates">
                                                                </div>
                                                                <div class="col-md-2 text-center">

                                                                    <input type="text" id="startfx"
                                                                        class="form-control" />

                                                                </div>
                                                                <div class="col-md-2 text-center">

                                                                    <input type="text" id="endfx"
                                                                        class="form-control" />

                                                                </div>

                                                                <div class="col-md-1 text-center">

                                                                    <button type="button" class="btn btn-dark"
                                                                        id="savesinglefx">SAVE
                                                                    </button>

                                                                </div>

                                                            </div>
                                                            <br />
                                                            <br />
                                                            <h5 class="card-title">Actual Hours (Fairfax)</h5>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table
                                                                        class="mb-0 table table-borderless table-striped table-hover text-center"
                                                                        id="bhf">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Date</th>
                                                                                <th class="text-center">Month</th>
                                                                                <th class="text-center">Day</th>
                                                                                <th class="text-center">Start</th>
                                                                                <th class="text-center">End</th>
                                                                                <th class="text-center">Actions</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody></tbody>
                                                                        <!-- <tbody id="singlefx">
                                                                            <?php
                                                                                $result = $conexion->query('SELECT * FROM '. $tabla_calendario_horas .' WHERE id_clinica = 1 ORDER BY bh_date DESC;');
                                                                                while($row=$result->fetch_assoc()){
                                                                                    $fecha = $row['bh_date'];
                                                                                    $fechaComoEntero = strtotime($fecha);
                                                                                    $mes = date("F", $fechaComoEntero);
                                                                                    $dia = date("l", $fechaComoEntero);

                                                                                    $time_start = $row["bh_start"];
                                                                                    $start = date("g:i A",strtotime("$time_start"));
                                                                                    $time_end = $row["bh_end"];
                                                                                    $end = date("g:i A",strtotime("$time_end"));
                                                                                    
                                                                            ?>
                                                                            <tr>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $fecha; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $mes; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $dia; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $start; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $end; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    
                                                                                </td>
                                                                            </tr>
                                                                            <?php } ?> 
                                                                        </tbody>-->
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- EVENTS-->
                                                    <!-- <div class="col-md-5">
                                                        <div class="card-body">
                                                            <h5 class="card-title">This Week</h5>
                                                            <div class="position-relative form-group">
                                                                <div id="actualbhFX"></div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                <!-- </div>
                                                <div class="tab-pane show" id="tab-eg7-1" role="tabpanel">
                                                    <div class="col-md-7">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Business Hours (Fairfax)</h5>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <table
                                                                        class="display align-middle mb-0 table table-borderless table-striped table-hover dataTable no-footer"
                                                                        id="example" role="grid"
                                                                        aria-describedby="example_info">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Day</th>
                                                                                <th class="text-center">Start
                                                                                    Time</th>
                                                                                <th class="text-center">End Time
                                                                                </th>
                                                                                <th class="text-center">Or
                                                                                    Closed</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="bhfairfax">
                                                                            
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                            <div class="card-body col-md-12">
                                                                <button type="button" class="btn btn-dark pull-right"
                                                                    id="save_bh_fairfax" name="save_bh_fairfax">Save
                                                                    Changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Event</h5>
                                                            <div class="position-relative form-group">
                                                                <label for="contact_email">Event</label>
                                                                <input type="text" class="form-control" id="event"
                                                                    name="event" placeholder="input a event name"
                                                                    required>
                                                            </div>
                                                            <div class="position-relative form-group">
                                                                <label for="contact_email">Date</label>
                                                                <input type="date" class="form-control" id="event"
                                                                    name="date" required>
                                                            </div>
                                                            <div class="position-relative form-group">
                                                                <button type="button" class="btn btn-alternate"
                                                                    id="save_event" name="save_event">Save
                                                                    Event</button>
                                                            </div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h5 class="card-title">Actual Hours</h5>
                                                            <div class="position-relative form-group">
                                                                <div id="actualbhFX"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3 card">
                                        <div class="card-header">
                                            <ul class="nav nav-justified">
                                                <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-0"
                                                        class="nav-link active show">Single</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-1"
                                                        class="nav-link show">Month</a></li>
                                            </ul>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active show" id="tab-eg7-0" role="tabpanel">
                                                    <div class="col-md-12">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Business Hours (Manassas)</h5>
                                                            <div class="row">
                                                                <div class="col-md-7">

                                                                    <input type="text" class="form-control"
                                                                        id="selectMSdates"
                                                                        placeholder="Pick the multiple dates">
                                                                </div>
                                                                <div class="col-md-2 text-center">

                                                                    <input type="text" id="startms"
                                                                        class="form-control" />

                                                                </div>
                                                                <div class="col-md-2 text-center">

                                                                    <input type="text" id="endms"
                                                                        class="form-control" />

                                                                </div>

                                                                <div class="col-md-1 text-center">

                                                                    <button type="button" class="btn btn-dark"
                                                                        id="savesinglems">SAVE
                                                                    </button>

                                                                </div>

                                                            </div>
                                                            <br />
                                                            <br />
                                                            <h5 class="card-title">Actual Hours (Mannassas)</h5>
                                                            <div class="row">
                                                                <div class="table-responsive">
                                                                    <table class="mb-0 table table-borderless table-striped table-hover text-center"
                                                                        id="bhm">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Date</th>
                                                                                <th class="text-center">Month</th>
                                                                                <th class="text-center">Day</th>
                                                                                <th class="text-center">Start</th>
                                                                                <th class="text-center">End</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="singlems">
                                                                            <?php
                                                                                $result = $conexion->query('SELECT * FROM '. $tabla_calendario_horas .' WHERE id_clinica = 2 ORDER BY bh_date DESC;');
                                                                                while($row=$result->fetch_assoc()){
                                                                                    $fecha = $row['bh_date'];
                                                                                    $fechaComoEntero = strtotime($fecha);
                                                                                    $mes = date("F", $fechaComoEntero);
                                                                                    $dia = date("l", $fechaComoEntero);

                                                                                    $time_start = $row["bh_start"];
                                                                                    $start = date("g:i A",strtotime("$time_start"));
                                                                                    $time_end = $row["bh_end"];
                                                                                    $end = date("g:i A",strtotime("$time_end"));
                                                                                    
                                                                            ?>
                                                                            <tr>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $fecha; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $mes; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $dia; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $start; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $end; ?></td>
                                                                            </tr>
                                                                            <?php } ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane show" id="tab-eg7-1" role="tabpanel">
                                                    <div class="col-md-7">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Business Hours (Manassas)</h5>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <table
                                                                        class="display align-middle mb-0 table table-borderless table-striped table-hover dataTable no-footer"
                                                                        id="example" role="grid"
                                                                        aria-describedby="example_info">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Day</th>
                                                                                <th class="text-center">Start
                                                                                    Time</th>
                                                                                <th class="text-center">End Time
                                                                                </th>
                                                                                <th class="text-center">Or
                                                                                    Closed</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="bhmanassas">
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                            <div class="card-body col-md-12">
                                                                <button type="button" class="btn btn-dark pull-right"
                                                                    id="save_bh_manassas" name="save_bh_manassas">Save
                                                                    Changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- EVENTS-->
                                                    <div class="col-md-5">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Actual Hours</h5>
                                                            <div class="position-relative form-group">
                                                                <div id="actualbhMS"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-row">
                                        <div class="col-md-7">
                                            <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <h5 class="card-title">Business Hours (Manassas)</h5>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <table
                                                                class="display align-middle mb-0 table table-borderless table-striped table-hover dataTable no-footer"
                                                                id="example" role="grid"
                                                                aria-describedby="example_info">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">Day</th>
                                                                        <th class="text-center">Start Time</th>
                                                                        <th class="text-center">End Time</th>
                                                                        <th class="text-center">Or Closed</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="bhmanassas">

                                                                </tbody>
                                                            </table>
                                                        </div>

                                                    </div>
                                                    <div class="card-body col-md-12">
                                                        <button type="button" class="btn btn-dark pull-right"
                                                            id="save_bh_ms" name="save_bh_ms">Save Changes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <h5 class="card-title">Event</h5>
                                                    <div class="position-relative form-group">
                                                        <label for="contact_email">Event</label>
                                                        <input type="text" class="form-control" id="event" name="event"
                                                            placeholder="input a event name" required>
                                                    </div>
                                                    <div class="position-relative form-group">
                                                        <label for="contact_email">Date</label>
                                                        <input type="date" class="form-control" id="event" name="date"
                                                            required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <h5 class="card-title">Actual Hours</h5>
                                                    <div class="position-relative form-group">
                                                        <div id="actualbhMS"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3 card">
                                        <div class="card-header">
                                            <ul class="nav nav-justified">
                                                <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-0"
                                                        class="nav-link active show">Single</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-1"
                                                        class="nav-link show">Month</a></li>
                                            </ul>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active show" id="tab-eg7-0" role="tabpanel">
                                                    <div class="col-md-12">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Business Hours (Woodbridge)</h5>
                                                            <div class="row">
                                                                <div class="col-md-7">

                                                                    <input type="text" class="form-control"
                                                                        id="selectWEdates"
                                                                        placeholder="Pick the multiple dates">
                                                                </div>
                                                                <div class="col-md-2 text-center">

                                                                    <input type="text" id="startwe"
                                                                        class="form-control" />

                                                                </div>
                                                                <div class="col-md-2 text-center">

                                                                    <input type="text" id="endwe"
                                                                        class="form-control" />

                                                                </div>

                                                                <div class="col-md-1 text-center">

                                                                    <button type="button" class="btn btn-dark"
                                                                        id="savesinglewe">SAVE
                                                                    </button>

                                                                </div>

                                                            </div>
                                                            <br />
                                                            <br />
                                                            <h5 class="card-title">Actual Hours (Woodbridge)</h5>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table
                                                                        class="mb-0 table table-borderless table-striped table-hover text-center"
                                                                        id="bhw">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Date</th>
                                                                                <th class="text-center">Month</th>
                                                                                <th class="text-center">Day</th>
                                                                                <th class="text-center">Start</th>
                                                                                <th class="text-center">End</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="singlewe">
                                                                            <?php
                                                                                $result = $conexion->query('SELECT * FROM '. $tabla_calendario_horas .' WHERE id_clinica = 3 ORDER BY bh_date DESC;');
                                                                                while($row=$result->fetch_assoc()){
                                                                                    $fecha = $row['bh_date'];
                                                                                    $fechaComoEntero = strtotime($fecha);
                                                                                    $mes = date("F", $fechaComoEntero);
                                                                                    $dia = date("l", $fechaComoEntero);

                                                                                    $time_start = $row["bh_start"];
                                                                                    $start = date("g:i A",strtotime("$time_start"));
                                                                                    $time_end = $row["bh_end"];
                                                                                    $end = date("g:i A",strtotime("$time_end"));
                                                                                    
                                                                            ?>
                                                                            <tr>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $fecha; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $mes; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $dia; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $start; ?></td>
                                                                                <td class="text-center" scope="row">
                                                                                    <?php echo $end; ?></td>
                                                                            </tr>
                                                                            <?php } ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane show" id="tab-eg7-1" role="tabpanel">
                                                    <div class="col-md-7">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Business Hours (Woodbridge)</h5>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <table
                                                                        class="display align-middle mb-0 table table-borderless table-striped table-hover dataTable no-footer"
                                                                        id="example" role="grid"
                                                                        aria-describedby="example_info">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Day</th>
                                                                                <th class="text-center">Start
                                                                                    Time</th>
                                                                                <th class="text-center">End Time
                                                                                </th>
                                                                                <th class="text-center">Or
                                                                                    Closed</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="bhwoodbridge">
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                            <div class="card-body col-md-12">
                                                                <button type="button" class="btn btn-dark pull-right"
                                                                    id="save_bh_woodbridge" name="save_bh_woodbridge">Save
                                                                    Changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- EVENTS-->
                                                    <div class="col-md-5">
                                                        <div class="card-body">
                                                            <h5 class="card-title">Actual Hours</h5>
                                                            <div class="position-relative form-group">
                                                                <div id="actualbhWE"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include 'footer.php'; ?>
            </div>

        </div>
    </div>

    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <script type="text/javascript" src="./assets/scripts/jquery-ui/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="./assets/scripts/jquery-ui/jquery-ui.css" />

    <!-- DATATABLE -->
    <script src="assets/dataTable/jquery.dataTables.min.js"></script>
    <script src="assets/dataTable/dataTables.bootstrap4.min.js"></script>

    <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            /* $('#bhf').DataTable(); */
            
            $('#bhm').DataTable();
            $('#bhw').DataTable();
        });
    </script>

    <!-- <script src="./calendar/business-hours.js"></script> -->
    <script src="calendar/singlebhs.js"></script>
</body>

</html>