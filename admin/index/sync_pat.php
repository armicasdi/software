<?php
    include("../include/eagle_con.php");
    require("../include/database.php");
    include("../include/funciones.php");

    $count_pat_update = 0;
    $count_pat_insert = 0;

    $conexion->query('CREATE TEMPORARY TABLE patient_eagle (
        id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        id_paciente varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
        patient_id varchar(10) NOT NULL,
        first_name varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
        last_name varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
        home_phone varchar(15) DEFAULT NULL,
        sex varchar(10) DEFAULT NULL,
        birth_date date DEFAULT NULL,
        discount_id int(11) DEFAULT NULL,
        prim_responsible_id varchar(10) DEFAULT NULL,
        prim_relationship varchar(10) DEFAULT NULL,
        prim_employer_id int(11) NULL,
        prim_outstanding_balance decimal(10,2) DEFAULT NULL,
        prim_benefits_remaining decimal(10,2) DEFAULT NULL,
        prim_remaining_deductible decimal(10,2) DEFAULT NULL,
        fee_level_id int(11) DEFAULT NULL,
        policy_holder_status varchar(10) DEFAULT NULL,
        email_address varchar(200) DEFAULT NULL,
        date_entered date DEFAULT NULL,
        current_bal decimal(10,2) DEFAULT NULL,
        thirty_day decimal(10,2) DEFAULT NULL,
        sixty_day decimal(10,2) DEFAULT NULL,
        ninety_day decimal(10,2) DEFAULT NULL,
        contract_balance decimal(10,2) DEFAULT NULL,
        estimated_insurance decimal(10,2) DEFAULT NULL,  
        INDEX index_1 (patient_id),
        INDEX index_2 (prim_employer_id),
        INDEX index_3 (estimated_insurance)
      ) ENGINE=MyISAM DEFAULT CHARSET=utf8;');

    $query = "SELECT patient_id,first_name,last_name,home_phone,sex,birth_date,discount_id,prim_responsible_id,prim_relationship,prim_employer_id,prim_outstanding_balance,prim_benefits_remaining,prim_remaining_deductible,fee_level_id,policy_holder_status,email_address,date_entered,current_bal,thirty_day,sixty_day,ninety_day,contract_balance,estimated_insurance FROM patient WHERE patient_id NOT IN ('000','00000','0001');";

    $to_insert = array();
    
    $result = odbc_exec($conexion_anywhere,$query);

    while (odbc_fetch_row($result)) 
    {
        $patient_id = odbc_result($result,"patient_id");
        if (strpos($patient_id, 'F') !== false) {
            $id_paciente = str_replace('F','FX',$patient_id);
            $id_paciente_eagle = $patient_id;
            $clinica = 1;
        }
        else if (strpos($patient_id, 'W') !== false) {
            $id_paciente = str_replace('W','WE',$patient_id);
            $id_paciente_eagle = $patient_id;
            $clinica = 3;
        }
        else {
            $id_paciente = 'MS'.$patient_id;
            $id_paciente_eagle = 'M'.$patient_id;
            $clinica = 2;
        }

        $first_name = trim(odbc_result($result,"first_name"));
        $last_name = trim(odbc_result($result,"last_name"));
        $home_phone = formatTelefono(odbc_result($result,"home_phone"));
        $sex = odbc_result($result,"sex");
        $birth_date = odbc_result($result,"birth_date") ?? "0000-00-00";
        $discount_id = odbc_result($result,"discount_id") ?? 0;
        $prim_responsible_id = odbc_result($result,"prim_responsible_id");
        $prim_relationship = odbc_result($result,"prim_relationship");
        $prim_employer_id = odbc_result($result,"prim_employer_id") ?? 0;
        $prim_outstanding_balance = odbc_result($result,"prim_outstanding_balance");
        $prim_benefits_remaining = odbc_result($result,"prim_benefits_remaining");
        $prim_remaining_deductible = odbc_result($result,"prim_remaining_deductible");
        $fee_level_id = odbc_result($result,"fee_level_id") ?? 0;
        $policy_holder_status = odbc_result($result,"policy_holder_status");
        $email_address = odbc_result($result,"email_address");
        $date_entered = odbc_result($result,"date_entered");
        $current_bal = odbc_result($result,"current_bal");
        $thirty_day = odbc_result($result,"thirty_day");
        $sixty_day = odbc_result($result,"sixty_day");
        $ninety_day = odbc_result($result,"ninety_day");
        $contract_balance = odbc_result($result,"contract_balance");
        $estimated_insurance = odbc_result($result,"estimated_insurance");

        $parentesis = '(NULL,"'.$id_paciente.'","'.$patient_id.'","'.$first_name.'","'.$last_name.'","'.$home_phone.'","'.$sex.'","'.$birth_date.'",'.$discount_id.',"'.$prim_responsible_id.'","'.$prim_relationship.'",'.$prim_employer_id.','.$prim_outstanding_balance.','.$prim_benefits_remaining.','.$prim_remaining_deductible.','.$fee_level_id.',"'.$policy_holder_status.'","'.$email_address.'","'.$date_entered.'",'.$current_bal.','.$thirty_day.','.$sixty_day.','.$ninety_day.','.$contract_balance.','.$estimated_insurance.')';

        array_push($to_insert,$parentesis);
    }

    $array_string = implode(",",$to_insert);

    $conexion->query('INSERT INTO patient_eagle (id,id_paciente,patient_id,first_name,last_name,home_phone,sex,birth_date,discount_id,prim_responsible_id,prim_relationship,prim_employer_id,prim_outstanding_balance,prim_benefits_remaining,prim_remaining_deductible,fee_level_id,policy_holder_status,email_address,date_entered,current_bal,thirty_day,sixty_day,ninety_day,contract_balance,estimated_insurance) VALUES '.$array_string.';');

    # ================ INSERTAR NUEVOS ================
    $resultado_insert = $conexion->query('SELECT id_paciente,patient_id,first_name,last_name,home_phone,sex,birth_date,discount_id,prim_responsible_id,prim_relationship,prim_employer_id,prim_outstanding_balance,prim_benefits_remaining,prim_remaining_deductible,fee_level_id,policy_holder_status,email_address,date_entered,current_bal,thirty_day,sixty_day,ninety_day,contract_balance,estimated_insurance FROM patient_eagle WHERE patient_id NOT IN (SELECT patient_id FROM patient);');
    if(!empty($resultado_insert)){
        while ($row=$resultado_insert->fetch_assoc()) {
            if(insertar('patient',$row)){
                $count_pat_insert++;
            }
        }
        echo $count_pat_insert." Nuevos pacientes han sido agregados satisfactoriamente".PHP_EOL;
    }else{
        echo "no encontre registros nuevos";
    }

    # ================ ACTUALIZAR SEGURO EN PACIENTES EXISTENTES ================

    $count_pat_update = $conexion->query('UPDATE
                            patient pat
                        INNER JOIN patient_eagle pate ON
                            pat.patient_id = pate.patient_id
                        SET
                            pat.discount_id = pate.discount_id,
                            pat.prim_responsible_id = pate.prim_responsible_id,
                            pat.prim_relationship = pate.prim_relationship,
                            pat.prim_employer_id = pate.prim_employer_id,
                            pat.prim_outstanding_balance = pate.prim_outstanding_balance,
                            pat.prim_benefits_remaining = pate.prim_benefits_remaining,
                            pat.prim_remaining_deductible = pate.prim_remaining_deductible,
                            pat.fee_level_id = pate.fee_level_id,
                            pat.policy_holder_status = pate.policy_holder_status,
                            pat.current_bal = pate.current_bal,
                            pat.thirty_day = pate.thirty_day,
                            pat.sixty_day = pate.sixty_day,
                            pat.ninety_day = pate.ninety_day,
                            pat.contract_balance = pate.contract_balance,
                            pat.estimated_insurance = pate.estimated_insurance
                        WHERE
                            pat.prim_employer_id != pate.prim_employer_id OR pat.estimated_insurance != pate.estimated_insurance;');
    
    echo PHP_EOL." REGISTROS MODIFICADOS: ". $count_pat_update;
    

    
