<?php 
    include 'include/funciones.php';
?>
<!doctype html>
<html lang="en">

<head>
    <?php include 'head.php'; ?>
    <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />


    <style>
        table.dataTable thead {
            background: linear-gradient(to right, #4A00E0, #8E2DE2);
            color: white;
        }
    </style>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php 
            include 'navbar.php';
        ?>
        <div class="ui-theme-settings">
            <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning">
                <i class="fa fa-cog fa-w-16 fa-spin fa-2x"></i>
            </button>
        </div>
        <div class="app-main">
            <?php
                include 'sidebar.php';
            ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-clock icon-gradient bg-premium-dark">
                                    </i>
                                </div>
                                <div>Business Hours
                                    <div class="page-title-subheading">Manage all the business hours for the calendar
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav" id="myTab">

                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                <span>Fairfax</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                <span>Manassas</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                                <span>Woodbridge</span>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3 card">
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <div class="card-body">
                                                    <h5 class="card-title">Business Hours (Fairfax)</h5>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="position-relative form-group">
                                                                <label for="selectFXdates" class="">Dates</label>
                                                                <input type="text" class="form-control"
                                                                    id="selectFXdates"
                                                                    placeholder="You can pick multiple dates">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="position-relative form-group">
                                                                <label for="startfx" class="">Start Time</label>
                                                                <input type="text" id="startfx" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="position-relative form-group">
                                                                <label for="startfx" class="">End Time</label>
                                                                <input type="text" id="endfx" class="form-control" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-1 text-center">
                                                            <div class="position-relative form-group">
                                                                <label for="savesinglefx" class=""></label>
                                                                <button type="button" class="btn btn-dark"
                                                                    id="savesinglefx">SAVE
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <h5 class="card-title text-center">Current Business Hours (Fairfax)
                                                    </h5>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table
                                                                class="mb-0 table table-borderless table-striped table-hover text-center"
                                                                id="bhf">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">Date</th>
                                                                        <th class="text-center">Month</th>
                                                                        <th class="text-center">Day</th>
                                                                        <th class="text-center">Start</th>
                                                                        <th class="text-center">End</th>
                                                                        <th class="text-center">Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="singlefx">
                                                                    <?php
                                                                        $result = $conexion->query('SELECT * FROM '. $tabla_calendario_horas .' WHERE id_clinica = 1 ORDER BY bh_date DESC;');
                                                                        while($row=$result->fetch_assoc()){
                                                                            $idBH = $row['id_hora'];
                                                                            $clinic = $row['id_clinica'];
                                                                            $fecha = $row['bh_date'];
                                                                            $date = date("m/d/Y",strtotime("$fecha"));
                                                                            $date_delete = date("l jS \of F Y",strtotime("$fecha"));
                                                                            $fechaComoEntero = strtotime($fecha);
                                                                            $mes = date("F", $fechaComoEntero);
                                                                            $dia = date("l", $fechaComoEntero);

                                                                            $time_start = $row["bh_start"];
                                                                            $start = date("g:i A",strtotime("$time_start"));
                                                                            
                                                                            $time_end = $row["bh_end"];
                                                                            $end = date("g:i A",strtotime("$time_end"));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $fecha; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $mes; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $dia; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $start; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $end; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <button type="button"
                                                                                data-toggle="modal"
                                                                                data-target=".editCurrentHour" 
                                                                                data-id="<?php echo $idBH;?>"
                                                                                data-fecha="<?php echo $date;?>"
                                                                                data-start="<?php echo $start;?>"
                                                                                data-end="<?php echo $end;?>"
                                                                                data-clinic="<?php echo $clinic;?>"
                                                                                class="editButton mb-2 mr-2 btn btn-alternate">
                                                                                <i class="fa fa-edit"></i>
                                                                            </button>
                                                                            <button type="button"
                                                                                data-id="<?php echo $idBH;?>"
                                                                                data-fecha="<?php echo $date_delete;?>"
                                                                                data-clinic="<?php echo $clinic;?>"
                                                                                class="deleteButton mb-2 mr-2 btn btn-danger">
                                                                                <i class="fa fa-times"></i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3 card">
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <div class="card-body">
                                                    <h5 class="card-title">Business Hours (Manassas)</h5>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="position-relative form-group">
                                                                <label for="selectFXdates" class="">Dates</label>
                                                                <input type="text" class="form-control"
                                                                    id="selectMSdates"
                                                                    placeholder="You can pick multiple dates">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="position-relative form-group">
                                                                <label for="startms" class="">Start Time</label>
                                                                <input type="text" id="startms" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="position-relative form-group">
                                                                <label for="endms" class="">End Time</label>
                                                                <input type="text" id="endms" class="form-control" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-1 text-center">
                                                            <div class="position-relative form-group">
                                                                <label for="savesinglems" class=""></label>
                                                                <button type="button" class="btn btn-dark"
                                                                    id="savesinglems">SAVE
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <h5 class="card-title text-center">Current Business Hours (Manassas)
                                                    </h5>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table
                                                                class="mb-0 table table-borderless table-striped table-hover text-center"
                                                                id="bhm">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">Date</th>
                                                                        <th class="text-center">Month</th>
                                                                        <th class="text-center">Day</th>
                                                                        <th class="text-center">Start</th>
                                                                        <th class="text-center">End</th>
                                                                        <th class="text-center">Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="singlems">
                                                                    <?php
                                                                        $result = $conexion->query('SELECT * FROM '. $tabla_calendario_horas .' WHERE id_clinica = 2 ORDER BY bh_date DESC;');
                                                                        while($row=$result->fetch_assoc()){
                                                                            $idBH = $row['id_hora'];
                                                                            $clinic = $row['id_clinica'];
                                                                            $fecha = $row['bh_date'];
                                                                            $date = date("m/d/Y",strtotime("$fecha"));
                                                                            $date_delete = date("l jS \of F Y",strtotime("$fecha"));
                                                                            $fechaComoEntero = strtotime($fecha);
                                                                            $mes = date("F", $fechaComoEntero);
                                                                            $dia = date("l", $fechaComoEntero);

                                                                            $time_start = $row["bh_start"];
                                                                            $start = date("g:i A",strtotime("$time_start"));
                                                                            
                                                                            $time_end = $row["bh_end"];
                                                                            $end = date("g:i A",strtotime("$time_end"));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $fecha; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $mes; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $dia; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $start; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $end; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <button type="button"
                                                                                data-toggle="modal"
                                                                                data-target=".editCurrentHour" 
                                                                                data-id="<?php echo $idBH;?>"
                                                                                data-fecha="<?php echo $date;?>"
                                                                                data-start="<?php echo $start;?>"
                                                                                data-end="<?php echo $end;?>"
                                                                                data-clinic="<?php echo $clinic;?>"
                                                                                class="editButton mb-2 mr-2 btn btn-alternate">
                                                                                <i class="fa fa-edit"></i>
                                                                            </button>
                                                                            <button type="button"
                                                                                data-id="<?php echo $idBH;?>"
                                                                                data-fecha="<?php echo $date_delete;?>"
                                                                                data-clinic="<?php echo $clinic;?>"
                                                                                class="deleteButton mb-2 mr-2 btn btn-danger">
                                                                                <i class="fa fa-times"></i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3 card">
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <div class="card-body">
                                                    <h5 class="card-title">Business Hours (Woodbridge)</h5>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="position-relative form-group">
                                                                <label for="selectFXdates" class="">Dates</label>
                                                                <input type="text" class="form-control"
                                                                    id="selectWEdates"
                                                                    placeholder="You can pick multiple dates">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="position-relative form-group">
                                                                <label for="startwe" class="">Start Time</label>
                                                                <input type="text" id="startwe" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="position-relative form-group">
                                                                <label for="endwe" class="">End Time</label>
                                                                <input type="text" id="endwe" class="form-control" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-1 text-center">
                                                            <div class="position-relative form-group">
                                                                <label for="savesinglewe" class=""></label>
                                                                <button type="button" class="btn btn-dark"
                                                                    id="savesinglewe">SAVE
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <h5 class="card-title text-center">Current Business Hours (Woodbridge)
                                                    </h5>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table
                                                                class="mb-0 table table-borderless table-striped table-hover text-center"
                                                                id="bhw">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">Date</th>
                                                                        <th class="text-center">Month</th>
                                                                        <th class="text-center">Day</th>
                                                                        <th class="text-center">Start</th>
                                                                        <th class="text-center">End</th>
                                                                        <th class="text-center">Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="singlewe">
                                                                    <?php
                                                                        $result = $conexion->query('SELECT * FROM '. $tabla_calendario_horas .' WHERE id_clinica = 3 ORDER BY id_hora DESC;');
                                                                        while($row=$result->fetch_assoc()){
                                                                            $idBH = $row['id_hora'];
                                                                            $clinic = $row['id_clinica'];
                                                                            $fecha = $row['bh_date'];
                                                                            $date = date("m/d/Y",strtotime("$fecha"));
                                                                            $date_delete = date("l jS \of F Y",strtotime("$fecha"));
                                                                            $fechaComoEntero = strtotime($fecha);
                                                                            $mes = date("F", $fechaComoEntero);
                                                                            $dia = date("l", $fechaComoEntero);

                                                                            $time_start = $row["bh_start"];
                                                                            $start = date("g:i A",strtotime("$time_start"));
                                                                            
                                                                            $time_end = $row["bh_end"];
                                                                            $end = date("g:i A",strtotime("$time_end"));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $fecha; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $mes; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $dia; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $start; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <?php echo $end; ?></td>
                                                                        <td class="text-center" scope="row">
                                                                            <button type="button"
                                                                                data-toggle="modal"
                                                                                data-target=".editCurrentHour" 
                                                                                data-id="<?php echo $idBH;?>"
                                                                                data-fecha="<?php echo $date;?>"
                                                                                data-start="<?php echo $start;?>"
                                                                                data-end="<?php echo $end;?>"
                                                                                data-clinic="<?php echo $clinic;?>"
                                                                                class="editButton mb-2 mr-2 btn btn-alternate">
                                                                                <i class="fa fa-edit"></i>
                                                                            </button>
                                                                            <button type="button"
                                                                                data-id="<?php echo $idBH;?>"
                                                                                data-fecha="<?php echo $date_delete;?>"
                                                                                data-clinic="<?php echo $clinic;?>"
                                                                                class="deleteButton mb-2 mr-2 btn btn-danger">
                                                                                <i class="fa fa-times"></i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include 'footer.php'; ?>
            </div>

        </div>
    </div>

    <script type="text/javascript" src="./assets/scripts/main.js"></script>
    <!-- <script type="text/javascript" src="./assets/scripts/jquery-ui/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="./assets/scripts/jquery-ui/jquery-ui.css" /> -->

    <!-- DATATABLE -->
    <script src="assets/dataTable/jquery.dataTables.min.js"></script>
    <script src="assets/dataTable/dataTables.bootstrap4.min.js"></script>

    <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#bhf').DataTable();
            $('#bhm').DataTable();
            $('#bhw').DataTable();
        });
    </script>

    <!-- <script src="./calendar/business-hours.js"></script> -->
    <script src="calendar/singlebhs.js"></script>
</body>

</html>

<!-- Update Current Business Hour Modal -->

<div class="modal fade editCurrentHour" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Current Business Hour</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label for="curdate">Date</label>
                            <input type="text" id="curdate" class="form-control"/>
                            <input type="hidden" id="idcur" class="form-control"/>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="curstart">Start Time</label>
                            <input type="text" id="curstart" class="form-control" />
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="curend">End Time</label>
                            <input type="text" id="curend" class="form-control" />
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="editBH">SAVE CHANGES</button>
                <button type="button" class="btn btn-dark" data-dismiss="modal">CANCEL</button>
                
            </div>
        </div>
    </div>
</div>