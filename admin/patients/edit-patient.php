<?php
    require("database.php");
    include("funciones.php");

    if(isset($_POST['PatientIDhidden'])){

        $campos_paciente = array(
            'id_paciente_eagle' => escapar($_POST['PatientIDhidden']),
            'paciente_nombres' => escapar($_POST['PatientFirstName']),
            'paciente_apellidos' => escapar($_POST['PatientLastName']),
            'paciente_genero' => escapar($_POST['genre']),
            'paciente_fechanac' => escapar($_POST['PatientBirthDate']),	
            'paciente_direccion' => escapar($_POST['homeA']),
            'paciente_ocupacion' => escapar($_POST['occupation']),
            'paciente_direc_tra' => escapar($_POST['workA']),
            'paciente_idioma' => escapar($_POST['language']),
            'paciente_contacto' => escapar($_POST['contact_phone_number']),	
            'paciente_contacto2' => escapar($_POST['contact_home_phone_number']),
            'paciente_email' => escapar($_POST['contact_email']),
            'paciente_estado_civil' => escapar($_POST['status'])
        );
        	
        $campos_paciente_contacto = array(
            'id_paciente' => escapar($_POST['PatientIDhidden']),
            'emergencia_nombre' => escapar($_POST['emergency_contact_person_name']),
            'emergencia_relacion' => escapar($_POST['emergency_contact_relationship']),
            'emergencia_contacto' => escapar($_POST['emergency_contact_phone_number']),
            'emergencia_email' => escapar($_POST['emergency_contact_email'])
        );
        
        $campos_paciente_familia = array(
            'id_paciente' => escapar($_POST['PatientIDhidden']),
            'familia_esposa'	=> escapar($_POST['wifeName']),
            'familia_madre' => escapar($_POST['motherName']),
            'familia_padre' => escapar($_POST['fatherName']),
            'familia_hijos' => escapar($_POST['childrens']),
            'familia_hermanos' => escapar($_POST['siblings'])
        );

        if(insertar($tabla_pacientes,$campos_paciente) 
            && insertar($tabla_pacientes_contacto_emergencia,$campos_paciente_contacto)
            && insertar($tabla_pacientes_familia,$campos_paciente_familia))
        { 
        
            $_SESSION['alert']=array('success','Patient Successfully Registered');
        }else{
            $_SESSION['alert']=array('error','Patient Could Not Be Saved. Please try again');
        }
    }
        
?>