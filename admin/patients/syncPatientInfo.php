<?php 
    include("../include/eagle_con.php");
    require("../include/database.php");
    include("../include/funciones.php");

    $conexion->query('CREATE TEMPORARY TABLE `patients` (
        `patient_id` varchar(10) NOT NULL,
        `first_name` varchar(10) NOT NULL,
        `last_name` varchar(150) NOT NULL,
        `home_phone` varchar(150) NOT NULL,
        `birth_date` date NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=utf8;');

    #REGISTROS DESDE HACE DOS YEAR
    $query = "SELECT patient_id,first_name,last_name,home_phone,birth_date FROM patient WHERE date_entered BETWEEN dateadd(YEAR,-4,NOW()) AND NOW() ORDER BY patient_id DESC;";
    $patient_to_insert = array();
    $result = odbc_exec($conexion_anywhere,$query);

    while (odbc_fetch_row($result)) 
    {
        $patient_id = odbc_result($result,"patient_id");
        $first_name = odbc_result($result,"first_name");
        $last_name = odbc_result($result,"last_name");
        $home_phone = odbc_result($result,"home_phone");
        $birth_date = odbc_result($result,"birth_date");
        if (strpos($patient_id, 'M') === FALSE && strpos($patient_id, 'F') === FALSE && strpos($patient_id, 'W') === FALSE) {
            $id_pat_to_insert = trim('M'.$patient_id);
        }else{
            $id_pat_to_insert = trim($patient_id);
        }

        $parentesis = '("'.$id_pat_to_insert.'", "'.$first_name.'", "'.$last_name.'", "'.$home_phone.'", "'.$birth_date.'")';

        array_push($patient_to_insert,$parentesis);
    }

    $array_string = implode(",",$patient_to_insert);

    $conexion->query('INSERT INTO '.$tabla_patients.' (patient_id, first_name, last_name, home_phone, birth_date) VALUES '.$array_string.';');

    #MySQL
    $resultado = $conexion->query('SELECT * FROM '.$tabla_patients.';');

    while($row=$resultado->fetch_assoc()){
        $pat_id = $row['patient_id'];
        $name = $row['first_name'];
        $last = $row['last_name'];
        $birth = $row['birth_date'];
        $contact = $row['home_phone'];

        if (strpos($pat_id, 'F') !== false) {
            $id_paciente = str_replace('F','FX',$pat_id);
            $clinica = 1;
        }
        else if (strpos($pat_id, 'W') !== false) {
            $id_paciente = str_replace('W','WE',$pat_id);
            $clinica = 3;
        }
        else if (strpos($pat_id, 'M') !== false){
            $id_paciente = str_replace('M','MS',$pat_id);
            $clinica = 2;
        }

            $pat_compare = queryOne('SELECT paciente_nombres,paciente_apellidos,paciente_fechanac,paciente_contacto,id_clinica FROM '.$tabla_pacientes.' WHERE id_paciente = "'.$id_paciente.'";');

            if($name !== $pat_compare['paciente_nombres'] || $last !== $pat_compare['paciente_apellidos']){
                $condicion = array(
                    'id_paciente' => $id_paciente
                );
                $campos_paciente = array(
                    'paciente_nombres' => $name,
                    'paciente_apellidos' => $last,
                    'paciente_fechanac' => $birth,
                    'paciente_contacto' => $contact,
                    'id_clinica' => $clinica,
                );

                $conexion->autocommit(false);

                $actualizar_paciente = actualizar($tabla_pacientes,$campos_paciente,$condicion);
            }

            if($actualizar_paciente){
                $conexion->commit();
                echo "Patient information synchronized with eaglesoft";
            }else{
                $conexion->rollback();
                echo "No se actualizo";
            }

            $conexion->autocommit(true);

    }
?>