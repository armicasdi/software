<?php 
    include("../include/eagle_con.php");
    require("../include/database.php");
    include("../include/funciones.php");

    #REGISTROS DESDE HACE DOS YEAR
    $query = "SELECT patient_id,first_name,last_name,home_phone,birth_date FROM patient 
    WHERE patient_id LIKE '%F%' ORDER BY patient_id DESC";
    $result = odbc_exec($conexion_anywhere,$query);
    $cn= 0; $cl= 0; $cd= 0; $cc = 0;

    while (odbc_fetch_row($result)) 
    {
        $patient_id = odbc_result($result,"patient_id");
        $first_name = odbc_result($result,"first_name");
        $last_name = odbc_result($result,"last_name");
        $home_phone = odbc_result($result,"home_phone");
        $birth_date = odbc_result($result,"birth_date");
        if (strpos($patient_id, 'M') === FALSE && strpos($patient_id, 'F') === FALSE && strpos($patient_id, 'W') === FALSE) {
            $id_paciente_eagle = trim('M'.$patient_id);
        }else{
            $id_paciente_eagle = trim($patient_id);
        }

        $reg = queryOne('SELECT * FROM '.$tabla_pacientes.' WHERE id_paciente_eagle = "'.$id_paciente_eagle.'";');

        if (strpos($id_paciente_eagle, 'F') !== false) {
            $id_paciente = str_replace('F','FX',$id_paciente_eagle);
            $clinica = 1;
        }
        else if (strpos($id_paciente_eagle, 'W') !== false) {
            $id_paciente = str_replace('W','WE',$id_paciente_eagle);
            $clinica = 3;
        }
        else if (strpos($id_paciente_eagle, 'M') !== false){
            $id_paciente = str_replace('M','MS',$id_paciente_eagle);
            $clinica = 2;
        }

        if($first_name != $reg['paciente_nombres']){
            $nombres=$first_name;
            $condicion = array('id_paciente' => $id_paciente);
            $campos_paciente = array('paciente_nombres' => $nombres);
            actualizar($tabla_pacientes,$campos_paciente,$condicion);
            $cn++;
        }
        if($last_name != $reg['paciente_apellidos']){
            $apellidos=$last_name;
            $condicion = array('id_paciente' => $id_paciente);
            $campos_paciente = array('paciente_apellidos' => $apellidos);
            actualizar($tabla_pacientes,$campos_paciente,$condicion);
            $cl++;
        }
        if(date('Y-m-d',strtotime($birth_date)) != $reg['paciente_fechanac']){
            $dob=date('Y-m-d',strtotime($birth_date));
            $condicion = array('id_paciente' => $id_paciente);
            $campos_paciente = array('paciente_fechanac' => $dob);
            actualizar($tabla_pacientes,$campos_paciente,$condicion);
            $cd++;
        }
        /* if(formatTelefono($reg['paciente_contacto'])==""){
            $contacto=formatTelefono($home_phone);
            $condicion = array('id_paciente' => $id_paciente);
            $campos_paciente = array('paciente_contacto' => $contacto);
            $actualizar_paciente = actualizar($tabla_pacientes,$campos_paciente,$condicion);
        } */
    }
    echo $cn;
    echo PHP_EOL;
    echo $cl;
    echo PHP_EOL;
    echo $cd;
    echo PHP_EOL;
?>