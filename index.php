<?php
	

	include "login/assets/funciones.php";
	$ale;
	session_start();
	if(!empty($_SESSION['rol'])){
		if($_SESSION['rol']=="admin"){
			header("Location: ".substr_replace("index.php", "", $_SERVER["REQUEST_URI"])."admin/index.php");
		}elseif($_SESSION['rol']=="user"){
			header("Location: ".substr_replace("index.php", "", $_SERVER["REQUEST_URI"])."user/calendar.php");
		}
		
	}else{
		//session_destroy();
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>PaymentPlan Soft</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="login/images/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/css/util.css">
	<link rel="stylesheet" type="text/css" href="login/css/main.css">
	<!--===============================================================================================-->

	<!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
		integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>

<body>

	<!--  -->
	<div class="simpleslide100">
		<div class="simpleslide100-item bg-img1" style="background-image: url('login/images/bg01.jpg');"></div>
		<div class="simpleslide100-item bg-img1" style="background-image: url('login/images/bg02.jpg');"></div>
	</div>

	<div class="flex-col-c-sb size1 overlay1">
		<!--  -->
		<div class="w-full flex-w flex-sb-m p-l-80 p-r-80 p-t-22 p-lr-15-sm">
			<div class="wrappic1 m-r-30 m-t-10 m-b-10">
				<a href="#"><img src="login/images/icons/logo.png" alt="LOGO"></a>
			</div>
		</div>

		<!--  -->
		<div class="flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-120">
			<div class="container">
				<div class="d-flex justify-content-center">
					<div class="card">
						<div class="card-header">
							<h3 class="l1-txt1 txt-center">Log in </h3>
						</div>
						<div class="card-body">
							<form action="" method="POST">
								<div class="input-group form-group">
									<div id="search-wrapper">
										<input type="text" name="username" id="username" placeholder="username" class="search" />
										<i class="fa fa-user"></i>
									</div>
								</div>
								<div class="input-group form-group">
									<div id="search-wrapper">
										<input type="password" name="password" id="password"  placeholder="password" class="search"/>
										<i class="fa fa-key"></i>
									</div>
								</div>
								<div class="row align-items-center remember">
									<input type="checkbox">Remember Me
								</div>
								<div class="form-group">
									<input type="submit" value="Login" name="login" id="login"
										class="btn float-right login_btn size2 m1-txt1 flex-c-m how-btn1">
								</div>
								<div id="error"></div>
							</form>
						</div>
						<div class="card-footer">
							<!-- <div class="d-flex justify-content-center links">
								Don't have an account?<a href="#">Sign Up</a>
							</div> -->
							<div class="d-flex justify-content-center links">
								Welcome to <a href="#">Payment Plan Soft </a>
							</div>
							<div class="d-flex justify-content-center links">
								<a href="#">Forgot your password?</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!--===============================================================================================-->
	<script src="login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<!--===============================================================================================-->
	<script src="login/vendor/tilt/tilt.jquery.min.js"></script>
	<script>
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
	<!--===============================================================================================-->
	<script src="login/js/main.js"></script>
	<script src="login/assets/app.js"></script>

</body>

</html>